// Rollup Configuration

import fs from 'fs';
import path from 'path';
import { env } from 'process';
import { name, version, dependencies } from '../package.json';
import vue from 'rollup-plugin-vue';
import babel from 'rollup-plugin-babel';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import postcss from 'rollup-plugin-postcss';
import { terser } from 'rollup-plugin-terser';
import vizualizer from 'rollup-plugin-visualizer';

// Catch environment from process.env.BUILD
const environment = env.BUILD;

// Paths
const base = path.resolve(__dirname, '..');
const src = path.resolve(base, 'src');
const dist = path.resolve(base, 'dist');

// Ensure dist directory exists
if (!fs.existsSync(dist)) {
    fs.mkdirSync(dist);
}

// Strip off the @scope from the package name
let packageName = name.replace(/@fso\//, '');

// Banner
const banner =
`/**
  * ${packageName} - ${version}
  * (c) ${new Date().getFullYear()} FSO Technology Services, University of Arizona
  * created by Riley Taylor, maintained and improved by the FAST dev team
  * @license MIT
  */\n`;

// External libraries, anything in package.json `dependencies`, plus Vue
const externals = ['vue', ...Object.keys(dependencies)];

// Base config used by every export option
let baseConfig = {
    input: path.resolve(src, 'index.js'),
    external: externals,
    plugins: [
        postcss({
            config: {
                path: path.resolve(base, 'postcss.config.js')
            }
        }),
        vue({
            css: false,
        }),
        resolve({
            browser: true
        }),
        commonjs(),
        babel({
            exclude: path.resolve(base, 'node_modules/**'),
            runtimeHelpers: true
        })
    ]
};

// Add watch config if this is dev mode
if (environment === 'development') {
    baseConfig = {
        ...baseConfig,
        watch: {
            clearScreen: false,
            include: path.resolve(src)
        }
    };
}
  
// browser build (UMD) for script tag imports and apps not using webpack/babel
let browserConfig = Object.assign({}, baseConfig, {
    ...baseConfig,
    output: {
        format: 'umd',
        file: path.resolve(dist, `${packageName}.js`),
        exports: 'named',
        name: packageName,
        banner: banner,
        sourcemap: true,
        globals: {
            'inputmask': 'InputMask',
            'velocity-animate': 'Velocity',
            vue: 'Vue',
            'vue-multiselect': 'VueMultiselect'
        }
    }
});

// Minified browser build
let minConfig = Object.assign({}, browserConfig, {
    ...browserConfig,
    plugins: [
        ...browserConfig.plugins,
        terser({
            output: {
                // Preserve banner/important comments in output
                comments: function(node, comment) {
                    const { type, value } = comment;
                    if (type == "comment2") {
                        // multiline comment
                        return /@preserve|@license|@cc_on/i.test(value);
                    }
                }
            }
        })
    ],
    output: {
        ...browserConfig.output,
        file: path.resolve(dist, `${packageName}.min.js`),
        sourcemap: false
    }
});

// Server-side rendering build (common.js)
let ssrConfig = Object.assign({}, baseConfig, {
    ...baseConfig,
    output: {
        format: 'cjs',
        file: path.resolve(dist, `${packageName}.ssr.js`),
        exports: 'named',
        name: packageName,
        banner: banner,
        sourcemap: true,
    }
});

// ESM for use with webpack/rollup in apps/libraries that do their own minifying
let esmConfig = Object.assign({}, baseConfig, {
    ...baseConfig,
    plugins: [
        ...baseConfig.plugins,
        vizualizer({
            filename: path.resolve(base, 'build/stats.html')
        })
    ],
    output: {
        format: 'esm',
        file: path.resolve(dist, `${packageName}.esm.js`),
        name: packageName,
        banner: banner,
        sourcemap: true
    }
});

// We only need to export the esm build for development purposes. In prod, export all formats.
let exports = [];
if (environment === 'development') {
    exports.push(esmConfig);
} else if (environment === 'production') {
    exports.push(browserConfig, minConfig, esmConfig, ssrConfig);
}

export default [
    ...exports
];
