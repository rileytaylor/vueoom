// Components
// Imported as a group for easy install, and individually for export
import * as components from './components';
import {
    Alert,
    Calendar,
    CheckBox,
    CollapsePanel,
    Command,
    CommandDropdown,
    CommandGroup,
    DateInput,
    DateRangeInput,
    DropdownDivider,
    DropdownHeader,
    DropdownItem,
    Icon,
    FsoFooter,
    FormGroup,
    Modal,
    Navbar,
    NavbarBrand,
    NavbarContent,
    NavbarCollapse,
    NavbarForm,
    NavbarItem,
    NavbarItemDropdown,
    NavbarText,
    NavbarToggler,
    Popover,
    Radio,
    RadioGroup,
    Redbar,
    SegmentedToggle,
    SegmentedToggleOption,
    SelectSingle,
    SelectMultiple,
    Stepper,
    StepperItem,
    TextBox,
    TextView,
    Toolbar,
    Toast,
    ToastGroup
} from './components';

// Directives
// Imported as a group for easy install, and individually for export
import * as directives from './directives';
import {
    fMask,
    fPopover
} from './directives';

// Global Methods
import { addToast } from './methods';

// Helpers
import { Animation, ArrayHelper } from './utils';

// Mixins
import { 
    CommandMixin,
    ClickoutMixin,
    InputMixin,
    InputAddonMixin
} from './mixins';

// The install() function to add the plugin to the Vue instance
const install = (Vue, config) => {
    if (config === null) {
        config = {};
    }

    // Register components
    for (const c of Object.values(components)) {
        Vue.component(c.name, c);
    }

    // Register directives
    for (const d in directives) {
        Vue.directive(d, directives[d]);
    }

    // Register Global methods
    Vue.prototype.$toast = addToast;
};

const Vueoom = {
    install
};

// Automatic installation if Vue has been added to the global scope.
if (typeof window !== 'undefined' && window.Vue) {
    window.Vue.use(Vueoom);
}

export {
    // Default export for installation
    Vueoom as default,
    // Helpers
    Animation,
    ArrayHelper,
    // Mixins
    CommandMixin,
    ClickoutMixin,
    InputMixin,
    InputAddonMixin,
    // Individual Components
    Alert,
    Calendar,
    CheckBox,
    CollapsePanel,
    Command,
    CommandDropdown,
    CommandGroup,
    DateInput,
    DateRangeInput,
    DropdownDivider,
    DropdownHeader,
    DropdownItem,
    Icon,
    FsoFooter,
    FormGroup,
    Modal,
    Navbar,
    NavbarBrand,
    NavbarContent,
    NavbarCollapse,
    NavbarForm,
    NavbarItem,
    NavbarItemDropdown,
    NavbarText,
    NavbarToggler,
    Popover,
    Radio,
    RadioGroup,
    Redbar,
    SegmentedToggle,
    SegmentedToggleOption,
    SelectSingle,
    SelectMultiple,
    Stepper,
    StepperItem,
    TextBox,
    TextView,
    Toolbar,
    Toast,
    ToastGroup,
    // Individual Directives
    fMask,
    fPopover
};

