import { toastEventBus } from '../utils/toastEventBus';

const addToast = (notification) => {
    toastEventBus.$emit('add', notification);
};

export default addToast;
