// Various utility functions for internal use

// Determine if an element is an HTML Element
export const isElement = el => Boolean(el && el.nodeType === Node.ELEMENT_NODE);

// Get an element given an ID
export const getById = id => document.getElementById(/^#/.test(id) ? id.slice(1) : id) || null;

export const toType = val => typeof val;

export const isUndefined = val => val === undefined;

export const isNull = val => val === null;

export const isObject = obj => obj !== null && typeof obj === 'object';

export const isFunction = val => toType(val) === 'function';

export const isBoolean = val => toType(val) === 'boolean';

export const isString = val => toType(val) === 'string';

export const isNumber = val => toType(val) === 'number';

export const isPrimitive = val => isBoolean(val) || isString(val) || isNumber(val);

export const isDate = val => val instanceof Date;

export const isPromise = val =>
    !isUndefined(val) && !isNull(val) && isFunction(val.then) && isFunction(val.catch);

/**
 * Sleep an async function for a specified number of milliseconds in a non-blocking manner
 * @param {Number} ms milliseconds
 */
export const sleep = ms => new Promise(res => setTimeout(res, ms));
