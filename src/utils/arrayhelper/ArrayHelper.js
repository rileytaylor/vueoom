/**
 * Compare the arrays with es6 mind-blowing awesomeness
 * see: http://stackoverflow.com/a/34256998
 */

// Private Functions

// Test
const isArray = Array.isArray;

// Strict Comparison
const equal = x => y => x === y;

// Loose Comparison
/*eslint-disable*/
const looseEqual = x => y => x == y;
/*eslint-enable*/

// Simple comparison
// arrayCompare :: (a -> b -> Bool) -> [a] -> [b] -> Bool
const compare = f => ([x, ...xs]) => ([y, ...ys]) => {
    if (x === undefined && y === undefined) {
        return true;
    } else if (!f(x)(y)) {
        return false;
    } else {
        return compare(f)(xs)(ys);
    }
};

// Deep comparison
// arrayDeepCompare :: (a -> b -> Bool) -> [a] -> [b] -> Bool
const deepCompare = f => compare(a => b => {
    if (isArray(a) && isArray(b)) {
        return deepCompare(f)(a)(b);
    } else {
        return f(a)(b);
    }
});


// Public Functions
const arrayCompare = function (a1, a2, type) {
    if (type === 'loose') {
        return compare(looseEqual)(a1)(a2);
    } else {
        return compare(equal)(a1)(a2);
    }
};

const arrayDeepCompare = function (a1, a2, type) {
    if (type === 'loose') {
        return deepCompare(looseEqual)(a1)(a2);
    } else {
        return deepCompare(equal)(a1)(a2);
    }
};

export default {
    arrayCompare,
    arrayDeepCompare
};
