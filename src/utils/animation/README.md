The animation utilities can be imported via:

```javascript
import { Animation } from 'vueoom';
```

Animation requires animate.css to work.
