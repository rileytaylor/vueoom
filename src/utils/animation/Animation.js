/**
 * This set of helper functions assists with enacting animations on elements through code.
 * It is designed to work with animate.css (https://github.com/daneden/animate.css), though
 * could be extended or modified to work with other libraries as well.
 * See each function for usage.
 */

const end = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

// Run one animation on an element. This is intended for attention getters
// Usage: AnimationHelper.animateOnce('#idOfElement', 'shake');
const animateOnce = function (element, animation) {
    $(element)
        .addClass('animated ' + animation)
        .one(end, function () {
            $(element).removeClass('animated ' + animation);
        });
};

// Run two animations back to back, the last when the first finishes
// Usage: AnimationHelper.animateTwice('#idOfElement', 'first', 'second')
// Usage example: AnimationHelper.animateTwice('#modal', 'fadeInDown', 'fadeOutLeft')
const animateTwice = function (element, animation1, animation2) {
    $(element)
        .addClass('animated ' + animation1)
        .one(end, function () {
            $(element)
                .removeClass(animation1)
                .addClass(animation2)
                .one(end, function () {
                    $(element).removeClass('animated ' + animation2);
                });
        });
};

const animateInfinite = function (element, animation) {
    $(element)
        .addClass('animated infinite ' + animation);
};

// TODO: Promise based function? similar to or based on https://github.com/lukejacksonn/Actuate
// This will allow for chaining with delays and triggers

export default {
    animateOnce,
    animateTwice,
    animateInfinite
};
