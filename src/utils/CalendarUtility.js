/**
 * CalendarUtility includes functions for formatting strings as datetimes and vice-versa, as
 * well as some other various utility functions for comparing dates, etc. This is primarily
 * built to be useful for the Calendar, DateInput, and DateRangeInput components.
 * 
 * Usage:
 *     import CalendarUtility from '.../CalendarUtility';
 *     let calendarUtility = new CalendarUtility()
 *     
 * @class
 */
class CalendarUtility {
    // TODO: Allow custom definitions via override, and make 'format' parameters in the included functions just for overriding the default here.
    constructor () {
        this._stringFormat = 'YYYY-MM-DD';
        this._formatRE = /,|\.|-| |\/|\\/;
        this._dayRE = /D+/;
        this._monthRE = /M+/;
        this._yearRE = /Y+/;
    }

    /**
     * Parse a date string based on a formatter
     * @param {String} val the return value of the component
     * @param {String} format a format to follow for parsing (i.e. `YYYY-MM-DD`)
     * @returns {Date}
     */
    parseDateFromString (val, format) {
        let day, month, year;

        const dateParts = val.split(this._formatRE);
        const formatParts = format.split(this._formatRE);
        const partsSize = formatParts.length;

        for (let i = 0; i < partsSize; i++) {
            if (formatParts[i].match(this._dayRE)) {
                day = parseInt(dateParts[i], 10);
            } else if (formatParts[i].match(this._monthRE)) {
                month = parseInt(dateParts[i], 10);
            } else if (formatParts[i].match(this._yearRE)) {
                year = parseInt(dateParts[i], 10);
            }
        }

        const date = new Date(year, month - 1, day);

        return date;
    }

    /**
     * Convert a date back to the `dateFormat` property's format for emitting
     * @param {Date} date the datetime to format
     * @returns {String} the formatted datetime
     */
    formatDateToString (date) {
        if (!date) {
            return '';
        } else {
            // Use date to build the format string, padding numbers based on the format.
            return this._stringFormat
                .replace(this._yearRE, () => date.getFullYear())
                .replace(this._monthRE, match => this.padNumber(date.getMonth() + 1, match.length))
                .replace(this._dayRE, match => this.padNumber(date.getDate(), match.length));
        }
    }

    /**
     * Determine if two dates are the same
     * @param {Date} date1
     * @param {Date} date2
     * @returns {Boolean} if dates are the same or not
     */
    isSameDate (date1, date2) {
        return (date1.getDate() === date2.getDate()) &&
            (date1.getMonth() === date2.getMonth()) &&
            (date1.getFullYear() === date2.getFullYear());
    }

    /**
     * Pad numbers to the length we want based on the `dateFormat`
     * @param {Number} num the number
     * @param {Number} pad the lengt to pad to
     * @returns {Number} a padded number
     */
    padNumber (num, pad) {
        if (typeof num !== undefined) {
            return num.toString().length > pad
                ? num
                : new Array(pad - num.toString().length + 1).join('0') + num; 
        } else {
            return undefined;
        }
    }
}

export default CalendarUtility;
