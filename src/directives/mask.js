// TODO: Temporarily fix this for SSR, need to do better in the long run though
// import InputMask from 'inputmask';
let InputMask = require('inputmask');

let findInput = el => {
    let result;
    try {
        if (el.tagName === 'INPUT' || el.tagName === 'TEXTAREA') {
            result = el;
        } else if (el.getElementsByTagName('input').length > 0) {
            result = el.getElementsByTagName('input')[0];
        } else if (el.getElementsByTagName('textarea').length > 0) {
            result = el.getElementsByTagName('textarea')[0];
        }
        if (result === undefined) throw "element is undefined: this is not a valid element";
    } catch (e) {
        console.error('v-f-mask can\'t identify a valid element to mask.');
        console.error(e);
    }
    return result;
};

let valueIsString = value => ((typeof value === 'string') || value instanceof String);

let mask = (el, mods, value) => {
    if (value.length < 1) { return; }

    // Determine if el is an `input` or if we need to go deeper for the `input` (i.e. using a Vueoom component)
    let element = findInput(el);
    // Stop executing if there is no element to work with
    if (!element) return;

    // Handle modifiers
    let isJit = mods.jit ? true : false;
    let isGreedy = mods.greedy ? true : false;

    // If the value is just a string, lets make it an object
    let mask = valueIsString(value)
        ? { mask: value }
        : value;

    // Build config object
    let maskConfig = {
        ...mask,
        jitMasking: isJit,
        greedy: isGreedy
    };
    
    InputMask(maskConfig).mask(element);
};

let unmask = el => {
    let element = findInput(el);
    if (!element) return;
    InputMask.remove(element);
};

export default {
    bind (el, { modifiers, value }) {
        mask(el, modifiers, value);
    },
    unbind (el) {
        unmask(el);
    }
};
