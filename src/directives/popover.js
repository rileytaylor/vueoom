import Vue from 'vue';
import Popover from '../components/Popover.vue';

const TRIGGERS = {
    click: 'click',
    hover: 'hover',
    focus: 'focus'
};

const parseModifiers = (modifiers) => {
    // Default config
    let config = {
        animation: true,
        delay: 0,
        disableFlip: false,
        offset: undefined,
        placement: 'auto',
        trigger: 'click'
    };

    // Handle trigger config modifiers
    let triggers = [];
    Object.keys(TRIGGERS).forEach(trigger => {
        if (modifiers[trigger]) {
            triggers.push(trigger);
        }
    });
    console.log(triggers);
    if (triggers.length > 0) {
        config.trigger = triggers.join(' ');
    }

    // Handle general config modifiers
    Object.keys(modifiers).forEach(mod => {
        if (/^noAnimation$/.test(mod)) {
            config.animation = false;
        } else if (/^delay-\d+$/.test(mod)) {
            //config.delay = mod;
        } else if (/^disableFlip$/.test(mod)) {
            config.disableFlip = mod;
        } else if (/^offset-\d+$/.test(mod)) { 
            //config.offset = mod;
        } else if (/^(auto|top(-start|-end)?|bottom(-start|-end)?|left(-start|-end)?|right(-start|-end)?)$/.test(mod)) {
            config.placement = mod;
        }
    });

    console.log(config);

    return config;
};

export default {
    bind(el, { modifiers, value }) {
        const FPopover = Vue.extend(Popover);

        const title = el.getAttribute('title') || '';
        let config = parseModifiers(modifiers);

        el.$fPopover = new FPopover({
            propsData: {
                animation: config.animation,
                content: value,
                delay: config.delay,
                disableFlip: config.disableFlip,
                offset: config.offset,
                placement: config.placement,
                target: el,
                title: title,
                trigger: config.trigger
            }
        });
    },
    inserted (el) {
        el.$fPopover.$mount();
    },
    update (el) {
        el.$fPopover.update();
    },
    unbind (el) {
        el.$fPopover.$destroy();
    }
};
