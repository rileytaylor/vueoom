/* 
 * Common methods for form input controls, including `CheckBox`, `TextBox`, `TextView`, and `SelectBox`
 */

export default {
    props: {
        // Text
        label: {
            type: String,
            default: undefined
        },
        placeholder: {
            type: String,
            default: undefined
        },
        description: {
            type: String,
            default: undefined
        },
        // State
        success: {
            type: Boolean,
            default: false
        },
        warning: {
            type: Boolean,
            default: false
        },
        danger: {
            type: Boolean,
            default: false
        },
        successMessage: {
            type: String,
            default: ''
        },
        warningMessage: {
            type: String,
            default: ''
        },
        dangerMessage: {
            type: String,
            default: ''
        },
        // Toggle Inline UI
        inline: {
            type: Boolean,
            defalt: false
        },
        inlineDescription: {
            type: Boolean,
            default: false
        },
        // Control size
        size: {
            type: String,
            default: 'normal',
            validator: function (value) {
                return [
                    'small',
                    'normal',
                    'large'
                ].indexOf(value) > -1;
            }
        },
        // Disable the control
        disabled: {
            type: Boolean,
            default: false
        },
        // Browser autocomplete
        autocomplete: {
            type: Boolean,
            default: true
        }
    },
    computed: {
        isAutocomplete () {
            if (this.autocomplete === true) {
                return 'on';
            } else {
                return 'off';
            }
        },
        isDescription () {
            if (this.description === undefined) {
                return false;
            } else {
                return true;
            }
        },
        isLabel () {
            if (this.label === undefined) {
                return false;
            } else {
                return true;
            }
        },
        stateEnabled () {
            if (this.success === true || this.warning === true || this.danger === true) {
                return true;
            } else {
                return false;
            }
        }
    }
};
