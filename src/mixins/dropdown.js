import Popper from 'popper.js';
import uuid from 'uuid/v4';

import eventBus from '../utils/dropdownEventBus';

const placements = {
    NORMAL: 'bottom-start',
    ALIGN_RIGHT: 'bottom-end',
    UP_NORMAL: 'top-start',
    UP_ALIGN_RIGHT: 'top-end'
};

export default {
    props: {
        /**
         * Open the dropdown above the parent element
         * @prop {Boolean} dropup
         */
        dropup: {
            type: Boolean,
            default: false
        },
        /**
         * Align the dropdown to the right side of the parent element.
         * @prop {Boolean} alignRight
         */
        alignRight: {
            type: Boolean,
            default: false
        },
        /**
         * Define an offset for rendering the dropdown
         * @prop {Number|String} offset
         */
        offset: {
            type: [Number, String],
            default: undefined
        },
        /**
         * Disable flipping the axis of the dropdown when its content would 
         * normally be cut off by the window.
         * @prop {Boolean} noFlip
         */
        noFlip: {
            type: Boolean,
            default: false
        },
        /**
         * Automatically close the dropdown when an internal item is clicked?
         * @prop {Boolean} closeOnClick
         */
        closeOnClick: {
            type: Boolean,
            default: true
        }
    },
    data () {
        return {
            show: false
        };
    },
    computed: {
        id () {
            return `dropdown-${uuid()}`;
        },
        placement () {
            let placement = placements.NORMAL;
            if (this.dropup && this.alignRight) {
                // up and right
                placement = placements.UP_ALIGN_RIGHT;
            } else if (this.dropup) {
                // up
                placement = placements.UP_NORMAL;
            } else if (this.alignRight) {
                // right
                placement = placements.ALIGN_RIGHT;
            
            }
            return placement;
        }
    },
    watch: {
        show (val, old) {
            if (val === old) {
                return;
            }
            if (val) {
                this.$emit('shown');
                this.showDropdown();
            } else {
                this.$emit('hidden');
                this.hideDropdown();
            }
        },
        disabled (val, old) {
            if (val === old) {
                return;
            }
            if (val) {
                this.show = false;
            }
        }
    },
    created () {
        // Non-reactive property to hold popover config callback
        this._dropdown = null;
    },
    mounted () {
        eventBus.$on('dropdown::item-click', this.onItemClick);
    },
    beforeDestroy () {
        this.show = false;
        this.hideDropdown();
    },
    methods: {
        /**
         * Toggle the dropdowns visibility
         */
        toggle () {
            this.show = !this.show;
        },
        /**
         * Hide the dropdown and handle garbage collection
         */
        hideDropdown () {
            if (this._dropdown) {
                this._dropdown.destroy();
            }
            this._dropdown = null;
        },
        /**
         * Show the dropdown
         */
        showDropdown () {
            // Don't do anything if this is disabled
            if (this.disabled) { return; }
            // Make sure the dropdown doesn't already exist
            this.hideDropdown();
            // Make the dropdown
            this._dropdown = new Popper(
                this.$el,
                this.$refs.dropdown,
                {
                    placement: this.placement,
                    modifiers: {
                        offset: {
                            offset: this.offset || 0
                        },
                        flip: {
                            enabled: !this.noFlip
                        }
                    },
                });
        },
        /**
         * Handle the dropdown if an `DropdownItem` inside this dropdown is clicked.
         * @param {String} attachedTo a dropdown id to close
         */
        onItemClick (attachedTo) {
            if (this.closeOnClick) {
                // Ensure the event was intended for this dropdown
                if (attachedTo !== this.id) {
                    return;
                }
                // Hide the dropdown if this is the right dropdown
                this.show = false;
            }
        }
    }
};
