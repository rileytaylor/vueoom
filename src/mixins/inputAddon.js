/*
 * Mixin to provide form-input-addons to inputs that can use them.
 */

export default {
    data () {
        return {
            addonVisibility: {
                start: false,
                end: false
            }
        };
    },
    props: {
        addon: {
            type: String,
            default: 'none',
            validator: function (value) {
                return [
                    'none',
                    'both',
                    'start',
                    'end'
                ].indexOf(value) > -1;
            }
        }
    },
    methods: {
        addonConfig () {
            if (this.addon === 'none') {
                this.addonVisibility.start = false;
                this.addonVisibility.end = false;
            } else if (this.addon === 'both') {
                this.addonVisibility.start = true;
                this.addonVisibility.end = true;
            } else if (this.addon === 'start') {
                this.addonVisibility.start = true;
                this.addonVisibility.end = false;
            } else if (this.addon === 'end') {
                this.addonVisibility.start = false;
                this.addonVisibility.end = true;
            }
        }
    },
    mounted () {
        this.addonConfig();
    }
};
