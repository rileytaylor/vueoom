/**
 * Mixin for reusable bits for `Command`, and `CommandDropdown`
 */

export default {
    props: {
        active: {
            type: Boolean,
            default: false
        },
        label: {
            type: String,
            default: undefined
        },
        icon: {
            type: String,
            default: undefined
        },
        type: {
            type: String,
            default: 'secondary',
            validator: function (value) {
                return [
                    'secondary',
                    'success',
                    'warning',
                    'danger',
                    'info',
                    'primary',
                    'link',
                    'uared'
                ].indexOf(value) > -1;
            }
        },
        size: {
            type: String,
            default: 'normal',
            validator: function (value) {
                return [
                    'large',
                    'normal',
                    'small'
                ].indexOf(value) > -1;
            }
        },
        disabled: {
            type: Boolean,
            default: false
        },
        outline: {
            type: Boolean,
            default: false
        },
    },
    computed: {
        isOutline: function () {
            if (this.outline === true) {
                return 'btn-outline-';
            } else {
                return 'btn-';
            }
        },
        btnSize: function () {
            if (this.size === 'large') {
                return 'btn-lg';
            } else if (this.size === 'small') {
                return 'btn-sm';
            } else {
                return '';
            }
        }
    },
};
