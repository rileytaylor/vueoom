import PopperJs from 'popper.js';

export default class Popper {
    // This is a stage-2 proposal that needs to be enabled
    // static placements = PopperJs.placements;

    constructor() {
        return {
            destroy: () => {},
            scheduleUpdate: () => {}
        };
    }
}
