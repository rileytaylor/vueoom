<p style="text-align: center"><img src="./static/icons/vueoom-512.svg" width="256" height="256"></p>

_"When this thing gets up to 88 mph, you're gonna see some serious shit." -Doc Brown_

___

Vueoom is a Vue.js plugin that installs various components, mixins, directives, and helper utilities in your app.

## Usage

See [Usage documentation](docs/usage.md).

## Development

Vueoom is a Vue.js plugin found under `src/`. It is not an executable web-app or application. The Vueoom package is built with [Rollup](https://rollupjs.org), which packages the plugin into one file and transpiles it with [Babel](https://babeljs.io).

The documentation site serves as the application to build and test Vueoom in. The documentation site (found under `docs/`) is built with [Nuxt.js](https://nuxtjs.org), a Vue.js library for building static sites. To get started with development, you'll need to install `nuxt` globally on your machine.

```shell
npm install -g nuxt
```

Then, you can use the npm commands to build, develop, and generate the plugin and documentation site. The commands are as follows:

| Command               | Description |
| --------------------- | :---------- |
| npm run build:dev     | Build the plugin |
| npm run build:propd   | Build plugin and watch for changes |
| npm run docs:dev      | Runs the documentation site's development server |
| npm run docs:build    | Geneate the SSR site for deployment |
| npm run lint          | Lint the files with `eslint` |
| npm run lint:html     | Lint and export results as an html file (`./eslintlog.html`) |
| npm run nom           | Destroy the `node_modules` directory and re-install all node modules |
| npm run release       | Create a release. See [releasing instructions](#releasing-a-new-version) |

When developing anything in Vueoom, be sure to update the documentation under the `docs/` folder. Not every component has to be documented, but it's a good idea to document as much as possible.

## Releasing a new version

Releasing a new version is easy. Generally, you should do the following steps in a new branch (i.e. `release-x.x.x`). Read these steps all the way through before making your first release.

1. **Make sure you are up to date.** Be sure to `git pull`, commit any final changes, and perform a rebase on develop to make sure you are 100% ready.
2. **Update the changelog.** Make any changelog updated now.
3. **Determine the Version Number.** We use the semver of `major.minor.patch`. Be sure to see what the previous version is in the `package.json` and determine what makes sense.
4. **Do it! [_lightsaber sounds_]** Run `npm run release -- <version>`, with `<version>` being the next version number. This executes `/build/release.sh`, which does the following for you:
    - Run `npm run change-version` to modify all our assets with the correct version number for releasing.
    - Run `npm run build` to compile all the assets with the new versions. Make sure this succeeds and places the appropriate files in the `dist/` folder
    - Deploy the release to our Sonatype Nexus instance (as long as you've configured everything properly, see [here](#deploying-to-sonatype-nexus))
    - Tag the release in git
5. Now, run `git push && git push --tags` to push the compiled code and the new tag to gitlab.

Gitlab CI will take care of the docs site, CDN, and nexus repository deploys when the pipeline succeeds.
- CDN is updated when a release is created and a tag is pushed to Gitlab
- Docs site is always updated whenever a deploy runs. See [below](#Documentation-site) for more information.

## Documentation Site

The documentation site is deployed via Gitlab CI to an AWS S3 bucket. Each branch has a folder in the bucket, which is cleaned up manually. The `master/` folder is mapped to our docs site url, but otherwise you can look at the [Gitlab CI environments page](https://gitlab.fso.arizona.edu/FAST/vueoom/environments) to see a direct link for each branch.
