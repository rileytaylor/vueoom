---
title: "Dropdown"
description: "A mixin that allows for creating dropdowns using Popper.js"
flag: "success"
flagText: "New"
---

The Dropdown mixin includes universal functionality for dropdowns, making it easy to add `popper.js` functionality to any element.

## Usage

To use:

```js
import DropdownMixin from 'vueoom';

export default {
...
    mixins: [
        DropdownMixin
    ],
...
};
```

**Usage inside of Vueoom:** Import the mixin as `../mixins/dropdown`.

### Wiring everything up
To make this mixin work, you'll need to include a few functions and properties in your component.

Most importantly, you'll need a click event that triggers the `toggle()` method, usually on a button or link element. Next, the dropdown content should have the ref `dropdown` and `v-show="show"`, which is the property this mixin uses to hide and show the dropdown area.

Here's an example:

```html
<div>
    <a @click="toggle"
       aria-haspopup="true" :aria-expanded="show">
        Show the Dropdown
    </a>
    <div ref="dropdown" v-show="show">
        <p>I'm in a dropdown!</p>
    </div>
</div>
```

It's a good idea to apply the `aria-` classes as well as shown above to aid screenreader software.

You should also use the [Clickout mixin]() to make sure the dropdown closes whenever someone clicks outside of its area of the screen.

### Allowing inner components to close the dropdown on click
If you have components inside of the dropdown and you'd like them to close the dropdown on click, you'll need to do something similar to the following wherever you handle clicking.

```js
// Ensure the parent dropdown is closed. don't emit if there isn't a dropdown to attach to
// First we see if the parent is a dropdown (note that it has to be open!), then emit so
// that it's listender will close it. We know if it's a dropdown if it has the `_dropdown` property.
let attachedTo = this.$parent._dropdown
    ? this.$parent.id
    : undefined;

if (attachedTo) {
    eventBus.$emit('dropdown::item-click', attachedTo);
}
```

You'll also need to import the `dropdownEventBus`. It lives at `mixins/dropdown/dropdownEventBus`, so you'll need to import it appropriately depending on whether you are inside Vueoom or not.

```js
import eventBus from '...';
```

Then, as long as the dropdown root component (the one that uses this mixin) has `closeOnClick` enabled, it will listen for that event and behave appropriately.

Essentially, as long as you emit on the dropdown event bus with a dropdown id, it'll work.

## Component Properties

#### Properties
| Name | Description | Default Value |
|:-----|:------------|:-----|
| dropup | Open the dropdown above the parent | `false` |
| alignRight | Align the dropdown to the right side of the parent| `false` |
| offset | Define an offset for rendering the dropdown | `undefined` |
| noFlip | Disable flipping on the axis of the dropdown if its content would normally be cut off by the window| `false` |
| closeOnClick\* | Automatically close the dropdown when an internal item is clicked | `true` |

\* Note that this requires [steps mentioned above](#allowing-inner-components-to-close-the-dropdown-on-click).

#### Computed
| Name | Description | Returns |
|:-----|:------------|:-----|
| id | For internal use | `dropdown-xxxx...` |
| placement | for internal use (the placement for `popper.js`| n/a 

#### Events
| Event | Description |
|:------|:------------|
| shown | The dropdown is visible |
| hidden | The dropdown is hidden |

## Programmatic use 
Any element with the dropdown mixin attached to it can be used through it's internal methods.

- **.toggle()** show or hide the dropdown
- **.hideDropdown()** hide the dropdown
- **.showDropdown()** show the dropdown
