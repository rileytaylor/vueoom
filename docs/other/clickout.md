---
title: "Clickout"
description: "Allow a component to react to clicks outside of its template"
flag: "success"
flagText: "New"
---

This is a simple event listener that gets registered on mount. Whenever someone clicks while the element is still mounted, the event will be fired and if the element is _not_ clicked on, it'll trigger the components `onClickOut()` method, where you can handle hiding/destroying (if necessary) the component.

To use:

```js
import ClickoutMixin from 'vueoom';

export default {
...
    mixins: [
        ClickoutMixin
    ],
    methods: {
        ...
        onClickOut () {

        },
        ...
    }
};
```

### Using inside Vueoom

Import the mixin as `../mixins/clickout`;
