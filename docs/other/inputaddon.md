---
title: "Input Addon"
description: "Add bootstrap <code>form-input-addons</code> to Input elements"
---

The Input Addon mixin handles bootstrap input addons for input elements.

To use:

```js
import InputAddonMixin from 'vueoom';

export default {
...
    mixins: [
        InputAddonMixin
    ]
};
```

### Using inside Vueoom

Import the mixin as `../mixins/inputAddon`;

### Registering addons in the template

To use it in a component, place the `input-start` and `input-end` span's around the input element:

```html
<span v-if="addonVisibility.start" class="input-group-addon">
    <slot name="addon-start"></slot>
</span>
<input />
<span v-if="addonVisibility.end" class="input-group-addon">
    <slot name="addon-end"></slot>
</span>
```

To provide addons, feed a text span or icon element to either or both of the slots:

```html
<input-control-example>
    <div slot="addon-start">
        <span>Search</span>
    </div>
    <div slot="addon-end">
        <icon icon="search"></icon>
    </div>
</input-control-example>
```

Your component will place the input addons on mount.

## Properties
| Name | Description | Default Value |
|:-----|:------------|:-----|
| addon | where to display input addon: `both`, `start`, `end`, or `none` | `'none'` |



