---
title: "Input"
description: "Generic Input methods, options, and properties for creating custom Input components."
---

The Input mixin includes common properties used in input elements.

To use:

```js
import InputMixin from 'vueoom';

export default {
...
    mixins: [
        InputMixin
    ]
};
```

### Using inside Vueoom

Import the mixin as `../mixins/input`;

## Properties
| Name | Description | Default Value |
|:-----|:------------|:-----|
| label | text label for the element | `undefined` |
| placeholder | watermark text inside the element shown before input is added | `undefined` |
| description | text to display below | `undefined` |
| disabled | disable the element | `false` |
| inline | render the label inline | `false` |
| autocomplete | turn off browser autocomplete | `true` |
| size | make the control smaller or larger | `'normal'` |
| success | display success contextual state | `false` |
| warning | display warning contextual state | `false` |
| danger | display danger contextual state | `false` |
| successMessage | message to display when `success` is true | `''` |
| warningMessage | message to display when `warning` is true | `''` |
| dangerMessage | message to display when `danger` is true | `''` |

## Computed
| Name | Description | Returns |
|:-----|:------------|:-----|
| isAutocomplete | returns the proper autocomplete property (`on` or `off`) | `String` |
| isDescription | returns whether there is a description | `Boolean` |
| isLabel | returns whether there is a label | `Boolean` |
| stateEnabled | returns whether there is a bootstrap validation state active | `Boolean` |
