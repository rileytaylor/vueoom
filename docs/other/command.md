---
title: "Command"
description: "Generic Command methods, options, and properties for creating custom Command components."
---

The Command mixin includes universal functionality for button controls.

To use:

```js
import CommandMixin from 'vueoom';

export default {
...
    mixins: [
        CommandMixin
    ]
};
```

### Using inside Vueoom

Import the mixin as `../mixins/command`;

## Properties
| Name | Description | Default Value |
|:-----|:------------|:-----|
| active | toggle active state | `false` |
| label | button text | `undefined` |
| icon | Font Awesome icon name | `undefined` |
| type | bootstrap button type | `'secondary'` |
| size | large or small | `'normal'` |
| disabled | disable the element | `false` |
| outline | use bootstrap outline style | `false` |

## Computed
| Name | Description | Returns |
|:-----|:------------|:-----|
| isOutline | returns the proper bootstrap class if `outline` is true or not | `String` |
| btnSize | returns the proper bootstrap class if a size is specified other than normal | `String` |

