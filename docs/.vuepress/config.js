/**
 * TODO: Things to work on:
 * - customize pwa refresh popup?
 * - figure out why the cloudfront isn't working properly for the dev-branch sites
 * - url and ssl cert
 * - algolia docsearch?
 */

module.exports = {
    title: 'Vueoom',
    description: 'FSO\'s own homebrewed Vue component framework. It makes our apps go fast. Vroom Vroom!',
    dest: 'public',
    base: '/',
    evergreen: true,

    // Markdown Configuration
    markdown: {
        extendMarkdown: md => {
            md.use(require('markdown-it-html5-embed'), {
                html5embed: {
                    useImageSyntax: true,
                    useLinkSyntax: false
                }
            })
            md.use(require('@centerforopenscience/markdown-it-video'), {
                youtube: { width: 640, height: 390 },
                vimeo: { width: 500, height: 281 }
            })
            // Include other markdown files with `{% include filepath %}`
            md.use(require('markdown-it-include'), {
                root: '.', // Root of the repo
                includeRe: /\{\%\s*include\s*(.+?)\s*\%\}/
            })
        }
    },

    // Plugins
    plugins: {
        '@vuepress/back-to-top': true,
        '@vuepress/pwa': {
            serviceWorker: true,
            updatePopup: true
        },
        // '@vuepress/register-components': {
        //     // componentsDir: './components'
        // }
    },

    // Set html head assets and metadata
    head: [
        ['link', { rel: 'icon', href: '/favicon.ico' }],
        ['link', { rel: 'manifest', href: '/manifest.json' }],
        ['meta', { name: 'theme-color', content: '#4a634e' }],
        ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
        ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
        // ['link', { rel: 'apple-touch-icon', href: `/icons/apple-touch-icon-152x152.png` }],
        // ['link', { rel: 'mask-icon', href: '/icons/icon.svg', color: '#4a634e' }],
        // ['meta', { name: 'msapplication-TileImage', content: '/icons/icon-144x144.png' }],
        // ['meta', { name: 'msapplication-TileColor', content: '#403635' }],
        // Font
        // ['link', { rel: 'stylesheet', href: 'https://brand.arizona.edu/sites/default/files/v2/ua-brand-fonts/milo.css'}],
        // FSO Bootstrap
        // ['link', { rel: 'stylesheet', href: '../../node_modules/bootstrap/dist/css/bootstrap.min.css'}]
    ],

    // Override browserslist config in postcss until vuepress gets its dependencies in order
    postcss: { 
        plugins: [
            require('autoprefixer')({
                browsers: ['>1%', 'last 2 versions']
            })
        ] 
    },

    // Theme configuration
    themeConfig: {
        nav: [
            { text: 'Home', link: '/' },
            { text: 'Components', link: '/components/' },
            { text: 'Directives', link: '/directives/' },
            { text: 'Other', link: '/other/' },
            { text: 'Usage', link: '/usage' },
            { text: 'Changelog', link: '/changelog' }
        ],
        sidebar: {
            '/components/': [
                '/components/',
                'Alert',
                'Calendar',
                'CheckBox',
                'CollapsePanel',
                'Command',
                'CommandDropdown',
                'CommandGroup',
                'DateInput',
                'DateRange',
                'FastFooter',
                'FastRedBar',
                'FormGroup',
                'Icon',
                'Modal',
                'Navbar',
                'Popover',
                'Radio',
                'SegmentedToggle',
                'SelectMultiple',
                'SelectSingle',
                'Stepper',
                'TextBox',
                'TextView',
                'Toast',
                'Toolbar'
            ],
            '/directives/': [
                '/directives/',
                'Mask',
                'Popover'
            ],
            '/other/': [
                '/other/',
                {
                    name: 'Mixins',
                    collapsable: false,
                    children: [
                      'clickout',
                      'command',
                      'dropdown',
                      'input',
                      'inputaddon'
                    ]
                }
            ],
            '/changelog/': 'auto',
            '/usage/': 'auto'
        },
        logo: '/icons/vueoom-512.svg',
        repo: 'https://gitlab.fso.arizona.edu/fast/vueoom',
        repoLabel: 'Repository',
        lastUpdated: 'Last Updated',
        docsDir: 'docs',
        docsBranch: 'master',
        docsRepo: 'https://gitlab.fso.arizona.edu/fast/vueoom',
        editLinks: true,
        editLinkText: 'Improve this page!',
        serviceWorker: {
            updatePopup: true
        }
    }
};
  