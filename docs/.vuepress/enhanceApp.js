// import Vueoom from '../../dist/vueoom.esm.js';
// import '../../node_modules/bootstrap/dist/css/bootstrap.css';

export default ({
    Vue, // the version of Vue being used in the VuePress app
    options, // the options for the root Vue instance
    router, // the router instance for the app
    siteData // site metadata
}) => {
    // ...apply enhancements to the app 

    // Use Vueoom
    // Vue.use(Vueoom);
}
