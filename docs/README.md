---
home: true
heroImage: icons/vueoom-512.png
actionText: Get Started! →
actionLink: /usage/
features:
- title: What's in it?
  details: Vueoom is packed full of components, directives, and utilities that are the building blocks of FSO applications. All of these components are built on the back of FSO-Bootstrap, providing a unified experience across apps.
- title: Pro Tip
  details: Use the search bar at the top to find what you are looking for.
- title: We heard you like apps
  details: For quick reference and a great welcome to the world of 2019, install this app to your desktop. That's right, it's a PWA. Hit that ... menu on your favorite browser that supports PWA's and install. It's that easy!
footer: MIT Licensed | Copyright © 2019 University of Arizona, Financial Services Office IT
---

<Demo />
