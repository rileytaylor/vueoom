---
sidebar: auto
---

# Usage

> _"When this thing gets up to 88 mph, you're gonna see some serious shit." -Doc Brown_

Vueoom can be imported into your app a number of different ways, with different packages for different deployment strategies.

## NPM (Node Package Manager)

::: tip Accessing our registry
Our packages are hosted on a private npm registry. To access it, you'll need to associate our registry with the `@fso` scope. You can do this by typing `npm config set @fso:registry https://zealot.fso.arizona.edu/repository/npm-internal/`
:::

In your terminal:

```shell
npm install --save-dev @fso/vueoom
```

::: tip Installing from Gitlab
Sometimes you might want to test out an unreleased version. To do so, install from gitlab with `npm isntall --save-dev git+https://gitlab.fso.arizona.edu/fast/vueoom#develop`. This will install the `develop` branch, but you can place any git tag or branch name here instead
:::

Then, in your app entry (usually `app.js` or `App.vue`):

```js
import vueoom from 'vueoom';

Vue.use(vueoom);
```

You also need to import styles from both Bootstrap (unless you already import or include them elsewhere) and Vueoom:

```js
import 'bootstrap/dist/css/bootstrap.css';
```

That's it. 

::: tip What about importing bootstrap's javascript?
We include any bootstrap javascript (or write our own) used directly by our components ourselves, so there is no need to do so unless you want to interact with methods from bootstrap directly.
:::

### Using with webpack + babel (ESM)
If you would like, you can consume an ES module instead of the browser module that Vueoom defaults to. This is ideal if you plan on compiling Vueoom with webpack. To import the ES module, use:

```js
import vueoom from 'vueoom/dist/vueoom.esm.js';

Vue.use(vueoom);
```
Depending on your environment, this may happen automatically as Vueoom informs apps which package should be for what type of import.

### Using with server-side-rendering (SSR)
For a SSR project you'll need to import the ssr version of Vueoom, which is designed to work properly without a `window` variable. To use it, import like so:
```js
import vueoom from 'vueoom/dist/vueoom.ssr.js';

Vue.use(vueoom);
```

### Importing individual components

Individual components can be imported directly from their files in `src/components/`.

For example:

```js
import Command from 'vueoom/src/components/Command.vue';

Vue.component('my-component', {
    components: {
        Command
    }
})
```

Keep in mind that you are responsible for transpiling components imported individually to ES5 with webpack and babel (or other pipelines).

## CDN (Script Tags)

TODO
