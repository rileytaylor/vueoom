---
title: "Modal"
description: "A triggerable dialog that displays above other content."
---
Modals are opinionated dialog boxes that are great for forms or providing additional information without taking up valuable screen real-estate. They can also be used for notifications and warnings. The Modal component uses bootstrap styles without any use of jQuery.

## Usage

```html
<modal title="Good News Everybody!">
    <div slot="content">
        <p>Look, some content.</p>
    </div>
</modal>    
```

#### Toggling Visibility

Modals are hidden by default. There are several ways to toggle their visibiliy.

##### `v-model`

Visibility can be toggled through `v-model`, which will wire up to the `visibility` property and the `changed` event.

```html
<template>
    <command label="Open Modal" @action="modalVisible = !modalVisible"/>

    <modal ref="modal" title="I was opened by v-model" v-model="modalVisible">
        <template slot="content">
            <p>Why hello there!</p>
        </template>
    </modal>
</template>

<script>
export default {
    data () {
        return {
            modalVisible: false
        };
    }
}
</script>
```

*Note:* Don't use the `visibility` property while also using `v-model`.

##### Programatically with `show()` and `hide()` component methods

By adding a `ref` to the modal, you can access it's internal methods, specifically `show()` and `hide()`.

```html
<template>
    <command label="Open Modal" @action="showModal"/>

    <modal ref="modal" title="I was opened by a ref">
        <template slot="content">
            <p>Why hello there!</p>
            <p>Close me with the button below...</p>
        </template>
        <template slot="footer">
            <command label="Close Modal" @action="closeModal"/>
        </template>
    </modal>
</template>

<script>
export default {
    methods: {
        showModal () {
            this.$refs.modal.show();
        },
        hideModal () {
            this.$refs.modal.hide();
        }
    }
}
</script>
```

#### Header, Content, Footer
By default, the header will display the `title` property and a close button. The footer will also have a single close button by default. You can replace the default header and footer through slots.

```html
<modal>
    <template slot="header">
        <h2>And now, for something completely different...</h2>
    </template>
    <template slot="content">
        <p>Look, some content.</p>
    </template>
    <template slot="footer">
        <command label="Cancel" type="danger" icon="remove" 
                 @action="doSomething"></command>
    </template>
</modal>    
```

The default header shows the title and a close button. The default footer provides a close button. Keep in mind if you override the header you might need to include the title in it, unless you don't want to show it.

#### Size
Modals have a default size that should suffice for most uses. If necessary, the `size` property can be used to create smaller or larger modals. The large modal includes a breakpoint for narrow widths. If you want a larger modal, use the `custom` size. The custom modal size will fill the screen in narrow widths (with some padding) and allow you to define a custom max/min width using the `minWidth` and `maxWidth` properties. This is especially benificial for creating very wide modals that scale down will in narrower widths. The default `maxWidth` is 800px, while the default `minWidth` is 300px, which should suffice for most situations that the large modal isn't wide enough.

```html
<template>
    <command label="Toggle Small Modal" @action="smModal = !smModal"/>
    <command label="Toggle Normal Modal" @action="normalModal = !normalModal"/>
    <command label="Toggle Large Modal" @action="lgModal = !lgModal"/>
    <command label="Toggle Custom Modal" @action="customModal = !customModal"/>
    <command label="Toggle Custom Modal 2" @action="customModal2 = !customModal2"/>

    <modal title="I'm really small." v-model="smModal">
        <div slot="content">
            <p>So much room for activities!</p>
        </div>
    </modal>   

    <modal title="I'm just right." v-model="normalModal">
        <div slot="content">
            <p>So much room for activities!</p>
        </div>
    </modal>    

    <modal title="I'm large." v-model="lgModal">
        <div slot="content">
            <p>So much room for activities!</p>
        </div>
    </modal>   

    <modal title="I'm custom and fill the window when narrow." v-model="customModal">
        <div slot="content">
            <p>So much room for activities!</p>
        </div>
    </modal>

    <modal title="I'm a really wide custom modal." :maxWidth="1000" v-model="customModal2">
        <div slot="content">
            <p>So much room for activities!</p>
        </div>
    </modal>   
</template>

<script>
export default {
    data () {
        return {
            smModal: false,
            normalModal: false,
            lgModal: false,
            customModal: false,
            customModal2: false
        };
    }
}
</script>
```

Valid options include: `small`, `large`, `custom`, and `normal`.


#### Vertical Centering

Vertically center the modal with the `centered` property.

*Note:* While the code is written, this will not function until `FSO-Bootstrap` is updated to Bootstrap v4.X.

```html
<template>
    <command label="Open Centered Modal" @action="modalVisible = !modalVisible"/>

    <modal ref="modal" title="I'm Centered" v-model="modalVisible" centered>
        <template slot="content">
            <p>Why hello there!</p>
        </template>
    </modal>
</template>

<script>
export default {
    data () {
        return {
            modalVisible: false
        };
    }
}
</script>
```

#### Dark Background

To render the modal in a dark theme, use the `dark` property.

```html
<template>
    <command label="Open Dark Modal" @action="modalVisible = !modalVisible"/>

    <modal ref="modal" title="Night Mode" v-model="modalVisible" dark>
        <template slot="content">
            <p>Hello darkness, my old friend</p>
            <p>I've come to talk with you again</p>
            <p>Because a vision softly creeping</p>
            <p>Left its seeds while I was sleeping</p>
            <p>And the vision that was planted in my brain</p>
            <p>Still remains</p>
            <p>Within the sound of silence</p>
        </template>
    </modal>
</template>

<script>
export default {
    data () {
        return {
            modalVisible: false
        };
    }
}
</script>
```

## Properties
| Name | Description | Default Value |
|:-----|:------------|:--------------|
| title  | the modal title | `''` |
| visible | set initial visibility of the modal* | `false` |
| size | change the size of the modal | `'normal'` |
| minWidth | define a min width (only valid when using `:size="custom"`) | `300` |
| maxWidth | define a max width (only valid when using `:size="custom"`) | `800` |
| centered | center the modal vertically in the window | `false` |
| dark | use the dark variant | `false` |
| disableBackdropClose | prevent close by clicking outside the modal dialog box| `false` |
| disableEscClose | prevent close when pressing the `esc` key | `false` |
| noFade | disable animations | `false` |
| hideHeader | hide the header | `false` |
| hideFooter | hide the footer | `false` |
| hideBackdrop | hide the opaque backdrop | `false` |

\* Do not use this while also using `v-model`!

## Events
| Name | Description |
|:---- |:----------- |
| @change | fires when a modal is hidden/shown with a boolean value as it's payload |
| @shown | fires once the modal has been made visible to the user |
| @hidden | fires once the modal has been hidden from the user |

## Slots
| Name | Description |
|:-----|:------------|
| header | the header |
| content | main content |
| footer | the footer |
