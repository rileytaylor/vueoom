---
title: "CheckBox"
description: "A custom checkbox for cross-browser consistency that replaces the default html checkbox input"
---

Checkboxes are great for obtaining truthy/falsy data from users. They are implemented using bootstrap's custom checkbox and play nicely with forms. By default the checkbox will use the primary bootstrap theme color.

## Usage
Checkboxes handle their checked state via the `v-model` binding, which ties to the `checked` property and `@change` event. The `id` property is required.

```html
<check-box id="checkbox4" label="things?"
          v-model="stuff"></check-box>
```

#### Description
You can optionally add a discription to be rendered with the checkbox.

```html
<check-box id="checkbox4" label="things?" description="All the things."
          v-model="stuff"></check-box>
```

#### Disabling
A checkbox can be disabled via the `disabled` flag.

```html
<check-box id="checkbox4" label="things?" disabled
          v-model="stuff"></check-box>
```

#### Contextual states
Like other bootstrap form elements, you can add a visual contextual state to notify the user of success or failure of a form element's data, or even present a warning about the selection. Three states are available: `success`, `warning`, and `danger`.

```html
<check-box id="checkboxsuccess" label="item 1" description="First thing."
          v-model="items"
          :success="true"
          successMessage="Hey, you did it!"></check-box>

<check-box id="checkboxwarning" label="item 2"
          v-model="items"
          :warning="true"
          warningMessage="If you could just check this, that'd be great."></check-box>

<check-box id="checkboxdanger" label="item 3"
          v-model="items" inline
          :danger="true"
          dangerMessage="Oh dear, you ruined it."></check-box>
```

The contextual states work nicely with all other properties. You can define multiple states for a checkbox and toggle each seperately.

#### Indeterminate
To set the checkbox indeterminate, use the `indeterminate` property:

```html
<check-box id="checkboxindeterminate" label="things?" :indeterminate="stufInd"
          v-model="stuff"></check-box>
```

Keep in mind that the inteterminate state can get out of sync if the box is checked, so you may need to manage that yourself.

#### Detecting changes
A `v-model` works well when you need to toggle a single data field in a javascript object or a property in a component, but it doesn't let you run code when you see that update happen. You could watch the data point and execute a watcher when it changes, or you could skip the `v-model` and tie directly into the `checked` property and `@update` event. This is handy when rendering several checkboxes in a `v-for` and you don't know what each one will necessarily be named or what data point it will tie into.

```html
<div class="form-group item" v-for="col in columndefs" :key="col.headerName">
    <check-box id="col.field" label="col.headerName"
              :checked="displayedcols[col.field]"
              @change="updateVisibility"></check-box>
</div>
```

Then, you can use javascript to process the field and it's checked boolean value.

```js
updateVisibility (visible, field) {
    this.doSomething(visible, field);
}
```

#### Other Html properties

Any other valid html properties (such as `readonly`) can also be applied to this component and they will be handled properly. For example:

```html
<check-box id="attrBindingExample" label="I'm not editable!" readonly/>
```

## Properties

| Name | Description | Default Value |
|:-----|:------------|:-----|
| checked | whether the box is checked or not | `false` |
| id (required) | identity of this checkbox for `@change` | n/a |
| label | the label identifying the checkbox | `''` |
| description | display an optional description underneath the checkbox | `''` |
| success | display success contextual state | `false` |
| warning | display warning contextual state | `false` |
| danger | display danger contextual state | `false` |
| successMessage | message to display when `success` is true | `''` |
| warningMessage | message to display when `warning` is true | `''` |
| dangerMessage | message to display when `danger` is true | `''` |
| disabled | toggle disabled state | `false` |
| inline | render the description inline | `false` |
| indeterminate | set the indeterminate state | `false` |

## Events
| Name | Description | Values |
|:-----|:------------|:-------|
| @change | raised when the box is checked | Boolean, `id` |
