---
title: "Form Group"
description: "A wrapper component to provide labelling and help text for elements in a form, including a universal layout system for form elements."
---

`FormGroup` is meant to wrap a html element so that it flows well in layout with other form elements in a form (be that literally inside a `<form>` element or just in layout with other form elements). Most form controls in Vueoom are already designed to work well in forms out of the box (often including the `.form-group` class already). However, if you want to place something else inside of a form area such as a custom-built control, a Vueoom control like `SegmentedToggle` that is not designed specifically for forms, or even something as complex as a map viewer or captcha verification control, you will need to use the `FormGroup` component to wrap them.

*Fun fact:* Vueoom's form controls use this component in the background to render themselves neatly in forms already.

## Usage

```html
<form-group>
    <segmented-toggle type="primary" name="delivery" 
                      :options="options" value="ground" @input="deliverySelected"></segmented-toggle>
</form-group>
```

#### Label

`FormGroup` comes with the ability to include a `label`, which can render above or inline, depending on the form layout. To aid with accessibility, include the `label-for` property with a valid css selector. Id's usually work best with screen readers.

```html
<form-group label="Delivery Type" label-for="deliveryPicker">
    <segmented-toggle id="deliveryPicker" type="primary" name="delivery" 
                      :options="options" value="ground" @input="deliverySelected"></segmented-toggle>
</form-group>
```

#### But I don't want to give it a label...

It might look nicer without a label, but you'll want to include it anyway since screenreader software. To keep things kosher for screenreeders *but* to keep your _fitter, happier, more productive, comfortable, not drinking too much, regular exercise at the gym three days a week, getting along better with your associate employee contemporaries, at ease_ layout without ruining it for the screen reader users, just use the `label-sr-only` property!

```html
<form-group label="Delivery Type" label-for="deliveryPicker" label-sr-only>
    <segmented-toggle id="deliveryPicker" type="primary" name="delivery" 
                      :options="options" value="ground" @input="deliverySelected"></segmented-toggle>
</form-group>
```

#### Label Size

To make a label better match the size of the form control, use the `label-size` property. This is only important if the input element inside of the `FormGroup` has a size of `large` or `small`, or if you need to better fit the size of something custom. A basic bootstrap input is used for the example.

```html
<form-group label="Small" label-for="small" inline label-size="small">
    <input id="small" type="text" class="form-control form-control-sm">
</form-group>
<form-group label="Normal" label-for="normal" inline>
    <input id="normal" type="text" class="form-control">
</form-group>
<form-group label="Large" label-for="large" inline label-size="large">
    <input id="large" type="text" class="form-control form-control-lg">
</form-group>
```

Note that this works best with an inline form, but it can be used in block-layouts too.

#### Description

To include a description (aka 'help text') in a form group, use the `description` property. Most of the form elements in Vueoom already include a `description` property to include help text, but non-form components that are used inside of a `FormGroup` can take advantage of this property to do so as well.

```html
<form-group label="Delivery Type" label-for="#deliveryPicker"
            description="Delivery costs will be calculated after selecting an option above.">
    <segmented-toggle id="deliveryPicker" type="primary" name="delivery" 
                      :options="options" value="ground" @input="deliverySelected"></segmented-toggle>
</form-group>
```

#### Inline

Use the `inline` property to render a `FormGroup` inline.

```html
<form-group label="SSN:" inline>
    <input type="number" placeholder="XXX-XX-XXXX"/>
</form-group>
```

When rendering inline, a `FormGroup` uses a column-based layout to align all labels and form elements in the form area nicely. By default, this will default to a `col-sm-2` for the label and a `col-sm-10` for the form control, which will return to a non-inline layout on smaller screens.

*Warning!* When using inside of an inline form (i.e. `<form class="form-inline">`), be sure _not_ to use the inline tag, since this breaks layout. Additionally, inline forms will usually exclude a `label` and `description`. If you feel you need some sort of help text, add it outside of the `form-group` and take a look at bootstrap's form guide for how to style it appropriatly.

To keep things kosher for users of screen readers, keep the `label` property but hide it from the page using the `label-sr-only` property.

## Properties
| Name | Description | Default Value |
|:-----|:------------|:-----|
| label | a string of text to display as the label for the component | `''` |
| label-for | which form element to target, usually by id. | `''` |
| label-size | what text size to use for the label, should match the form element being wrapped | `'normal'` |
| label-sr-only | hide the label, but keep it in the html for use by screen reader software | `false` |
| description | provide a description to display underneath the form element | `''` |
| inline | toggle rendering the form inline | `false` |



