---
title: "Command Group"
description: "Combine multiple Command and Command Dropdowns together."
---

Command Group allows several Commands to be stitched together.

## Usage
Command Group is a wrapper with a single default slot. Place any commands you want to join together inside. Commands can be of any type and style, and Command Dropdown's will work as well.

```html
<command-group>
    <command label="Button" type="primary"></command>
    <command label="Another Button" type="primary"></command>
    <command label="Outline Button" type="primary" outline></command>
</command-group>
```

### Size
To apply a command size to all included commands, use the `size` property.

```html
<command-group size="small">
    <command label="Button" type="primary"></command>
    <command label="Another Button" type="primary"></command>
    <command label="Outline Button" type="primary" outline></command>
</command-group>

<command-group size="large">
    <command label="Button" type="primary"></command>
    <command label="Another Button" type="primary"></command>
    <command label="Outline Button" type="primary" outline></command>
</command-group>
```

This will override any sizing on included components, though it creates some UI issues with nested `<command-group>` and `<input-group`> from time to time.

### Vertical Alignment
To align the command group vertically, pass the `vertical` flag.

```html
<command-group vertical>
    <command label="Button" type="primary"></command>
    <command label="Another Button" type="primary"></command>
    <command label="Outline Button" type="primary" outline></command>
</command-group>
```

### Assistive Technologies
To provide a label for assistive technologies, use the `assistiveLabel` property and pass in a string. This will announce the group to someone using a screen reader. If the grouping is more of a UI thing than it is a specific grouping of controls, feel free to omit it.

```html
<command-group assistiveLabel="Buttons of Choice">
    <command label="Button" type="primary"></command>
    <command label="Another Button" type="primary"></command>
    <command label="Outline Button" type="primary" outline></command>
</command-group>
```

## Properties
| Name | Description | Default Value |
|:-----|:------------|:-----|
| vertical | align vertical instead of the default horizontal | `false` |

