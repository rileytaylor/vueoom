---
title: "Popover"
description: "A tooltip-like control that can be applied to any element."
flag: "primary"
flagText: "New"
---
Popovers allow an element like a button to toggle a hidden element for confirmation or additional options. It could also be used as a tooltip with more information.

**When should I use a popover over a tooltip?** If your tooltip is more than a few words, consider switching to a popover.

This component is also available as a directive, `v-f-popover`.

## Usage

**Note:** This component makes use of Popper.js to render the hidden content and deal with positioning.

A typical usage of this component is through clicking, hovering, focus, or manually via v-model.

```html
<command id="click"
         label="Click me!" type="primary" />
<popover target="click"
         position="bottom"
         trigger="click"
         title="Title"
         content="Hello! I'm a popover shown via a click!" />
<command id="hover"
         label="Hover me!" type="primary" />
<popover target="hover"
         position="bottom"
         trigger="click"
         title="Title"
         content="Hello! I'm a popover shown via hover!" />
<command id="focus"
         label="Tab to me!" type="primary" />
<popover target="focus"
         position="bottom"
         trigger="click"
         title="Title"
         content="Hello! I'm a popover shown via focus!" />
<command id="none"
         v-model="openPop = !openPop"
         label="Click me!" type="primary" />
<popover target="focus"
         position="bottom"
         v-model="openPop"
         title="Title"
         content="Hello! I'm a popover shown manually!" />
```

```js
export default {
    ...
    data () {
        return {
            openPop: false
        };
    },
    ...
}
```

Thanks to the usage of v-model, you can realistically tie visibility up to any other event or programattic trigger as well.

### Title and Content
Setting the content of a popover is simple using the `title` and `content` properties. Additionally, the `title` and `content` slots can be used to place html in those slots rather than use the default html, usuful to provide actionable popover elements.

```html
<command id="content1"
         label="open" type="primary" />
<popover target="content1"
         visible
         title="Title"
         position="bottom"
         content="I have a title and content" />
<command id="content2"
         label="open" type="primary" />
<popover target="content2"
         visible
         title="Just a title"
         position="bottom" />
<command id="content3"
         label="open" type="primary" />
<popover target="content3"
         visible
         position="bottom"
         content="I only have content!" />
<command id="content4"
         label="open" type="primary" />
<popover target="content4"
         visible
         position="bottom">
    <template v-slot:title>
        <span><i>Italic Title</i></span>
    </template>
    <template v-slot:default>
        <command label="actionable content" />
    </template>
</popover>
```

For reference, this is the default template for each slot. Use the classes `bs-popover-title` and `bs-popover-body` to render everything the same as normal, or forge your own path.

```html
<template v-slot:title>
    <h3 class="bs-popover-header">
        Title
    </h3>
</template>
<template v-slot:default>
    <div class="bs-popover-body">
        Content
    </div>
</template>
```

### Position
Setting the position for rendering the popover is Popper.js's specialty. There are 4 "positions" (`top`, `bottom`, `left` and `right`) and 2 "modifiers" for each position (`-start` and `-end`). Valid options include: `auto`, `left`, `left-start`, `left-end`, `top`, `top-start`, `top-end`, `right`, `right-start`, `right-end`, `bottom`, `bottom-start`, `bottom-end`.

`auto` will render as seems best based on surrounding content and the closeness of the popover to the edge of the browser window.

TODO: Put a v-for with all examples here.

### Delay
To add a delay to the popover render, pass a number or an object (`{ start: Number, end: Number }`) to the `delay` prop. Passing a Number only will set both the start and end delay.

```html
<popover target="content3"
         visible
         position="bottom"
         content="I only have content!" />
<command id="content4"
         label="open" type="primary" />
```

### Disable
To disable the popover, use the `disabled` prop.

### Always visible
If (for some reason) you need to ensure the popover is visible, simply set `visible` to true and provide no `trigger` events.

```html
<command id="host"
         label="open" type="primary" />
<popover target="host"
         visible
         title="Title"
         position="bottom"
         content="Hello! I'm a popover!" />
```

_NOTE: This is currently broken if visible is set to true on page load. #sorrynotsorry_

### Offset
In addition to the placement properties, you can manually specify an `offset`, or a number of pixels to translate the rendered popover. It will not allow you to move the arrow beyond the width/height of the target element.

In the following example, the `placement` is set to `bottom-start`, but the `offset` of `10` will make the arrow render closer to the center of the button.

You will rarely use this option, but it's there if you need it.

```html
<command id="host"
         label="open" type="primary" />
<popover target="host"
         trigger="click"
         title="Title"
         position="bottom-start"
         offset="10"
         content="Hello! I'm a popover!" />
```

### Programattic access
If you need to work on the popover programmatically, you can use the `.toggle()`, `.show()`, and `.hide()` methods, which do exactly as you expect them to.

```html
<command id="host" click="togglePopover"
         label="open" type="primary" />
<popover ref="popover"
         target="host"
         title="Title"
         position="bottom"
         content="Hello! I'm a popover!" />
```

```js
togglePopover () {
    this.$refs.popover.toggle();
}
```

## Properties
| Name | Description | Default Value |
|:-----|:------------|:-----|\
| animation | toggle the animation usage | `true` |
| container | explicitely set where the popover renders | `body` |
| content | the content | `''` |
| delay | set a delay at the start, end, or both | `0` |
| disabled | disable the popover | `false` |
| disableFlip | prevent flipping on it's axis if it moves out of view on the page | `false` |
| offset | define an offset for rendering | `undefined` |
| placement | alignment of the popover compared to the target element | `auto` |
| target | an identifier for the target element, must be an `id` | required |
| title | the title | `''` |
| trigger | set the trigger event type | `none` |
| visible | set initial render visibility, or make reactive through v-model | `false` |

## Events
| Name | Description |
|:-----|:------------|
| visibilityupdated | update the parent visibility state |
| hide   | hide method triggered |
| hidden | successfully hidden |
| show   | show method triggered |
| shown  | successfully shown|

## Slots
| Name | Description |
|:-----|:------------|
| title | replace the title content |
| default | the content of the popover, overrides the `content` prop |
