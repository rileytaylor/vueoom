---
title: "Command Dropdown"
description: "A button that includes a dropdown menu."
flag: "info"
flagText: "Updated"
---

Create buttons that display menus on click, either with or without an additional connected button. This is an implementation of bootstrap's Dropdown component. Instead of using the included javascript, we render the dropdown with `popper.js`.

## Usage
The basic principles of a command dropdown are the same as the command component (meaning you can set the command style, use icons, and disable it). Additionally you will need to define the dropdown content.

```html
<command-dropdown label="Dropdown" type="primary">
    <dropdown-item>Action</dropdown-item>
    <dropdown-item>Another action</dropdown-item>
    <dropdown-item>Something else here</dropdown-item>
</command-dropdown>
```

#### Split

To render a split dropdown (which has a command and a smaller button that triggers the menu), use the `split` property. This then allows access to the `@action` event to handle clicking of the main button.

```html
<command-dropdown label="Dropdown" type="primary" split>
    <dropdown-item>Action</dropdown-item>
    <dropdown-item>Another action</dropdown-item>
    <dropdown-item>Something else here</dropdown-item>
</command-dropdown>
```

#### Types
There are several types available, corresponding to bootstrap's color variations.

```html
<command-dropdown label="Primary" type="primary"></command-dropdown>

<command-dropdown label="UA Red" type="uared"></command-dropdown>

<command-dropdown label="Secondary" type="secondary"></command-dropdown>

<command-dropdown label="Info" type="info"></command-dropdown>

<command-dropdown label="Warning" type="warning"></command-dropdown>

<command-dropdown label="Danger" type="danger"></command-dropdown>

<command-dropdown label="Success" type="success"></command-dropdown>
```

The available types are: `primary`, `uared`, `secondary`, `info`, `warning`, `danger`, `success`.

#### Outline
The `outline` flag removes background and puts color on the border and text.

```html
<command-dropdown label="Primary" type="primary" outline></command-dropdown>

<command-dropdown label="UA Red" type="uared" outline></command-dropdown>

<command-dropdown label="Secondary" type="secondary" outline></command-dropdown>

<command-dropdown label="Info" type="info" outline></command-dropdown>

<command-dropdown label="Warning" type="warning" outline></command-dropdown>

<command-dropdown label="Danger" type="danger" outline></command-dropdown>

<command-dropdown label="Success" type="success" outline></command-dropdown>
```

#### Size
To change the command from it's default size to something larger or smaller, use the `size` property.

```html
<command-dropdown label="Primary" type="primary" size="large"></command-dropdown>

<command-dropdown label="Primary" type="primary" size="small"></command-dropdown>
```

#### Menu Content
Vueoom includes several components that help build out your menu's content using the standard bootstrap designs.

##### Dropdown Item
`<dropdown-item>` fills the primary role of the dropdown menu. By default, it's type is `link` (an anchor element) which has a default `to` of `#` to prevent it from causing unexpected navigation, though you'll of course want to replace this. You can also configure it as a `<button>` or `<router-link>`, (type property set to `button` or `router-link`, respectively). As a button you can use the `@action` event handler to handle click events.

`router-link` is the same as `link`, using the `to` property to navigate, however, it will use vue-router directly, which can be desireable when navigating within the application.

Any other attributes can be used and they will be applied to the element. For example, you can use other `<router-link>` properties directly on this element.

```html
<command-dropdown label="Dropdown" type="info">
    <dropdown-item>Link that does nothing</dropdown-item>
    <dropdown-item to="https://portal.fso.arizona.edu">Go to Portal...</dropdown-item>
    <dropdown-item disabled>Disabled</dropdown-item>
    <dropdown-item type="button" @action="someMethod">A Button</dropdown-item>
    <dropdown-item type="router-link" to="/home">A router-link for vue-router navigation</dropdown-item>
</command-dropdown>
```

##### Dropdown Header
The `<dropdown-header>` allows labeling groups of items.

```html
<command-dropdown label="Dropdown" type="info">
    <dropdown-header>Mexican Food</dropdown-header>
    <dropdown-item>Tacos</dropdown-item>
    <dropdown-item>Burritos</dropdown-item>
    <dropdown-item>Enchiladas</dropdown-item>
    <dropdown-header>Italian Food</dropdown-header>
    <dropdown-item>Pasta</dropdown-item>
</command-dropdown>
```

##### Dropdown Divider
The `<dropdown-divider>` creates a line between elements.

```html
<command-dropdown label="Dropdown" type="info">
    <dropdown-header>Mexican Food</dropdown-header>
    <dropdown-item>Tacos</dropdown-item>
    <dropdown-item>Burritos</dropdown-item>
    <dropdown-item>Enchiladas</dropdown-item>
    <dropdown-header>Italian Food</dropdown-header>
    <dropdown-item>Pasta</dropdown-item>
</command-dropdown>
```

##### Custom menu content
If necessary, any html content can be placed inside, like this implementation without use of the included `dropdown-` components. The following uses the normal bootstrap dropdown content configuration and styles. Keep in mind that links and buttons will render abnormally inside of dropdowns. If you need something really custom, you should consider the `Popover` component.

```html
<command-dropdown label="Dropdown" icon="cog">
    <h6 class="dropdown-header">Dropdown header</h6>
    <button class="dropdown-item" type="button">Action</button>
    <button class="dropdown-item" type="button">Another action</button>
    <div class="dropdown-divider"></div>
    <button class="dropdown-item" type="button">Something else here</button>
</command-dropdown>
```

#### Menu Alignment
The menu is left aligned by default. To make it right aligned, simply use the `alignRight` tag.

```html
<command-dropdown label="Dropdown" alignRight>
    <button class="dropdown-item" type="button">Action</button>
    <button class="dropdown-item" type="button">Another action</button>
</command-dropdown>
```

Additionally, the menu will automatically render within the window, switching it's location along the x-axis or by flipping to a dropup/default config if necessary. To disable this, use the `noFlip` property.

To render the menu with an offset, you can pass either a number (representing pixels) or any valid css unit.

```html
<command-dropdown label="Dropdown" offset="30">
    <button class="dropdown-item" type="button">Action</button>
    <button class="dropdown-item" type="button">Another action</button>
</command-dropdown>

<command-dropdown label="Dropdown" offset="3em">
    <button class="dropdown-item" type="button">Action</button>
    <button class="dropdown-item" type="button">Another action</button>
</command-dropdown>
```

#### Dropup
To make the menu render above the button, use the `dropup` variant.
```html
<command-dropdown label="Dropdown" icon="cog" dropup>
    <h6 class="dropdown-header">Dropdown header</h6>
    <button class="dropdown-item" type="button">Action</button>
    <button class="dropdown-item" type="button">Another action</button>
    <div class="dropdown-divider"></div>
    <button class="dropdown-item" type="button">Something else here</button>
</command-dropdown>
```

#### Disabled
To render the dropdown as disabled, use the disabled flag. Often times this is best used hooked up to a local property or data value to toggle it's state.

```html
<command-dropdown label="Dropdown" type="primary" disabled></command-dropdown>
```

#### Active
If you need to manually render the command as active, set the `active` property.

```html
<command-dropdown label="Dropdown" type="primary" active></command-dropdown>
```

#### Programatic API

The `CommandDropdown` can be interacted with via javascript. To toggle the dropdown, use `.toggle()`:

```html
<command-dropdown ref="dropdown"></command-dropdown>
```

```js
this.$refs.dropdown.toggle();
```

There are other internal methods available, but they should generally not be used since they handle complete destruction/creation of the dropdown's javascript API.

## Component Properties

### `<command-dropdown>`

#### Properties
| Name | Description | Default Value |
|:-----|:------------|:-----|
| active | set the ui active | `false` |
| label | the label text | `''` |
| type | set the color | `secondary` |
| size | set the size | `normal` |
| disabled | toggle the disabled state | `false` |
| outline | use the outline style | `false` |
| icon | name a font awesome icon to use on the left side of the button | n/a |
| dropup | render the menu above | `false` |
| alignRight | render the menu right-aligned | `false` |
| offset | provide an offset value to change the rendering location of the menu | `0` |
| noFlip | disable automatic flipping when the dropdown would render outside of the visible window | `false` |
| split | render a split-button dropdown | `false` |

#### Events
| Name | Description |
|:-----|:------------|
| @click | respond to `click` events |


### `<dropdown-item>`

#### Properties
| Name | Description | Default Value |
|:-----|:------------|:-----|
| type | render as a `button`, `link`, or `router-link` | `link` |
| disabled | disable the item | `false` |
| to | desired navigation, either a full hyperlink or a vue-router route | `#` |

#### Events
| Name | Description |
|:-----|:------------|
| @action | respond to `click` events |
| @shown | triggered when the menu is shown |
| @hidden | triggered when the menu is hidden |
