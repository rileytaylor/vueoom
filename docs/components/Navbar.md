---
title: "Navbar"
description: "A centralized navigation component that makes use of vue-router"
flag: "primary"
flagText: "New"
---
Navbars are simple to use and are built on several components.

A common use case would look like the following, which has a branding area for the app name and icon. The main area for navigation is collapsible, which is toggled by a component that is only visible when the content is collapsed.

**Known Bugs:** As of Vueoom 1.1.3, there are a few issues in this component regarding the `position` property and flex-layouts within the `NavbarCollapse` component. These should be resolved in Vueoom 1.2 after migrating to FSO Bootstrap 1.x.


```html
<navbar breakpoint="md">
    <navbar-toggler class="pr-2" target="nav" />
    <navbar-brand title="Vueoom">
        <img src="../../static/icons/vueoom-512.svg" width="32" height="32">
    </navbar-brand>
    <navbar-collapse id="nav">
        <navbar-content>
            <navbar-item to="#">
                Home
            </navbar-item>
            <navbar-item to="#">
                Components
            </navbar-item>
            <navbar-item to="#">
                Directives
            </navbar-item>
            <navbar-item to="#">
                Mixins
            </navbar-item>
            <navbar-item to="#">
                Utils
            </navbar-item>
        </navbar-content>
        <navbar-form class="ml-auto">
            <button class="btn btn-sage" type="button" @click="goToReleaseNotes">
                <icon icon="sticky-note-o" />
                <span class="ml-1">Release Notes</span>
            </button>
        </navbar-form>
    </navbar-collapse>
</navbar>
```

## Usage

### Collapsing
Collapsing is handled through the `NavbarCollapse` and `NavbarToggle` components as well as the `breakpoint` property of the `Navbar`, which work together to create a responsive layout.

To define a collapsible area, first give the `NavbarCollapse` an `id`, which is used by the `NavbarToggle`'s `target` property to toggle visibility of the correct navbar. While most apps will only have 1 navbar, the ability to run multiple is built into Vueoom.

```html
<navbar-toggle target="some-id" />
<navbar-collapse id="some-id">
    ...
</navbar-collapse>
```

Next, make sure you set a breakpoint on the `Navbar`. This tells the navbar when to collapse content and show the toggle button. The options for the navbar's `breakpoint` property are: `sm`, `md`, `lg`, and `xl`, just like most bootstrap breakpoints. `sm` will collapse content at narrower widths. For most apps with only a few links in the navbar, `md` will be the best bet.

A collapse normally will include all content of the navbar, but it doesn't have to. You can choose to only include some of the navbar content in the collapsable area.

```html
<navbar theme="dark" background="dark" breakpoint="md">
    <navbar-toggler class="pr-2" target="nav" />
    <navbar-brand title="Vueoom">
        <img src="../../static/icons/vueoom-512.svg" width="32" height="32">
    </navbar-brand>
    <navbar-collapse id="nav">
        <navbar-content>
            <navbar-item to="#">
                Home
            </navbar-item>
            <navbar-item to="#">
                Components
            </navbar-item>
            <navbar-item to="#">
                Directives
            </navbar-item>
            <navbar-item to="#">
                Mixins
            </navbar-item>
            <navbar-item to="#">
                Utils
            </navbar-item>
        </navbar-content>
    </navbar-collapse>
    <navbar-form class="ml-auto">
        <button class="btn btn-sage" type="button" @click="goToReleaseNotes">
            <icon icon="sticky-note-o" />
            <span class="ml-1">Release Notes</span>
        </button>
    </navbar-form>
</navbar>
```
Just be sure you handle any responsiveness requirements for things not included in the collapse.

#### Placing the Toggle
Note that in most of the examples on this page, the toggle icon is placed on the left-hand side. This is more inline with what people are used to on phones and desktop applications _instead_ of where bootstrap normally places it on the right-hand side. You can still place it on the right if you'd like. Here is the example from the beginning of this page, but with a right-aligned icon:

```html
<navbar theme="dark" background="dark" breakpoint="md">
    <navbar-brand title="Vueoom">
        <img src="../../static/icons/vueoom-512.svg" width="32" height="32">
    </navbar-brand>
    <navbar-toggler target="nav" class="ml-auto" />
    <navbar-collapse id="nav">
        <navbar-content>
            <navbar-item to="#">
                Home
            </navbar-item>
            <navbar-item to="#">
                Components
            </navbar-item>
            <navbar-item to="#">
                Directives
            </navbar-item>
            <navbar-item to="#">
                Mixins
            </navbar-item>
            <navbar-item to="#">
                Utils
            </navbar-item>
        </navbar-content>
        <navbar-form class="ml-auto">
            <button class="btn btn-sage" type="button" @click="goToReleaseNotes">
                <icon icon="sticky-note-o" />
                <span class="ml-1">Release Notes</span>
            </button>
        </navbar-form>
    </navbar-collapse>
</navbar>
```

The toggle component can be placed almost anywhere within the navbar, you'll just need to handle alignment. Technically, you could place a `NavbarToggle` anywhere in the application, but that's not recommended in most cases.

### Theme and Background
The `theme` and `background` properties work together to style the navbar. Each has three options: `light`, `dark`, and `custom`. `light` is the default for both.

```html
<navbar theme="light" background="light">
    <navbar-brand title="Vueoom">
        <img src="../../static/icons/vueoom-512.svg" width="32" height="32">
    </navbar-brand>
    <navbar-content>
        <navbar-item to="#">
            Home
        </navbar-item>
    </navbar-content>
</navbar>
```

```html
<navbar theme="dark" background="dark">
    <navbar-brand title="Vueoom">
        <img src="../../static/icons/vueoom-512.svg" width="32" height="32">
    </navbar-brand>
    <navbar-content>
        <navbar-item to="#">
            Home
        </navbar-item>
    </navbar-content>
</navbar>
```

#### Custom Theme and Background
By setting the `custom` property, you'll need to also supply a css class to `customBackgroundClass` and/or `customThemeClass`. 

##### Bootstrap Backgrounds
You have a few options built into FSO Bootstrap for use. Any of the `.bg-*` background classes will work just fine for `customBackgroundClass`, meaning you can set this to `bg-primary` for the primary color background, or `bg-accent` for a UA Red background.

```html
<navbar theme="light" background="bg-primary">
    <navbar-brand title="Vueoom">
        <img src="../../static/icons/vueoom-512.svg" width="32" height="32">
    </navbar-brand>
    <navbar-content>
        <navbar-item to="#">
            Home
        </navbar-item>
    </navbar-content>
</navbar>
```

```html
<navbar theme="light" background="bg-accent">
    <navbar-brand title="Vueoom">
        <img src="../../static/icons/vueoom-512.svg" width="32" height="32">
    </navbar-brand>
    <navbar-content>
        <navbar-item to="#">
            Home
        </navbar-item>
    </navbar-content>
</navbar>
```

##### Completely Custom Backgrounds

These class names should be somewhere in your application and will be used to style the navbar. When working with a custom background you may find that you can use either the light or dark `theme` options. It's recommended you make your background either bright or dark enough to support either one, since colored text/iconography can start to look... odd. But it's there if you need it.

```html
<navbar theme="dark" background="pinkNavbar">
    <navbar-brand title="Vueoom">
        <img src="../../static/icons/vueoom-512.svg" width="32" height="32">
    </navbar-brand>
    <navbar-content>
        <navbar-item to="#">
            Home
        </navbar-item>
    </navbar-content>
</navbar>
```

```css
.pinkNavbar {
    ...
}
```

For your reference, these are the styles for the `light` and `dark` backgrounds taken from FSO Bootstrap:

```css
.bg-light {
  background-color: #f8f9fa !important;
}

.bg-dark {
  background-color: #343a40 !important;
}
```
##### Completely Custom Themes

Here are the styles for the `light` theme which can be used to base a custom theme off of:

```css
navbar-light .navbar-brand {
  color: rgba(0, 0, 0, 0.9);
}

.navbar-light .navbar-brand:hover, .navbar-light .navbar-brand:focus {
  color: rgba(0, 0, 0, 0.9);
}

.navbar-light .navbar-nav .nav-link {
  color: rgba(0, 0, 0, 0.5);
}

.navbar-light .navbar-nav .nav-link:hover, .navbar-light .navbar-nav .nav-link:focus {
  color: rgba(0, 0, 0, 0.7);
}

.navbar-light .navbar-nav .nav-link.disabled {
  color: rgba(0, 0, 0, 0.3);
}

.navbar-light .navbar-nav .show > .nav-link,
.navbar-light .navbar-nav .active > .nav-link,
.navbar-light .navbar-nav .nav-link.show,
.navbar-light .navbar-nav .nav-link.active {
  color: rgba(0, 0, 0, 0.9);
}

.navbar-light .navbar-toggler {
  color: rgba(0, 0, 0, 0.5);
  border-color: rgba(0, 0, 0, 0.1);
}

.navbar-light .navbar-text {
  color: rgba(0, 0, 0, 0.5);
}

.navbar-light .navbar-text a {
  color: rgba(0, 0, 0, 0.9);
}

.navbar-light .navbar-text a:hover, .navbar-light .navbar-text a:focus {
  color: rgba(0, 0, 0, 0.9);
}
```

### Placement
The navbar's css position can be modified via the `placement` property. This allows for a "sticky" navbar, or for ensuring it lives at the top or bottom of the page.

```html
<navbar placement="sticky-top">
    <navbar-brand title="Vueoom">
        <img src="../../static/icons/vueoom-512.svg" width="32" height="32">
    </navbar-brand>
</navbar>
```

```html
<navbar placement="fixed-top">
    <navbar-brand title="Vueoom">
        <img src="../../static/icons/vueoom-512.svg" width="32" height="32">
    </navbar-brand>
</navbar>
```

```html
<navbar placement="fixed-bottom">
    <navbar-brand title="Vueoom">
        <img src="../../static/icons/vueoom-512.svg" width="32" height="32">
    </navbar-brand>
</navbar>
```

Note that your mileage may vary with placement depending on where in your html the navbar has been defined. In partiuculary, "sticky-top" uses `position: sticky`, which is not fully supported in all browsers.

### Content
There are several pre-built components you can place inside of a navbar. If you want to use anything other than what's listed below, be sure to wrap it in a `NavbarContent` or `NavbarForm` component for proper layout.

#### Brand
The `NavbarBrand` component serves as the header/title area of your application. You can place an icon inside of the component, as well as define a `title`. Neither is required, though you should use one. You can also define a `to` location which will serve as a hyperlink. By default, this is `/` so that clicking on the title/icon will take the user back to the apps homepage.

```html
<navbar>
    <navbar-brand title="Vueoom">
        <img src="../../static/icons/vueoom-512.svg" width="32" height="32">
    </navbar-brand>
</navbar>
```

```html
<navbar>
    <navbar-brand>
        <img src="../../static/icons/vueoom-512.svg" width="32" height="32">
    </navbar-brand>
</navbar>
```

```html
<navbar>
    <navbar-brand title="Vueoom" />
</navbar>
```

#### Content Wrapper
This is simply a wrapper around the bootstrap `nav` class, and is usually used to hold `NavbarLink` and `NavbarDropdown` components. It configures the navigation well for a11y support for those using screenreaders, so it is recommended you only place nagivation-related elements in a `NavbarContent` component. For everything else, use a `NavbarForm`.

#### Links 
Links are defined with the `NavbarLink` component. Each `NavbarLink` wraps a `<router-link>` component for use with vue-router.

A minimal `NavbarLink` looksl like this:

```html
<navbar-item to="#">Link 1</navbar-item>
```

All that it needs is a name for the link ('Link 1') and a place to navigate `to`.

If you desire, you can also customize the `replace`, `append`, `exact`, and `tag` properties used by `<router-link>`. For more information on vue-router's properties, see [their docs](https://router.vuejs.org/api/#router-link-props).

**Note:** In particular, you should use the `exact` property on any links that go to the root route (i.e. their `to` is `'/'`), because the active style will be applied to it for every sub-route as well.

You can also disable a `NavbarLink` with the `disable` property.

#### Item Dropdowns
Item Dropdowns are defined with the `NavbarItemDropdown` component. This functions just like a `CommandDropdown` and shows a menu of items. It can be themed to match your navbar's theme, though you may find that the `light` and `dark` options are more than enough to match most custom backgrounds.

This `NavbarItemDropdown` also uses `DropdownItem` components internally. For more info on the `DropdownItem` component, see [its docs]().

```html
<navbar>
    <navbar-brand title="Vueoom">
        <img src="../../static/icons/vueoom-512.svg" width="32" height="32">
    </navbar-brand>
    <navbar-item-dropdown label="Dropdown">
        <dropdown-item to="#1">
            Link 1
        </dropdown-item>
        <dropdown-item to="#2">
            Link 2
        </dropdown-item>
        <dropdown-item to="#3">
            Link 13
        </dropdown-item>
    </navbar-item-dropdown>
</navbar>
```

Using the dark theme:

```html
<navbar theme="dark" background="dark">
    <navbar-brand title="Vueoom">
        <img src="../../static/icons/vueoom-512.svg" width="32" height="32">
    </navbar-brand>
    <navbar-item-dropdown label="Dropdown" theme="dark">
        <navbar-link to="#1">
            Link 1
        </navbar-link>
        <navbar-link to="#2">
            Link 2
        </navbar-link>
        <navbar-link to="#3">
            Link 13
        </navbar-link>
    </navbar-item-dropdown>
</navbar>
```

Like the navbar, you can also pass in a `customThemeClass` to define your own theme. For reference, this is the theme used for a dark dropdown, which should show all things you need to modify from the default bootstrap dropdown classes.

```scss
.dropdown-menu__dark {
    background-color: #343a40 !important;
    border-color: $gray-light;

    .dropdown-item {
        color: #fff;

        &:hover {
            background: lighten(#343a40, 10%);
        }
    }

    .dropdown-divider {
        background-color: $gray-light;
    }

    .dropdown-header {
        color: lighten($gray-light, 5%);
    }
}
```

#### Form Wrapper
The `NavbarForm` wrapper component is simply a bootstrap `form-inline` class with a few modifications meant to help it behave well inside of a navbar. Place buttons, inputs, and other form-related or general controls *that aren't* navigation.

#### Text
Use the `NavbarText` component to place a string of text on the navbar.

```html
<navbar>
    <navbar-brand title="Vueoom">
        <img src="../../static/icons/vueoom-512.svg" width="32" height="32">
    </navbar-brand>
    <navbar-text>
        Makes our apps go fast. Vroom. Vroom.
    </navbar-text>
</navbar>
```

## Component Properties

### `<navbar>`

#### Properties
| Property | Description | Default Value |
|:---------|:------------|:-------------:|
| theme | Define the theme color | `light` |
| background | Define the background color | `light` |
| breakpoint | Set the breakpoint for collapsing content | `none` |
| customBackgroundClass | Define a custom background class selector for the background | `''` |
| customThemeClass |Define a custom theme class selector for the theme | `''` |
| placement | Set the placement of the navbar | `default` |

#### Slots
| Slot | Description | Default Content |
|:-----|:------------|:----------------|
| default | Place navbar content here | none |

### `<navbar-brand>`

#### Properties
| Property | Description | Default Value |
|:---------|:------------|:-------------:|
| title | Define the application name | `''` |
| to | Define the link for the title and icon | `/` |

#### Slots
| Slot | Description | Default Content |
|:-----|:------------|:----------------|
| default | Place the icon here | none |

### `<navbar-collapse>`

#### Properties
| Property | Description | Default Value |
|:---------|:------------|:-------------:|
| id | an id for receiving toggle events | n/a |

#### Events
| Event | Description | Payload |
|:------|:------------|:-------:|
| @shown | The collapse has been made visible | `[ id ]` |
| @hidden | The collapse has been hidden | `[ id ]` |

#### Slots
| Slot | Description | Default Content |
|:-----|:------------|:----------------|
| default | Place content to be collapsed here | none |

### `<navbar-item>`

#### Properties
| Property | Description | Default Value |
|:---------|:------------|:-------------:|
| to | The route to navigate to | n/a |
| replace | Nuke browser history when navigating when true | `false` |
| append | Indicate that `to` should append to the url, not replace | `false` |
| tag | Define the html tag the router-link should render as | `'a'` |
| exact | Force exact matching of route to make the link active | `false` |
| disable | Disable the link | `false` |

#### Slots
| Slot | Description | Default Content |
|:-----|:------------|:----------------|
| default | Place the link name here | none |

### `<navbar-item-dropdown>`

#### Properties
| Property | Description | Default Value |
|:---------|:------------|:-------------:|
| label | Label the dropdown link | `''` |
| theme | Define the background color of the dropdown | `light` |
| customThemeClass | Define a custom background class selector for the component background | `''` |
| dropup | Open the dropdown above the parent | `false` |
| alignRight | Align the dropdown to the right side of the parent| `false` |
| offset | Define an offset for rendering the dropdown | `undefined` |
| noFlip | Disable flipping on the axis of the dropdown if its content would normally be cut off by the window| `false` |
| closeOnClick | Automatically close the dropdown when an internal item is clicked | `true` |

#### Slots
| Slot | Description | Default Content |
|:-----|:------------|:----------------|
| default | Place content for the dropdown here | none |

### `<navbar-toggler>`

#### Properties
| Property | Description | Default Value |
|:---------|:------------|:-------------:|
| target | the id of a `NavbarCollapse` to toggle | n/a |

## Programmatic access
While not recommended for the navbar, if you need to manually take control of toggling, you can use the various `toggle()` methods on each component. The recommended approach for accessing components internal methods is to set a ref on their template, and then access via `this.$refs.someRef.method()`

- **NavbarToggler**: use `.toggle()` to send a toggle event to the `NavbarCollapse` defined as it's `target`. There isn't a built in way to pass in a different target. If you need to manage multiple `NavbarCollapse` components, use the toggle method attached to those components instead of a toggler.
- **NavbarCollapse**: Use `toggle()` to show or hide it immediately. You can also use `hide()` and `show()` methods.
