---
title: "Collapse Panel"
description: "A collapsible panel with a header area"
---

The Collapse Panel is a great way to allow the user to hide information in the UI.

## Usage
`<collapse-panel>` is more or less a simple wrapper around a bootstrap card component that will take care of collapsability for you.

```html
<collapse-panel title="It's a Panel">
    <div slot="content">
        <p>I'm in a collapsable panel!</p>
    </div>
</collapse-panel>
```

#### Summary
When the panel is collapsed, you can provide some information to show in the header area.

```html
<collapse-panel title="It's a Panel">
    <div slot="content">
        <p>I'm in a collapsable panel!</p>
    </div>
    <div slot="summary">
        <p>I'm a summary!</p>
    </div>
</collapse-panel>
```

Keep in mind that the header area is only so big... be sure your summary content will fit in. A string that is made to `...` at the end or content that you know has a maximum length is best for this summary.

## Properties
| Name | Description | Default Value |
|:-----|:------------|:-----|
| title (required) | the header | `''` |
| collapsed | toggle the panel | `false` |

## Slots
| Name | Description |
|:----|:-----------|
| content | the main content that is hidden |
| summary | a brief section of the header shown while collapsed |
