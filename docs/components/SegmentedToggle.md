---
title: "Segmented Toggle"
description: "A checkbox-like toggling system that allows toggling between values, similar to the iOS segmented picker."
---
`SegmentedToggle` provides a selection control that functions similarly to bootstrap's Radio Buttons (in fact, it uses the same styling), but behaves more closely to the iOS Segmented Picker control.

## Usage

The `SegmentedToggle` control requires that one element always be selected, and it will handle updating the UI of other options when the selection changes. The `value` property is for use with data-binding to the control and will always have the currently selected object as its value.

```html
<segmented-toggle type="primary" name="pets" 
                  :options="options" value="both" @input="petSelected"/>
```

In addition to providing the `options` array, you also need to provide a `name` since this control is really just a fancy `<input type="radio">` at it's core.

#### Usage with forms

To use inside of a form or among other form elements, `SegmentedToggle` can be used within a `FormGroup` component, like so:

```html
<form-group label="Pets?">
    <segmented-toggle type="primary" name="pets" 
                      :options="options" value="both" @input="selectPets"/>
</form-group>
```

This way, it'll work just like any other form element and can be used in place of a `CheckBox`, `Radio`, or even a simple `SelectBox` or `MultiSelect`.

#### Group Label

If you want to label the control (and aren't using it inside of a `FormGroup`) use the `group-label` property.

```html
<segmented-toggle type="primary" name="pets" group-label="Pets!" 
                  :options="options" value="both" @input="petSelected"/>
```

#### Type

This control supports all bootstrap button styles, inlcuding using the `outline` property for outline button styles. See the `Command` component documentation for more information on available options.

#### Disabled

The `SegmentedToggle` can be disabled just like any other input.

```html
<segmented-toggle type="primary" name="pets" disabled
                  :options="options" value="both" @input="petSelected"/>
```

#### Full Width

If you want the control to take up the full width of it's parent (similar to `block` for `Command` components) use the `full-width` property.

```html
<div style="width: 100%">
    <segmented-toggle type="primary" name="pets" full-width
                      :options="options" value="both" @input="petSelected"/>
</div>
```

### `<segmented-toggle-option>`

The `SegmentedToggle` component makes use of the `SegmentedToggleOption` component internally to render each toggle element. Currently, this is not usable outside of this component _but_ is exported by Vueoom so that it can be used in the future if this component eventually allows creating selection options through markup in addition to the `options` object.

## Properties
| Name | Description | Default Value |
|:-----|:------------|:-----:|
| name | (required) a name to aid internal use of the component | n/a |
| groupLabel | a label for the component | `''` |
| value | initial value to toggle in the control | `''` |
| options | (required) An array of objects to display as options | n/a |
| type | the button type to use for styling | `'secondary'` |
| outline | toggle using outline button styles | `false` |
| disabled | disable the control | `false` |
| fullWidth | force the component to take up the full width of its parent | `false` |

## Events
| Name | Description |
|:-----|:------------|
| @input | emits the selected value |
