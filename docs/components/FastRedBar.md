---
title: "Redbar"
description: "A simple style component for the header of FSO applications"
flag: "primary"
flagText: "New"
---
## Usage

Nothing should be placed inside Redbar and it should only be used as a header.

**Why 'Redbar'?** Ask the UA Branding people. It's normally red, so there you go.

```html
<redbar />
```

### Background
By default this component uses UA Red (bootstrap's `bg-accent` class), and it should always use this default on official production websites. You can override this behavior with the `background` property by defining a different class.

```html
<redbar background="bg-primary" />
```

```html
<redbar background="custom-pink-bg" />
```

```css
.custom-pink-bg {
    background-color: pink !important;
}
```

## Component Properties

### Properties
| Name | Description | Default Value |
|:-----|:------------|:-----|
| background | set the background class of the bar| `'bg-accent'` |
