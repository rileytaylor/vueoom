---
title: "Select Multiple"
description: "A selection component for multiple-choice uses"
---
`SelectMultiple` is a wrapper around [vue-multiselect](https://vue-multiselect.js.org/), providing UI styling and additional bootstrap functionality such as contextual states. It specifically sets the component to multiple mode, and this component helps ensure the binding works out properly.  As it is a wrapper, be sure to check out the documentation linked above for advanced usage, however, all basic information is presented below. We do set a few defaults for ease-of-use, but those can be overriden as necessary. Internally, this component forwards all attributes that aren't first handled by us directly to vue-multiselect.

## Usage

For a properly functioning multiple selection box, you need to provide it an `id`, `v-model` binding, and `options` array.

```html
<select-multiple id="multiSelect" v-model="example" :options="exampleOptions" />
```

Any other vue-multiselect properties can be applied directly to this wrapper component and they will be routed properly. This is especially helpful if you need to use any properties of `vue-multiselect` that are currently not covered by this documentation. To see all properties, refer to [this guide](https://vue-multiselect.js.org/#sub-props).

#### Options

An options array is an array of objects, each object containing `text` and `value` key's, like so:

```js
const options = [ 
    { text: 'Apples', value: 'apple' },
    { text: 'Bananas', value: 'banana' },
    { text: 'Oranges', value: 'orange' },
    { text: 'Grapefruit', value: 'gfruit' },
    { text: 'Pear', value: 'pear' }
];
```

`text` will be what is shown to the user, while `value` will be what is send up to the component's parent through the `v-model` binding. You can customize the keys used by the select through the `selectionLabel` and `returnValue` properties, which return `text` and `value` by default, respectively.

#### Selection

The component will return an array of selected values (as strings) and display the selected options as "tags" in the control in the UI.

When using multiple selection, it can be useful to hide selected values from the dropdown list. To do so, use the `hide-selected` property. This will prevent the user from de-selecting options in the dropdown, but they can still be removed via the tag in the input itself.

```html
<select-multiple id="fruitSelect" label="Fruit Choices:" placeholder="Select a fruit..."
                 v-model="fruits1" :options="fruitOptions" hide-selected>
</select-multiple>
```

#### Label, Placeholder, and Description

As a form element, `SelectMultiple` allows some general formatting just like any other form element in vueoom. Use the `label` property to set the label. `placeholder` will set the placeholder. `description` will place small descriptive text below the component.

```html
<select-multiple id="fruitSelect" label="Fruit Choices:" placeholder="Select a fruit..."
                 v-model="fruits2" :options="fruitOptions"
                 description="Be sure to eat enough fruit every day.">
</select-multiple>
```

#### Disabling

To disable the component, simply use the `disabled` property.

```html
<select-multiple id="multiSelect" v-model="example" :options="exampleOptions" disabled/>
```

**Construction Area:** This doesn't work for some reason, sorry!

#### Inline rendering

To render the component inline, use the `inline` property. The description can also be rendered inline with `inlineDescription`:

```html
<select-multiple id="fruitSelect" label="Fruit Choices:" placeholder="Select a fruit..."
                 v-model="fruits4" :options="fruitOptions"
                 description="Be sure to eat enough fruit every day."
                 inline inlineDescription>
</select-multiple>
```

#### Contextual States
Like other bootstrap form elements, you can add a visual contextual state to notify the user of success or failure of a form element's data, or even present a warning about the selection. Three states are available: `success`, `warning`, and `danger`.

```html
<select-multiple label="Fruit Choices:" placeholder="Select a fruit.."
                 v-model="fruits3" :options="fruitOptions"
                 :danger="danger" danger-message="That's it, you bought the farm."
                 :warning="warning" warning-message="Woah there, hold up!"
                 :success="success" success-message="Congrats, you retired to Millionaire Acres!">
</select-multiple>
```

The contextual states work nicely with all other properties. You can define multiple states and toggle each seperately.

#### General Configuration

The vue-multiselect docs describe much of the following, but for the sake of providing examples and making it easy to reference, lets go over some common configurations.

##### Searching

By default, `SelectMultiple` will allow users to search the options by typing in the input when the dropdown is open. To disable this, set `searchable` to false.

```html
<select-multiple id="searchSelect" v-model="search" :options="exampleOptions" label="Searchable"/>
<select-multiple id="noSearchSelect" v-model="noSearch" :options="exampleOptions" label="Not Searchable"/>
```
##### State/Event handling

There are a few properties that can be used to govern how the component behaves: 
- `closeOnSelect`: close the dropdown when an option is selected (`true` by default)
- `clearOnSelect`: clear the search area when an option is selected (`true` by default)
- `maxHeight`: Set the maximum height of the dropdown area
- `max`: Set the maximum number of selections
- `allowEmpty`: Once an option is selected, the user cannot leave the field blank (similar to a radio) (`false` by default)
- `blockKeys`: prevent certain keys from triggering events. An array of strings, such as `['Tab', 'Enter']`.

#### Asynchronous Selection

Asynchronous selection is useful for a selection box tied to an external api or generated dataset, sort of like an auto-complete text box. For information on this mode, see [the docs here](https://vue-multiselect.js.org/#sub-asynchronous-select).

#### Tagging

Tagging allows the user to supply new options within the selection input. This makes use of the `@tag` event.

**Construction Area:** This is currently not implemented in our wrapper component.

#### Customizing Templates

**Construction Area:** Currently, you cannot customize any of the slot templates found in vue-multiselect.

#### Option groups

**Construction Area:** This is currently not implemented in our wrapper component.

## Properties
| Name | Description | Default Value |
|:-----|:------------|:-----|
| id | (required) identifier | n/a |
| label | text label for the element | `undefined` |
| placeholder | watermark text inside the element shown before input is added | `Select an option...` |
| description | text to display below the element | `undefined` |
| inline | render the label and component inline | `false` |
| inlineDescription | render the description inline | `false` |
| value | initial items to display in the element, as strings | `[]` |
| options | (required) An array of objects to display as options | n/a |
| returnValue | the value to return from the options object | `'value'` |
| selectionLabel | The value to show in teh options selection and when an item is selected | `'text'` |
| success | display success contextual state | `false` |
| warning | display warning contextual state | `false` |
| danger | display danger contextual state | `false` |
| successMessage | message to display when `success` is true | `''` |
| warningMessage | message to display when `warning` is true | `''` |
| dangerMessage | message to display when `danger` is true | `''` |

## Events
| Name | Description |
|:-----|:------------|
| @input | emits the selected values as an array of strings, based on the key in each option specified by `returnValue` |
