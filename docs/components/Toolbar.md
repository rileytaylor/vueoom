---
title: "Toolbar"
description: "Organize commands, inputs, and other form elements in a responsive manner"
---
The Toolbar component is an opinionated impelementation of the Windows UWP command bar. Primary functions will go on the left side, while secondary functions will go on the right, providing a seperation of concerns. Each will automatically collapse at specified breakpoints so that secondary functions will reduce in size sooner since they are less important or accessed less often.

Most apps will be fine using this component, though if you need something more custom you should roll your own. At it's core, this is meerly a wrapper using flexboxes with some helpful css styling to manage the spacing between elements.

*Note:* all command elements within a command bar should include icons to take advantage of the collapsing feature.

## Usage

The command bar takes up to two slots, `primary` and `secondary`.

```html
<toolbar :secondaryBreakpoint="800" :primaryBreakpoint="400">
    <div slot="primary">
        
    </div>
    <div slot="secondary">
        
    </div>
</toolbar>
```

The toolbar will automatically collapse `Command`, `CommandGroup`, and `CommandDropdown` elements (and any bootstrap-based `button`'s) to just their icons at the specified breakpoints. `TextBox` and `SelectBox` elements (and any bootstrap-based `form-group` or `input-group` components) will also function, though you will need to independantly manage their width if you choose to use anything but the default max-width that bootstrap applies. This can easily be done through conditional styling.

### Breakpoints

The toolbar will collapse secondary elements at a window width of 800px and the primary elements at a window width of 600px. This is probably terrible for your application, and 99% of use cases (unless you plan on using the `disableCollapse` property).

Once all your commands are in place, you'll need to test out different breakpoints to find what works for a specific toolbar. Generally you will want to make sure that the secondary section collapses _before_ the primary section does. it is also possible that you don't want to collapse the primary section. In that case, set it to `0`.

### Disabling breakpoints

Sometimes you might want to disable the breakpoints, be it programatically or because you need to do something really custom. Never fear, simply add the `disableCollapse` tag to the toolbar. Since this is a boolean value you can tie it to your javascript code or provide a conditional function that returns truthy or falsy.

### Element strategies

When adding elements to the toolbar, there are a few things to consider. As a general set of guidelines, always do the following:
- Give buttons an icon
- Don't use external labels (such as on form controls) and opt for placeholder text instead
- Form controls with addon elements with short text or icons are better than just placeholders
- If you have more than 4 primary elements and more than 6 secondary elements, consider consolidating functionality through a dropdown such as the `CommandDropdown`
- Elements used in a toolbar should be app-wide functionality. Opening modals, displaying/hiding sections of the page, managing settings, searching thorugh the main content, and other similar tasks are great for the toolbar. If the element performs an action inside a non-primary piece of the page (such as sorting a list that shows up in the bottom corner that is not the primary information source on the page), you should find a better place for the element.

## Properties
| Name | Description | Default Value |
|:-----|:------------|:-----|
| disableCollapse | disable all collapsing | n/a |
| primaryBreakpoint | the window width (in pixels) to collapse elements in the primary slot | `600` |
| secondaryBreakpoint | the window width (in pixels) to collapse elements in the secondary slot | `800` |

## Slots
| Name | Description |
| ---- | ----------- |
| primary | primary functions that render on the left |
| secondary | secondary functions that render on the right |
