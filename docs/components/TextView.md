---
title: "Text View"
description: "A multi-line text input"
---
The TextView control 

## Usage

All data fields are optional aside from id.

```html
<text-view id="george" label="Name:" v-model="name"></text-view>
```
#### Description
You can optionally add a discription to be rendered with the textview.

```html
<text-view id="george" label="Name:" v-model="name"
          description="Just put your name here, nice and simple."></text-view>
```

#### Placeholders
A textview can use a placeholder when it has no content.

```html
<text-view id="george" label="Name:" v-model="name"
          placeholder="Last, First Middle"></text-view>
```

#### Rendering inline
A textview with a description can be rendered inline and take up less horizontal space.

```html
<text-view id="george" label="Name:" v-model="name" inline></text-view>
```

#### Disabling
A textview can be disabled via the `disabled` flag.

```html
<text-view id="george" label="Name:" v-model="name" disabled></text-view>
```

#### Contextual states
Like other bootstrap form elements, you can add a visual contextual state to notify the user of success or failure of a form element's data, or even present a warning about the selection. Three states are available: `success`, `warning`, and `danger`.

```html
<text-view id="george" label="SSN#:" placeholder="XXX-XX-XXXX" v-model="ssn"
           description="Please provide so we can own you." 
           :success="box.success" :warning="box.warning" :danger="box.danger"
           successMessage="Congratz! We own you!"
           warningMessage="Seriously, just type it. You'll feel better."
           dangerMessage="That's not a SSN. Try again fool."></text-view>
```

The contextual states work nicely with all other properties. You can define multiple states for a checkbox and toggle each seperately.


#### Autocomplete
To disable browser autocomplete, set `autocomplete` to false.

```html
<text-view id="text" label="Text:" :autocomplete="false"></text-view>
```

#### Other Html properties

Any other valid html properties (such as `max`, `min`, `readonly`) can also be applied to this component and they will be handled properly. For example:

```html
<text-view id="attrBindingExample" label="It's Over:" value="9000" readonly/>
```

## Properties
| Name | Description | Default Value |
|:-----|:------------|:-----|
| id | (required) identifier | n/a |
| label | text label for the element | `undefined` |
| placeholder | watermark text inside the element shown before input is added | `undefined` |
| value | initial text to display in the element | n/a |
| description | text to display below | `undefined` |
| disabled | disable the element | `false` |
| inline | render the label inline | `false` |
| autocomplete | allow browswer autocompletion | `true` |
| success | display success contextual state | `false` |
| warning | display warning contextual state | `false` |
| danger | display danger contextual state | `false` |
| successMessage | message to display when `success` is true | `''` |
| warningMessage | message to display when `warning` is true | `''` |
| dangerMessage | message to display when `danger` is true | `''` |


## Events
| Name | Description |
|:-----|:------------|
| @input | raises when the textbox recieves input |
