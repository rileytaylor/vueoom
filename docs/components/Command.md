---
title: "Command"
description: "A configurable button control"
---

Commands are an implementation of html button inputs, allowing for easy customization with bootstrap and use of vue click events.

```html
<command label="Edit" icon="pencil" type="secondary"
         @action="openEdit" :disabled="!hasRowSelected">
</command>
```

## Usage

#### Types
There are several types available, corresponding to bootstrap's color variations.

```html
<command label="Primary" type="primary"></command>

<command label="UA Red" type="uared"></command>

<command label="Secondary" type="secondary"></command>

<command label="Info" type="info"></command>

<command label="Warning" type="warning"></command>

<command label="Danger" type="danger"></command>

<command label="Success" type="success"></command>

<command label="Link" type="link"></command>
```

The available types are: `primary`, `uared`, `secondary`, `info`, `warning`, `danger`, `success`, and `link`.

#### Outline
The `outline` flag removes background and puts color on the border and text.

```html
<command label="Primary" type="primary" outline></command>

<command label="UA Red" type="uared" outline></command>

<command label="Secondary" type="secondary" outline></command>

<command label="Info" type="info" outline></command>

<command label="Warning" type="warning" outline></command>

<command label="Danger" type="danger" outline></command>

<command label="Success" type="success" outline></command>

<command label="Link" type="link" outline></command>
```

#### Size
To change the command from it's default size to something larger or smaller, use the `size` property.

```html
<command label="Primary" type="primary" size="large"></command>

<command label="Primary" type="primary" size="small"></command>
```

Block will make the command a block element that takes up the full width of it's container.

```html
<div style="width: 100%;">
    <command label="Primary" type="primary" block></command>
</div>
```

#### Disabled
To render the command as disabled, use the disabled flag. Often times this is best used hooked up to a local property or data value to toggle it's state.

```html
<command label="Primary" type="primary" disabled></command>
```

### Active
If you need to manually render the command as active, set the `active` property.

```html
<command label="Primary" type="primary" active></command>
```

## Properties
| Name | Description | Default Value |
|:-----|:------------|:-----|
| active | set the ui active | `false` |
| block | Render the button as a block-level, full width element | `false` |
| disabled | toggle the disabled state | `false` |
| icon | name a font awesome icon to use on the left side of the button | n/a |
| label | the label text | `''` |
| outline | use the outline style | `false` |
| size | change the button size | `normal` |
| type | set the color | `secondary` |

## Events
| Name | Description |
| ---- | ----------- |
| @action | respond to `click` events |
