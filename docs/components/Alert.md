---
title: "Alert"
description: "Inline notifications for notices or as feedback to user input"
---


Alerts are an implementation of the bootstrap alert component, ideal for hiding/showing via vue properties within html content.

## Usage
Alerts are simple to use, doing a lot of the work for your based on a few properties. The `id` field is required so that the alert can identify itself somehow.

```html
<alert id="some-id" type="success"
       notice="Well Done!" message="You successfully read this important message.">
</alert>
```

#### Types
There are four alert types available through bootstrap: `success`, `warning`, `danger`, and `info`.

```html
<alert id="success" type="success"
       notice="Well Done!" 
       message="You successfully read this important message.">
</alert>
<alert id="warning" type="warning"
       notice="Hold up!" 
       message="You should double check this.">
</alert>
<alert id="danger" type="danger"
       notice="Uh oh!" 
       message="Everythings Bjorked, get out of here while you still can!">
</alert>
<alert id="info" type="info"
       notice="Hey, Listen." 
       message="Don't forget to break all of the jars in every house you walk into.">
</alert>
```

Each has an associated icon that can be shown or hidden via the `icon` property.

```html
<alert id="no-icon" type="info" :icon="false"
       notice="Icons?" message="We don't need no stinkin icons.">
</alert>
```

#### Dismissing
An alert can be made dismissible, which will display an X for the user to close it with.

```html
<alert id="dissmissable" type="danger" dismissible
       notice="I'm dismissable!" message="Bu bye.">
</alert>
```

#### Custom content
You can also define your own html content to use within the alert:

```html
<alert id="some-id" type="success">
    <template slot="content">
        <command type="secondary" label="look i'm a button"></command>
        <p>Some text</p>
    </template>
</alert>
```

#### Interacing with Javascript

If you need to work with the alert via javascript, you can use the `hide()`, `show()`, and `toggle()` methods, like so:

```html
<alert ref="alert" type="info" message="I'm toggled by javascript!"/>
<command label="show" @action="showAlert"/>
<command label="hide" @action="hideAlert"/>
<command label="toggle" @action="toggleAlert"/>
```

```js
showAlert () { this.$refs.alert.show(); }
hideAlert () { this.$refs.alert.hide(); }
toggleAlert () { this.$refs.alert.toggle(); }
```

#### Timed Alert

An alert can be scheduled to hide itself after a number of milliseconds. To do so, use the `timeout` property or the `setDismiss()` method. The timeout will begin when the alert is shown.

```html
<command label="show" @action="showAlert"/>
<command label="set dismiss via code" @action="viaCode"/>
<alert ref="alert" type="info" message="I clean up after myself!" :timeout="5000"/>
```

```js
viaCode () {
    const timerId = this.$refs.alert.setDismiss(5000);
    return timerId;
}
```

You can optionally catch the timer's id (the returned id from `window.setTimeout()`) and handle cancelling or destroying the timeout yourself, or you can call the `clearDismiss()` method of the `Alert` component.


## Properties
| Name | Description | Default Value |
|:-----|:------------|:-----|
| id (required) | an identifier, required. | n/a |
| type | the bootstrap style variant to use | `info` |
| notice | the title | `''` |
| message | the message to display | `''` |
| dismissible | the alert will have it's own X button to close itself | `false` |
| icon | toggle visibility of the icon | `true` |
| timeout | set a timer to hide the alert (ms) | `null` |

## Events
| Name | Description |
| ---- | ----------- |
| @hidden | fires when the alert `close` instance is called |
| @shown | fires when the alert has been closed |

## Slots
| Name | Description |
| ---- | ----------- |
| content | Override the default alert content |
