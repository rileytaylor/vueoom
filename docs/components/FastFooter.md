---
title: "Footer"
description: "A simple component for the footer of FSO applications"
flag: "primary"
flagText: "New"
---

## Usage

```html
<fso-footer app-name="Vueoom" />
```
### Application Name
As shown above, you are required to provide the application name with the `app-name` property so the 'Contact us!' link fills in the correct application name within the subject line.

### Background
The color is set by a css class from FSO Bootstrap (`bg-cool-gray` by default). If you would like to override this behavior, you can use the `background` property to give it either another `bg-*` class or define your own locally.

```html
<fso-footer app-name="Vueoom" background="bg-warm-gray" />
```

```html
<fso-footer app-name="Vueoom" background="custom-pink-backround" />
```

```css
.custom-pink-background {
    background-color: pink;
}
```

## Component Properties

### Properties
| Name | Description | Default Value |
|:-----|:------------|:-----|
| background | set the background css class of the footer | `'bg-cool-gray` |
| app-name | set the application name for email subject line | n/a |
