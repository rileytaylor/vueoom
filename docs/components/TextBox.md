---
title: "Text Box"
description: "A text input"
---
The TextBox control is, well, a text box.

## Usage

All data fields are optional aside from id.

```html
<text-box id="george" type="text" label="Name:" v-model="name"></text-box>
```

#### Types
Since the textbox wraps the standard html input control, you can also specify the same types to allow the browser to do it's job in rendering.

```html
<text-box id="text" type="text" label="Text:"></text-box>
<text-box id="password" type="password" label="Password:"></text-box>
<text-box id="datetime-local" type="datetime-local" label="Date and Time:"></text-box>
<text-box id="date" type="date" label="Date:"></text-box>
<text-box id="month" type="month" label="Month:"></text-box>
<text-box id="time" type="time" label="Time:"></text-box>
<text-box id="week" type="week" label="Week:"></text-box>
<text-box id="number" type="number" label="Number:"></text-box>
<text-box id="email" type="email" label="Email:"></text-box>
<text-box id="url" type="url" label="Url:"></text-box>
<text-box id="search" type="search" label="Search:"></text-box>
<text-box id="tel" type="tel" label="Phone:"></text-box>
<text-box id="color" type="color" label="Color:"></text-box>
```

#### Description
You can optionally add a discription to be rendered with the textbox.

```html
<text-box id="george" type="text" label="Name:" v-model="name"
          description="Just put your name here, nice and simple."></text-box>
```

#### Placeholders
A textbox can use a placeholder when it has no content.

```html
<text-box id="george" type="text" label="Name:" v-model="name"
          placeholder="Last, First Middle"></text-box>
```

#### Rendering inline
A textbox with a description can be rendered inline and take up less horizontal space.

```html
<text-box id="george" type="text" label="Name:" v-model="name" inline></text-box>
```

#### Disabling
A textbox can be disabled via the `disabled` property.

```html
<text-box id="george" type="text" label="Name:" v-model="name" disabled></text-box>
```

#### Size
To render the control as a larger or smaller element, use the `size` property. Available options include `small`, `normal`, and `large`.

```html
<text-box id="small" type="text" label="Small:" size="small"></text-box>

<text-box id="normal" type="text" label="Normal:"></text-box>

<text-box id="large" type="text" label="Large:" size="large"></text-box>
```

#### Contextual states
Like other bootstrap form elements, you can add a visual contextual state to notify the user of success or failure of a form element's data, or even present a warning about the selection. Three states are available: `success`, `warning`, and `danger`.

```html
<text-box id="george" type="text" label="SSN#:" placeholder="XXX-XX-XXXX" v-model="ssn"
          description="Please provide so we can own you." 
          :success="box.success" :warning="box.warning" :danger="box.danger"
          successMessage="Congratz! We own you!"
          warningMessage="Seriously, just type it. You'll feel better."
          dangerMessage="That's not a SSN. Try again fool."></text-box>
```

The contextual states work nicely with all other properties. You can define multiple states for a checkbox and toggle each seperately.

#### Addons
Textboxes support bootstrap's [Input Addons](https://fast.pages.fso.arizona.edu/fso-bootstrap/components/input-group/#basic-example) through the `addons` property and the `addon-start` and `addon-end` slots.

```html
<text-box type="search" placeholder="Search all..."
          v-model="searchString"
          addon="end">
    <div slot="addon-end">
        <icon icon="search"></icon>
    </div>
</text-box>
```

A common use of an addon is to provide an alternative label or an icon, or even to denote characters the user does not need to enter themselves. The addon slots support any html content, but it is advised to keep it to text and icons whenever possible.

```html
<text-box type="number" placeholder="0" addon="both">
    <div slot="addon-start">
        <p>$</p>
    </div>
    <div slot="addon-end">
        <p>.00</p>
    </div>
</text-box>

<text-box type="number" placeholder="username" addon="start">
    <div slot="addon-start">
        <p>@</p>
    </div>
</text-box>
```

#### Autocomplete
To disable browser autocomplete, set `autocomplete` to false.

```html
<text-box id="text" type="text" label="Text:" :autocomplete="false"></text-box>
```

#### Other Html properties

Any other valid html properties (such as `max`, `min`, `readonly`) can also be applied to this component and they will be handled properly. For example:

```html
<text-box id="attrBindingExample" type="number" label="It's Over:" value="9000" readonly/>
```

## Properties
| Name | Description | Default Value |
|:-----|:------------|:-----|
| id | (required) identifier | n/a |
| label | text label for the element | `undefined` |
| placeholder | watermark text inside the element shown before input is added | `undefined` |
| value | initial text to display in the element | n/a |
| description | text to display below | `undefined` |
| disabled | disable the element | `false` |
| inline | render the label inline | `false` |
| autocomplete | turn off browser autocomplete | `true` |
| size | make the control smaller or larger | `normal` |
| addon | render addons before, after, or before and after | `none` |
| success | display success contextual state | `false` |
| warning | display warning contextual state | `false` |
| danger | display danger contextual state | `false` |
| successMessage | message to display when `success` is true | `''` |
| warningMessage | message to display when `warning` is true | `''` |
| dangerMessage | message to display when `danger` is true | `''` |

## Events
| Name | Description |
|:-----|:------------|
| @input | raises when the textbox recieves input |

## Slots
| Name | Description |
|:-----|:------------|
| addon-start | content to display before the textbox |
| addon-end | content to display after the textbox |
