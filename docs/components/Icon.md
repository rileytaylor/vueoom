---
title: "Icon"
description: "Easily add a Font Awesome icon to your markup"
---

Easily render font awesome icons. Currently, this version of the control only support Font Awesome v4.x. For icon options, see [Font Awesome](http://fontawesome.io/icons/).

## Usage
To render an icon, use the name without the `fa-` tag.

```html
<icon icon="free-code-camp"></icon>
```

#### Size

Icons will automatically size to their containers since they are svg images.

If you need to do so, sizes can be increased relative to their container using the `size` property. Available sizes include: `large`, `2x`, `3x`, `4x`, and `5x`.

```html
<icon icon="rebel"></icon>
<icon icon="rebel" size="large"></icon>
<icon icon="rebel" size="2x"></icon>
<icon icon="rebel" size="3x"></icon>
<icon icon="rebel" size="4x"></icon>
<icon icon="rebel" size="5x"></icon>
```

#### Fixed Width Icons

The render the icon with the same horizontal margins as all other icons, use the `fixedWidth` property.

```html
<div class="list-group">
    <li class="list-group-item"><icon icon="free-code-camp" fixedWidth></icon>&nbsp; Free Code Camp</li>
    <li class="list-group-item"><icon icon="slack" fixedWidth></icon>&nbsp; Slack</li>
    <li class="list-group-item"><icon icon="rebel" fixedWidth></icon>&nbsp; Rebel Alliance</li>
    <li class="list-group-item"><icon icon="empire" fixedWidth></icon>&nbsp; Empire</li>
</div>
```

#### List Rendering

To render icons as bullet point replacements in a list, use the `isInList` property. Be sure to use this inside of a `<li>` tag, enclosed in a `<ul class="fa-ul">` tag.

```html
<ul class="fa-ul">
    <li><icon icon="free-code-camp" isInList></icon>Item 1</li>
    <li><icon icon="slack" isInList></icon>Item 2</li>
    <li><icon icon="rebel" isInList></icon>Item 3</li>
    <li><icon icon="empire" isInList></icon>Item 4</li>
</ul>
```

#### Bordered

An icon can have a border drawn around it with the `bordered` property.

```html
<icon icon="rebel" bordered></icon> 
```

#### Pull

To pull the icon to the left or right, use the `pull` property. An icon can be pulled to the left or right.

```html
<p max-width="600px"><icon icon="quote-left" pull="left" size="3x" bordered></icon>...tomorrow we will run faster, stretch out our arms farther... And then one fine morning&mdash; So we beat on, boats against the current, borne back ceaselessly into the past.</p>
```

#### Rotation

Icons can be rotated with the `rotate` property. Setting this to `90`, `180`, or `270` will rotate it x degrees clockwise.

```html
<icon icon="rebel"></icon>
<icon icon="rebel" rotate="90"></icon>
<icon icon="rebel" rotate="180"></icon>
<icon icon="rebel" rotate="270"></icon>
```

#### Flipped Icons

Icons can be flipped using the `flip` property. Set it to `horizontal` or `vertical` to flip it on it's axis.

```html
<icon icon="firefox"></icon>
<icon icon="firefox" flip="vertical"></icon>
<icon icon="firefox" flip="horizontal"></icon>
```

#### Animated Icons

To animate icons, use the `animate` property to either spin or pulse the icon.

```html
<icon icon="rebel" size="5x" animation="spin"></icon>
<icon icon="rebel" size="5x" animation="pulse"></icon>
<icon icon="spiner" size="5x" animation="spin"></icon>
<icon icon="spinner" size="5x" animation="pulse"></icon>
<icon icon="circle-o-notch" size="5x" animation="spin"></icon>
```

This is not the only way to apply animations to an icon... plenty of options exist with [Animate.css](https://daneden.github.io/animate.css/), [css animations](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Animations/Using_CSS_animations), and [Velocity.js](http://velocityjs.org/).

## Accessibility

Accessibility is extra important for icons since screen readers can't do anything with them. All `<icon>` components use the `aria-hidden` tag so that screen readers will skip them.

This is fine when you have an icon that exists attached to some sort of text, as you would in a button or in a list. However, if your icon exists independantly of any label text&mdash; such as if you have a settings icon in a corner, or a menu opening icon in a navbar&mdash; you should provide more information. There are two ways to do this:

* Label the element enclosing the icon.

```html
<a href="#" aria-label="Filthy rebel scum!">
    <icon :icon="rebel"></icon>
</a>
```

* Add an `sr-only` class to a `<span>` at the same level.

```html
<icon :icon="rebel"></icon>
<span class="sr-only">Filthy rebel scum!</span>
```

It is also a good idea to add a tooltip to icons without labels, which can be done using the `popover` component or the `v-tooltip` directive. (*Note:* currently in development)

## Properties
| Name | Description | Default Value |
|:-----|:------------|:-----|
| icon (required) | the icon to render | n/a |
| size | increase the size within its container | `undefined` |
| fixedWidth | render with the same width | `false` |
| isInList | use easily in a `<li>` | `false` |
| pull | use as a pulled icon in text | `undefined` |
| bordered | add a light border | `false` |
| animation | spin or pulse the icon clockwise | `undefined` |
| rotate | rotate the image on the x or y axis | `undefined` |
| flip | flip the image on the x or y axis | `undefined` |
