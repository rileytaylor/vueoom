---
title: "Date Range"
description: "A date range selection component with helpful quick actions."
fla": "warning"
flagText: "Beta"
---
The DateRangeInput component allows selecting a start and end date, simplifying user experience by providing one input instead of two.

```html
<date-range v-model="range"></date-range>
```

```js
export default {
    data () {
        return {
            range: {}
        }
    }
}
```

Once a range is selected, the Apply button will emit the `@input` event and update the v-model, which would look like this:
```js
export default {
    data () {
        return {
            range: {
                start: 'YYYY-MM-DD',
                end: 'YYYY-MM-DD'
            }
        }
    }
}
```

