---
title: "Stepper"
description: "A wizard component that allows breaking pages into steps."
---
The `Stepper` component is for wizards. In fact, this `Stepper` component is proudly endorsed by the Hogwarts School of Witchcraft and Wizardry.

While there is no restriction on content shown inside of each step of the wizard, the component is best used to provide information 1 section at a time, or for use in splitting up forms. Whenever using the `Stepper` component, be sure to think about whether the information needs to be broken up like so. Nothing is more annoying than the over-zealous installation wizard that could be solved with a sligthly longer single-page frame.

## Usage

The `Stepper` component is more or less a component that renders information about the steps (each step being the `StepperItem` component). The component itself is just a header/information display as well as a navigation source of truth. It will show the user where in the process of the wizard they are. This can be fairly detailed, or it could be a simple listing of the number of steps to complete. The `Stepper` component does not provide any navigation controls by default, so you will need to build that into the application either in the parent or (more preferably, in most cases) inside of each `StepperItem`.

The `Stepper` component _does not_ hold the content of each step. You'll need to hide/show that independantly, like so:

```html
<stepper v-model="currentStep" ref="stepper">
    <stepper-item title="Sign Up" icon="pencil"/>
    <stepper-item title="Confirm" icon="eye"/>
    <stepper-item title="Finished" icon="check"/>
</stepper>
<div v-if="currentStep === 1" :key="currentStep">
    <p>Sign Up form</p>
</div>
<div v-else-if="currentStep === 2" :key="currentStep">
    <p>Confirm your email...</p>
</div>
<div v-else :key="currentStep">
    <p>Cool, go ahead and log in now.</p>
</div>
```

You are responsible for any alignment and styling outside of the header's internal content.

#### Create steps via a property

_This is a work in progress and is not yet implemented._

#### Customizing the Steps

Most of the customization of the `Stepper` is done inside of each `StepperItem`. All Steppers can have a title and a description. They can also have a label _or_ an icon, but not both.

```html
<stepper-item title="Finished" description="You are all done." icon="mail-o"/>
```

Icons are pulled from font awesome just like the `Icon` component. Labels are great if you only care about alphanumerical steps (i.e. `1`, `2`, `3` or `A`, `B`, `C`, etc.)

The title and description fields can be useful for providing more details beyond the icon or label.

#### Progress

The `Stepper` component will update the UI as the user navigates through the steps. Completed steps will change to a blue color to indicate they are finished, as well as the label or icon being replaced with a checkmark icon.

Future versions of this component will allow for validation coloring as well as customization of the finished icon and coloring.

## Properties

#### `<stepper>`
| Name | Description | Default Value |
|:-----|:------------|:-----|
| current |  the current step of the wizard (bound to v-model) | `1` |
| steps | an array of objects to generate the steps of the wizard | `undefined` |

#### `<stepper-item>`
| Name | Description | Default Value |
|:-----|:------------|:-----|
| label | a number or string to use instead of an icon | `''` |
| icon | a font awesome icon to display instead of a label | `undefined` |
| title | the step title | `''` |
| description | a short text description to display below the title | `''` |

## Events

#### `<stepper>`
| Name | Description |
|:-----|:------------|
| @change | toggle movement backwards or forwards in the wizard |
