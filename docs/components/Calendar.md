---
title: "Calendar"
description: "A simple calendar widget. Used by our `datepicker` and `daterange` components."
flag: "warning"
flagText: "Beta"
---

The Calendar widget is simply a calendar, sized as for a mobile device. It is used by our `DateInput` and `DateRangeInput` components, but could be useful elsewhere as well. It supports single and range selection, jumping to months and years, and more.

```html
<p>{{ date }}</p>
<calendar v-model="date">
```

### Specifying the format
By default, the calendar control will use the `YYYY-MM-DD` format for accepting and returning datetimes. You can configure this in other ways via the `dateFormat` property.

### Disabling Dates
To disable dates, you can pass in a function at the `isDisabledDate` property.

### i18n
The `weekdays` and `months` props can be overriden for internationalization purposes and providing alternate names.
