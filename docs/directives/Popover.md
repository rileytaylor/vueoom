---
title: 'Popover'
name: "Popover"
description: "A directive that applies a tooltip-like control to any element."
flag: "primary"
flagText: "New"
---

When an entire popover component definition isn't necessary, use this directive to append a popover to an element.

```html
<command id="host"
         v-f-popover.top.hover="'Made from a directive, woot woot!'" title="Title"
         label="open" type="primary" />
```

## Usage

The directive can take in most (but not all) of the `Popover` component's options. Most of these options are passed in as modifiers, while `title` is passed in as an attribute on the host element, and the content is specified as the value of the directive.

```
v-f-popover.[mod].[...].[mod]="content" title="title"
```

Where `[mod]` is a modifier described below.

### Acceptable modifiers
Each property of the `Popover` component has a mod or mods that can be used:

- *animation*: to disable animation, pass in `noAnimation`.
- *delay*: to set the delay, pass in a number appended to `delay-`. i.e., for a 200 second delay, pass in `delay-200`. You can only set a single delay, not a seperate delay for start and end this way.
- *disableFlip*: to disable flipping, pass in `disableFlip`.
- *offset*: to set an offset, pass in a number appended to `offset-`. i.e., for a 10 px offset, pass in `offset-10`.
- *position*: to set up the position, pass in one of the following: `auto`, `left`, `left-start`, `left-end`, `top`, `top-start`, `top-end`, `right`, `right-start`, `right-end`, `bottom`, `bottom-start`, `bottom-end`. The default if none are specified is `auto`. If multiple are passed, the last one will become the value set.
- *trigger*: Triggers are passed in as individual modifiers, and each trigger will be accounted for. `click` is the default and does not need to be passed in unless you want it to be used with other types:
    - `click`: allow triggering via click
    - `hover`: allow triggering via hover
    - `focus`: allow triggering via focus

### Examples

A simple popover with only content, using all defaults:
```
v-f-popover="'This is a popover!'"
```

A popover with a ---
title:
```
v-f-popover="'This is a popover with a title!'" title="Title"
```

A popover with only a ---
title:
```
v-f-popover title="Title"
```

Setting various positions:
```
v-f-popover.top="'This is a popover!'"
v-f-popover.bottom="'This is a popover!'"
v-f-popover.bottom-start="'This is a popover!'"
v-f-popover.right="'This is a popover!'"
```

Setting various triggers:
```
v-f-popover.hover="'This is a popover!'"
v-f-popover.focus="'This is a popover!'"
v-f-popover.click.hover.focus="'This is a popover!'"
```

Kitchen sink:
```
v-f-popover.click.hover.focus.top.noAnimation.delay-200.offset-10="'Kitchen Sink!'"
```
