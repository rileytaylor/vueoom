---
title: "Mask"
description: "A directive to perform masking on text input controls"
flag: "primary"
flagText: "New"
---

`v-f-mask` is a directive that allows for "masking" of html `<input>` elements. It is easy to configure, aids user input, and uses the fantastic [`inputmask`](https://github.com/RobinHerbots/Inputmask) library from Robin Herbots. This directive wraps the most commonly used pieces of the library. If you need to do anything more complex you will need to import the library and use it manually on an input, which is briefly dicussed at the end of this documentation.

## Usage

```html
<text-box id="example" label="SSN#:" v-f-mask="'999-99-9999'">
```

That's it! Simply send a string (note, it needs to be a string!) to the directive and you'll see a mask render on the input element. 

Alternatively, you can pass in an object literal:

```html
<text-box id="example" label="SSN#:" v-f-mask="{ mask: '999-99-9999' }">
```

Using object literals is important if you would like to use some of the more complex options available that aren't covered by this directive or it's modifiers. Keep in mind that if you use any config options that are handled already by modifiers (see below), the modifier will _always_ override anything you set.

This works with any Vueoom component that wraps an input or textarea, or any of the following html elements:

- `<input type="text">`
- `<input type="search">`
- `<input type="tel">`
- `<input type="password">`
- `<textarea>`

Note that when using `v-f-mask` on a wrapper component like any Vueoom form element this supports, the directive will find the _first_ `<input>` or `<textarea>` it can locate inside the dom tree. 

#### Masking definitions

Use the following to build your masking template:

- `9` : numeric
- `a` : alphabetical
- `*` : alphanumeric
- `[]`: optional character wrapper
- `{}`: dynamic masking
- `|` : alternator
- `\\` : escape character

Keep in mind that the input will no longer allow any input longer than the defined mask, nor allow any other characters for each definition.

##### Optional character wrapping

To define an optional character, use `[]`.

```html
<text-box id="example" label="Produce Code:" v-f-mask="'9999[9]'">
```

In the above example, the user will only need to enter the first 4 digits, but can add a 5th (to get those organic produce codes of course!)

Combining that with the `notGreedy` modifier will mean that the mask shows the fewest digits possible.

##### Dynamic masking

Dynamic masking allows for more complex implementations that change during input. This is ideal for masking an email. To learn more about this, see [the following documentation](https://github.com/RobinHerbots/Inputmask/blob/4.x/README.md#dynamic-masks).

An email mask, for example, might look like this:

```html
<text-box id="email" label="Email:" v-f-mask.notGreedy="'*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]'">
```

This example is fairly limited on allowing chainable domains like `email.arizona.edu`, but it's an example.

##### Alternator marks

Alternator marks function like an OR statement, allowing up to 3 choices:

```html
<text-box id="alternator" label="Something:" v-f-mask="'a|9'">
```

##### Aliases

The `inputmask` library includes some aliases for common masks. These include `datetime`, 

For more information on using the `datetime` alias, see: https://github.com/RobinHerbots/Inputmask/blob/4.x/README_date.md


#### Modifiers

The built-in modifiers allow for easy toggling of features of the input mask library that change how the mask renders.

##### Just In Time masking

jit masking will only show the mask for user entered characters. To enable this, use the `jit` modifier:

```html
<text-box id="exampleJIT" label="SSN#:" v-f-mask.jit="'999-99-9999'">
```

##### Greedy

*Note:* this does not appear to be working properly, despite being configured properly...

To make the mask greedy, use the `greedy` modifier:

```html
<text-box id="example" label="Produce Code:" v-f-mask.greedy="'9999[9]'">
```

## More complex implementations

If you need to use the `inputmask` library to implement something more complex, use it like so:

```js

import InputMask from 'inputmask';

InputMask('999-999-9999').mask(this.$refs.somePhoneInput.$el);
```

This will add a phone number mask to an element, which we selected by ref. You can also select using any other normal html selector (i.e. `document.getElementsById()`, etc.). See the [documentation](https://github.com/RobinHerbots/Inputmask) for more information on usage and the more complex configurations that are possible.

A good place to configure a custom input mask would be in the `mounted()` method of your component.
