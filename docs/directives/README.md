# Directives

The Prime Directive states that the normal development of a web app should not be interfered with and that all directives are expendable to maintain this principle.
