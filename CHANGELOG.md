## 1.2.0

### Changes
- Brand new rollup config with better packages. Vueoom now includes builds for browser, ssr, and esm, as well as a minified browser package.
- No more seperate css file. css is now compiled into the components, which make for less easy override but a better development experience.
- Better implementation of the modal which avoids prop mutation in defaults

### Breaking Changes
- [#31](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/31) `Popover` is a brand new, completely different component. It's a billion times better!
- [#31](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/31) new `v-f-popover` directive as an alternate use for popovers
- [!107](https://gitlab.fso.arizona.edu/FAST/vueoom/merge_requests/107) Modal's `content` slot is now the default slot
- [!108](https://gitlab.fso.arizona.edu/FAST/vueoom/merge_requests/108) `Command`, `CommandDropdown` now use a `@click` event instead of `@action`. They don't actually implement any logic around the `@click` event either, so buttons can use whatever events they want natively.

## 1.1.6

### Fixes
Whoops... turns out we [needed some extra babel config](https://gitlab.fso.arizona.edu/FAST/vueoom/commit/c4e6444054464b925ed1166db7e20bccc1bd4356) to enable some of the features used in the previous release.

## 1.1.5

### Changes
- [#55](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/59) `DateRangeInput` control initial implementation

## 1.1.4

### Changes
- [#59](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/59) Fixed moment importing bug

## 1.1.3

### Changes
- [#51](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/51) `Redbar` component added
- [#53](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/53) `Navbar` component added
- [#54](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/54) `FsoFooter` component added
- [#56](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/56) broke out dropdown functionality into a mixin used by both the `CommandDropdown` and `NavbarItemDropdown`, with potential for use elsewhere

### Breaking Changes
- `DropdownItem`: Properties have changed significantly with the addition of `<router-link></router-link>` as an option
  - `button` has turned into `type` with the following options: `button`, `link`, and `router-link`
  - `href` is now `to` to be consistent with vue.js naming and vue-router

## 1.1.2

### Bug Fixes
- [#49](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/49) single select can now be de-selected

### Changes 
- [#45](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/45) improved deployment practices for the docs site and cdn versioning

## 1.1.1

### Changes
- [#41](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/41) the dropdown now closes when an item is selected or someone clicks outside of the dropdown
- [#48](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/48) minor style tweak to the `SelectSingle` and `SelectMultiple` styles so that they work nicely in flex layouts

## 1.1.0 - "Woah, this is heavy"

[![](https://media.giphy.com/media/3C64mkL45QjAc/giphy.gif)](https://media.giphy.com/media/3C64mkL45QjAc/giphy.gif)

### Changes
- [#40](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/40) More refined and usable selection components: `SelectMultiple` and `SelectSingle`

### Breaking Changes
- [#40](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/40) `MultiSelect` component removed and replaced by `SelectMultiple` and `SelectSingle`
- [#40](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/40) Removing `SelectBox` since it isn't currently used anywhere and `SelectSingle` is more preferable.
- [#42](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/42) Removed `AgGridPlugin`, any future plugins will be developed as their own project
- [#39](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/39) Removed `Select2`
- [#39](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/39) Removed all jquery from the project

## 1.0.1

### Changes
- [#37](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/37): Attributes that aren't properties can be bound to the actual html input elements rathe than the root of the component, making them much more useful as wrapper components.
- [#3](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/3): No more jQuery!!!!! (at least on non-deprecated components)
- [#11](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/11): New `v-f-mark` directive allows for enforcing formatting with good user input assistance via masking

Note that the `AgGridPlugin` will be no longer supported since `ag-grid-vue` is more suitable for new versions of ag-grid.

## 1.0.0 - "He's alive! The Docs alive... he's in the old west but he's alive!"

[![](http://img.youtube.com/vi/ndOpdTOHLPs/0.jpg)](http://www.youtube.com/watch?v=ndOpdTOHLPs "The Docs Alive - Click to play")

### Changes
- [#36](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/36): Fixes for the `Toolbar` component, which needed some bug fixing and to actually be exported as a part of the framework. Who would of thunk it?
- [#34](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/34): New Components! `MultiSelect`, `Stepper`, `SegmentedToggle`, `FormGroup`
- [!57](https://gitlab.fso.arizona.edu/FAST/vueoom/merge_requests/57): Fix a bug in the popover that was messing with the z-index ordering
- [#35](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/35): Lots of other fixes for modals, commands, and documentation

## 1.0.0 Beta 4

### Breaking Changes
- [#9](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/9): `CommandDropdown` migrated to `popper.js`, includes some breaking changes. The biggest being that dropdowns are now non-split by default.
- [#32](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/32): `Modal` component re-written. An id is no longer required and some of the options have changed, most notably the `wide` being removed and replaced with multiple `size` options.

## 1.0.0 Beta 3

### Changes
- [#27](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/27): Added `AgGridPlugin` with `AgGrid` component
- General documentation site improvements
- [#18](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/18): General fixes for input controls
- New `Toolbar` Component

### Breaking Changes
- [3b2328db](https://gitlab.fso.arizona.edu/FAST/vueoom/commit/3b2328db93d1fc9a4ff2a8f9b842b2d9f7051240): `Helpers` renamed to `Utils`. `AnimationHelper` renamed to `Animation`

## 1.0.0 Beta 2

### Changes
- Better releasing process via Rollup. We now export both `umd` and `es` modules.

### Breaking Changes
- [e6eba078](https://gitlab.fso.arizona.edu/FAST/vueoom/commit/e6eba07861e421c49bd89fbea59c4d1e52042932): Deprecated the `select2` component due to instability and intent to kill it in the future. In the meantime, manually import it
- [9262fdc2](https://gitlab.fso.arizona.edu/FAST/vueoom/commit/9262fdc244b8eb2699b7f5c19ad2c92cb7293193): You must import the `dist/vueoom.css` file independantly from the javascript. See the readme for more details.

## 1.0.0 Beta 1 - "When this baby hits 88 mph... you're gonna see some serious shit."

This release includes several bug fixes and improvements for components, as well as more complete documentation.

### Changes
- [#8](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/8): Components with shared code now inherit from mixins.
- [#8](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/8): Finished TextView component.
- [#16](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/16): New toast notification system with ToastGroup and `Vue.$toast`

### Breaking Changes
- [#8](https://gitlab.fso.arizona.edu/FAST/vueoom/issues/8): Command size is now done through the `size` property instead of `large` and `small`. Size takes either `large`, `normal`, or `small` as options, defaulting to `normal`.

This release also uses Rollup to package the component for consumption as an npm module, and includes several archetectural changes for the development process.

## 1.0.0 Alpha - "Hoverboards don't work on water- unless you've got power!"

This is the initial release that includes the components more or less as they existed within Renewit. There are some renames for clarification and to not conflict with html element names. 

There are still a lot of bugs and missing features that should be included to make this work well as a component framework.
