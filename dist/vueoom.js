/**
  * vueoom - 1.2.0
  * (c) 2019 FSO Technology Services, University of Arizona
  * created by Riley Taylor, maintained and improved by the FAST dev team
  * @license MIT
  */

(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('vue'), require('vue-multiselect')) :
    typeof define === 'function' && define.amd ? define(['exports', 'vue', 'vue-multiselect'], factory) :
    (global = global || self, factory(global.vueoom = {}, global.Vue, global.VueMultiselect));
}(this, function (exports, Vue, VueMultiselect) { 'use strict';

    Vue = Vue && Vue.hasOwnProperty('default') ? Vue['default'] : Vue;
    VueMultiselect = VueMultiselect && VueMultiselect.hasOwnProperty('default') ? VueMultiselect['default'] : VueMultiselect;

    //
    //
    //
    //
    var script = {
      name: "Icon",
      props: {
        icon: {
          type: String,
          required: true
        },
        size: {
          type: String,
          default: undefined,
          validator: function validator(value) {
            return ['large', '2x', '3x', '4x', '5x'].indexOf(value) > -1;
          }
        },
        fixedWidth: {
          type: Boolean,
          default: false
        },
        isInList: {
          type: Boolean,
          default: false
        },
        pull: {
          type: String,
          default: undefined,
          validator: function validator(value) {
            return ['left', 'right'].indexOf(value > -1);
          }
        },
        bordered: {
          type: Boolean,
          default: false
        },
        animation: {
          type: String,
          default: undefined,
          validator: function validator(value) {
            return ['pulse', 'spin'].indexOf(value > -1);
          }
        },
        rotate: {
          type: String,
          default: undefined,
          validator: function validator(value) {
            return ['90', '180', '270'].indexOf(value > -1);
          }
        },
        flip: {
          type: String,
          default: undefined,
          validator: function validator(value) {
            return ['vertical', 'horizontal'].indexOf(value > -1);
          }
        }
      },
      computed: {
        getClasses: function getClasses() {
          var result = ['fa', 'fa-' + this.icon];
          var size = this.getSize();
          if (size) result.push(size);
          if (this.fixedWidth !== false) result.push('fa-fw');
          if (this.isInList !== false) result.push('fa-li');
          if (this.bordered !== false) result.push('fa-border');
          if (this.pull) result.push('fa-pull-' + this.pull);
          if (this.animation) result.push('fa-' + this.animation);
          if (this.rotate) result.push('fa-rotate-' + this.rotate);
          if (this.flip) result.push('fa-flip-' + this.flip);
          return result;
        }
      },
      methods: {
        getSize: function getSize() {
          var result;

          if (this.size !== undefined) {
            this.size == 'large' ? result = 'fa-lg' : result = 'fa-' + this.size;
          }

          return result;
        }
      }
    };

    function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier
    /* server only */
    , shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
      if (typeof shadowMode !== 'boolean') {
        createInjectorSSR = createInjector;
        createInjector = shadowMode;
        shadowMode = false;
      } // Vue.extend constructor export interop.


      var options = typeof script === 'function' ? script.options : script; // render functions

      if (template && template.render) {
        options.render = template.render;
        options.staticRenderFns = template.staticRenderFns;
        options._compiled = true; // functional template

        if (isFunctionalTemplate) {
          options.functional = true;
        }
      } // scopedId


      if (scopeId) {
        options._scopeId = scopeId;
      }

      var hook;

      if (moduleIdentifier) {
        // server build
        hook = function hook(context) {
          // 2.3 injection
          context = context || // cached call
          this.$vnode && this.$vnode.ssrContext || // stateful
          this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext; // functional
          // 2.2 with runInNewContext: true

          if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
            context = __VUE_SSR_CONTEXT__;
          } // inject component styles


          if (style) {
            style.call(this, createInjectorSSR(context));
          } // register component module identifier for async chunk inference


          if (context && context._registeredComponents) {
            context._registeredComponents.add(moduleIdentifier);
          }
        }; // used by ssr in case component is cached and beforeCreate
        // never gets called


        options._ssrRegister = hook;
      } else if (style) {
        hook = shadowMode ? function () {
          style.call(this, createInjectorShadow(this.$root.$options.shadowRoot));
        } : function (context) {
          style.call(this, createInjector(context));
        };
      }

      if (hook) {
        if (options.functional) {
          // register for functional component in vue file
          var originalRender = options.render;

          options.render = function renderWithStyleInjection(h, context) {
            hook.call(context);
            return originalRender(h, context);
          };
        } else {
          // inject component registration as beforeCreate hook
          var existing = options.beforeCreate;
          options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
      }

      return script;
    }

    var normalizeComponent_1 = normalizeComponent;

    /* script */
    const __vue_script__ = script;

    /* template */
    var __vue_render__ = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('i',{class:_vm.getClasses,attrs:{"aria-hidden":"true"}})};
    var __vue_staticRenderFns__ = [];

      /* style */
      const __vue_inject_styles__ = undefined;
      /* scoped */
      const __vue_scope_id__ = undefined;
      /* module identifier */
      const __vue_module_identifier__ = undefined;
      /* functional template */
      const __vue_is_functional_template__ = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var Icon = normalizeComponent_1(
        { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
        __vue_inject_styles__,
        __vue_script__,
        __vue_scope_id__,
        __vue_is_functional_template__,
        __vue_module_identifier__,
        undefined,
        undefined
      );

    //
    var script$1 = {
      name: "Alert",
      components: {
        Icon: Icon
      },
      props: {
        type: {
          type: String,
          default: 'info',
          validator: function validator(value) {
            return ['success', 'warning', 'danger', 'info'].indexOf(value) > -1;
          }
        },
        notice: {
          type: String,
          default: ''
        },
        message: {
          type: String,
          default: ''
        },
        dismissible: {
          type: Boolean,
          default: false
        },
        timeout: {
          type: Number,
          default: null
        },
        icon: {
          type: Boolean,
          default: true
        }
      },
      data: function data() {
        return {
          isShown: true,
          dismiss: undefined
        };
      },
      computed: {
        iconForType: function iconForType() {
          if (this.type === 'success') {
            return 'check-circle';
          } else if (this.type === 'warning') {
            return 'exclamation-circle';
          } else if (this.type === 'danger') {
            return 'times-circle';
          } else {
            return 'info-circle';
          }
        }
      },
      updated: function updated() {
        this.timeout ? this.setDismiss() : null;
      },
      methods: {
        /**
         * Hide the alert
         */
        hide: function hide() {
          this.isShown = false;
          this.$emit('hidden');
        },

        /**
         * Show the alert
         */
        show: function show() {
          this.isShown = true;
          this.$emit('shown');
        },

        /**
         * Toggle visibility of the alert
         */
        toggle: function toggle() {
          this.isShown ? this.hide() : this.show();
        },

        /**
         * Set a timer to hide the alert after a specified number of milliseconds
         * @prop {Number} timeout ms after which to hide the alert, overriden by the `timeout` property
         * @returns {Object} the `setTimeout` function callback
         */
        setDismiss: function setDismiss(timeout) {
          var vm = this;
          this.dismiss = setTimeout(function () {
            vm.hide();
          }, vm.timeout || timeout);
          return this.dismiss;
        },

        /**
         * Clear a timer, preventing any timed close defined in `timeout` or via the `setDismiss()` method
         */
        clearDismiss: function clearDismiss() {
          clearTimeout(this.dismiss);
          this.dismiss = undefined;
        }
      }
    };

    /* script */
    const __vue_script__$1 = script$1;

    /* template */
    var __vue_render__$1 = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.isShown),expression:"isShown"}],staticClass:"alert",class:[ 'alert-' + _vm.type, { 'alert-dismissable': _vm.dismissible }],attrs:{"role":"alert"}},[(_vm.dismissible)?_c('button',{staticClass:"close ml-2",attrs:{"type":"button","aria-label":"Close"},on:{"click":_vm.hide}},[_c('span',{attrs:{"aria-hidden":"true"}},[_vm._v("×")]),_vm._v(" "),_c('span',{staticClass:"sr-only"},[_vm._v("Close")])]):_vm._e(),_vm._v(" "),(_vm.icon)?_c('icon',{staticClass:"mr-2",attrs:{"icon":_vm.iconForType}}):_vm._e(),_vm._v(" "),_vm._t("content",[_c('span',{staticClass:"font-weight-bold"},[_vm._v(_vm._s(_vm.notice)+" ")]),_c('span',{staticClass:"font-weight-normal"},[_vm._v(_vm._s(_vm.message))])])],2)};
    var __vue_staticRenderFns__$1 = [];

      /* style */
      const __vue_inject_styles__$1 = undefined;
      /* scoped */
      const __vue_scope_id__$1 = undefined;
      /* module identifier */
      const __vue_module_identifier__$1 = undefined;
      /* functional template */
      const __vue_is_functional_template__$1 = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var Alert = normalizeComponent_1(
        { render: __vue_render__$1, staticRenderFns: __vue_staticRenderFns__$1 },
        __vue_inject_styles__$1,
        __vue_script__$1,
        __vue_scope_id__$1,
        __vue_is_functional_template__$1,
        __vue_module_identifier__$1,
        undefined,
        undefined
      );

    var commonjsGlobal = typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

    function commonjsRequire () {
    	throw new Error('Dynamic requires are not currently supported by rollup-plugin-commonjs');
    }

    function createCommonjsModule(fn, module) {
    	return module = { exports: {} }, fn(module, module.exports), module.exports;
    }

    var runtime_1 = createCommonjsModule(function (module) {
    /**
     * Copyright (c) 2014-present, Facebook, Inc.
     *
     * This source code is licensed under the MIT license found in the
     * LICENSE file in the root directory of this source tree.
     */

    var runtime = (function (exports) {

      var Op = Object.prototype;
      var hasOwn = Op.hasOwnProperty;
      var undefined$1; // More compressible than void 0.
      var $Symbol = typeof Symbol === "function" ? Symbol : {};
      var iteratorSymbol = $Symbol.iterator || "@@iterator";
      var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
      var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

      function wrap(innerFn, outerFn, self, tryLocsList) {
        // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
        var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
        var generator = Object.create(protoGenerator.prototype);
        var context = new Context(tryLocsList || []);

        // The ._invoke method unifies the implementations of the .next,
        // .throw, and .return methods.
        generator._invoke = makeInvokeMethod(innerFn, self, context);

        return generator;
      }
      exports.wrap = wrap;

      // Try/catch helper to minimize deoptimizations. Returns a completion
      // record like context.tryEntries[i].completion. This interface could
      // have been (and was previously) designed to take a closure to be
      // invoked without arguments, but in all the cases we care about we
      // already have an existing method we want to call, so there's no need
      // to create a new function object. We can even get away with assuming
      // the method takes exactly one argument, since that happens to be true
      // in every case, so we don't have to touch the arguments object. The
      // only additional allocation required is the completion record, which
      // has a stable shape and so hopefully should be cheap to allocate.
      function tryCatch(fn, obj, arg) {
        try {
          return { type: "normal", arg: fn.call(obj, arg) };
        } catch (err) {
          return { type: "throw", arg: err };
        }
      }

      var GenStateSuspendedStart = "suspendedStart";
      var GenStateSuspendedYield = "suspendedYield";
      var GenStateExecuting = "executing";
      var GenStateCompleted = "completed";

      // Returning this object from the innerFn has the same effect as
      // breaking out of the dispatch switch statement.
      var ContinueSentinel = {};

      // Dummy constructor functions that we use as the .constructor and
      // .constructor.prototype properties for functions that return Generator
      // objects. For full spec compliance, you may wish to configure your
      // minifier not to mangle the names of these two functions.
      function Generator() {}
      function GeneratorFunction() {}
      function GeneratorFunctionPrototype() {}

      // This is a polyfill for %IteratorPrototype% for environments that
      // don't natively support it.
      var IteratorPrototype = {};
      IteratorPrototype[iteratorSymbol] = function () {
        return this;
      };

      var getProto = Object.getPrototypeOf;
      var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
      if (NativeIteratorPrototype &&
          NativeIteratorPrototype !== Op &&
          hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
        // This environment has a native %IteratorPrototype%; use it instead
        // of the polyfill.
        IteratorPrototype = NativeIteratorPrototype;
      }

      var Gp = GeneratorFunctionPrototype.prototype =
        Generator.prototype = Object.create(IteratorPrototype);
      GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
      GeneratorFunctionPrototype.constructor = GeneratorFunction;
      GeneratorFunctionPrototype[toStringTagSymbol] =
        GeneratorFunction.displayName = "GeneratorFunction";

      // Helper for defining the .next, .throw, and .return methods of the
      // Iterator interface in terms of a single ._invoke method.
      function defineIteratorMethods(prototype) {
        ["next", "throw", "return"].forEach(function(method) {
          prototype[method] = function(arg) {
            return this._invoke(method, arg);
          };
        });
      }

      exports.isGeneratorFunction = function(genFun) {
        var ctor = typeof genFun === "function" && genFun.constructor;
        return ctor
          ? ctor === GeneratorFunction ||
            // For the native GeneratorFunction constructor, the best we can
            // do is to check its .name property.
            (ctor.displayName || ctor.name) === "GeneratorFunction"
          : false;
      };

      exports.mark = function(genFun) {
        if (Object.setPrototypeOf) {
          Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
        } else {
          genFun.__proto__ = GeneratorFunctionPrototype;
          if (!(toStringTagSymbol in genFun)) {
            genFun[toStringTagSymbol] = "GeneratorFunction";
          }
        }
        genFun.prototype = Object.create(Gp);
        return genFun;
      };

      // Within the body of any async function, `await x` is transformed to
      // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
      // `hasOwn.call(value, "__await")` to determine if the yielded value is
      // meant to be awaited.
      exports.awrap = function(arg) {
        return { __await: arg };
      };

      function AsyncIterator(generator) {
        function invoke(method, arg, resolve, reject) {
          var record = tryCatch(generator[method], generator, arg);
          if (record.type === "throw") {
            reject(record.arg);
          } else {
            var result = record.arg;
            var value = result.value;
            if (value &&
                typeof value === "object" &&
                hasOwn.call(value, "__await")) {
              return Promise.resolve(value.__await).then(function(value) {
                invoke("next", value, resolve, reject);
              }, function(err) {
                invoke("throw", err, resolve, reject);
              });
            }

            return Promise.resolve(value).then(function(unwrapped) {
              // When a yielded Promise is resolved, its final value becomes
              // the .value of the Promise<{value,done}> result for the
              // current iteration.
              result.value = unwrapped;
              resolve(result);
            }, function(error) {
              // If a rejected Promise was yielded, throw the rejection back
              // into the async generator function so it can be handled there.
              return invoke("throw", error, resolve, reject);
            });
          }
        }

        var previousPromise;

        function enqueue(method, arg) {
          function callInvokeWithMethodAndArg() {
            return new Promise(function(resolve, reject) {
              invoke(method, arg, resolve, reject);
            });
          }

          return previousPromise =
            // If enqueue has been called before, then we want to wait until
            // all previous Promises have been resolved before calling invoke,
            // so that results are always delivered in the correct order. If
            // enqueue has not been called before, then it is important to
            // call invoke immediately, without waiting on a callback to fire,
            // so that the async generator function has the opportunity to do
            // any necessary setup in a predictable way. This predictability
            // is why the Promise constructor synchronously invokes its
            // executor callback, and why async functions synchronously
            // execute code before the first await. Since we implement simple
            // async functions in terms of async generators, it is especially
            // important to get this right, even though it requires care.
            previousPromise ? previousPromise.then(
              callInvokeWithMethodAndArg,
              // Avoid propagating failures to Promises returned by later
              // invocations of the iterator.
              callInvokeWithMethodAndArg
            ) : callInvokeWithMethodAndArg();
        }

        // Define the unified helper method that is used to implement .next,
        // .throw, and .return (see defineIteratorMethods).
        this._invoke = enqueue;
      }

      defineIteratorMethods(AsyncIterator.prototype);
      AsyncIterator.prototype[asyncIteratorSymbol] = function () {
        return this;
      };
      exports.AsyncIterator = AsyncIterator;

      // Note that simple async functions are implemented on top of
      // AsyncIterator objects; they just return a Promise for the value of
      // the final result produced by the iterator.
      exports.async = function(innerFn, outerFn, self, tryLocsList) {
        var iter = new AsyncIterator(
          wrap(innerFn, outerFn, self, tryLocsList)
        );

        return exports.isGeneratorFunction(outerFn)
          ? iter // If outerFn is a generator, return the full iterator.
          : iter.next().then(function(result) {
              return result.done ? result.value : iter.next();
            });
      };

      function makeInvokeMethod(innerFn, self, context) {
        var state = GenStateSuspendedStart;

        return function invoke(method, arg) {
          if (state === GenStateExecuting) {
            throw new Error("Generator is already running");
          }

          if (state === GenStateCompleted) {
            if (method === "throw") {
              throw arg;
            }

            // Be forgiving, per 25.3.3.3.3 of the spec:
            // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
            return doneResult();
          }

          context.method = method;
          context.arg = arg;

          while (true) {
            var delegate = context.delegate;
            if (delegate) {
              var delegateResult = maybeInvokeDelegate(delegate, context);
              if (delegateResult) {
                if (delegateResult === ContinueSentinel) continue;
                return delegateResult;
              }
            }

            if (context.method === "next") {
              // Setting context._sent for legacy support of Babel's
              // function.sent implementation.
              context.sent = context._sent = context.arg;

            } else if (context.method === "throw") {
              if (state === GenStateSuspendedStart) {
                state = GenStateCompleted;
                throw context.arg;
              }

              context.dispatchException(context.arg);

            } else if (context.method === "return") {
              context.abrupt("return", context.arg);
            }

            state = GenStateExecuting;

            var record = tryCatch(innerFn, self, context);
            if (record.type === "normal") {
              // If an exception is thrown from innerFn, we leave state ===
              // GenStateExecuting and loop back for another invocation.
              state = context.done
                ? GenStateCompleted
                : GenStateSuspendedYield;

              if (record.arg === ContinueSentinel) {
                continue;
              }

              return {
                value: record.arg,
                done: context.done
              };

            } else if (record.type === "throw") {
              state = GenStateCompleted;
              // Dispatch the exception by looping back around to the
              // context.dispatchException(context.arg) call above.
              context.method = "throw";
              context.arg = record.arg;
            }
          }
        };
      }

      // Call delegate.iterator[context.method](context.arg) and handle the
      // result, either by returning a { value, done } result from the
      // delegate iterator, or by modifying context.method and context.arg,
      // setting context.delegate to null, and returning the ContinueSentinel.
      function maybeInvokeDelegate(delegate, context) {
        var method = delegate.iterator[context.method];
        if (method === undefined$1) {
          // A .throw or .return when the delegate iterator has no .throw
          // method always terminates the yield* loop.
          context.delegate = null;

          if (context.method === "throw") {
            // Note: ["return"] must be used for ES3 parsing compatibility.
            if (delegate.iterator["return"]) {
              // If the delegate iterator has a return method, give it a
              // chance to clean up.
              context.method = "return";
              context.arg = undefined$1;
              maybeInvokeDelegate(delegate, context);

              if (context.method === "throw") {
                // If maybeInvokeDelegate(context) changed context.method from
                // "return" to "throw", let that override the TypeError below.
                return ContinueSentinel;
              }
            }

            context.method = "throw";
            context.arg = new TypeError(
              "The iterator does not provide a 'throw' method");
          }

          return ContinueSentinel;
        }

        var record = tryCatch(method, delegate.iterator, context.arg);

        if (record.type === "throw") {
          context.method = "throw";
          context.arg = record.arg;
          context.delegate = null;
          return ContinueSentinel;
        }

        var info = record.arg;

        if (! info) {
          context.method = "throw";
          context.arg = new TypeError("iterator result is not an object");
          context.delegate = null;
          return ContinueSentinel;
        }

        if (info.done) {
          // Assign the result of the finished delegate to the temporary
          // variable specified by delegate.resultName (see delegateYield).
          context[delegate.resultName] = info.value;

          // Resume execution at the desired location (see delegateYield).
          context.next = delegate.nextLoc;

          // If context.method was "throw" but the delegate handled the
          // exception, let the outer generator proceed normally. If
          // context.method was "next", forget context.arg since it has been
          // "consumed" by the delegate iterator. If context.method was
          // "return", allow the original .return call to continue in the
          // outer generator.
          if (context.method !== "return") {
            context.method = "next";
            context.arg = undefined$1;
          }

        } else {
          // Re-yield the result returned by the delegate method.
          return info;
        }

        // The delegate iterator is finished, so forget it and continue with
        // the outer generator.
        context.delegate = null;
        return ContinueSentinel;
      }

      // Define Generator.prototype.{next,throw,return} in terms of the
      // unified ._invoke helper method.
      defineIteratorMethods(Gp);

      Gp[toStringTagSymbol] = "Generator";

      // A Generator should always return itself as the iterator object when the
      // @@iterator function is called on it. Some browsers' implementations of the
      // iterator prototype chain incorrectly implement this, causing the Generator
      // object to not be returned from this call. This ensures that doesn't happen.
      // See https://github.com/facebook/regenerator/issues/274 for more details.
      Gp[iteratorSymbol] = function() {
        return this;
      };

      Gp.toString = function() {
        return "[object Generator]";
      };

      function pushTryEntry(locs) {
        var entry = { tryLoc: locs[0] };

        if (1 in locs) {
          entry.catchLoc = locs[1];
        }

        if (2 in locs) {
          entry.finallyLoc = locs[2];
          entry.afterLoc = locs[3];
        }

        this.tryEntries.push(entry);
      }

      function resetTryEntry(entry) {
        var record = entry.completion || {};
        record.type = "normal";
        delete record.arg;
        entry.completion = record;
      }

      function Context(tryLocsList) {
        // The root entry object (effectively a try statement without a catch
        // or a finally block) gives us a place to store values thrown from
        // locations where there is no enclosing try statement.
        this.tryEntries = [{ tryLoc: "root" }];
        tryLocsList.forEach(pushTryEntry, this);
        this.reset(true);
      }

      exports.keys = function(object) {
        var keys = [];
        for (var key in object) {
          keys.push(key);
        }
        keys.reverse();

        // Rather than returning an object with a next method, we keep
        // things simple and return the next function itself.
        return function next() {
          while (keys.length) {
            var key = keys.pop();
            if (key in object) {
              next.value = key;
              next.done = false;
              return next;
            }
          }

          // To avoid creating an additional object, we just hang the .value
          // and .done properties off the next function object itself. This
          // also ensures that the minifier will not anonymize the function.
          next.done = true;
          return next;
        };
      };

      function values(iterable) {
        if (iterable) {
          var iteratorMethod = iterable[iteratorSymbol];
          if (iteratorMethod) {
            return iteratorMethod.call(iterable);
          }

          if (typeof iterable.next === "function") {
            return iterable;
          }

          if (!isNaN(iterable.length)) {
            var i = -1, next = function next() {
              while (++i < iterable.length) {
                if (hasOwn.call(iterable, i)) {
                  next.value = iterable[i];
                  next.done = false;
                  return next;
                }
              }

              next.value = undefined$1;
              next.done = true;

              return next;
            };

            return next.next = next;
          }
        }

        // Return an iterator with no values.
        return { next: doneResult };
      }
      exports.values = values;

      function doneResult() {
        return { value: undefined$1, done: true };
      }

      Context.prototype = {
        constructor: Context,

        reset: function(skipTempReset) {
          this.prev = 0;
          this.next = 0;
          // Resetting context._sent for legacy support of Babel's
          // function.sent implementation.
          this.sent = this._sent = undefined$1;
          this.done = false;
          this.delegate = null;

          this.method = "next";
          this.arg = undefined$1;

          this.tryEntries.forEach(resetTryEntry);

          if (!skipTempReset) {
            for (var name in this) {
              // Not sure about the optimal order of these conditions:
              if (name.charAt(0) === "t" &&
                  hasOwn.call(this, name) &&
                  !isNaN(+name.slice(1))) {
                this[name] = undefined$1;
              }
            }
          }
        },

        stop: function() {
          this.done = true;

          var rootEntry = this.tryEntries[0];
          var rootRecord = rootEntry.completion;
          if (rootRecord.type === "throw") {
            throw rootRecord.arg;
          }

          return this.rval;
        },

        dispatchException: function(exception) {
          if (this.done) {
            throw exception;
          }

          var context = this;
          function handle(loc, caught) {
            record.type = "throw";
            record.arg = exception;
            context.next = loc;

            if (caught) {
              // If the dispatched exception was caught by a catch block,
              // then let that catch block handle the exception normally.
              context.method = "next";
              context.arg = undefined$1;
            }

            return !! caught;
          }

          for (var i = this.tryEntries.length - 1; i >= 0; --i) {
            var entry = this.tryEntries[i];
            var record = entry.completion;

            if (entry.tryLoc === "root") {
              // Exception thrown outside of any try block that could handle
              // it, so set the completion value of the entire function to
              // throw the exception.
              return handle("end");
            }

            if (entry.tryLoc <= this.prev) {
              var hasCatch = hasOwn.call(entry, "catchLoc");
              var hasFinally = hasOwn.call(entry, "finallyLoc");

              if (hasCatch && hasFinally) {
                if (this.prev < entry.catchLoc) {
                  return handle(entry.catchLoc, true);
                } else if (this.prev < entry.finallyLoc) {
                  return handle(entry.finallyLoc);
                }

              } else if (hasCatch) {
                if (this.prev < entry.catchLoc) {
                  return handle(entry.catchLoc, true);
                }

              } else if (hasFinally) {
                if (this.prev < entry.finallyLoc) {
                  return handle(entry.finallyLoc);
                }

              } else {
                throw new Error("try statement without catch or finally");
              }
            }
          }
        },

        abrupt: function(type, arg) {
          for (var i = this.tryEntries.length - 1; i >= 0; --i) {
            var entry = this.tryEntries[i];
            if (entry.tryLoc <= this.prev &&
                hasOwn.call(entry, "finallyLoc") &&
                this.prev < entry.finallyLoc) {
              var finallyEntry = entry;
              break;
            }
          }

          if (finallyEntry &&
              (type === "break" ||
               type === "continue") &&
              finallyEntry.tryLoc <= arg &&
              arg <= finallyEntry.finallyLoc) {
            // Ignore the finally entry if control is not jumping to a
            // location outside the try/catch block.
            finallyEntry = null;
          }

          var record = finallyEntry ? finallyEntry.completion : {};
          record.type = type;
          record.arg = arg;

          if (finallyEntry) {
            this.method = "next";
            this.next = finallyEntry.finallyLoc;
            return ContinueSentinel;
          }

          return this.complete(record);
        },

        complete: function(record, afterLoc) {
          if (record.type === "throw") {
            throw record.arg;
          }

          if (record.type === "break" ||
              record.type === "continue") {
            this.next = record.arg;
          } else if (record.type === "return") {
            this.rval = this.arg = record.arg;
            this.method = "return";
            this.next = "end";
          } else if (record.type === "normal" && afterLoc) {
            this.next = afterLoc;
          }

          return ContinueSentinel;
        },

        finish: function(finallyLoc) {
          for (var i = this.tryEntries.length - 1; i >= 0; --i) {
            var entry = this.tryEntries[i];
            if (entry.finallyLoc === finallyLoc) {
              this.complete(entry.completion, entry.afterLoc);
              resetTryEntry(entry);
              return ContinueSentinel;
            }
          }
        },

        "catch": function(tryLoc) {
          for (var i = this.tryEntries.length - 1; i >= 0; --i) {
            var entry = this.tryEntries[i];
            if (entry.tryLoc === tryLoc) {
              var record = entry.completion;
              if (record.type === "throw") {
                var thrown = record.arg;
                resetTryEntry(entry);
              }
              return thrown;
            }
          }

          // The context.catch method must only be called with a location
          // argument that corresponds to a known catch block.
          throw new Error("illegal catch attempt");
        },

        delegateYield: function(iterable, resultName, nextLoc) {
          this.delegate = {
            iterator: values(iterable),
            resultName: resultName,
            nextLoc: nextLoc
          };

          if (this.method === "next") {
            // Deliberately forget the last sent value so that we don't
            // accidentally pass it on to the delegate.
            this.arg = undefined$1;
          }

          return ContinueSentinel;
        }
      };

      // Regardless of whether this script is executing as a CommonJS module
      // or not, return the runtime object so that we can declare the variable
      // regeneratorRuntime in the outer scope, which allows this module to be
      // injected easily by `bin/regenerator --include-runtime script.js`.
      return exports;

    }(
      // If this script is executing as a CommonJS module, use module.exports
      // as the regeneratorRuntime namespace. Otherwise create a new empty
      // object. Either way, the resulting object will be used to initialize
      // the regeneratorRuntime variable at the top of this file.
      module.exports
    ));

    try {
      regeneratorRuntime = runtime;
    } catch (accidentalStrictMode) {
      // This module should not be running in strict mode, so the above
      // assignment should always work unless something is misconfigured. Just
      // in case runtime.js accidentally runs in strict mode, we can escape
      // strict mode using a global Function call. This could conceivably fail
      // if a Content Security Policy forbids using Function, but in that case
      // the proper solution is to fix the accidental strict mode problem. If
      // you've misconfigured your bundler to force strict mode and applied a
      // CSP to forbid Function, and you're not willing to fix either of those
      // problems, please detail your unique predicament in a GitHub issue.
      Function("r", "regeneratorRuntime = r")(runtime);
    }
    });

    var runtime = /*#__PURE__*/Object.freeze({
        default: runtime_1,
        __moduleExports: runtime_1
    });

    var require$$0 = ( runtime && runtime_1 ) || runtime;

    var regenerator = require$$0;

    function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
      try {
        var info = gen[key](arg);
        var value = info.value;
      } catch (error) {
        reject(error);
        return;
      }

      if (info.done) {
        resolve(value);
      } else {
        Promise.resolve(value).then(_next, _throw);
      }
    }

    function _asyncToGenerator(fn) {
      return function () {
        var self = this,
            args = arguments;
        return new Promise(function (resolve, reject) {
          var gen = fn.apply(self, args);

          function _next(value) {
            asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
          }

          function _throw(err) {
            asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
          }

          _next(undefined);
        });
      };
    }

    var asyncToGenerator = _asyncToGenerator;

    var _typeof_1 = createCommonjsModule(function (module) {
    function _typeof2(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof2 = function _typeof2(obj) { return typeof obj; }; } else { _typeof2 = function _typeof2(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof2(obj); }

    function _typeof(obj) {
      if (typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol") {
        module.exports = _typeof = function _typeof(obj) {
          return _typeof2(obj);
        };
      } else {
        module.exports = _typeof = function _typeof(obj) {
          return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof2(obj);
        };
      }

      return _typeof(obj);
    }

    module.exports = _typeof;
    });

    function _classCallCheck(instance, Constructor) {
      if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
      }
    }

    var classCallCheck = _classCallCheck;

    function _defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    function _createClass(Constructor, protoProps, staticProps) {
      if (protoProps) _defineProperties(Constructor.prototype, protoProps);
      if (staticProps) _defineProperties(Constructor, staticProps);
      return Constructor;
    }

    var createClass = _createClass;

    /**
     * CalendarUtility includes functions for formatting strings as datetimes and vice-versa, as
     * well as some other various utility functions for comparing dates, etc. This is primarily
     * built to be useful for the Calendar, DateInput, and DateRangeInput components.
     * 
     * Usage:
     *     import CalendarUtility from '.../CalendarUtility';
     *     let calendarUtility = new CalendarUtility()
     *     
     * @class
     */
    var CalendarUtility =
    /*#__PURE__*/
    function () {
      // TODO: Allow custom definitions via override, and make 'format' parameters in the included functions just for overriding the default here.
      function CalendarUtility() {
        classCallCheck(this, CalendarUtility);

        this._stringFormat = 'YYYY-MM-DD';
        this._formatRE = /,|\.|-| |\/|\\/;
        this._dayRE = /D+/;
        this._monthRE = /M+/;
        this._yearRE = /Y+/;
      }
      /**
       * Parse a date string based on a formatter
       * @param {String} val the return value of the component
       * @param {String} format a format to follow for parsing (i.e. `YYYY-MM-DD`)
       * @returns {Date}
       */


      createClass(CalendarUtility, [{
        key: "parseDateFromString",
        value: function parseDateFromString(val, format) {
          var day, month, year;
          var dateParts = val.split(this._formatRE);
          var formatParts = format.split(this._formatRE);
          var partsSize = formatParts.length;

          for (var i = 0; i < partsSize; i++) {
            if (formatParts[i].match(this._dayRE)) {
              day = parseInt(dateParts[i], 10);
            } else if (formatParts[i].match(this._monthRE)) {
              month = parseInt(dateParts[i], 10);
            } else if (formatParts[i].match(this._yearRE)) {
              year = parseInt(dateParts[i], 10);
            }
          }

          var date = new Date(year, month - 1, day);
          return date;
        }
        /**
         * Convert a date back to the `dateFormat` property's format for emitting
         * @param {Date} date the datetime to format
         * @returns {String} the formatted datetime
         */

      }, {
        key: "formatDateToString",
        value: function formatDateToString(date) {
          var _this = this;

          if (!date) {
            return '';
          } else {
            // Use date to build the format string, padding numbers based on the format.
            return this._stringFormat.replace(this._yearRE, function () {
              return date.getFullYear();
            }).replace(this._monthRE, function (match) {
              return _this.padNumber(date.getMonth() + 1, match.length);
            }).replace(this._dayRE, function (match) {
              return _this.padNumber(date.getDate(), match.length);
            });
          }
        }
        /**
         * Determine if two dates are the same
         * @param {Date} date1
         * @param {Date} date2
         * @returns {Boolean} if dates are the same or not
         */

      }, {
        key: "isSameDate",
        value: function isSameDate(date1, date2) {
          return date1.getDate() === date2.getDate() && date1.getMonth() === date2.getMonth() && date1.getFullYear() === date2.getFullYear();
        }
        /**
         * Pad numbers to the length we want based on the `dateFormat`
         * @param {Number} num the number
         * @param {Number} pad the lengt to pad to
         * @returns {Number} a padded number
         */

      }, {
        key: "padNumber",
        value: function padNumber(num, pad) {
          if (_typeof_1(num) !== undefined) {
            return num.toString().length > pad ? num : new Array(pad - num.toString().length + 1).join('0') + num;
          } else {
            return undefined;
          }
        }
      }]);

      return CalendarUtility;
    }();

    var calendarUtility = new CalendarUtility();
    var script$2 = {
      name: "Calendar",
      components: {
        Icon: Icon
      },
      props: {
        /**
         * The bound value
         * @type {String}
         * @default ''
         */
        value: {
          type: String,
          default: ''
        },

        /**
         * A function to determine disabled dates
         * @type {Function}
         * @default []
         */
        isDisabledDate: {
          type: Function,
          default: function _default() {
            return false;
          }
        },
        weekdays: {
          type: Array,
          default: function _default() {
            return ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
          }
        },
        months: {
          type: Array,
          default: function _default() {
            return ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
          }
        },
        dateFormat: {
          type: String,
          default: 'YYYY-MM-DD'
        },

        /**
         * Provide a start and end date. All dates within this range will be highlighted.
         * Primarily for use by the DateRangeInput component.
         */
        highlight: {
          type: Object,
          default: function _default() {
            return {};
          }
        }
      },
      data: function data() {
        return {
          showMonthSelect: false,
          showYearSelect: false,
          transitionDirection: undefined,
          currentPeriod: this.getCurrentPeriod(this.value)
        };
      },
      computed: {
        isMonthSelect: function isMonthSelect() {
          return this.showMonthSelect;
        },
        isYearSelect: function isYearSelect() {
          return this.showYearSelect;
        },

        /**
         * A datetime formatted value
         * @returns {Date} a date object
         */
        selectedValue: function selectedValue() {
          // Format if it isn't a blank string
          return this.value ? calendarUtility.parseDateFromString(this.value, this.dateFormat) : undefined;
        },

        /**
         * Generate the UI for the calendar based on the currently selected month and year
         * @returns {Array} array with arrays of 7 days
         */
        currentMonth: function currentMonth() {
          var _this = this;

          /**
           * Final `day` object looks like:
           * {
           *     date: Date,
           *     key: '',
           *     selected: Boolean,
           *     today: Boolean,
           *     disabled: Boolean,
           *     outOfRange: true / undefined
           * }
           */
          var _this$currentPeriod = this.currentPeriod,
              year = _this$currentPeriod.year,
              month = _this$currentPeriod.month;
          var days = [];
          var today = new Date();
          var date = new Date(year, month, 1); // Add the previous month dates to the start of the first week

          var startDay = date.getDay() || 7;

          if (startDay > 1) {
            // Backwards loop!
            for (var i = startDay - 1; i >= 0; i--) {
              var prevDate = new Date(date);
              prevDate.setDate(-i); // Append a date previous, noting that it should be grayed out

              days.push({
                date: prevDate,
                outOfRange: true
              });
            }
          } // Add current days in the month until we have filled the month


          while (date.getMonth() === month) {
            days.push({
              date: new Date(date)
            });
            date.setDate(date.getDate() + 1);
          } // Add the next month dates to the end of the last week


          var daysLeft = 7 - days.length % 7;

          for (var _i = 1; _i <= daysLeft; _i++) {
            var nextDate = new Date(date);
            nextDate.setDate(_i); // Append the next date, noting that it should be grayed out

            days.push({
              date: nextDate,
              outOfRange: true
            });
          } // Fill out the rest of the day objects


          days.forEach(function (day) {
            day.disabled = _this.isDisabledDate(day.date);
            day.today = calendarUtility.isSameDate(day.date, today); // Is selected if the date is the same as the selectedValue (i.e. value prop as datetime)

            day.selected = _this.selectedValue ? calendarUtility.isSameDate(day.date, _this.selectedValue) : false;
            day.key = [day.date.getFullYear(), day.date.getMonth() + 1, day.date.getDate()].join('-');
          }); // Return days as a chunked array of weeks

          return this.groupDaysByWeek(days);
        },

        /**
         * Returns a transition class to animate objects when moving left/right due to month changes
         * @returns {String} a transition css class identifier
         */
        transitionClass: function transitionClass() {
          return this.transitionDirection ? "slideTo".concat(this.transitionDirection) : undefined;
        },

        /**
         * Determines if the calendar is being asked to highlight a range or not
         */
        isRange: function isRange() {
          return this.highlight !== {} ? true : false;
        },

        /**
         * Return the highlighting range as Date objects
         */
        highlightRange: function highlightRange() {
          var start = this.highlight.start ? calendarUtility.parseDateFromString(this.highlight.start, this.dateFormat) : undefined;
          var end = this.highlight.end ? calendarUtility.parseDateFromString(this.highlight.end, this.dateFormat) : undefined;
          return {
            'start': start,
            'end': end
          };
        }
      },
      watch: {
        // Watch the `value` and make sure the `currentPeriod` changes to reflect it
        value: function value(_value) {
          var isValid = _value ? Boolean(_value) : true;

          if (isValid) {
            // Make sure calendar is showing the correct month
            this.currentPeriod = this.getCurrentPeriod(_value);
          }
        },
        // watch the `currentPeriod` and remove the applied transition class after the animation has occurred
        currentPeriod: function () {
          var _currentPeriod = asyncToGenerator(
          /*#__PURE__*/
          regenerator.mark(function _callee() {
            return regenerator.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    if (!this.transitionDirection) {
                      _context.next = 4;
                      break;
                    }

                    _context.next = 3;
                    return new Promise(function (resolve) {
                      return setTimeout(resolve, 500);
                    });

                  case 3:
                    this.transitionDirection = undefined;

                  case 4:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));

          function currentPeriod() {
            return _currentPeriod.apply(this, arguments);
          }

          return currentPeriod;
        }()
      },
      methods: {
        // Event Handling

        /**
         * React to a date selection
         * @param {Object} day a day object
         */
        dateSelect: function dateSelect(day) {
          if (!day.disabled) {
            this.$emit('input', calendarUtility.formatDateToString(day.date));
          }
        },
        monthSelect: function monthSelect(month) {// console.log(month);
        },
        yearSelect: function yearSelect(year) {// console.log(year);
        },

        /**
         * Use the forward and backward buttons to change the month
         * @param {Number} change the value to increment the month by, default 1 (forward 1 month)
         */
        changeMonth: function changeMonth() {
          var change = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
          var oldDate = new Date(this.currentPeriod.year, this.currentPeriod.month);
          var newDate = new Date(oldDate.getFullYear(), oldDate.getMonth() + change);
          this.transitionDirection = newDate.getMonth() !== oldDate.getMonth() ? newDate > oldDate ? 'Next' : 'Previous' : undefined;
          this.currentPeriod = {
            month: newDate.getMonth(),
            year: newDate.getFullYear()
          };
        },
        toggleMonthSelect: function toggleMonthSelect() {},
        toggleYearSelect: function toggleYearSelect() {},
        showDateSelect: function showDateSelect() {},
        clear: function clear() {
          this.$emit('input', '');
        },
        // Utility Functions

        /**
         * Get the current period from the selected value
         * @param {String} val the return value of the component
         * @returns {Object}
         */
        getCurrentPeriod: function getCurrentPeriod(val) {
          // Get the date from the value. If date is `''`, it's assumed to be today.
          var date = !val ? new Date() : calendarUtility.parseDateFromString(val, this.dateFormat);
          return {
            month: date.getMonth(),
            year: date.getFullYear()
          };
        },

        /**
         * Chunk the days in the month by week (i.e. groups of 7)
         * @param {Array} daysArray an array of days visible on a calendar page for a given month
         * @returns {Array} a chunked array
         */
        groupDaysByWeek: function groupDaysByWeek(daysArray) {
          var daysByWeek = []; // Push a smaller array of only 7 days using splice to make a new array

          while (daysArray.length) {
            daysByWeek.push(daysArray.splice(0, 7));
          }

          return daysByWeek;
        },

        /**
         * Return the proper classes for a highlighted date
         * @param {Object} day a day object
         * @returns {Object}
         */
        highlightingClasses: function highlightingClasses(day) {
          return {
            // See if the day is in the highlightRange
            highlighted: day.date >= this.highlightRange.start && day.date <= this.highlightRange.end,
            // If the result is zero, this is the first day
            'highlighted--first': day.date - this.highlightRange.start == 0,
            // If the result is zero, this is the last day
            'highlighted--last': day.date - this.highlightRange.end == 0
          };
        }
      }
    };

    function styleInject(css, ref) {
      if ( ref === void 0 ) ref = {};
      var insertAt = ref.insertAt;

      if (!css || typeof document === 'undefined') { return; }

      var head = document.head || document.getElementsByTagName('head')[0];
      var style = document.createElement('style');
      style.type = 'text/css';

      if (insertAt === 'top') {
        if (head.firstChild) {
          head.insertBefore(style, head.firstChild);
        } else {
          head.appendChild(style);
        }
      } else {
        head.appendChild(style);
      }

      if (style.styleSheet) {
        style.styleSheet.cssText = css;
      } else {
        style.appendChild(document.createTextNode(css));
      }
    }

    var css = "@-webkit-keyframes slideFromLeft-data-v-5873d2af{from{opacity:0;transform:translate3d(-.5em,0,0)}to{opacity:1;transform:translate3d(0,0,0)}}@keyframes slideFromLeft-data-v-5873d2af{from{opacity:0;transform:translate3d(-.5em,0,0)}to{opacity:1;transform:translate3d(0,0,0)}}@-webkit-keyframes slideFromRight-data-v-5873d2af{from{opacity:0;transform:translate3d(.5em,0,0)}to{opacity:1;transform:translate3d(0,0,0)}}@keyframes slideFromRight-data-v-5873d2af{from{opacity:0;transform:translate3d(.5em,0,0)}to{opacity:1;transform:translate3d(0,0,0)}}.slideToPrevious[data-v-5873d2af]{-webkit-animation:slideFromLeft-data-v-5873d2af .5s;animation:slideFromLeft-data-v-5873d2af .5s}.slideToNext[data-v-5873d2af]{-webkit-animation:slideFromRight-data-v-5873d2af .5s;animation:slideFromRight-data-v-5873d2af .5s}.calendar[data-v-5873d2af]{position:relative;display:inline-block;padding:.25em;max-width:20em;font-size:.9rem}.calendar .header[data-v-5873d2af]{display:flex;flex-flow:row;justify-content:space-around;padding:.5em 0}.calendar .header .header__back[data-v-5873d2af],.calendar .header .header__forward[data-v-5873d2af],.calendar .header .header__month[data-v-5873d2af],.calendar .header .header__year[data-v-5873d2af]{display:inline-block;text-align:center;vertical-align:middle;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;background-color:transparent;cursor:pointer}.calendar .header .header__back[data-v-5873d2af]:hover,.calendar .header .header__forward[data-v-5873d2af]:hover,.calendar .header .header__month[data-v-5873d2af]:hover,.calendar .header .header__year[data-v-5873d2af]:hover{background-color:#eceeef}.calendar .header .header__month[data-v-5873d2af],.calendar .header .header__year[data-v-5873d2af]{font-weight:400}.calendar .header .header__back[data-v-5873d2af],.calendar .header .header__forward[data-v-5873d2af]{width:30px}.calendar .calendar-content .day-view[data-v-5873d2af]{width:100%;display:table;table-layout:fixed;position:relative;margin:0;z-index:5}.calendar .calendar-content .day-view tr[data-v-5873d2af]{border:0;background:0 0!important}.calendar .calendar-content .day-view th div[data-v-5873d2af]{font-weight:400;color:#636c72;margin:0 auto;text-align:center}.calendar .calendar-content .day-view td[data-v-5873d2af]{height:2.5rem}.calendar .calendar-content .day-view td .cellContent[data-v-5873d2af]{border-radius:100%;transition:background .1s,color .1s;min-width:100%;min-height:100%;display:flex;justify-content:center;align-items:center}.calendar .calendar-content .day-view td.selectable[data-v-5873d2af]{cursor:pointer}.calendar .calendar-content .day-view td.outOfRange[data-v-5873d2af]{color:#c7c7c7}.calendar .calendar-content .day-view td.today[data-v-5873d2af]{color:#378dbd}.calendar .calendar-content .day-view td.disabled[data-v-5873d2af]{opacity:.5}.calendar .calendar-content .day-view td.highlighted[data-v-5873d2af]{background-color:#1e5288;color:#fff}.calendar .calendar-content .day-view td.highlighted.highlighted--first[data-v-5873d2af]{border-top-left-radius:100%;border-bottom-left-radius:100%}.calendar .calendar-content .day-view td.highlighted.highlighted--last[data-v-5873d2af]{border-top-right-radius:100%;border-bottom-right-radius:100%}.calendar .calendar-content .day-view td.selected .cellContent[data-v-5873d2af]{background-color:#378dbd;color:#fff}.calendar .calendar-content .day-view td:hover .cellContent[data-v-5873d2af]{background-color:#378dbd;color:#fff}";
    styleInject(css);

    /* script */
    const __vue_script__$2 = script$2;
    /* template */
    var __vue_render__$2 = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"calendar"},[_c('div',{staticClass:"header"},[_c('div',{staticClass:"header__back",attrs:{"type":"button"},on:{"click":function($event){$event.stopPropagation();return _vm.changeMonth(-1)}}},[_c('icon',{attrs:{"icon":"caret-left"}})],1),_vm._v(" "),_c('div',{staticClass:"header__month",class:_vm.transitionClass,attrs:{"type":"button","aria-pressed":_vm.isMonthSelect},on:{"click":function($event){$event.stopPropagation();return _vm.toggleMonthSelect($event)}}},[_vm._v("\n            "+_vm._s(_vm.months[_vm.currentPeriod.month])+"\n        ")]),_vm._v(" "),_c('div',{staticClass:"header__year",class:_vm.transitionClass,attrs:{"type":"button","aria-pressed":_vm.isYearSelect},on:{"click":function($event){$event.stopPropagation();return _vm.toggleYearSelect($event)}}},[_vm._v("\n            "+_vm._s(_vm.currentPeriod.year)+"\n        ")]),_vm._v(" "),_c('div',{staticClass:"header__forward",attrs:{"type":"button"},on:{"click":function($event){$event.stopPropagation();return _vm.changeMonth(1)}}},[_c('icon',{attrs:{"icon":"caret-right"}})],1)]),_vm._v(" "),_c('div',{staticClass:"calendar-content"},[(_vm.isYearSelect)?_c('div',{staticClass:"year-view"}):(_vm.isMonthSelect)?_c('div',{staticClass:"month-view"}):_c('table',{staticClass:"day-view"},[_c('thead',[_c('tr',_vm._l((_vm.weekdays),function(weekday){return _c('th',{key:weekday},[_c('div',[_vm._v("\n                            "+_vm._s(weekday)+"\n                        ")])])}),0)]),_vm._v(" "),_c('tbody',{ref:"calendarDates",class:_vm.transitionClass},_vm._l((_vm.currentMonth),function(week,weekIndex){return _c('tr',{key:weekIndex},_vm._l((week),function(day){return _c('td',{key:day.key,class:Object.assign(
                                {
                                    selected: day.selected,
                                    selectable: !day.disabled,
                                    disabled: day.disabled,
                                    today: day.today,
                                    outOfRange: day.outOfRange,
                                },
                                _vm.isRange ? _vm.highlightingClasses(day) : {}
                            ),on:{"click":function($event){return _vm.dateSelect(day)}}},[_c('div',{staticClass:"cellContent"},[_vm._v("\n                            "+_vm._s(day.date.getDate())+"\n                        ")])])}),0)}),0)])])])};
    var __vue_staticRenderFns__$2 = [];

      /* style */
      const __vue_inject_styles__$2 = undefined;
      /* scoped */
      const __vue_scope_id__$2 = "data-v-5873d2af";
      /* module identifier */
      const __vue_module_identifier__$2 = undefined;
      /* functional template */
      const __vue_is_functional_template__$2 = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var Calendar = normalizeComponent_1(
        { render: __vue_render__$2, staticRenderFns: __vue_staticRenderFns__$2 },
        __vue_inject_styles__$2,
        __vue_script__$2,
        __vue_scope_id__$2,
        __vue_is_functional_template__$2,
        __vue_module_identifier__$2,
        undefined,
        undefined
      );

    /* 
     * Common methods for form input controls, including `CheckBox`, `TextBox`, `TextView`, and `SelectBox`
     */
    var inputMixin = {
      props: {
        // Text
        label: {
          type: String,
          default: undefined
        },
        placeholder: {
          type: String,
          default: undefined
        },
        description: {
          type: String,
          default: undefined
        },
        // State
        success: {
          type: Boolean,
          default: false
        },
        warning: {
          type: Boolean,
          default: false
        },
        danger: {
          type: Boolean,
          default: false
        },
        successMessage: {
          type: String,
          default: ''
        },
        warningMessage: {
          type: String,
          default: ''
        },
        dangerMessage: {
          type: String,
          default: ''
        },
        // Toggle Inline UI
        inline: {
          type: Boolean,
          defalt: false
        },
        inlineDescription: {
          type: Boolean,
          default: false
        },
        // Control size
        size: {
          type: String,
          default: 'normal',
          validator: function validator(value) {
            return ['small', 'normal', 'large'].indexOf(value) > -1;
          }
        },
        // Disable the control
        disabled: {
          type: Boolean,
          default: false
        },
        // Browser autocomplete
        autocomplete: {
          type: Boolean,
          default: true
        }
      },
      computed: {
        isAutocomplete: function isAutocomplete() {
          if (this.autocomplete === true) {
            return 'on';
          } else {
            return 'off';
          }
        },
        isDescription: function isDescription() {
          if (this.description === undefined) {
            return false;
          } else {
            return true;
          }
        },
        isLabel: function isLabel() {
          if (this.label === undefined) {
            return false;
          } else {
            return true;
          }
        },
        stateEnabled: function stateEnabled() {
          if (this.success === true || this.warning === true || this.danger === true) {
            return true;
          } else {
            return false;
          }
        }
      }
    };

    //
    var script$3 = {
      name: "CheckBox",
      mixins: [inputMixin],
      inheritAttrs: false,
      model: {
        prop: 'checked',
        event: 'change'
      },
      props: {
        checkedValue: {
          type: Boolean,
          default: null
        },
        checked: {
          type: Boolean,
          default: false
        },
        indeterminate: {
          type: Boolean,
          default: false
        },
        id: {
          type: String,
          required: true
        }
      },
      watch: {
        'indeterminate': function indeterminate() {
          this.setIndeterminate();
        }
      },
      mounted: function mounted() {
        this.setIndeterminate();
      },
      methods: {
        update: function update(val) {
          var value = val;

          if (val === undefined) {
            value = false;
          }

          this.$emit('change', value, this.id);
        },

        /**
         * Set the indeterminate value of the built-in checkbox input.
         */
        setIndeterminate: function setIndeterminate() {
          this.$refs.checkbox.indeterminate = this.indeterminate;
        }
      }
    };

    /* script */
    const __vue_script__$3 = script$3;

    /* template */
    var __vue_render__$3 = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"form-check",class:[{'disabled': _vm.disabled},
                                     {'form-check-inline': _vm.inline},
                                     {'has-success': _vm.success}, 
                                     {'has-warning': _vm.warning}, 
                                     {'has-danger': _vm.danger}]},[_c('label',{staticClass:"custom-control custom-checkbox"},[_c('input',_vm._b({ref:"checkbox",staticClass:"custom-control-input",class:[{'form-control-success': _vm.success}, 
                                                         {'form-control-warning': _vm.warning}, 
                                                         {'form-control-danger': _vm.danger}],attrs:{"id":_vm.id,"type":"checkbox","disabled":_vm.disabled},domProps:{"checked":_vm.checked},on:{"change":function($event){return _vm.update($event.target.checked)}}},'input',_vm.$attrs,false)),_vm._v("\n        "+_vm._s(_vm.label)+"\n        "),_c('span',{staticClass:"custom-control-indicator"})]),_vm._v(" "),(_vm.stateEnabled && _vm.success)?_c('div',{staticClass:"form-control-feedback"},[_vm._v(_vm._s(_vm.successMessage))]):_vm._e(),_vm._v(" "),(_vm.stateEnabled && _vm.warning)?_c('div',{staticClass:"form-control-feedback"},[_vm._v(_vm._s(_vm.warningMessage))]):_vm._e(),_vm._v(" "),(_vm.stateEnabled && _vm.danger)?_c('div',{staticClass:"form-control-feedback"},[_vm._v(_vm._s(_vm.dangerMessage))]):_vm._e(),_vm._v(" "),(_vm.isDescription)?_c('small',{staticClass:"text-muted",class:[{'form-text': !_vm.inlineDescription}],attrs:{"id":_vm.id + 'Help'}},[_vm._v("\n        "+_vm._s(_vm.description)+"\n    ")]):_vm._e()])};
    var __vue_staticRenderFns__$3 = [];

      /* style */
      const __vue_inject_styles__$3 = undefined;
      /* scoped */
      const __vue_scope_id__$3 = undefined;
      /* module identifier */
      const __vue_module_identifier__$3 = undefined;
      /* functional template */
      const __vue_is_functional_template__$3 = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var CheckBox = normalizeComponent_1(
        { render: __vue_render__$3, staticRenderFns: __vue_staticRenderFns__$3 },
        __vue_inject_styles__$3,
        __vue_script__$3,
        __vue_scope_id__$3,
        __vue_is_functional_template__$3,
        __vue_module_identifier__$3,
        undefined,
        undefined
      );

    /**
     * Mixin for reusable bits for `Command`, and `CommandDropdown`
     */
    var commandMixin = {
      props: {
        active: {
          type: Boolean,
          default: false
        },
        label: {
          type: String,
          default: undefined
        },
        icon: {
          type: String,
          default: undefined
        },
        type: {
          type: String,
          default: 'secondary',
          validator: function validator(value) {
            return ['secondary', 'success', 'warning', 'danger', 'info', 'primary', 'link', 'uared'].indexOf(value) > -1;
          }
        },
        size: {
          type: String,
          default: 'normal',
          validator: function validator(value) {
            return ['large', 'normal', 'small'].indexOf(value) > -1;
          }
        },
        disabled: {
          type: Boolean,
          default: false
        },
        outline: {
          type: Boolean,
          default: false
        }
      },
      computed: {
        isOutline: function isOutline() {
          if (this.outline === true) {
            return 'btn-outline-';
          } else {
            return 'btn-';
          }
        },
        btnSize: function btnSize() {
          if (this.size === 'large') {
            return 'btn-lg';
          } else if (this.size === 'small') {
            return 'btn-sm';
          } else {
            return '';
          }
        }
      }
    };

    //
    var script$4 = {
      name: "Command",
      components: {
        Icon: Icon
      },
      mixins: [commandMixin],
      props: {
        block: {
          type: Boolean,
          default: false
        }
      }
    };

    var css$1 = ".btn-flex[data-v-86c24d44]{display:inline-flex;align-items:center}";
    styleInject(css$1);

    /* script */
    const __vue_script__$4 = script$4;
    /* template */
    var __vue_render__$4 = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('button',_vm._g(_vm._b({staticClass:"btn",class:[_vm.isOutline + _vm.type, _vm.btnSize, _vm.block ? 'btn-block' : 'btn-flex', { 'active': _vm.active }],attrs:{"type":"button","disabled":_vm.disabled,"aria-pressed":_vm.active}},'button',_vm.$attrs,false),_vm.$listeners),[(_vm.icon)?_c('icon',{class:[{'mr-2': _vm.label}],attrs:{"icon":_vm.icon}}):_vm._e(),_vm._v(" "),(_vm.label)?_c('div',{staticClass:"btn-label"},[_vm._v("\n        "+_vm._s(_vm.label)+"\n    ")]):_vm._e()],1)};
    var __vue_staticRenderFns__$4 = [];

      /* style */
      const __vue_inject_styles__$4 = undefined;
      /* scoped */
      const __vue_scope_id__$4 = "data-v-86c24d44";
      /* module identifier */
      const __vue_module_identifier__$4 = undefined;
      /* functional template */
      const __vue_is_functional_template__$4 = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var Command = normalizeComponent_1(
        { render: __vue_render__$4, staticRenderFns: __vue_staticRenderFns__$4 },
        __vue_inject_styles__$4,
        __vue_script__$4,
        __vue_scope_id__$4,
        __vue_is_functional_template__$4,
        __vue_module_identifier__$4,
        undefined,
        undefined
      );

    //
    var script$5 = {
      name: "CollapsePanel",
      components: {
        Command: Command
      },
      props: {
        title: {
          type: String,
          required: true
        },
        collapsed: {
          type: Boolean,
          required: false,
          default: false
        }
      },
      data: function data() {
        return {
          collapsedState: false
        };
      },
      computed: {
        iconState: function iconState() {
          if (this.collapsedState === true) {
            return "chevron-up";
          } else {
            return "chevron-down";
          }
        }
      },
      watch: {
        collapsed: function collapsed(collapse) {
          if (collapse !== undefined) {
            this.collapsedState = collapse;
          }
        }
      },
      created: function created() {
        this.collapsedState = this.collapsed;
      },
      methods: {
        toggleCollapse: function toggleCollapse() {
          this.collapsedState = !this.collapsedState;
        }
      }
    };

    var css$2 = ".card-header h5[data-v-4805b3b0]{margin-bottom:0}";
    styleInject(css$2);

    /* script */
    const __vue_script__$5 = script$5;
    /* template */
    var __vue_render__$5 = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"card"},[_c('div',{staticClass:"card-header d-flex justify-content-start align-items-center"},[_c('h5',[_vm._v(_vm._s(_vm.title))]),_vm._v(" "),(_vm.collapsedState === true)?_c('div',{staticClass:"ml-2"},[_vm._t("summary")],2):_vm._e(),_vm._v(" "),_c('div',{staticClass:"ml-auto"},[_c('command',{attrs:{"icon":_vm.iconState,"small":""},on:{"action":_vm.toggleCollapse}})],1)]),_vm._v(" "),_c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.collapsedState === false),expression:"collapsedState === false"}],staticClass:"card-block"},[_vm._t("content")],2)])};
    var __vue_staticRenderFns__$5 = [];

      /* style */
      const __vue_inject_styles__$5 = undefined;
      /* scoped */
      const __vue_scope_id__$5 = "data-v-4805b3b0";
      /* module identifier */
      const __vue_module_identifier__$5 = undefined;
      /* functional template */
      const __vue_is_functional_template__$5 = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var CollapsePanel = normalizeComponent_1(
        { render: __vue_render__$5, staticRenderFns: __vue_staticRenderFns__$5 },
        __vue_inject_styles__$5,
        __vue_script__$5,
        __vue_scope_id__$5,
        __vue_is_functional_template__$5,
        __vue_module_identifier__$5,
        undefined,
        undefined
      );

    // Modified from https://github.com/bootstrap-vue/bootstrap-vue/blob/dev/src/mixins/clickout.js
    var clickoutMixin = {
      mounted: function mounted() {
        if (typeof document !== 'undefined') {
          document.documentElement.addEventListener('click', this._clickOutListener);
        }
      },
      beforeDestroy: function beforeDestroy() {
        if (typeof document !== 'undefined') {
          document.documentElement.removeEventListener('click', this._clickOutListener);
        }
      },
      methods: {
        _clickOutListener: function _clickOutListener(e) {
          if (!this.$el.contains(e.target)) {
            if (this.onClickOut) {
              this.onClickOut();
            }
          }
        }
      }
    };

    /**!
     * @fileOverview Kickass library to create and place poppers near their reference elements.
     * @version 1.15.0
     * @license
     * Copyright (c) 2016 Federico Zivolo and contributors
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy
     * of this software and associated documentation files (the "Software"), to deal
     * in the Software without restriction, including without limitation the rights
     * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
     * copies of the Software, and to permit persons to whom the Software is
     * furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in all
     * copies or substantial portions of the Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
     * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
     * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
     * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
     * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
     * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
     * SOFTWARE.
     */
    var isBrowser = typeof window !== 'undefined' && typeof document !== 'undefined';

    var longerTimeoutBrowsers = ['Edge', 'Trident', 'Firefox'];
    var timeoutDuration = 0;
    for (var i = 0; i < longerTimeoutBrowsers.length; i += 1) {
      if (isBrowser && navigator.userAgent.indexOf(longerTimeoutBrowsers[i]) >= 0) {
        timeoutDuration = 1;
        break;
      }
    }

    function microtaskDebounce(fn) {
      var called = false;
      return function () {
        if (called) {
          return;
        }
        called = true;
        window.Promise.resolve().then(function () {
          called = false;
          fn();
        });
      };
    }

    function taskDebounce(fn) {
      var scheduled = false;
      return function () {
        if (!scheduled) {
          scheduled = true;
          setTimeout(function () {
            scheduled = false;
            fn();
          }, timeoutDuration);
        }
      };
    }

    var supportsMicroTasks = isBrowser && window.Promise;

    /**
    * Create a debounced version of a method, that's asynchronously deferred
    * but called in the minimum time possible.
    *
    * @method
    * @memberof Popper.Utils
    * @argument {Function} fn
    * @returns {Function}
    */
    var debounce = supportsMicroTasks ? microtaskDebounce : taskDebounce;

    /**
     * Check if the given variable is a function
     * @method
     * @memberof Popper.Utils
     * @argument {Any} functionToCheck - variable to check
     * @returns {Boolean} answer to: is a function?
     */
    function isFunction(functionToCheck) {
      var getType = {};
      return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
    }

    /**
     * Get CSS computed property of the given element
     * @method
     * @memberof Popper.Utils
     * @argument {Eement} element
     * @argument {String} property
     */
    function getStyleComputedProperty(element, property) {
      if (element.nodeType !== 1) {
        return [];
      }
      // NOTE: 1 DOM access here
      var window = element.ownerDocument.defaultView;
      var css = window.getComputedStyle(element, null);
      return property ? css[property] : css;
    }

    /**
     * Returns the parentNode or the host of the element
     * @method
     * @memberof Popper.Utils
     * @argument {Element} element
     * @returns {Element} parent
     */
    function getParentNode(element) {
      if (element.nodeName === 'HTML') {
        return element;
      }
      return element.parentNode || element.host;
    }

    /**
     * Returns the scrolling parent of the given element
     * @method
     * @memberof Popper.Utils
     * @argument {Element} element
     * @returns {Element} scroll parent
     */
    function getScrollParent(element) {
      // Return body, `getScroll` will take care to get the correct `scrollTop` from it
      if (!element) {
        return document.body;
      }

      switch (element.nodeName) {
        case 'HTML':
        case 'BODY':
          return element.ownerDocument.body;
        case '#document':
          return element.body;
      }

      // Firefox want us to check `-x` and `-y` variations as well

      var _getStyleComputedProp = getStyleComputedProperty(element),
          overflow = _getStyleComputedProp.overflow,
          overflowX = _getStyleComputedProp.overflowX,
          overflowY = _getStyleComputedProp.overflowY;

      if (/(auto|scroll|overlay)/.test(overflow + overflowY + overflowX)) {
        return element;
      }

      return getScrollParent(getParentNode(element));
    }

    var isIE11 = isBrowser && !!(window.MSInputMethodContext && document.documentMode);
    var isIE10 = isBrowser && /MSIE 10/.test(navigator.userAgent);

    /**
     * Determines if the browser is Internet Explorer
     * @method
     * @memberof Popper.Utils
     * @param {Number} version to check
     * @returns {Boolean} isIE
     */
    function isIE(version) {
      if (version === 11) {
        return isIE11;
      }
      if (version === 10) {
        return isIE10;
      }
      return isIE11 || isIE10;
    }

    /**
     * Returns the offset parent of the given element
     * @method
     * @memberof Popper.Utils
     * @argument {Element} element
     * @returns {Element} offset parent
     */
    function getOffsetParent(element) {
      if (!element) {
        return document.documentElement;
      }

      var noOffsetParent = isIE(10) ? document.body : null;

      // NOTE: 1 DOM access here
      var offsetParent = element.offsetParent || null;
      // Skip hidden elements which don't have an offsetParent
      while (offsetParent === noOffsetParent && element.nextElementSibling) {
        offsetParent = (element = element.nextElementSibling).offsetParent;
      }

      var nodeName = offsetParent && offsetParent.nodeName;

      if (!nodeName || nodeName === 'BODY' || nodeName === 'HTML') {
        return element ? element.ownerDocument.documentElement : document.documentElement;
      }

      // .offsetParent will return the closest TH, TD or TABLE in case
      // no offsetParent is present, I hate this job...
      if (['TH', 'TD', 'TABLE'].indexOf(offsetParent.nodeName) !== -1 && getStyleComputedProperty(offsetParent, 'position') === 'static') {
        return getOffsetParent(offsetParent);
      }

      return offsetParent;
    }

    function isOffsetContainer(element) {
      var nodeName = element.nodeName;

      if (nodeName === 'BODY') {
        return false;
      }
      return nodeName === 'HTML' || getOffsetParent(element.firstElementChild) === element;
    }

    /**
     * Finds the root node (document, shadowDOM root) of the given element
     * @method
     * @memberof Popper.Utils
     * @argument {Element} node
     * @returns {Element} root node
     */
    function getRoot(node) {
      if (node.parentNode !== null) {
        return getRoot(node.parentNode);
      }

      return node;
    }

    /**
     * Finds the offset parent common to the two provided nodes
     * @method
     * @memberof Popper.Utils
     * @argument {Element} element1
     * @argument {Element} element2
     * @returns {Element} common offset parent
     */
    function findCommonOffsetParent(element1, element2) {
      // This check is needed to avoid errors in case one of the elements isn't defined for any reason
      if (!element1 || !element1.nodeType || !element2 || !element2.nodeType) {
        return document.documentElement;
      }

      // Here we make sure to give as "start" the element that comes first in the DOM
      var order = element1.compareDocumentPosition(element2) & Node.DOCUMENT_POSITION_FOLLOWING;
      var start = order ? element1 : element2;
      var end = order ? element2 : element1;

      // Get common ancestor container
      var range = document.createRange();
      range.setStart(start, 0);
      range.setEnd(end, 0);
      var commonAncestorContainer = range.commonAncestorContainer;

      // Both nodes are inside #document

      if (element1 !== commonAncestorContainer && element2 !== commonAncestorContainer || start.contains(end)) {
        if (isOffsetContainer(commonAncestorContainer)) {
          return commonAncestorContainer;
        }

        return getOffsetParent(commonAncestorContainer);
      }

      // one of the nodes is inside shadowDOM, find which one
      var element1root = getRoot(element1);
      if (element1root.host) {
        return findCommonOffsetParent(element1root.host, element2);
      } else {
        return findCommonOffsetParent(element1, getRoot(element2).host);
      }
    }

    /**
     * Gets the scroll value of the given element in the given side (top and left)
     * @method
     * @memberof Popper.Utils
     * @argument {Element} element
     * @argument {String} side `top` or `left`
     * @returns {number} amount of scrolled pixels
     */
    function getScroll(element) {
      var side = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'top';

      var upperSide = side === 'top' ? 'scrollTop' : 'scrollLeft';
      var nodeName = element.nodeName;

      if (nodeName === 'BODY' || nodeName === 'HTML') {
        var html = element.ownerDocument.documentElement;
        var scrollingElement = element.ownerDocument.scrollingElement || html;
        return scrollingElement[upperSide];
      }

      return element[upperSide];
    }

    /*
     * Sum or subtract the element scroll values (left and top) from a given rect object
     * @method
     * @memberof Popper.Utils
     * @param {Object} rect - Rect object you want to change
     * @param {HTMLElement} element - The element from the function reads the scroll values
     * @param {Boolean} subtract - set to true if you want to subtract the scroll values
     * @return {Object} rect - The modifier rect object
     */
    function includeScroll(rect, element) {
      var subtract = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

      var scrollTop = getScroll(element, 'top');
      var scrollLeft = getScroll(element, 'left');
      var modifier = subtract ? -1 : 1;
      rect.top += scrollTop * modifier;
      rect.bottom += scrollTop * modifier;
      rect.left += scrollLeft * modifier;
      rect.right += scrollLeft * modifier;
      return rect;
    }

    /*
     * Helper to detect borders of a given element
     * @method
     * @memberof Popper.Utils
     * @param {CSSStyleDeclaration} styles
     * Result of `getStyleComputedProperty` on the given element
     * @param {String} axis - `x` or `y`
     * @return {number} borders - The borders size of the given axis
     */

    function getBordersSize(styles, axis) {
      var sideA = axis === 'x' ? 'Left' : 'Top';
      var sideB = sideA === 'Left' ? 'Right' : 'Bottom';

      return parseFloat(styles['border' + sideA + 'Width'], 10) + parseFloat(styles['border' + sideB + 'Width'], 10);
    }

    function getSize(axis, body, html, computedStyle) {
      return Math.max(body['offset' + axis], body['scroll' + axis], html['client' + axis], html['offset' + axis], html['scroll' + axis], isIE(10) ? parseInt(html['offset' + axis]) + parseInt(computedStyle['margin' + (axis === 'Height' ? 'Top' : 'Left')]) + parseInt(computedStyle['margin' + (axis === 'Height' ? 'Bottom' : 'Right')]) : 0);
    }

    function getWindowSizes(document) {
      var body = document.body;
      var html = document.documentElement;
      var computedStyle = isIE(10) && getComputedStyle(html);

      return {
        height: getSize('Height', body, html, computedStyle),
        width: getSize('Width', body, html, computedStyle)
      };
    }

    var classCallCheck$1 = function (instance, Constructor) {
      if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
      }
    };

    var createClass$1 = function () {
      function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
          var descriptor = props[i];
          descriptor.enumerable = descriptor.enumerable || false;
          descriptor.configurable = true;
          if ("value" in descriptor) descriptor.writable = true;
          Object.defineProperty(target, descriptor.key, descriptor);
        }
      }

      return function (Constructor, protoProps, staticProps) {
        if (protoProps) defineProperties(Constructor.prototype, protoProps);
        if (staticProps) defineProperties(Constructor, staticProps);
        return Constructor;
      };
    }();





    var defineProperty = function (obj, key, value) {
      if (key in obj) {
        Object.defineProperty(obj, key, {
          value: value,
          enumerable: true,
          configurable: true,
          writable: true
        });
      } else {
        obj[key] = value;
      }

      return obj;
    };

    var _extends = Object.assign || function (target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];

        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }

      return target;
    };

    /**
     * Given element offsets, generate an output similar to getBoundingClientRect
     * @method
     * @memberof Popper.Utils
     * @argument {Object} offsets
     * @returns {Object} ClientRect like output
     */
    function getClientRect(offsets) {
      return _extends({}, offsets, {
        right: offsets.left + offsets.width,
        bottom: offsets.top + offsets.height
      });
    }

    /**
     * Get bounding client rect of given element
     * @method
     * @memberof Popper.Utils
     * @param {HTMLElement} element
     * @return {Object} client rect
     */
    function getBoundingClientRect(element) {
      var rect = {};

      // IE10 10 FIX: Please, don't ask, the element isn't
      // considered in DOM in some circumstances...
      // This isn't reproducible in IE10 compatibility mode of IE11
      try {
        if (isIE(10)) {
          rect = element.getBoundingClientRect();
          var scrollTop = getScroll(element, 'top');
          var scrollLeft = getScroll(element, 'left');
          rect.top += scrollTop;
          rect.left += scrollLeft;
          rect.bottom += scrollTop;
          rect.right += scrollLeft;
        } else {
          rect = element.getBoundingClientRect();
        }
      } catch (e) {}

      var result = {
        left: rect.left,
        top: rect.top,
        width: rect.right - rect.left,
        height: rect.bottom - rect.top
      };

      // subtract scrollbar size from sizes
      var sizes = element.nodeName === 'HTML' ? getWindowSizes(element.ownerDocument) : {};
      var width = sizes.width || element.clientWidth || result.right - result.left;
      var height = sizes.height || element.clientHeight || result.bottom - result.top;

      var horizScrollbar = element.offsetWidth - width;
      var vertScrollbar = element.offsetHeight - height;

      // if an hypothetical scrollbar is detected, we must be sure it's not a `border`
      // we make this check conditional for performance reasons
      if (horizScrollbar || vertScrollbar) {
        var styles = getStyleComputedProperty(element);
        horizScrollbar -= getBordersSize(styles, 'x');
        vertScrollbar -= getBordersSize(styles, 'y');

        result.width -= horizScrollbar;
        result.height -= vertScrollbar;
      }

      return getClientRect(result);
    }

    function getOffsetRectRelativeToArbitraryNode(children, parent) {
      var fixedPosition = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

      var isIE10 = isIE(10);
      var isHTML = parent.nodeName === 'HTML';
      var childrenRect = getBoundingClientRect(children);
      var parentRect = getBoundingClientRect(parent);
      var scrollParent = getScrollParent(children);

      var styles = getStyleComputedProperty(parent);
      var borderTopWidth = parseFloat(styles.borderTopWidth, 10);
      var borderLeftWidth = parseFloat(styles.borderLeftWidth, 10);

      // In cases where the parent is fixed, we must ignore negative scroll in offset calc
      if (fixedPosition && isHTML) {
        parentRect.top = Math.max(parentRect.top, 0);
        parentRect.left = Math.max(parentRect.left, 0);
      }
      var offsets = getClientRect({
        top: childrenRect.top - parentRect.top - borderTopWidth,
        left: childrenRect.left - parentRect.left - borderLeftWidth,
        width: childrenRect.width,
        height: childrenRect.height
      });
      offsets.marginTop = 0;
      offsets.marginLeft = 0;

      // Subtract margins of documentElement in case it's being used as parent
      // we do this only on HTML because it's the only element that behaves
      // differently when margins are applied to it. The margins are included in
      // the box of the documentElement, in the other cases not.
      if (!isIE10 && isHTML) {
        var marginTop = parseFloat(styles.marginTop, 10);
        var marginLeft = parseFloat(styles.marginLeft, 10);

        offsets.top -= borderTopWidth - marginTop;
        offsets.bottom -= borderTopWidth - marginTop;
        offsets.left -= borderLeftWidth - marginLeft;
        offsets.right -= borderLeftWidth - marginLeft;

        // Attach marginTop and marginLeft because in some circumstances we may need them
        offsets.marginTop = marginTop;
        offsets.marginLeft = marginLeft;
      }

      if (isIE10 && !fixedPosition ? parent.contains(scrollParent) : parent === scrollParent && scrollParent.nodeName !== 'BODY') {
        offsets = includeScroll(offsets, parent);
      }

      return offsets;
    }

    function getViewportOffsetRectRelativeToArtbitraryNode(element) {
      var excludeScroll = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      var html = element.ownerDocument.documentElement;
      var relativeOffset = getOffsetRectRelativeToArbitraryNode(element, html);
      var width = Math.max(html.clientWidth, window.innerWidth || 0);
      var height = Math.max(html.clientHeight, window.innerHeight || 0);

      var scrollTop = !excludeScroll ? getScroll(html) : 0;
      var scrollLeft = !excludeScroll ? getScroll(html, 'left') : 0;

      var offset = {
        top: scrollTop - relativeOffset.top + relativeOffset.marginTop,
        left: scrollLeft - relativeOffset.left + relativeOffset.marginLeft,
        width: width,
        height: height
      };

      return getClientRect(offset);
    }

    /**
     * Check if the given element is fixed or is inside a fixed parent
     * @method
     * @memberof Popper.Utils
     * @argument {Element} element
     * @argument {Element} customContainer
     * @returns {Boolean} answer to "isFixed?"
     */
    function isFixed(element) {
      var nodeName = element.nodeName;
      if (nodeName === 'BODY' || nodeName === 'HTML') {
        return false;
      }
      if (getStyleComputedProperty(element, 'position') === 'fixed') {
        return true;
      }
      var parentNode = getParentNode(element);
      if (!parentNode) {
        return false;
      }
      return isFixed(parentNode);
    }

    /**
     * Finds the first parent of an element that has a transformed property defined
     * @method
     * @memberof Popper.Utils
     * @argument {Element} element
     * @returns {Element} first transformed parent or documentElement
     */

    function getFixedPositionOffsetParent(element) {
      // This check is needed to avoid errors in case one of the elements isn't defined for any reason
      if (!element || !element.parentElement || isIE()) {
        return document.documentElement;
      }
      var el = element.parentElement;
      while (el && getStyleComputedProperty(el, 'transform') === 'none') {
        el = el.parentElement;
      }
      return el || document.documentElement;
    }

    /**
     * Computed the boundaries limits and return them
     * @method
     * @memberof Popper.Utils
     * @param {HTMLElement} popper
     * @param {HTMLElement} reference
     * @param {number} padding
     * @param {HTMLElement} boundariesElement - Element used to define the boundaries
     * @param {Boolean} fixedPosition - Is in fixed position mode
     * @returns {Object} Coordinates of the boundaries
     */
    function getBoundaries(popper, reference, padding, boundariesElement) {
      var fixedPosition = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;

      // NOTE: 1 DOM access here

      var boundaries = { top: 0, left: 0 };
      var offsetParent = fixedPosition ? getFixedPositionOffsetParent(popper) : findCommonOffsetParent(popper, reference);

      // Handle viewport case
      if (boundariesElement === 'viewport') {
        boundaries = getViewportOffsetRectRelativeToArtbitraryNode(offsetParent, fixedPosition);
      } else {
        // Handle other cases based on DOM element used as boundaries
        var boundariesNode = void 0;
        if (boundariesElement === 'scrollParent') {
          boundariesNode = getScrollParent(getParentNode(reference));
          if (boundariesNode.nodeName === 'BODY') {
            boundariesNode = popper.ownerDocument.documentElement;
          }
        } else if (boundariesElement === 'window') {
          boundariesNode = popper.ownerDocument.documentElement;
        } else {
          boundariesNode = boundariesElement;
        }

        var offsets = getOffsetRectRelativeToArbitraryNode(boundariesNode, offsetParent, fixedPosition);

        // In case of HTML, we need a different computation
        if (boundariesNode.nodeName === 'HTML' && !isFixed(offsetParent)) {
          var _getWindowSizes = getWindowSizes(popper.ownerDocument),
              height = _getWindowSizes.height,
              width = _getWindowSizes.width;

          boundaries.top += offsets.top - offsets.marginTop;
          boundaries.bottom = height + offsets.top;
          boundaries.left += offsets.left - offsets.marginLeft;
          boundaries.right = width + offsets.left;
        } else {
          // for all the other DOM elements, this one is good
          boundaries = offsets;
        }
      }

      // Add paddings
      padding = padding || 0;
      var isPaddingNumber = typeof padding === 'number';
      boundaries.left += isPaddingNumber ? padding : padding.left || 0;
      boundaries.top += isPaddingNumber ? padding : padding.top || 0;
      boundaries.right -= isPaddingNumber ? padding : padding.right || 0;
      boundaries.bottom -= isPaddingNumber ? padding : padding.bottom || 0;

      return boundaries;
    }

    function getArea(_ref) {
      var width = _ref.width,
          height = _ref.height;

      return width * height;
    }

    /**
     * Utility used to transform the `auto` placement to the placement with more
     * available space.
     * @method
     * @memberof Popper.Utils
     * @argument {Object} data - The data object generated by update method
     * @argument {Object} options - Modifiers configuration and options
     * @returns {Object} The data object, properly modified
     */
    function computeAutoPlacement(placement, refRect, popper, reference, boundariesElement) {
      var padding = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : 0;

      if (placement.indexOf('auto') === -1) {
        return placement;
      }

      var boundaries = getBoundaries(popper, reference, padding, boundariesElement);

      var rects = {
        top: {
          width: boundaries.width,
          height: refRect.top - boundaries.top
        },
        right: {
          width: boundaries.right - refRect.right,
          height: boundaries.height
        },
        bottom: {
          width: boundaries.width,
          height: boundaries.bottom - refRect.bottom
        },
        left: {
          width: refRect.left - boundaries.left,
          height: boundaries.height
        }
      };

      var sortedAreas = Object.keys(rects).map(function (key) {
        return _extends({
          key: key
        }, rects[key], {
          area: getArea(rects[key])
        });
      }).sort(function (a, b) {
        return b.area - a.area;
      });

      var filteredAreas = sortedAreas.filter(function (_ref2) {
        var width = _ref2.width,
            height = _ref2.height;
        return width >= popper.clientWidth && height >= popper.clientHeight;
      });

      var computedPlacement = filteredAreas.length > 0 ? filteredAreas[0].key : sortedAreas[0].key;

      var variation = placement.split('-')[1];

      return computedPlacement + (variation ? '-' + variation : '');
    }

    /**
     * Get offsets to the reference element
     * @method
     * @memberof Popper.Utils
     * @param {Object} state
     * @param {Element} popper - the popper element
     * @param {Element} reference - the reference element (the popper will be relative to this)
     * @param {Element} fixedPosition - is in fixed position mode
     * @returns {Object} An object containing the offsets which will be applied to the popper
     */
    function getReferenceOffsets(state, popper, reference) {
      var fixedPosition = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

      var commonOffsetParent = fixedPosition ? getFixedPositionOffsetParent(popper) : findCommonOffsetParent(popper, reference);
      return getOffsetRectRelativeToArbitraryNode(reference, commonOffsetParent, fixedPosition);
    }

    /**
     * Get the outer sizes of the given element (offset size + margins)
     * @method
     * @memberof Popper.Utils
     * @argument {Element} element
     * @returns {Object} object containing width and height properties
     */
    function getOuterSizes(element) {
      var window = element.ownerDocument.defaultView;
      var styles = window.getComputedStyle(element);
      var x = parseFloat(styles.marginTop || 0) + parseFloat(styles.marginBottom || 0);
      var y = parseFloat(styles.marginLeft || 0) + parseFloat(styles.marginRight || 0);
      var result = {
        width: element.offsetWidth + y,
        height: element.offsetHeight + x
      };
      return result;
    }

    /**
     * Get the opposite placement of the given one
     * @method
     * @memberof Popper.Utils
     * @argument {String} placement
     * @returns {String} flipped placement
     */
    function getOppositePlacement(placement) {
      var hash = { left: 'right', right: 'left', bottom: 'top', top: 'bottom' };
      return placement.replace(/left|right|bottom|top/g, function (matched) {
        return hash[matched];
      });
    }

    /**
     * Get offsets to the popper
     * @method
     * @memberof Popper.Utils
     * @param {Object} position - CSS position the Popper will get applied
     * @param {HTMLElement} popper - the popper element
     * @param {Object} referenceOffsets - the reference offsets (the popper will be relative to this)
     * @param {String} placement - one of the valid placement options
     * @returns {Object} popperOffsets - An object containing the offsets which will be applied to the popper
     */
    function getPopperOffsets(popper, referenceOffsets, placement) {
      placement = placement.split('-')[0];

      // Get popper node sizes
      var popperRect = getOuterSizes(popper);

      // Add position, width and height to our offsets object
      var popperOffsets = {
        width: popperRect.width,
        height: popperRect.height
      };

      // depending by the popper placement we have to compute its offsets slightly differently
      var isHoriz = ['right', 'left'].indexOf(placement) !== -1;
      var mainSide = isHoriz ? 'top' : 'left';
      var secondarySide = isHoriz ? 'left' : 'top';
      var measurement = isHoriz ? 'height' : 'width';
      var secondaryMeasurement = !isHoriz ? 'height' : 'width';

      popperOffsets[mainSide] = referenceOffsets[mainSide] + referenceOffsets[measurement] / 2 - popperRect[measurement] / 2;
      if (placement === secondarySide) {
        popperOffsets[secondarySide] = referenceOffsets[secondarySide] - popperRect[secondaryMeasurement];
      } else {
        popperOffsets[secondarySide] = referenceOffsets[getOppositePlacement(secondarySide)];
      }

      return popperOffsets;
    }

    /**
     * Mimics the `find` method of Array
     * @method
     * @memberof Popper.Utils
     * @argument {Array} arr
     * @argument prop
     * @argument value
     * @returns index or -1
     */
    function find(arr, check) {
      // use native find if supported
      if (Array.prototype.find) {
        return arr.find(check);
      }

      // use `filter` to obtain the same behavior of `find`
      return arr.filter(check)[0];
    }

    /**
     * Return the index of the matching object
     * @method
     * @memberof Popper.Utils
     * @argument {Array} arr
     * @argument prop
     * @argument value
     * @returns index or -1
     */
    function findIndex(arr, prop, value) {
      // use native findIndex if supported
      if (Array.prototype.findIndex) {
        return arr.findIndex(function (cur) {
          return cur[prop] === value;
        });
      }

      // use `find` + `indexOf` if `findIndex` isn't supported
      var match = find(arr, function (obj) {
        return obj[prop] === value;
      });
      return arr.indexOf(match);
    }

    /**
     * Loop trough the list of modifiers and run them in order,
     * each of them will then edit the data object.
     * @method
     * @memberof Popper.Utils
     * @param {dataObject} data
     * @param {Array} modifiers
     * @param {String} ends - Optional modifier name used as stopper
     * @returns {dataObject}
     */
    function runModifiers(modifiers, data, ends) {
      var modifiersToRun = ends === undefined ? modifiers : modifiers.slice(0, findIndex(modifiers, 'name', ends));

      modifiersToRun.forEach(function (modifier) {
        if (modifier['function']) {
          // eslint-disable-line dot-notation
          console.warn('`modifier.function` is deprecated, use `modifier.fn`!');
        }
        var fn = modifier['function'] || modifier.fn; // eslint-disable-line dot-notation
        if (modifier.enabled && isFunction(fn)) {
          // Add properties to offsets to make them a complete clientRect object
          // we do this before each modifier to make sure the previous one doesn't
          // mess with these values
          data.offsets.popper = getClientRect(data.offsets.popper);
          data.offsets.reference = getClientRect(data.offsets.reference);

          data = fn(data, modifier);
        }
      });

      return data;
    }

    /**
     * Updates the position of the popper, computing the new offsets and applying
     * the new style.<br />
     * Prefer `scheduleUpdate` over `update` because of performance reasons.
     * @method
     * @memberof Popper
     */
    function update() {
      // if popper is destroyed, don't perform any further update
      if (this.state.isDestroyed) {
        return;
      }

      var data = {
        instance: this,
        styles: {},
        arrowStyles: {},
        attributes: {},
        flipped: false,
        offsets: {}
      };

      // compute reference element offsets
      data.offsets.reference = getReferenceOffsets(this.state, this.popper, this.reference, this.options.positionFixed);

      // compute auto placement, store placement inside the data object,
      // modifiers will be able to edit `placement` if needed
      // and refer to originalPlacement to know the original value
      data.placement = computeAutoPlacement(this.options.placement, data.offsets.reference, this.popper, this.reference, this.options.modifiers.flip.boundariesElement, this.options.modifiers.flip.padding);

      // store the computed placement inside `originalPlacement`
      data.originalPlacement = data.placement;

      data.positionFixed = this.options.positionFixed;

      // compute the popper offsets
      data.offsets.popper = getPopperOffsets(this.popper, data.offsets.reference, data.placement);

      data.offsets.popper.position = this.options.positionFixed ? 'fixed' : 'absolute';

      // run the modifiers
      data = runModifiers(this.modifiers, data);

      // the first `update` will call `onCreate` callback
      // the other ones will call `onUpdate` callback
      if (!this.state.isCreated) {
        this.state.isCreated = true;
        this.options.onCreate(data);
      } else {
        this.options.onUpdate(data);
      }
    }

    /**
     * Helper used to know if the given modifier is enabled.
     * @method
     * @memberof Popper.Utils
     * @returns {Boolean}
     */
    function isModifierEnabled(modifiers, modifierName) {
      return modifiers.some(function (_ref) {
        var name = _ref.name,
            enabled = _ref.enabled;
        return enabled && name === modifierName;
      });
    }

    /**
     * Get the prefixed supported property name
     * @method
     * @memberof Popper.Utils
     * @argument {String} property (camelCase)
     * @returns {String} prefixed property (camelCase or PascalCase, depending on the vendor prefix)
     */
    function getSupportedPropertyName(property) {
      var prefixes = [false, 'ms', 'Webkit', 'Moz', 'O'];
      var upperProp = property.charAt(0).toUpperCase() + property.slice(1);

      for (var i = 0; i < prefixes.length; i++) {
        var prefix = prefixes[i];
        var toCheck = prefix ? '' + prefix + upperProp : property;
        if (typeof document.body.style[toCheck] !== 'undefined') {
          return toCheck;
        }
      }
      return null;
    }

    /**
     * Destroys the popper.
     * @method
     * @memberof Popper
     */
    function destroy() {
      this.state.isDestroyed = true;

      // touch DOM only if `applyStyle` modifier is enabled
      if (isModifierEnabled(this.modifiers, 'applyStyle')) {
        this.popper.removeAttribute('x-placement');
        this.popper.style.position = '';
        this.popper.style.top = '';
        this.popper.style.left = '';
        this.popper.style.right = '';
        this.popper.style.bottom = '';
        this.popper.style.willChange = '';
        this.popper.style[getSupportedPropertyName('transform')] = '';
      }

      this.disableEventListeners();

      // remove the popper if user explicity asked for the deletion on destroy
      // do not use `remove` because IE11 doesn't support it
      if (this.options.removeOnDestroy) {
        this.popper.parentNode.removeChild(this.popper);
      }
      return this;
    }

    /**
     * Get the window associated with the element
     * @argument {Element} element
     * @returns {Window}
     */
    function getWindow(element) {
      var ownerDocument = element.ownerDocument;
      return ownerDocument ? ownerDocument.defaultView : window;
    }

    function attachToScrollParents(scrollParent, event, callback, scrollParents) {
      var isBody = scrollParent.nodeName === 'BODY';
      var target = isBody ? scrollParent.ownerDocument.defaultView : scrollParent;
      target.addEventListener(event, callback, { passive: true });

      if (!isBody) {
        attachToScrollParents(getScrollParent(target.parentNode), event, callback, scrollParents);
      }
      scrollParents.push(target);
    }

    /**
     * Setup needed event listeners used to update the popper position
     * @method
     * @memberof Popper.Utils
     * @private
     */
    function setupEventListeners(reference, options, state, updateBound) {
      // Resize event listener on window
      state.updateBound = updateBound;
      getWindow(reference).addEventListener('resize', state.updateBound, { passive: true });

      // Scroll event listener on scroll parents
      var scrollElement = getScrollParent(reference);
      attachToScrollParents(scrollElement, 'scroll', state.updateBound, state.scrollParents);
      state.scrollElement = scrollElement;
      state.eventsEnabled = true;

      return state;
    }

    /**
     * It will add resize/scroll events and start recalculating
     * position of the popper element when they are triggered.
     * @method
     * @memberof Popper
     */
    function enableEventListeners() {
      if (!this.state.eventsEnabled) {
        this.state = setupEventListeners(this.reference, this.options, this.state, this.scheduleUpdate);
      }
    }

    /**
     * Remove event listeners used to update the popper position
     * @method
     * @memberof Popper.Utils
     * @private
     */
    function removeEventListeners(reference, state) {
      // Remove resize event listener on window
      getWindow(reference).removeEventListener('resize', state.updateBound);

      // Remove scroll event listener on scroll parents
      state.scrollParents.forEach(function (target) {
        target.removeEventListener('scroll', state.updateBound);
      });

      // Reset state
      state.updateBound = null;
      state.scrollParents = [];
      state.scrollElement = null;
      state.eventsEnabled = false;
      return state;
    }

    /**
     * It will remove resize/scroll events and won't recalculate popper position
     * when they are triggered. It also won't trigger `onUpdate` callback anymore,
     * unless you call `update` method manually.
     * @method
     * @memberof Popper
     */
    function disableEventListeners() {
      if (this.state.eventsEnabled) {
        cancelAnimationFrame(this.scheduleUpdate);
        this.state = removeEventListeners(this.reference, this.state);
      }
    }

    /**
     * Tells if a given input is a number
     * @method
     * @memberof Popper.Utils
     * @param {*} input to check
     * @return {Boolean}
     */
    function isNumeric(n) {
      return n !== '' && !isNaN(parseFloat(n)) && isFinite(n);
    }

    /**
     * Set the style to the given popper
     * @method
     * @memberof Popper.Utils
     * @argument {Element} element - Element to apply the style to
     * @argument {Object} styles
     * Object with a list of properties and values which will be applied to the element
     */
    function setStyles(element, styles) {
      Object.keys(styles).forEach(function (prop) {
        var unit = '';
        // add unit if the value is numeric and is one of the following
        if (['width', 'height', 'top', 'right', 'bottom', 'left'].indexOf(prop) !== -1 && isNumeric(styles[prop])) {
          unit = 'px';
        }
        element.style[prop] = styles[prop] + unit;
      });
    }

    /**
     * Set the attributes to the given popper
     * @method
     * @memberof Popper.Utils
     * @argument {Element} element - Element to apply the attributes to
     * @argument {Object} styles
     * Object with a list of properties and values which will be applied to the element
     */
    function setAttributes(element, attributes) {
      Object.keys(attributes).forEach(function (prop) {
        var value = attributes[prop];
        if (value !== false) {
          element.setAttribute(prop, attributes[prop]);
        } else {
          element.removeAttribute(prop);
        }
      });
    }

    /**
     * @function
     * @memberof Modifiers
     * @argument {Object} data - The data object generated by `update` method
     * @argument {Object} data.styles - List of style properties - values to apply to popper element
     * @argument {Object} data.attributes - List of attribute properties - values to apply to popper element
     * @argument {Object} options - Modifiers configuration and options
     * @returns {Object} The same data object
     */
    function applyStyle(data) {
      // any property present in `data.styles` will be applied to the popper,
      // in this way we can make the 3rd party modifiers add custom styles to it
      // Be aware, modifiers could override the properties defined in the previous
      // lines of this modifier!
      setStyles(data.instance.popper, data.styles);

      // any property present in `data.attributes` will be applied to the popper,
      // they will be set as HTML attributes of the element
      setAttributes(data.instance.popper, data.attributes);

      // if arrowElement is defined and arrowStyles has some properties
      if (data.arrowElement && Object.keys(data.arrowStyles).length) {
        setStyles(data.arrowElement, data.arrowStyles);
      }

      return data;
    }

    /**
     * Set the x-placement attribute before everything else because it could be used
     * to add margins to the popper margins needs to be calculated to get the
     * correct popper offsets.
     * @method
     * @memberof Popper.modifiers
     * @param {HTMLElement} reference - The reference element used to position the popper
     * @param {HTMLElement} popper - The HTML element used as popper
     * @param {Object} options - Popper.js options
     */
    function applyStyleOnLoad(reference, popper, options, modifierOptions, state) {
      // compute reference element offsets
      var referenceOffsets = getReferenceOffsets(state, popper, reference, options.positionFixed);

      // compute auto placement, store placement inside the data object,
      // modifiers will be able to edit `placement` if needed
      // and refer to originalPlacement to know the original value
      var placement = computeAutoPlacement(options.placement, referenceOffsets, popper, reference, options.modifiers.flip.boundariesElement, options.modifiers.flip.padding);

      popper.setAttribute('x-placement', placement);

      // Apply `position` to popper before anything else because
      // without the position applied we can't guarantee correct computations
      setStyles(popper, { position: options.positionFixed ? 'fixed' : 'absolute' });

      return options;
    }

    /**
     * @function
     * @memberof Popper.Utils
     * @argument {Object} data - The data object generated by `update` method
     * @argument {Boolean} shouldRound - If the offsets should be rounded at all
     * @returns {Object} The popper's position offsets rounded
     *
     * The tale of pixel-perfect positioning. It's still not 100% perfect, but as
     * good as it can be within reason.
     * Discussion here: https://github.com/FezVrasta/popper.js/pull/715
     *
     * Low DPI screens cause a popper to be blurry if not using full pixels (Safari
     * as well on High DPI screens).
     *
     * Firefox prefers no rounding for positioning and does not have blurriness on
     * high DPI screens.
     *
     * Only horizontal placement and left/right values need to be considered.
     */
    function getRoundedOffsets(data, shouldRound) {
      var _data$offsets = data.offsets,
          popper = _data$offsets.popper,
          reference = _data$offsets.reference;
      var round = Math.round,
          floor = Math.floor;

      var noRound = function noRound(v) {
        return v;
      };

      var referenceWidth = round(reference.width);
      var popperWidth = round(popper.width);

      var isVertical = ['left', 'right'].indexOf(data.placement) !== -1;
      var isVariation = data.placement.indexOf('-') !== -1;
      var sameWidthParity = referenceWidth % 2 === popperWidth % 2;
      var bothOddWidth = referenceWidth % 2 === 1 && popperWidth % 2 === 1;

      var horizontalToInteger = !shouldRound ? noRound : isVertical || isVariation || sameWidthParity ? round : floor;
      var verticalToInteger = !shouldRound ? noRound : round;

      return {
        left: horizontalToInteger(bothOddWidth && !isVariation && shouldRound ? popper.left - 1 : popper.left),
        top: verticalToInteger(popper.top),
        bottom: verticalToInteger(popper.bottom),
        right: horizontalToInteger(popper.right)
      };
    }

    var isFirefox = isBrowser && /Firefox/i.test(navigator.userAgent);

    /**
     * @function
     * @memberof Modifiers
     * @argument {Object} data - The data object generated by `update` method
     * @argument {Object} options - Modifiers configuration and options
     * @returns {Object} The data object, properly modified
     */
    function computeStyle(data, options) {
      var x = options.x,
          y = options.y;
      var popper = data.offsets.popper;

      // Remove this legacy support in Popper.js v2

      var legacyGpuAccelerationOption = find(data.instance.modifiers, function (modifier) {
        return modifier.name === 'applyStyle';
      }).gpuAcceleration;
      if (legacyGpuAccelerationOption !== undefined) {
        console.warn('WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!');
      }
      var gpuAcceleration = legacyGpuAccelerationOption !== undefined ? legacyGpuAccelerationOption : options.gpuAcceleration;

      var offsetParent = getOffsetParent(data.instance.popper);
      var offsetParentRect = getBoundingClientRect(offsetParent);

      // Styles
      var styles = {
        position: popper.position
      };

      var offsets = getRoundedOffsets(data, window.devicePixelRatio < 2 || !isFirefox);

      var sideA = x === 'bottom' ? 'top' : 'bottom';
      var sideB = y === 'right' ? 'left' : 'right';

      // if gpuAcceleration is set to `true` and transform is supported,
      //  we use `translate3d` to apply the position to the popper we
      // automatically use the supported prefixed version if needed
      var prefixedProperty = getSupportedPropertyName('transform');

      // now, let's make a step back and look at this code closely (wtf?)
      // If the content of the popper grows once it's been positioned, it
      // may happen that the popper gets misplaced because of the new content
      // overflowing its reference element
      // To avoid this problem, we provide two options (x and y), which allow
      // the consumer to define the offset origin.
      // If we position a popper on top of a reference element, we can set
      // `x` to `top` to make the popper grow towards its top instead of
      // its bottom.
      var left = void 0,
          top = void 0;
      if (sideA === 'bottom') {
        // when offsetParent is <html> the positioning is relative to the bottom of the screen (excluding the scrollbar)
        // and not the bottom of the html element
        if (offsetParent.nodeName === 'HTML') {
          top = -offsetParent.clientHeight + offsets.bottom;
        } else {
          top = -offsetParentRect.height + offsets.bottom;
        }
      } else {
        top = offsets.top;
      }
      if (sideB === 'right') {
        if (offsetParent.nodeName === 'HTML') {
          left = -offsetParent.clientWidth + offsets.right;
        } else {
          left = -offsetParentRect.width + offsets.right;
        }
      } else {
        left = offsets.left;
      }
      if (gpuAcceleration && prefixedProperty) {
        styles[prefixedProperty] = 'translate3d(' + left + 'px, ' + top + 'px, 0)';
        styles[sideA] = 0;
        styles[sideB] = 0;
        styles.willChange = 'transform';
      } else {
        // othwerise, we use the standard `top`, `left`, `bottom` and `right` properties
        var invertTop = sideA === 'bottom' ? -1 : 1;
        var invertLeft = sideB === 'right' ? -1 : 1;
        styles[sideA] = top * invertTop;
        styles[sideB] = left * invertLeft;
        styles.willChange = sideA + ', ' + sideB;
      }

      // Attributes
      var attributes = {
        'x-placement': data.placement
      };

      // Update `data` attributes, styles and arrowStyles
      data.attributes = _extends({}, attributes, data.attributes);
      data.styles = _extends({}, styles, data.styles);
      data.arrowStyles = _extends({}, data.offsets.arrow, data.arrowStyles);

      return data;
    }

    /**
     * Helper used to know if the given modifier depends from another one.<br />
     * It checks if the needed modifier is listed and enabled.
     * @method
     * @memberof Popper.Utils
     * @param {Array} modifiers - list of modifiers
     * @param {String} requestingName - name of requesting modifier
     * @param {String} requestedName - name of requested modifier
     * @returns {Boolean}
     */
    function isModifierRequired(modifiers, requestingName, requestedName) {
      var requesting = find(modifiers, function (_ref) {
        var name = _ref.name;
        return name === requestingName;
      });

      var isRequired = !!requesting && modifiers.some(function (modifier) {
        return modifier.name === requestedName && modifier.enabled && modifier.order < requesting.order;
      });

      if (!isRequired) {
        var _requesting = '`' + requestingName + '`';
        var requested = '`' + requestedName + '`';
        console.warn(requested + ' modifier is required by ' + _requesting + ' modifier in order to work, be sure to include it before ' + _requesting + '!');
      }
      return isRequired;
    }

    /**
     * @function
     * @memberof Modifiers
     * @argument {Object} data - The data object generated by update method
     * @argument {Object} options - Modifiers configuration and options
     * @returns {Object} The data object, properly modified
     */
    function arrow(data, options) {
      var _data$offsets$arrow;

      // arrow depends on keepTogether in order to work
      if (!isModifierRequired(data.instance.modifiers, 'arrow', 'keepTogether')) {
        return data;
      }

      var arrowElement = options.element;

      // if arrowElement is a string, suppose it's a CSS selector
      if (typeof arrowElement === 'string') {
        arrowElement = data.instance.popper.querySelector(arrowElement);

        // if arrowElement is not found, don't run the modifier
        if (!arrowElement) {
          return data;
        }
      } else {
        // if the arrowElement isn't a query selector we must check that the
        // provided DOM node is child of its popper node
        if (!data.instance.popper.contains(arrowElement)) {
          console.warn('WARNING: `arrow.element` must be child of its popper element!');
          return data;
        }
      }

      var placement = data.placement.split('-')[0];
      var _data$offsets = data.offsets,
          popper = _data$offsets.popper,
          reference = _data$offsets.reference;

      var isVertical = ['left', 'right'].indexOf(placement) !== -1;

      var len = isVertical ? 'height' : 'width';
      var sideCapitalized = isVertical ? 'Top' : 'Left';
      var side = sideCapitalized.toLowerCase();
      var altSide = isVertical ? 'left' : 'top';
      var opSide = isVertical ? 'bottom' : 'right';
      var arrowElementSize = getOuterSizes(arrowElement)[len];

      //
      // extends keepTogether behavior making sure the popper and its
      // reference have enough pixels in conjunction
      //

      // top/left side
      if (reference[opSide] - arrowElementSize < popper[side]) {
        data.offsets.popper[side] -= popper[side] - (reference[opSide] - arrowElementSize);
      }
      // bottom/right side
      if (reference[side] + arrowElementSize > popper[opSide]) {
        data.offsets.popper[side] += reference[side] + arrowElementSize - popper[opSide];
      }
      data.offsets.popper = getClientRect(data.offsets.popper);

      // compute center of the popper
      var center = reference[side] + reference[len] / 2 - arrowElementSize / 2;

      // Compute the sideValue using the updated popper offsets
      // take popper margin in account because we don't have this info available
      var css = getStyleComputedProperty(data.instance.popper);
      var popperMarginSide = parseFloat(css['margin' + sideCapitalized], 10);
      var popperBorderSide = parseFloat(css['border' + sideCapitalized + 'Width'], 10);
      var sideValue = center - data.offsets.popper[side] - popperMarginSide - popperBorderSide;

      // prevent arrowElement from being placed not contiguously to its popper
      sideValue = Math.max(Math.min(popper[len] - arrowElementSize, sideValue), 0);

      data.arrowElement = arrowElement;
      data.offsets.arrow = (_data$offsets$arrow = {}, defineProperty(_data$offsets$arrow, side, Math.round(sideValue)), defineProperty(_data$offsets$arrow, altSide, ''), _data$offsets$arrow);

      return data;
    }

    /**
     * Get the opposite placement variation of the given one
     * @method
     * @memberof Popper.Utils
     * @argument {String} placement variation
     * @returns {String} flipped placement variation
     */
    function getOppositeVariation(variation) {
      if (variation === 'end') {
        return 'start';
      } else if (variation === 'start') {
        return 'end';
      }
      return variation;
    }

    /**
     * List of accepted placements to use as values of the `placement` option.<br />
     * Valid placements are:
     * - `auto`
     * - `top`
     * - `right`
     * - `bottom`
     * - `left`
     *
     * Each placement can have a variation from this list:
     * - `-start`
     * - `-end`
     *
     * Variations are interpreted easily if you think of them as the left to right
     * written languages. Horizontally (`top` and `bottom`), `start` is left and `end`
     * is right.<br />
     * Vertically (`left` and `right`), `start` is top and `end` is bottom.
     *
     * Some valid examples are:
     * - `top-end` (on top of reference, right aligned)
     * - `right-start` (on right of reference, top aligned)
     * - `bottom` (on bottom, centered)
     * - `auto-end` (on the side with more space available, alignment depends by placement)
     *
     * @static
     * @type {Array}
     * @enum {String}
     * @readonly
     * @method placements
     * @memberof Popper
     */
    var placements = ['auto-start', 'auto', 'auto-end', 'top-start', 'top', 'top-end', 'right-start', 'right', 'right-end', 'bottom-end', 'bottom', 'bottom-start', 'left-end', 'left', 'left-start'];

    // Get rid of `auto` `auto-start` and `auto-end`
    var validPlacements = placements.slice(3);

    /**
     * Given an initial placement, returns all the subsequent placements
     * clockwise (or counter-clockwise).
     *
     * @method
     * @memberof Popper.Utils
     * @argument {String} placement - A valid placement (it accepts variations)
     * @argument {Boolean} counter - Set to true to walk the placements counterclockwise
     * @returns {Array} placements including their variations
     */
    function clockwise(placement) {
      var counter = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      var index = validPlacements.indexOf(placement);
      var arr = validPlacements.slice(index + 1).concat(validPlacements.slice(0, index));
      return counter ? arr.reverse() : arr;
    }

    var BEHAVIORS = {
      FLIP: 'flip',
      CLOCKWISE: 'clockwise',
      COUNTERCLOCKWISE: 'counterclockwise'
    };

    /**
     * @function
     * @memberof Modifiers
     * @argument {Object} data - The data object generated by update method
     * @argument {Object} options - Modifiers configuration and options
     * @returns {Object} The data object, properly modified
     */
    function flip(data, options) {
      // if `inner` modifier is enabled, we can't use the `flip` modifier
      if (isModifierEnabled(data.instance.modifiers, 'inner')) {
        return data;
      }

      if (data.flipped && data.placement === data.originalPlacement) {
        // seems like flip is trying to loop, probably there's not enough space on any of the flippable sides
        return data;
      }

      var boundaries = getBoundaries(data.instance.popper, data.instance.reference, options.padding, options.boundariesElement, data.positionFixed);

      var placement = data.placement.split('-')[0];
      var placementOpposite = getOppositePlacement(placement);
      var variation = data.placement.split('-')[1] || '';

      var flipOrder = [];

      switch (options.behavior) {
        case BEHAVIORS.FLIP:
          flipOrder = [placement, placementOpposite];
          break;
        case BEHAVIORS.CLOCKWISE:
          flipOrder = clockwise(placement);
          break;
        case BEHAVIORS.COUNTERCLOCKWISE:
          flipOrder = clockwise(placement, true);
          break;
        default:
          flipOrder = options.behavior;
      }

      flipOrder.forEach(function (step, index) {
        if (placement !== step || flipOrder.length === index + 1) {
          return data;
        }

        placement = data.placement.split('-')[0];
        placementOpposite = getOppositePlacement(placement);

        var popperOffsets = data.offsets.popper;
        var refOffsets = data.offsets.reference;

        // using floor because the reference offsets may contain decimals we are not going to consider here
        var floor = Math.floor;
        var overlapsRef = placement === 'left' && floor(popperOffsets.right) > floor(refOffsets.left) || placement === 'right' && floor(popperOffsets.left) < floor(refOffsets.right) || placement === 'top' && floor(popperOffsets.bottom) > floor(refOffsets.top) || placement === 'bottom' && floor(popperOffsets.top) < floor(refOffsets.bottom);

        var overflowsLeft = floor(popperOffsets.left) < floor(boundaries.left);
        var overflowsRight = floor(popperOffsets.right) > floor(boundaries.right);
        var overflowsTop = floor(popperOffsets.top) < floor(boundaries.top);
        var overflowsBottom = floor(popperOffsets.bottom) > floor(boundaries.bottom);

        var overflowsBoundaries = placement === 'left' && overflowsLeft || placement === 'right' && overflowsRight || placement === 'top' && overflowsTop || placement === 'bottom' && overflowsBottom;

        // flip the variation if required
        var isVertical = ['top', 'bottom'].indexOf(placement) !== -1;

        // flips variation if reference element overflows boundaries
        var flippedVariationByRef = !!options.flipVariations && (isVertical && variation === 'start' && overflowsLeft || isVertical && variation === 'end' && overflowsRight || !isVertical && variation === 'start' && overflowsTop || !isVertical && variation === 'end' && overflowsBottom);

        // flips variation if popper content overflows boundaries
        var flippedVariationByContent = !!options.flipVariationsByContent && (isVertical && variation === 'start' && overflowsRight || isVertical && variation === 'end' && overflowsLeft || !isVertical && variation === 'start' && overflowsBottom || !isVertical && variation === 'end' && overflowsTop);

        var flippedVariation = flippedVariationByRef || flippedVariationByContent;

        if (overlapsRef || overflowsBoundaries || flippedVariation) {
          // this boolean to detect any flip loop
          data.flipped = true;

          if (overlapsRef || overflowsBoundaries) {
            placement = flipOrder[index + 1];
          }

          if (flippedVariation) {
            variation = getOppositeVariation(variation);
          }

          data.placement = placement + (variation ? '-' + variation : '');

          // this object contains `position`, we want to preserve it along with
          // any additional property we may add in the future
          data.offsets.popper = _extends({}, data.offsets.popper, getPopperOffsets(data.instance.popper, data.offsets.reference, data.placement));

          data = runModifiers(data.instance.modifiers, data, 'flip');
        }
      });
      return data;
    }

    /**
     * @function
     * @memberof Modifiers
     * @argument {Object} data - The data object generated by update method
     * @argument {Object} options - Modifiers configuration and options
     * @returns {Object} The data object, properly modified
     */
    function keepTogether(data) {
      var _data$offsets = data.offsets,
          popper = _data$offsets.popper,
          reference = _data$offsets.reference;

      var placement = data.placement.split('-')[0];
      var floor = Math.floor;
      var isVertical = ['top', 'bottom'].indexOf(placement) !== -1;
      var side = isVertical ? 'right' : 'bottom';
      var opSide = isVertical ? 'left' : 'top';
      var measurement = isVertical ? 'width' : 'height';

      if (popper[side] < floor(reference[opSide])) {
        data.offsets.popper[opSide] = floor(reference[opSide]) - popper[measurement];
      }
      if (popper[opSide] > floor(reference[side])) {
        data.offsets.popper[opSide] = floor(reference[side]);
      }

      return data;
    }

    /**
     * Converts a string containing value + unit into a px value number
     * @function
     * @memberof {modifiers~offset}
     * @private
     * @argument {String} str - Value + unit string
     * @argument {String} measurement - `height` or `width`
     * @argument {Object} popperOffsets
     * @argument {Object} referenceOffsets
     * @returns {Number|String}
     * Value in pixels, or original string if no values were extracted
     */
    function toValue(str, measurement, popperOffsets, referenceOffsets) {
      // separate value from unit
      var split = str.match(/((?:\-|\+)?\d*\.?\d*)(.*)/);
      var value = +split[1];
      var unit = split[2];

      // If it's not a number it's an operator, I guess
      if (!value) {
        return str;
      }

      if (unit.indexOf('%') === 0) {
        var element = void 0;
        switch (unit) {
          case '%p':
            element = popperOffsets;
            break;
          case '%':
          case '%r':
          default:
            element = referenceOffsets;
        }

        var rect = getClientRect(element);
        return rect[measurement] / 100 * value;
      } else if (unit === 'vh' || unit === 'vw') {
        // if is a vh or vw, we calculate the size based on the viewport
        var size = void 0;
        if (unit === 'vh') {
          size = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        } else {
          size = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        }
        return size / 100 * value;
      } else {
        // if is an explicit pixel unit, we get rid of the unit and keep the value
        // if is an implicit unit, it's px, and we return just the value
        return value;
      }
    }

    /**
     * Parse an `offset` string to extrapolate `x` and `y` numeric offsets.
     * @function
     * @memberof {modifiers~offset}
     * @private
     * @argument {String} offset
     * @argument {Object} popperOffsets
     * @argument {Object} referenceOffsets
     * @argument {String} basePlacement
     * @returns {Array} a two cells array with x and y offsets in numbers
     */
    function parseOffset(offset, popperOffsets, referenceOffsets, basePlacement) {
      var offsets = [0, 0];

      // Use height if placement is left or right and index is 0 otherwise use width
      // in this way the first offset will use an axis and the second one
      // will use the other one
      var useHeight = ['right', 'left'].indexOf(basePlacement) !== -1;

      // Split the offset string to obtain a list of values and operands
      // The regex addresses values with the plus or minus sign in front (+10, -20, etc)
      var fragments = offset.split(/(\+|\-)/).map(function (frag) {
        return frag.trim();
      });

      // Detect if the offset string contains a pair of values or a single one
      // they could be separated by comma or space
      var divider = fragments.indexOf(find(fragments, function (frag) {
        return frag.search(/,|\s/) !== -1;
      }));

      if (fragments[divider] && fragments[divider].indexOf(',') === -1) {
        console.warn('Offsets separated by white space(s) are deprecated, use a comma (,) instead.');
      }

      // If divider is found, we divide the list of values and operands to divide
      // them by ofset X and Y.
      var splitRegex = /\s*,\s*|\s+/;
      var ops = divider !== -1 ? [fragments.slice(0, divider).concat([fragments[divider].split(splitRegex)[0]]), [fragments[divider].split(splitRegex)[1]].concat(fragments.slice(divider + 1))] : [fragments];

      // Convert the values with units to absolute pixels to allow our computations
      ops = ops.map(function (op, index) {
        // Most of the units rely on the orientation of the popper
        var measurement = (index === 1 ? !useHeight : useHeight) ? 'height' : 'width';
        var mergeWithPrevious = false;
        return op
        // This aggregates any `+` or `-` sign that aren't considered operators
        // e.g.: 10 + +5 => [10, +, +5]
        .reduce(function (a, b) {
          if (a[a.length - 1] === '' && ['+', '-'].indexOf(b) !== -1) {
            a[a.length - 1] = b;
            mergeWithPrevious = true;
            return a;
          } else if (mergeWithPrevious) {
            a[a.length - 1] += b;
            mergeWithPrevious = false;
            return a;
          } else {
            return a.concat(b);
          }
        }, [])
        // Here we convert the string values into number values (in px)
        .map(function (str) {
          return toValue(str, measurement, popperOffsets, referenceOffsets);
        });
      });

      // Loop trough the offsets arrays and execute the operations
      ops.forEach(function (op, index) {
        op.forEach(function (frag, index2) {
          if (isNumeric(frag)) {
            offsets[index] += frag * (op[index2 - 1] === '-' ? -1 : 1);
          }
        });
      });
      return offsets;
    }

    /**
     * @function
     * @memberof Modifiers
     * @argument {Object} data - The data object generated by update method
     * @argument {Object} options - Modifiers configuration and options
     * @argument {Number|String} options.offset=0
     * The offset value as described in the modifier description
     * @returns {Object} The data object, properly modified
     */
    function offset(data, _ref) {
      var offset = _ref.offset;
      var placement = data.placement,
          _data$offsets = data.offsets,
          popper = _data$offsets.popper,
          reference = _data$offsets.reference;

      var basePlacement = placement.split('-')[0];

      var offsets = void 0;
      if (isNumeric(+offset)) {
        offsets = [+offset, 0];
      } else {
        offsets = parseOffset(offset, popper, reference, basePlacement);
      }

      if (basePlacement === 'left') {
        popper.top += offsets[0];
        popper.left -= offsets[1];
      } else if (basePlacement === 'right') {
        popper.top += offsets[0];
        popper.left += offsets[1];
      } else if (basePlacement === 'top') {
        popper.left += offsets[0];
        popper.top -= offsets[1];
      } else if (basePlacement === 'bottom') {
        popper.left += offsets[0];
        popper.top += offsets[1];
      }

      data.popper = popper;
      return data;
    }

    /**
     * @function
     * @memberof Modifiers
     * @argument {Object} data - The data object generated by `update` method
     * @argument {Object} options - Modifiers configuration and options
     * @returns {Object} The data object, properly modified
     */
    function preventOverflow(data, options) {
      var boundariesElement = options.boundariesElement || getOffsetParent(data.instance.popper);

      // If offsetParent is the reference element, we really want to
      // go one step up and use the next offsetParent as reference to
      // avoid to make this modifier completely useless and look like broken
      if (data.instance.reference === boundariesElement) {
        boundariesElement = getOffsetParent(boundariesElement);
      }

      // NOTE: DOM access here
      // resets the popper's position so that the document size can be calculated excluding
      // the size of the popper element itself
      var transformProp = getSupportedPropertyName('transform');
      var popperStyles = data.instance.popper.style; // assignment to help minification
      var top = popperStyles.top,
          left = popperStyles.left,
          transform = popperStyles[transformProp];

      popperStyles.top = '';
      popperStyles.left = '';
      popperStyles[transformProp] = '';

      var boundaries = getBoundaries(data.instance.popper, data.instance.reference, options.padding, boundariesElement, data.positionFixed);

      // NOTE: DOM access here
      // restores the original style properties after the offsets have been computed
      popperStyles.top = top;
      popperStyles.left = left;
      popperStyles[transformProp] = transform;

      options.boundaries = boundaries;

      var order = options.priority;
      var popper = data.offsets.popper;

      var check = {
        primary: function primary(placement) {
          var value = popper[placement];
          if (popper[placement] < boundaries[placement] && !options.escapeWithReference) {
            value = Math.max(popper[placement], boundaries[placement]);
          }
          return defineProperty({}, placement, value);
        },
        secondary: function secondary(placement) {
          var mainSide = placement === 'right' ? 'left' : 'top';
          var value = popper[mainSide];
          if (popper[placement] > boundaries[placement] && !options.escapeWithReference) {
            value = Math.min(popper[mainSide], boundaries[placement] - (placement === 'right' ? popper.width : popper.height));
          }
          return defineProperty({}, mainSide, value);
        }
      };

      order.forEach(function (placement) {
        var side = ['left', 'top'].indexOf(placement) !== -1 ? 'primary' : 'secondary';
        popper = _extends({}, popper, check[side](placement));
      });

      data.offsets.popper = popper;

      return data;
    }

    /**
     * @function
     * @memberof Modifiers
     * @argument {Object} data - The data object generated by `update` method
     * @argument {Object} options - Modifiers configuration and options
     * @returns {Object} The data object, properly modified
     */
    function shift(data) {
      var placement = data.placement;
      var basePlacement = placement.split('-')[0];
      var shiftvariation = placement.split('-')[1];

      // if shift shiftvariation is specified, run the modifier
      if (shiftvariation) {
        var _data$offsets = data.offsets,
            reference = _data$offsets.reference,
            popper = _data$offsets.popper;

        var isVertical = ['bottom', 'top'].indexOf(basePlacement) !== -1;
        var side = isVertical ? 'left' : 'top';
        var measurement = isVertical ? 'width' : 'height';

        var shiftOffsets = {
          start: defineProperty({}, side, reference[side]),
          end: defineProperty({}, side, reference[side] + reference[measurement] - popper[measurement])
        };

        data.offsets.popper = _extends({}, popper, shiftOffsets[shiftvariation]);
      }

      return data;
    }

    /**
     * @function
     * @memberof Modifiers
     * @argument {Object} data - The data object generated by update method
     * @argument {Object} options - Modifiers configuration and options
     * @returns {Object} The data object, properly modified
     */
    function hide(data) {
      if (!isModifierRequired(data.instance.modifiers, 'hide', 'preventOverflow')) {
        return data;
      }

      var refRect = data.offsets.reference;
      var bound = find(data.instance.modifiers, function (modifier) {
        return modifier.name === 'preventOverflow';
      }).boundaries;

      if (refRect.bottom < bound.top || refRect.left > bound.right || refRect.top > bound.bottom || refRect.right < bound.left) {
        // Avoid unnecessary DOM access if visibility hasn't changed
        if (data.hide === true) {
          return data;
        }

        data.hide = true;
        data.attributes['x-out-of-boundaries'] = '';
      } else {
        // Avoid unnecessary DOM access if visibility hasn't changed
        if (data.hide === false) {
          return data;
        }

        data.hide = false;
        data.attributes['x-out-of-boundaries'] = false;
      }

      return data;
    }

    /**
     * @function
     * @memberof Modifiers
     * @argument {Object} data - The data object generated by `update` method
     * @argument {Object} options - Modifiers configuration and options
     * @returns {Object} The data object, properly modified
     */
    function inner(data) {
      var placement = data.placement;
      var basePlacement = placement.split('-')[0];
      var _data$offsets = data.offsets,
          popper = _data$offsets.popper,
          reference = _data$offsets.reference;

      var isHoriz = ['left', 'right'].indexOf(basePlacement) !== -1;

      var subtractLength = ['top', 'left'].indexOf(basePlacement) === -1;

      popper[isHoriz ? 'left' : 'top'] = reference[basePlacement] - (subtractLength ? popper[isHoriz ? 'width' : 'height'] : 0);

      data.placement = getOppositePlacement(placement);
      data.offsets.popper = getClientRect(popper);

      return data;
    }

    /**
     * Modifier function, each modifier can have a function of this type assigned
     * to its `fn` property.<br />
     * These functions will be called on each update, this means that you must
     * make sure they are performant enough to avoid performance bottlenecks.
     *
     * @function ModifierFn
     * @argument {dataObject} data - The data object generated by `update` method
     * @argument {Object} options - Modifiers configuration and options
     * @returns {dataObject} The data object, properly modified
     */

    /**
     * Modifiers are plugins used to alter the behavior of your poppers.<br />
     * Popper.js uses a set of 9 modifiers to provide all the basic functionalities
     * needed by the library.
     *
     * Usually you don't want to override the `order`, `fn` and `onLoad` props.
     * All the other properties are configurations that could be tweaked.
     * @namespace modifiers
     */
    var modifiers = {
      /**
       * Modifier used to shift the popper on the start or end of its reference
       * element.<br />
       * It will read the variation of the `placement` property.<br />
       * It can be one either `-end` or `-start`.
       * @memberof modifiers
       * @inner
       */
      shift: {
        /** @prop {number} order=100 - Index used to define the order of execution */
        order: 100,
        /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
        enabled: true,
        /** @prop {ModifierFn} */
        fn: shift
      },

      /**
       * The `offset` modifier can shift your popper on both its axis.
       *
       * It accepts the following units:
       * - `px` or unit-less, interpreted as pixels
       * - `%` or `%r`, percentage relative to the length of the reference element
       * - `%p`, percentage relative to the length of the popper element
       * - `vw`, CSS viewport width unit
       * - `vh`, CSS viewport height unit
       *
       * For length is intended the main axis relative to the placement of the popper.<br />
       * This means that if the placement is `top` or `bottom`, the length will be the
       * `width`. In case of `left` or `right`, it will be the `height`.
       *
       * You can provide a single value (as `Number` or `String`), or a pair of values
       * as `String` divided by a comma or one (or more) white spaces.<br />
       * The latter is a deprecated method because it leads to confusion and will be
       * removed in v2.<br />
       * Additionally, it accepts additions and subtractions between different units.
       * Note that multiplications and divisions aren't supported.
       *
       * Valid examples are:
       * ```
       * 10
       * '10%'
       * '10, 10'
       * '10%, 10'
       * '10 + 10%'
       * '10 - 5vh + 3%'
       * '-10px + 5vh, 5px - 6%'
       * ```
       * > **NB**: If you desire to apply offsets to your poppers in a way that may make them overlap
       * > with their reference element, unfortunately, you will have to disable the `flip` modifier.
       * > You can read more on this at this [issue](https://github.com/FezVrasta/popper.js/issues/373).
       *
       * @memberof modifiers
       * @inner
       */
      offset: {
        /** @prop {number} order=200 - Index used to define the order of execution */
        order: 200,
        /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
        enabled: true,
        /** @prop {ModifierFn} */
        fn: offset,
        /** @prop {Number|String} offset=0
         * The offset value as described in the modifier description
         */
        offset: 0
      },

      /**
       * Modifier used to prevent the popper from being positioned outside the boundary.
       *
       * A scenario exists where the reference itself is not within the boundaries.<br />
       * We can say it has "escaped the boundaries" — or just "escaped".<br />
       * In this case we need to decide whether the popper should either:
       *
       * - detach from the reference and remain "trapped" in the boundaries, or
       * - if it should ignore the boundary and "escape with its reference"
       *
       * When `escapeWithReference` is set to`true` and reference is completely
       * outside its boundaries, the popper will overflow (or completely leave)
       * the boundaries in order to remain attached to the edge of the reference.
       *
       * @memberof modifiers
       * @inner
       */
      preventOverflow: {
        /** @prop {number} order=300 - Index used to define the order of execution */
        order: 300,
        /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
        enabled: true,
        /** @prop {ModifierFn} */
        fn: preventOverflow,
        /**
         * @prop {Array} [priority=['left','right','top','bottom']]
         * Popper will try to prevent overflow following these priorities by default,
         * then, it could overflow on the left and on top of the `boundariesElement`
         */
        priority: ['left', 'right', 'top', 'bottom'],
        /**
         * @prop {number} padding=5
         * Amount of pixel used to define a minimum distance between the boundaries
         * and the popper. This makes sure the popper always has a little padding
         * between the edges of its container
         */
        padding: 5,
        /**
         * @prop {String|HTMLElement} boundariesElement='scrollParent'
         * Boundaries used by the modifier. Can be `scrollParent`, `window`,
         * `viewport` or any DOM element.
         */
        boundariesElement: 'scrollParent'
      },

      /**
       * Modifier used to make sure the reference and its popper stay near each other
       * without leaving any gap between the two. Especially useful when the arrow is
       * enabled and you want to ensure that it points to its reference element.
       * It cares only about the first axis. You can still have poppers with margin
       * between the popper and its reference element.
       * @memberof modifiers
       * @inner
       */
      keepTogether: {
        /** @prop {number} order=400 - Index used to define the order of execution */
        order: 400,
        /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
        enabled: true,
        /** @prop {ModifierFn} */
        fn: keepTogether
      },

      /**
       * This modifier is used to move the `arrowElement` of the popper to make
       * sure it is positioned between the reference element and its popper element.
       * It will read the outer size of the `arrowElement` node to detect how many
       * pixels of conjunction are needed.
       *
       * It has no effect if no `arrowElement` is provided.
       * @memberof modifiers
       * @inner
       */
      arrow: {
        /** @prop {number} order=500 - Index used to define the order of execution */
        order: 500,
        /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
        enabled: true,
        /** @prop {ModifierFn} */
        fn: arrow,
        /** @prop {String|HTMLElement} element='[x-arrow]' - Selector or node used as arrow */
        element: '[x-arrow]'
      },

      /**
       * Modifier used to flip the popper's placement when it starts to overlap its
       * reference element.
       *
       * Requires the `preventOverflow` modifier before it in order to work.
       *
       * **NOTE:** this modifier will interrupt the current update cycle and will
       * restart it if it detects the need to flip the placement.
       * @memberof modifiers
       * @inner
       */
      flip: {
        /** @prop {number} order=600 - Index used to define the order of execution */
        order: 600,
        /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
        enabled: true,
        /** @prop {ModifierFn} */
        fn: flip,
        /**
         * @prop {String|Array} behavior='flip'
         * The behavior used to change the popper's placement. It can be one of
         * `flip`, `clockwise`, `counterclockwise` or an array with a list of valid
         * placements (with optional variations)
         */
        behavior: 'flip',
        /**
         * @prop {number} padding=5
         * The popper will flip if it hits the edges of the `boundariesElement`
         */
        padding: 5,
        /**
         * @prop {String|HTMLElement} boundariesElement='viewport'
         * The element which will define the boundaries of the popper position.
         * The popper will never be placed outside of the defined boundaries
         * (except if `keepTogether` is enabled)
         */
        boundariesElement: 'viewport',
        /**
         * @prop {Boolean} flipVariations=false
         * The popper will switch placement variation between `-start` and `-end` when
         * the reference element overlaps its boundaries.
         *
         * The original placement should have a set variation.
         */
        flipVariations: false,
        /**
         * @prop {Boolean} flipVariationsByContent=false
         * The popper will switch placement variation between `-start` and `-end` when
         * the popper element overlaps its reference boundaries.
         *
         * The original placement should have a set variation.
         */
        flipVariationsByContent: false
      },

      /**
       * Modifier used to make the popper flow toward the inner of the reference element.
       * By default, when this modifier is disabled, the popper will be placed outside
       * the reference element.
       * @memberof modifiers
       * @inner
       */
      inner: {
        /** @prop {number} order=700 - Index used to define the order of execution */
        order: 700,
        /** @prop {Boolean} enabled=false - Whether the modifier is enabled or not */
        enabled: false,
        /** @prop {ModifierFn} */
        fn: inner
      },

      /**
       * Modifier used to hide the popper when its reference element is outside of the
       * popper boundaries. It will set a `x-out-of-boundaries` attribute which can
       * be used to hide with a CSS selector the popper when its reference is
       * out of boundaries.
       *
       * Requires the `preventOverflow` modifier before it in order to work.
       * @memberof modifiers
       * @inner
       */
      hide: {
        /** @prop {number} order=800 - Index used to define the order of execution */
        order: 800,
        /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
        enabled: true,
        /** @prop {ModifierFn} */
        fn: hide
      },

      /**
       * Computes the style that will be applied to the popper element to gets
       * properly positioned.
       *
       * Note that this modifier will not touch the DOM, it just prepares the styles
       * so that `applyStyle` modifier can apply it. This separation is useful
       * in case you need to replace `applyStyle` with a custom implementation.
       *
       * This modifier has `850` as `order` value to maintain backward compatibility
       * with previous versions of Popper.js. Expect the modifiers ordering method
       * to change in future major versions of the library.
       *
       * @memberof modifiers
       * @inner
       */
      computeStyle: {
        /** @prop {number} order=850 - Index used to define the order of execution */
        order: 850,
        /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
        enabled: true,
        /** @prop {ModifierFn} */
        fn: computeStyle,
        /**
         * @prop {Boolean} gpuAcceleration=true
         * If true, it uses the CSS 3D transformation to position the popper.
         * Otherwise, it will use the `top` and `left` properties
         */
        gpuAcceleration: true,
        /**
         * @prop {string} [x='bottom']
         * Where to anchor the X axis (`bottom` or `top`). AKA X offset origin.
         * Change this if your popper should grow in a direction different from `bottom`
         */
        x: 'bottom',
        /**
         * @prop {string} [x='left']
         * Where to anchor the Y axis (`left` or `right`). AKA Y offset origin.
         * Change this if your popper should grow in a direction different from `right`
         */
        y: 'right'
      },

      /**
       * Applies the computed styles to the popper element.
       *
       * All the DOM manipulations are limited to this modifier. This is useful in case
       * you want to integrate Popper.js inside a framework or view library and you
       * want to delegate all the DOM manipulations to it.
       *
       * Note that if you disable this modifier, you must make sure the popper element
       * has its position set to `absolute` before Popper.js can do its work!
       *
       * Just disable this modifier and define your own to achieve the desired effect.
       *
       * @memberof modifiers
       * @inner
       */
      applyStyle: {
        /** @prop {number} order=900 - Index used to define the order of execution */
        order: 900,
        /** @prop {Boolean} enabled=true - Whether the modifier is enabled or not */
        enabled: true,
        /** @prop {ModifierFn} */
        fn: applyStyle,
        /** @prop {Function} */
        onLoad: applyStyleOnLoad,
        /**
         * @deprecated since version 1.10.0, the property moved to `computeStyle` modifier
         * @prop {Boolean} gpuAcceleration=true
         * If true, it uses the CSS 3D transformation to position the popper.
         * Otherwise, it will use the `top` and `left` properties
         */
        gpuAcceleration: undefined
      }
    };

    /**
     * The `dataObject` is an object containing all the information used by Popper.js.
     * This object is passed to modifiers and to the `onCreate` and `onUpdate` callbacks.
     * @name dataObject
     * @property {Object} data.instance The Popper.js instance
     * @property {String} data.placement Placement applied to popper
     * @property {String} data.originalPlacement Placement originally defined on init
     * @property {Boolean} data.flipped True if popper has been flipped by flip modifier
     * @property {Boolean} data.hide True if the reference element is out of boundaries, useful to know when to hide the popper
     * @property {HTMLElement} data.arrowElement Node used as arrow by arrow modifier
     * @property {Object} data.styles Any CSS property defined here will be applied to the popper. It expects the JavaScript nomenclature (eg. `marginBottom`)
     * @property {Object} data.arrowStyles Any CSS property defined here will be applied to the popper arrow. It expects the JavaScript nomenclature (eg. `marginBottom`)
     * @property {Object} data.boundaries Offsets of the popper boundaries
     * @property {Object} data.offsets The measurements of popper, reference and arrow elements
     * @property {Object} data.offsets.popper `top`, `left`, `width`, `height` values
     * @property {Object} data.offsets.reference `top`, `left`, `width`, `height` values
     * @property {Object} data.offsets.arrow] `top` and `left` offsets, only one of them will be different from 0
     */

    /**
     * Default options provided to Popper.js constructor.<br />
     * These can be overridden using the `options` argument of Popper.js.<br />
     * To override an option, simply pass an object with the same
     * structure of the `options` object, as the 3rd argument. For example:
     * ```
     * new Popper(ref, pop, {
     *   modifiers: {
     *     preventOverflow: { enabled: false }
     *   }
     * })
     * ```
     * @type {Object}
     * @static
     * @memberof Popper
     */
    var Defaults = {
      /**
       * Popper's placement.
       * @prop {Popper.placements} placement='bottom'
       */
      placement: 'bottom',

      /**
       * Set this to true if you want popper to position it self in 'fixed' mode
       * @prop {Boolean} positionFixed=false
       */
      positionFixed: false,

      /**
       * Whether events (resize, scroll) are initially enabled.
       * @prop {Boolean} eventsEnabled=true
       */
      eventsEnabled: true,

      /**
       * Set to true if you want to automatically remove the popper when
       * you call the `destroy` method.
       * @prop {Boolean} removeOnDestroy=false
       */
      removeOnDestroy: false,

      /**
       * Callback called when the popper is created.<br />
       * By default, it is set to no-op.<br />
       * Access Popper.js instance with `data.instance`.
       * @prop {onCreate}
       */
      onCreate: function onCreate() {},

      /**
       * Callback called when the popper is updated. This callback is not called
       * on the initialization/creation of the popper, but only on subsequent
       * updates.<br />
       * By default, it is set to no-op.<br />
       * Access Popper.js instance with `data.instance`.
       * @prop {onUpdate}
       */
      onUpdate: function onUpdate() {},

      /**
       * List of modifiers used to modify the offsets before they are applied to the popper.
       * They provide most of the functionalities of Popper.js.
       * @prop {modifiers}
       */
      modifiers: modifiers
    };

    /**
     * @callback onCreate
     * @param {dataObject} data
     */

    /**
     * @callback onUpdate
     * @param {dataObject} data
     */

    // Utils
    // Methods
    var Popper = function () {
      /**
       * Creates a new Popper.js instance.
       * @class Popper
       * @param {Element|referenceObject} reference - The reference element used to position the popper
       * @param {Element} popper - The HTML / XML element used as the popper
       * @param {Object} options - Your custom options to override the ones defined in [Defaults](#defaults)
       * @return {Object} instance - The generated Popper.js instance
       */
      function Popper(reference, popper) {
        var _this = this;

        var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
        classCallCheck$1(this, Popper);

        this.scheduleUpdate = function () {
          return requestAnimationFrame(_this.update);
        };

        // make update() debounced, so that it only runs at most once-per-tick
        this.update = debounce(this.update.bind(this));

        // with {} we create a new object with the options inside it
        this.options = _extends({}, Popper.Defaults, options);

        // init state
        this.state = {
          isDestroyed: false,
          isCreated: false,
          scrollParents: []
        };

        // get reference and popper elements (allow jQuery wrappers)
        this.reference = reference && reference.jquery ? reference[0] : reference;
        this.popper = popper && popper.jquery ? popper[0] : popper;

        // Deep merge modifiers options
        this.options.modifiers = {};
        Object.keys(_extends({}, Popper.Defaults.modifiers, options.modifiers)).forEach(function (name) {
          _this.options.modifiers[name] = _extends({}, Popper.Defaults.modifiers[name] || {}, options.modifiers ? options.modifiers[name] : {});
        });

        // Refactoring modifiers' list (Object => Array)
        this.modifiers = Object.keys(this.options.modifiers).map(function (name) {
          return _extends({
            name: name
          }, _this.options.modifiers[name]);
        })
        // sort the modifiers by order
        .sort(function (a, b) {
          return a.order - b.order;
        });

        // modifiers have the ability to execute arbitrary code when Popper.js get inited
        // such code is executed in the same order of its modifier
        // they could add new properties to their options configuration
        // BE AWARE: don't add options to `options.modifiers.name` but to `modifierOptions`!
        this.modifiers.forEach(function (modifierOptions) {
          if (modifierOptions.enabled && isFunction(modifierOptions.onLoad)) {
            modifierOptions.onLoad(_this.reference, _this.popper, _this.options, modifierOptions, _this.state);
          }
        });

        // fire the first update to position the popper in the right place
        this.update();

        var eventsEnabled = this.options.eventsEnabled;
        if (eventsEnabled) {
          // setup event listeners, they will take care of update the position in specific situations
          this.enableEventListeners();
        }

        this.state.eventsEnabled = eventsEnabled;
      }

      // We can't use class properties because they don't get listed in the
      // class prototype and break stuff like Sinon stubs


      createClass$1(Popper, [{
        key: 'update',
        value: function update$$1() {
          return update.call(this);
        }
      }, {
        key: 'destroy',
        value: function destroy$$1() {
          return destroy.call(this);
        }
      }, {
        key: 'enableEventListeners',
        value: function enableEventListeners$$1() {
          return enableEventListeners.call(this);
        }
      }, {
        key: 'disableEventListeners',
        value: function disableEventListeners$$1() {
          return disableEventListeners.call(this);
        }

        /**
         * Schedules an update. It will run on the next UI update available.
         * @method scheduleUpdate
         * @memberof Popper
         */


        /**
         * Collection of utilities useful when writing custom modifiers.
         * Starting from version 1.7, this method is available only if you
         * include `popper-utils.js` before `popper.js`.
         *
         * **DEPRECATION**: This way to access PopperUtils is deprecated
         * and will be removed in v2! Use the PopperUtils module directly instead.
         * Due to the high instability of the methods contained in Utils, we can't
         * guarantee them to follow semver. Use them at your own risk!
         * @static
         * @private
         * @type {Object}
         * @deprecated since version 1.8
         * @member Utils
         * @memberof Popper
         */

      }]);
      return Popper;
    }();

    /**
     * The `referenceObject` is an object that provides an interface compatible with Popper.js
     * and lets you use it as replacement of a real DOM node.<br />
     * You can use this method to position a popper relatively to a set of coordinates
     * in case you don't have a DOM node to use as reference.
     *
     * ```
     * new Popper(referenceObject, popperNode);
     * ```
     *
     * NB: This feature isn't supported in Internet Explorer 10.
     * @name referenceObject
     * @property {Function} data.getBoundingClientRect
     * A function that returns a set of coordinates compatible with the native `getBoundingClientRect` method.
     * @property {number} data.clientWidth
     * An ES6 getter that will return the width of the virtual reference element.
     * @property {number} data.clientHeight
     * An ES6 getter that will return the height of the virtual reference element.
     */


    Popper.Utils = (typeof window !== 'undefined' ? window : global).PopperUtils;
    Popper.placements = placements;
    Popper.Defaults = Defaults;

    var rngBrowser = createCommonjsModule(function (module) {
    // Unique ID creation requires a high quality random # generator.  In the
    // browser this is a little complicated due to unknown quality of Math.random()
    // and inconsistent support for the `crypto` API.  We do the best we can via
    // feature-detection

    // getRandomValues needs to be invoked in a context where "this" is a Crypto
    // implementation. Also, find the complete implementation of crypto on IE11.
    var getRandomValues = (typeof(crypto) != 'undefined' && crypto.getRandomValues && crypto.getRandomValues.bind(crypto)) ||
                          (typeof(msCrypto) != 'undefined' && typeof window.msCrypto.getRandomValues == 'function' && msCrypto.getRandomValues.bind(msCrypto));

    if (getRandomValues) {
      // WHATWG crypto RNG - http://wiki.whatwg.org/wiki/Crypto
      var rnds8 = new Uint8Array(16); // eslint-disable-line no-undef

      module.exports = function whatwgRNG() {
        getRandomValues(rnds8);
        return rnds8;
      };
    } else {
      // Math.random()-based (RNG)
      //
      // If all else fails, use Math.random().  It's fast, but is of unspecified
      // quality.
      var rnds = new Array(16);

      module.exports = function mathRNG() {
        for (var i = 0, r; i < 16; i++) {
          if ((i & 0x03) === 0) r = Math.random() * 0x100000000;
          rnds[i] = r >>> ((i & 0x03) << 3) & 0xff;
        }

        return rnds;
      };
    }
    });

    var rngBrowser$1 = /*#__PURE__*/Object.freeze({
        default: rngBrowser,
        __moduleExports: rngBrowser
    });

    /**
     * Convert array of 16 byte values to UUID string format of the form:
     * XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
     */
    var byteToHex = [];
    for (var i$1 = 0; i$1 < 256; ++i$1) {
      byteToHex[i$1] = (i$1 + 0x100).toString(16).substr(1);
    }

    function bytesToUuid(buf, offset) {
      var i = offset || 0;
      var bth = byteToHex;
      // join used to fix memory issue caused by concatenation: https://bugs.chromium.org/p/v8/issues/detail?id=3175#c4
      return ([bth[buf[i++]], bth[buf[i++]], 
    	bth[buf[i++]], bth[buf[i++]], '-',
    	bth[buf[i++]], bth[buf[i++]], '-',
    	bth[buf[i++]], bth[buf[i++]], '-',
    	bth[buf[i++]], bth[buf[i++]], '-',
    	bth[buf[i++]], bth[buf[i++]],
    	bth[buf[i++]], bth[buf[i++]],
    	bth[buf[i++]], bth[buf[i++]]]).join('');
    }

    var bytesToUuid_1 = bytesToUuid;

    var bytesToUuid$1 = /*#__PURE__*/Object.freeze({
        default: bytesToUuid_1,
        __moduleExports: bytesToUuid_1
    });

    var rng = ( rngBrowser$1 && rngBrowser ) || rngBrowser$1;

    var bytesToUuid$2 = ( bytesToUuid$1 && bytesToUuid_1 ) || bytesToUuid$1;

    function v4(options, buf, offset) {
      var i = buf && offset || 0;

      if (typeof(options) == 'string') {
        buf = options === 'binary' ? new Array(16) : null;
        options = null;
      }
      options = options || {};

      var rnds = options.random || (options.rng || rng)();

      // Per 4.4, set bits for version and `clock_seq_hi_and_reserved`
      rnds[6] = (rnds[6] & 0x0f) | 0x40;
      rnds[8] = (rnds[8] & 0x3f) | 0x80;

      // Copy bytes to buffer, if provided
      if (buf) {
        for (var ii = 0; ii < 16; ++ii) {
          buf[i + ii] = rnds[ii];
        }
      }

      return buf || bytesToUuid$2(rnds);
    }

    var v4_1 = v4;

    var dropdownEventBus = new Vue();

    var placements$1 = {
      NORMAL: 'bottom-start',
      ALIGN_RIGHT: 'bottom-end',
      UP_NORMAL: 'top-start',
      UP_ALIGN_RIGHT: 'top-end'
    };
    var dropdownMixin = {
      props: {
        /**
         * Open the dropdown above the parent element
         * @prop {Boolean} dropup
         */
        dropup: {
          type: Boolean,
          default: false
        },

        /**
         * Align the dropdown to the right side of the parent element.
         * @prop {Boolean} alignRight
         */
        alignRight: {
          type: Boolean,
          default: false
        },

        /**
         * Define an offset for rendering the dropdown
         * @prop {Number|String} offset
         */
        offset: {
          type: [Number, String],
          default: undefined
        },

        /**
         * Disable flipping the axis of the dropdown when its content would 
         * normally be cut off by the window.
         * @prop {Boolean} noFlip
         */
        noFlip: {
          type: Boolean,
          default: false
        },

        /**
         * Automatically close the dropdown when an internal item is clicked?
         * @prop {Boolean} closeOnClick
         */
        closeOnClick: {
          type: Boolean,
          default: true
        }
      },
      data: function data() {
        return {
          show: false
        };
      },
      computed: {
        id: function id() {
          return "dropdown-".concat(v4_1());
        },
        placement: function placement() {
          var placement = placements$1.NORMAL;

          if (this.dropup && this.alignRight) {
            // up and right
            placement = placements$1.UP_ALIGN_RIGHT;
          } else if (this.dropup) {
            // up
            placement = placements$1.UP_NORMAL;
          } else if (this.alignRight) {
            // right
            placement = placements$1.ALIGN_RIGHT;
          }

          return placement;
        }
      },
      watch: {
        show: function show(val, old) {
          if (val === old) {
            return;
          }

          if (val) {
            this.$emit('shown');
            this.showDropdown();
          } else {
            this.$emit('hidden');
            this.hideDropdown();
          }
        },
        disabled: function disabled(val, old) {
          if (val === old) {
            return;
          }

          if (val) {
            this.show = false;
          }
        }
      },
      created: function created() {
        // Non-reactive property to hold popover config callback
        this._dropdown = null;
      },
      mounted: function mounted() {
        dropdownEventBus.$on('dropdown::item-click', this.onItemClick);
      },
      beforeDestroy: function beforeDestroy() {
        this.show = false;
        this.hideDropdown();
      },
      methods: {
        /**
         * Toggle the dropdowns visibility
         */
        toggle: function toggle() {
          this.show = !this.show;
        },

        /**
         * Hide the dropdown and handle garbage collection
         */
        hideDropdown: function hideDropdown() {
          if (this._dropdown) {
            this._dropdown.destroy();
          }

          this._dropdown = null;
        },

        /**
         * Show the dropdown
         */
        showDropdown: function showDropdown() {
          // Don't do anything if this is disabled
          if (this.disabled) {
            return;
          } // Make sure the dropdown doesn't already exist


          this.hideDropdown(); // Make the dropdown

          this._dropdown = new Popper(this.$el, this.$refs.dropdown, {
            placement: this.placement,
            modifiers: {
              offset: {
                offset: this.offset || 0
              },
              flip: {
                enabled: !this.noFlip
              }
            }
          });
        },

        /**
         * Handle the dropdown if an `DropdownItem` inside this dropdown is clicked.
         * @param {String} attachedTo a dropdown id to close
         */
        onItemClick: function onItemClick(attachedTo) {
          if (this.closeOnClick) {
            // Ensure the event was intended for this dropdown
            if (attachedTo !== this.id) {
              return;
            } // Hide the dropdown if this is the right dropdown


            this.show = false;
          }
        }
      }
    };

    //
    var script$6 = {
      name: "CommandDropdown",
      components: {
        Icon: Icon
      },
      mixins: [commandMixin, clickoutMixin, dropdownMixin],
      inheritAttrs: false,
      props: {
        // Override Mixin validator to disallow `link`
        type: {
          type: String,
          default: 'secondary',
          validator: function validator(value) {
            return ['secondary', 'success', 'warning', 'info', 'danger', 'primary', 'uared'].indexOf(value) > -1;
          }
        },
        split: {
          type: Boolean,
          default: false
        }
      },
      computed: {
        btnSize: function btnSize() {
          if (this.size === 'large') {
            return 'btn-group-lg';
          } else if (this.size === 'small') {
            return 'btn-group-sm';
          } else {
            return '';
          }
        }
      },
      methods: {
        /**
         * Respond to clicking outside of the component as per the clickout mixin
         */
        onClickOut: function onClickOut() {
          this.show = false;
        }
      }
    };

    var css$3 = ".btn-flex[data-v-601c2620]{display:inline-flex;align-items:center}.command-dropdown__menu[data-v-601c2620]{z-index:1000;min-width:10rem;padding:.5rem 0;margin:.125rem 0;font-size:1rem;color:#292b2c;text-align:left;list-style:none;background-color:#fff;background-clip:padding-box;border:1px solid rgba(0,0,0,.15);border-radius:.25rem}";
    styleInject(css$3);

    /* script */
    const __vue_script__$6 = script$6;
    /* template */
    var __vue_render__$6 = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"btn-group",class:[_vm.btnSize, {'dropup': _vm.dropup}],attrs:{"role":"group"}},[(_vm.split)?_c('button',_vm._g(_vm._b({staticClass:"btn btn-flex",class:[_vm.isOutline + _vm.type, _vm.btnSize, { 'active': _vm.active }],attrs:{"type":"button","disabled":_vm.disabled,"aria-pressed":_vm.active,"aria-expanded":!_vm.disabled && _vm.show}},'button',_vm.$attrs,false),_vm.$listeners),[(_vm.icon)?_c('icon',{class:[{'mr-2': _vm.label}],attrs:{"icon":_vm.icon}}):_vm._e(),_vm._v(" "),_c('div',{staticClass:"btn-label"},[_vm._v("\n            "+_vm._s(_vm.label)+"\n        ")])],1):_vm._e(),_vm._v(" "),_c('button',{ref:"toggle",staticClass:"btn btn-flex dropdown-toggle",class:[_vm.isOutline + _vm.type, _vm.btnSize, {'dropdown-toggle-split': _vm.split }, {'active': _vm.show}],attrs:{"type":"button","disabled":_vm.disabled,"aria-haspopup":"true"},on:{"click":_vm.toggle}},[(!_vm.split && _vm.icon)?_c('icon',{class:[{'mr-2': _vm.label}],attrs:{"icon":_vm.icon}}):_vm._e(),_vm._v(" "),(!_vm.split)?_c('div',{staticClass:"btn-label"},[_vm._v("\n            "+_vm._s(_vm.label)+"\n        ")]):_vm._e()],1),_vm._v(" "),_c('div',{directives:[{name:"show",rawName:"v-show",value:(!_vm.disabled && _vm.show),expression:"!disabled && show"}],ref:"dropdown",staticClass:"command-dropdown__menu"},[_vm._t("default")],2)])};
    var __vue_staticRenderFns__$6 = [];

      /* style */
      const __vue_inject_styles__$6 = undefined;
      /* scoped */
      const __vue_scope_id__$6 = "data-v-601c2620";
      /* module identifier */
      const __vue_module_identifier__$6 = undefined;
      /* functional template */
      const __vue_is_functional_template__$6 = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var CommandDropdown = normalizeComponent_1(
        { render: __vue_render__$6, staticRenderFns: __vue_staticRenderFns__$6 },
        __vue_inject_styles__$6,
        __vue_script__$6,
        __vue_scope_id__$6,
        __vue_is_functional_template__$6,
        __vue_module_identifier__$6,
        undefined,
        undefined
      );

    var script$7 = {
      name: 'DropdownDivider',
      render: function render(h) {
        return h('div', {
          class: ['dropdown-divider']
        });
      }
    };

    /* script */
    const __vue_script__$7 = script$7;

    /* template */

      /* style */
      const __vue_inject_styles__$7 = undefined;
      /* scoped */
      const __vue_scope_id__$7 = undefined;
      /* module identifier */
      const __vue_module_identifier__$7 = undefined;
      /* functional template */
      const __vue_is_functional_template__$7 = undefined;
      /* style inject */
      
      /* style inject SSR */
      

      
      var DropdownDivider = normalizeComponent_1(
        {},
        __vue_inject_styles__$7,
        __vue_script__$7,
        __vue_scope_id__$7,
        __vue_is_functional_template__$7,
        __vue_module_identifier__$7,
        undefined,
        undefined
      );

    var script$8 = {
      name: 'DropdownHeader',
      render: function render(h) {
        return h('h6', {
          class: ['dropdown-header']
        }, this.$slots.default);
      }
    };

    /* script */
    const __vue_script__$8 = script$8;

    /* template */

      /* style */
      const __vue_inject_styles__$8 = undefined;
      /* scoped */
      const __vue_scope_id__$8 = undefined;
      /* module identifier */
      const __vue_module_identifier__$8 = undefined;
      /* functional template */
      const __vue_is_functional_template__$8 = undefined;
      /* style inject */
      
      /* style inject SSR */
      

      
      var DropdownHeader = normalizeComponent_1(
        {},
        __vue_inject_styles__$8,
        __vue_script__$8,
        __vue_scope_id__$8,
        __vue_is_functional_template__$8,
        __vue_module_identifier__$8,
        undefined,
        undefined
      );

    function _defineProperty(obj, key, value) {
      if (key in obj) {
        Object.defineProperty(obj, key, {
          value: value,
          enumerable: true,
          configurable: true,
          writable: true
        });
      } else {
        obj[key] = value;
      }

      return obj;
    }

    var defineProperty$1 = _defineProperty;

    var defineProperty$2 = /*#__PURE__*/Object.freeze({
        default: defineProperty$1,
        __moduleExports: defineProperty$1
    });

    var defineProperty$3 = ( defineProperty$2 && defineProperty$1 ) || defineProperty$2;

    function _objectSpread(target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i] != null ? arguments[i] : {};
        var ownKeys = Object.keys(source);

        if (typeof Object.getOwnPropertySymbols === 'function') {
          ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) {
            return Object.getOwnPropertyDescriptor(source, sym).enumerable;
          }));
        }

        ownKeys.forEach(function (key) {
          defineProperty$3(target, key, source[key]);
        });
      }

      return target;
    }

    var objectSpread = _objectSpread;

    var script$9 = {
      name: 'DropdownItem',
      inheritAttrs: true,
      props: {
        type: {
          type: String,
          default: 'link',
          validator: function validator(value) {
            return ['button', 'router-link', 'link'].indexOf(value) > -1;
          }
        },
        to: {
          type: String,
          default: "#"
        },
        disabled: {
          type: Boolean,
          default: false
        }
      },
      computed: {
        /**
         * Determine the tag to render in html
         * @returns {String}
         */
        tag: function tag() {
          if (this.type === 'button') {
            return 'button';
          } else if (this.type === 'router-link') {
            return 'router-link';
          } else {
            // type === 'link'
            return 'a';
          }
        },

        /**
         * Generate the element attributes
         * @returns {Object}
         */
        attributes: function attributes() {
          var result = {
            disabled: this.disabled,
            role: 'menuItem'
          };

          if (this.type === 'link') {
            result['href'] = this.to;
          } else if (this.type === 'router-link') {
            result['to'] = this.to;
          }

          return result;
        }
      },
      methods: {
        onClick: function onClick() {
          if (!this.disabled) {
            // Emit an @action if this is a button
            if (this.type === 'button') {
              this.$emit('action', true);
            } // Ensure the parent dropdown is closed. don't emit if there isn't a dropdown to attach to
            // First we see if the parent is a dropdown (note that it has to be open!), then emit so
            // that it's listender will close it. We know if it's a dropdown if it has the `_dropdown` property.


            var attachedTo = this.$parent._dropdown ? this.$parent.id : undefined;

            if (attachedTo) {
              dropdownEventBus.$emit('dropdown::item-click', attachedTo);
            }
          }
        }
      },
      render: function render(h) {
        return h(this.tag, {
          class: ['dropdown-item'],
          attrs: objectSpread({}, this.attributes),
          on: {
            click: this.onClick
          },
          nativeOn: {
            click: this.onClick
          }
        }, this.$slots.default);
      }
    };

    /* script */
    const __vue_script__$9 = script$9;

    /* template */

      /* style */
      const __vue_inject_styles__$9 = undefined;
      /* scoped */
      const __vue_scope_id__$9 = undefined;
      /* module identifier */
      const __vue_module_identifier__$9 = undefined;
      /* functional template */
      const __vue_is_functional_template__$9 = undefined;
      /* style inject */
      
      /* style inject SSR */
      

      
      var DropdownItem = normalizeComponent_1(
        {},
        __vue_inject_styles__$9,
        __vue_script__$9,
        __vue_scope_id__$9,
        __vue_is_functional_template__$9,
        __vue_module_identifier__$9,
        undefined,
        undefined
      );

    //
    //
    //
    //
    //
    //
    //
    var script$a = {
      name: "CommandGroup",
      props: {
        vertical: {
          type: Boolean,
          default: false
        },
        assistiveLabel: {
          type: String,
          default: undefined
        },
        size: {
          type: String,
          default: 'normal',
          validator: function validator(value) {
            return ['large', 'normal', 'small'].indexOf(value) > -1;
          }
        }
      },
      computed: {
        btnSize: function btnSize() {
          if (this.size === 'large') {
            return 'btn-group-lg';
          } else if (this.size === 'small') {
            return 'btn-group-sm';
          } else {
            return '';
          }
        }
      }
    };

    /* script */
    const __vue_script__$a = script$a;

    /* template */
    var __vue_render__$7 = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"btn-group",class:[_vm.btnSize, { 'btn-group-vertical': _vm.vertical}],attrs:{"role":"group","aria-label":_vm.assistiveLabel}},[_vm._t("default")],2)};
    var __vue_staticRenderFns__$7 = [];

      /* style */
      const __vue_inject_styles__$a = undefined;
      /* scoped */
      const __vue_scope_id__$a = undefined;
      /* module identifier */
      const __vue_module_identifier__$a = undefined;
      /* functional template */
      const __vue_is_functional_template__$a = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var CommandGroup = normalizeComponent_1(
        { render: __vue_render__$7, staticRenderFns: __vue_staticRenderFns__$7 },
        __vue_inject_styles__$a,
        __vue_script__$a,
        __vue_scope_id__$a,
        __vue_is_functional_template__$a,
        __vue_module_identifier__$a,
        undefined,
        undefined
      );

    //
    var calendarUtility$1 = new CalendarUtility();
    var script$b = {
      name: "DateInput",
      props: {
        value: {
          type: String,
          default: ''
        }
      },
      data: function data() {
        return {
          inputValue: this.value,
          selectedValue: this.value,
          show: false
        };
      },
      methods: {
        updateFromInput: function updateFromInput(val) {
          // TODO: remove the format bit once the class can handle this
          var inputVal = calendarUtility$1.parseDateFromString(val, 'YYYY-MM-DD');
          this.inputValue = val;
          this.update(calendarUtility$1.formatDateToString(inputVal, 'YYYY-MM-DD'));
        },
        updateFromCalendar: function updateFromCalendar(val) {
          this.selectedValue = val;
          this.update(this.selectedValue);

          if (this.show === true) {
            this.show = false;
          }
        },
        update: function update(val) {
          this.$emit('input', val);
        },
        showCalendar: function showCalendar() {
          this.show = true;
        }
      }
    };

    var css$4 = "";
    styleInject(css$4);

    /* script */
    const __vue_script__$b = script$b;
    /* template */
    var __vue_render__$8 = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('text-box',{attrs:{"value":_vm.inputValue,"id":"temp"},on:{"input":_vm.updateFromInput,"focus":_vm.showCalendar,"click":_vm.showCalendar}}),_vm._v(" "),(_vm.show)?_c('div',[_c('calendar',{attrs:{"value":_vm.selectedValue},on:{"input":_vm.update}})],1):_vm._e()],1)};
    var __vue_staticRenderFns__$8 = [];

      /* style */
      const __vue_inject_styles__$b = undefined;
      /* scoped */
      const __vue_scope_id__$b = "data-v-04bfdd98";
      /* module identifier */
      const __vue_module_identifier__$b = undefined;
      /* functional template */
      const __vue_is_functional_template__$b = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var DateInput = normalizeComponent_1(
        { render: __vue_render__$8, staticRenderFns: __vue_staticRenderFns__$8 },
        __vue_inject_styles__$b,
        __vue_script__$b,
        __vue_scope_id__$b,
        __vue_is_functional_template__$b,
        __vue_module_identifier__$b,
        undefined,
        undefined
      );

    var moment = createCommonjsModule(function (module, exports) {
    (function (global, factory) {
        module.exports = factory();
    }(commonjsGlobal, (function () {
        var hookCallback;

        function hooks () {
            return hookCallback.apply(null, arguments);
        }

        // This is done to register the method called with moment()
        // without creating circular dependencies.
        function setHookCallback (callback) {
            hookCallback = callback;
        }

        function isArray(input) {
            return input instanceof Array || Object.prototype.toString.call(input) === '[object Array]';
        }

        function isObject(input) {
            // IE8 will treat undefined and null as object if it wasn't for
            // input != null
            return input != null && Object.prototype.toString.call(input) === '[object Object]';
        }

        function isObjectEmpty(obj) {
            if (Object.getOwnPropertyNames) {
                return (Object.getOwnPropertyNames(obj).length === 0);
            } else {
                var k;
                for (k in obj) {
                    if (obj.hasOwnProperty(k)) {
                        return false;
                    }
                }
                return true;
            }
        }

        function isUndefined(input) {
            return input === void 0;
        }

        function isNumber(input) {
            return typeof input === 'number' || Object.prototype.toString.call(input) === '[object Number]';
        }

        function isDate(input) {
            return input instanceof Date || Object.prototype.toString.call(input) === '[object Date]';
        }

        function map(arr, fn) {
            var res = [], i;
            for (i = 0; i < arr.length; ++i) {
                res.push(fn(arr[i], i));
            }
            return res;
        }

        function hasOwnProp(a, b) {
            return Object.prototype.hasOwnProperty.call(a, b);
        }

        function extend(a, b) {
            for (var i in b) {
                if (hasOwnProp(b, i)) {
                    a[i] = b[i];
                }
            }

            if (hasOwnProp(b, 'toString')) {
                a.toString = b.toString;
            }

            if (hasOwnProp(b, 'valueOf')) {
                a.valueOf = b.valueOf;
            }

            return a;
        }

        function createUTC (input, format, locale, strict) {
            return createLocalOrUTC(input, format, locale, strict, true).utc();
        }

        function defaultParsingFlags() {
            // We need to deep clone this object.
            return {
                empty           : false,
                unusedTokens    : [],
                unusedInput     : [],
                overflow        : -2,
                charsLeftOver   : 0,
                nullInput       : false,
                invalidMonth    : null,
                invalidFormat   : false,
                userInvalidated : false,
                iso             : false,
                parsedDateParts : [],
                meridiem        : null,
                rfc2822         : false,
                weekdayMismatch : false
            };
        }

        function getParsingFlags(m) {
            if (m._pf == null) {
                m._pf = defaultParsingFlags();
            }
            return m._pf;
        }

        var some;
        if (Array.prototype.some) {
            some = Array.prototype.some;
        } else {
            some = function (fun) {
                var t = Object(this);
                var len = t.length >>> 0;

                for (var i = 0; i < len; i++) {
                    if (i in t && fun.call(this, t[i], i, t)) {
                        return true;
                    }
                }

                return false;
            };
        }

        function isValid(m) {
            if (m._isValid == null) {
                var flags = getParsingFlags(m);
                var parsedParts = some.call(flags.parsedDateParts, function (i) {
                    return i != null;
                });
                var isNowValid = !isNaN(m._d.getTime()) &&
                    flags.overflow < 0 &&
                    !flags.empty &&
                    !flags.invalidMonth &&
                    !flags.invalidWeekday &&
                    !flags.weekdayMismatch &&
                    !flags.nullInput &&
                    !flags.invalidFormat &&
                    !flags.userInvalidated &&
                    (!flags.meridiem || (flags.meridiem && parsedParts));

                if (m._strict) {
                    isNowValid = isNowValid &&
                        flags.charsLeftOver === 0 &&
                        flags.unusedTokens.length === 0 &&
                        flags.bigHour === undefined;
                }

                if (Object.isFrozen == null || !Object.isFrozen(m)) {
                    m._isValid = isNowValid;
                }
                else {
                    return isNowValid;
                }
            }
            return m._isValid;
        }

        function createInvalid (flags) {
            var m = createUTC(NaN);
            if (flags != null) {
                extend(getParsingFlags(m), flags);
            }
            else {
                getParsingFlags(m).userInvalidated = true;
            }

            return m;
        }

        // Plugins that add properties should also add the key here (null value),
        // so we can properly clone ourselves.
        var momentProperties = hooks.momentProperties = [];

        function copyConfig(to, from) {
            var i, prop, val;

            if (!isUndefined(from._isAMomentObject)) {
                to._isAMomentObject = from._isAMomentObject;
            }
            if (!isUndefined(from._i)) {
                to._i = from._i;
            }
            if (!isUndefined(from._f)) {
                to._f = from._f;
            }
            if (!isUndefined(from._l)) {
                to._l = from._l;
            }
            if (!isUndefined(from._strict)) {
                to._strict = from._strict;
            }
            if (!isUndefined(from._tzm)) {
                to._tzm = from._tzm;
            }
            if (!isUndefined(from._isUTC)) {
                to._isUTC = from._isUTC;
            }
            if (!isUndefined(from._offset)) {
                to._offset = from._offset;
            }
            if (!isUndefined(from._pf)) {
                to._pf = getParsingFlags(from);
            }
            if (!isUndefined(from._locale)) {
                to._locale = from._locale;
            }

            if (momentProperties.length > 0) {
                for (i = 0; i < momentProperties.length; i++) {
                    prop = momentProperties[i];
                    val = from[prop];
                    if (!isUndefined(val)) {
                        to[prop] = val;
                    }
                }
            }

            return to;
        }

        var updateInProgress = false;

        // Moment prototype object
        function Moment(config) {
            copyConfig(this, config);
            this._d = new Date(config._d != null ? config._d.getTime() : NaN);
            if (!this.isValid()) {
                this._d = new Date(NaN);
            }
            // Prevent infinite loop in case updateOffset creates new moment
            // objects.
            if (updateInProgress === false) {
                updateInProgress = true;
                hooks.updateOffset(this);
                updateInProgress = false;
            }
        }

        function isMoment (obj) {
            return obj instanceof Moment || (obj != null && obj._isAMomentObject != null);
        }

        function absFloor (number) {
            if (number < 0) {
                // -0 -> 0
                return Math.ceil(number) || 0;
            } else {
                return Math.floor(number);
            }
        }

        function toInt(argumentForCoercion) {
            var coercedNumber = +argumentForCoercion,
                value = 0;

            if (coercedNumber !== 0 && isFinite(coercedNumber)) {
                value = absFloor(coercedNumber);
            }

            return value;
        }

        // compare two arrays, return the number of differences
        function compareArrays(array1, array2, dontConvert) {
            var len = Math.min(array1.length, array2.length),
                lengthDiff = Math.abs(array1.length - array2.length),
                diffs = 0,
                i;
            for (i = 0; i < len; i++) {
                if ((dontConvert && array1[i] !== array2[i]) ||
                    (!dontConvert && toInt(array1[i]) !== toInt(array2[i]))) {
                    diffs++;
                }
            }
            return diffs + lengthDiff;
        }

        function warn(msg) {
            if (hooks.suppressDeprecationWarnings === false &&
                    (typeof console !==  'undefined') && console.warn) {
                console.warn('Deprecation warning: ' + msg);
            }
        }

        function deprecate(msg, fn) {
            var firstTime = true;

            return extend(function () {
                if (hooks.deprecationHandler != null) {
                    hooks.deprecationHandler(null, msg);
                }
                if (firstTime) {
                    var args = [];
                    var arg;
                    for (var i = 0; i < arguments.length; i++) {
                        arg = '';
                        if (typeof arguments[i] === 'object') {
                            arg += '\n[' + i + '] ';
                            for (var key in arguments[0]) {
                                arg += key + ': ' + arguments[0][key] + ', ';
                            }
                            arg = arg.slice(0, -2); // Remove trailing comma and space
                        } else {
                            arg = arguments[i];
                        }
                        args.push(arg);
                    }
                    warn(msg + '\nArguments: ' + Array.prototype.slice.call(args).join('') + '\n' + (new Error()).stack);
                    firstTime = false;
                }
                return fn.apply(this, arguments);
            }, fn);
        }

        var deprecations = {};

        function deprecateSimple(name, msg) {
            if (hooks.deprecationHandler != null) {
                hooks.deprecationHandler(name, msg);
            }
            if (!deprecations[name]) {
                warn(msg);
                deprecations[name] = true;
            }
        }

        hooks.suppressDeprecationWarnings = false;
        hooks.deprecationHandler = null;

        function isFunction(input) {
            return input instanceof Function || Object.prototype.toString.call(input) === '[object Function]';
        }

        function set (config) {
            var prop, i;
            for (i in config) {
                prop = config[i];
                if (isFunction(prop)) {
                    this[i] = prop;
                } else {
                    this['_' + i] = prop;
                }
            }
            this._config = config;
            // Lenient ordinal parsing accepts just a number in addition to
            // number + (possibly) stuff coming from _dayOfMonthOrdinalParse.
            // TODO: Remove "ordinalParse" fallback in next major release.
            this._dayOfMonthOrdinalParseLenient = new RegExp(
                (this._dayOfMonthOrdinalParse.source || this._ordinalParse.source) +
                    '|' + (/\d{1,2}/).source);
        }

        function mergeConfigs(parentConfig, childConfig) {
            var res = extend({}, parentConfig), prop;
            for (prop in childConfig) {
                if (hasOwnProp(childConfig, prop)) {
                    if (isObject(parentConfig[prop]) && isObject(childConfig[prop])) {
                        res[prop] = {};
                        extend(res[prop], parentConfig[prop]);
                        extend(res[prop], childConfig[prop]);
                    } else if (childConfig[prop] != null) {
                        res[prop] = childConfig[prop];
                    } else {
                        delete res[prop];
                    }
                }
            }
            for (prop in parentConfig) {
                if (hasOwnProp(parentConfig, prop) &&
                        !hasOwnProp(childConfig, prop) &&
                        isObject(parentConfig[prop])) {
                    // make sure changes to properties don't modify parent config
                    res[prop] = extend({}, res[prop]);
                }
            }
            return res;
        }

        function Locale(config) {
            if (config != null) {
                this.set(config);
            }
        }

        var keys;

        if (Object.keys) {
            keys = Object.keys;
        } else {
            keys = function (obj) {
                var i, res = [];
                for (i in obj) {
                    if (hasOwnProp(obj, i)) {
                        res.push(i);
                    }
                }
                return res;
            };
        }

        var defaultCalendar = {
            sameDay : '[Today at] LT',
            nextDay : '[Tomorrow at] LT',
            nextWeek : 'dddd [at] LT',
            lastDay : '[Yesterday at] LT',
            lastWeek : '[Last] dddd [at] LT',
            sameElse : 'L'
        };

        function calendar (key, mom, now) {
            var output = this._calendar[key] || this._calendar['sameElse'];
            return isFunction(output) ? output.call(mom, now) : output;
        }

        var defaultLongDateFormat = {
            LTS  : 'h:mm:ss A',
            LT   : 'h:mm A',
            L    : 'MM/DD/YYYY',
            LL   : 'MMMM D, YYYY',
            LLL  : 'MMMM D, YYYY h:mm A',
            LLLL : 'dddd, MMMM D, YYYY h:mm A'
        };

        function longDateFormat (key) {
            var format = this._longDateFormat[key],
                formatUpper = this._longDateFormat[key.toUpperCase()];

            if (format || !formatUpper) {
                return format;
            }

            this._longDateFormat[key] = formatUpper.replace(/MMMM|MM|DD|dddd/g, function (val) {
                return val.slice(1);
            });

            return this._longDateFormat[key];
        }

        var defaultInvalidDate = 'Invalid date';

        function invalidDate () {
            return this._invalidDate;
        }

        var defaultOrdinal = '%d';
        var defaultDayOfMonthOrdinalParse = /\d{1,2}/;

        function ordinal (number) {
            return this._ordinal.replace('%d', number);
        }

        var defaultRelativeTime = {
            future : 'in %s',
            past   : '%s ago',
            s  : 'a few seconds',
            ss : '%d seconds',
            m  : 'a minute',
            mm : '%d minutes',
            h  : 'an hour',
            hh : '%d hours',
            d  : 'a day',
            dd : '%d days',
            M  : 'a month',
            MM : '%d months',
            y  : 'a year',
            yy : '%d years'
        };

        function relativeTime (number, withoutSuffix, string, isFuture) {
            var output = this._relativeTime[string];
            return (isFunction(output)) ?
                output(number, withoutSuffix, string, isFuture) :
                output.replace(/%d/i, number);
        }

        function pastFuture (diff, output) {
            var format = this._relativeTime[diff > 0 ? 'future' : 'past'];
            return isFunction(format) ? format(output) : format.replace(/%s/i, output);
        }

        var aliases = {};

        function addUnitAlias (unit, shorthand) {
            var lowerCase = unit.toLowerCase();
            aliases[lowerCase] = aliases[lowerCase + 's'] = aliases[shorthand] = unit;
        }

        function normalizeUnits(units) {
            return typeof units === 'string' ? aliases[units] || aliases[units.toLowerCase()] : undefined;
        }

        function normalizeObjectUnits(inputObject) {
            var normalizedInput = {},
                normalizedProp,
                prop;

            for (prop in inputObject) {
                if (hasOwnProp(inputObject, prop)) {
                    normalizedProp = normalizeUnits(prop);
                    if (normalizedProp) {
                        normalizedInput[normalizedProp] = inputObject[prop];
                    }
                }
            }

            return normalizedInput;
        }

        var priorities = {};

        function addUnitPriority(unit, priority) {
            priorities[unit] = priority;
        }

        function getPrioritizedUnits(unitsObj) {
            var units = [];
            for (var u in unitsObj) {
                units.push({unit: u, priority: priorities[u]});
            }
            units.sort(function (a, b) {
                return a.priority - b.priority;
            });
            return units;
        }

        function zeroFill(number, targetLength, forceSign) {
            var absNumber = '' + Math.abs(number),
                zerosToFill = targetLength - absNumber.length,
                sign = number >= 0;
            return (sign ? (forceSign ? '+' : '') : '-') +
                Math.pow(10, Math.max(0, zerosToFill)).toString().substr(1) + absNumber;
        }

        var formattingTokens = /(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|kk?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g;

        var localFormattingTokens = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g;

        var formatFunctions = {};

        var formatTokenFunctions = {};

        // token:    'M'
        // padded:   ['MM', 2]
        // ordinal:  'Mo'
        // callback: function () { this.month() + 1 }
        function addFormatToken (token, padded, ordinal, callback) {
            var func = callback;
            if (typeof callback === 'string') {
                func = function () {
                    return this[callback]();
                };
            }
            if (token) {
                formatTokenFunctions[token] = func;
            }
            if (padded) {
                formatTokenFunctions[padded[0]] = function () {
                    return zeroFill(func.apply(this, arguments), padded[1], padded[2]);
                };
            }
            if (ordinal) {
                formatTokenFunctions[ordinal] = function () {
                    return this.localeData().ordinal(func.apply(this, arguments), token);
                };
            }
        }

        function removeFormattingTokens(input) {
            if (input.match(/\[[\s\S]/)) {
                return input.replace(/^\[|\]$/g, '');
            }
            return input.replace(/\\/g, '');
        }

        function makeFormatFunction(format) {
            var array = format.match(formattingTokens), i, length;

            for (i = 0, length = array.length; i < length; i++) {
                if (formatTokenFunctions[array[i]]) {
                    array[i] = formatTokenFunctions[array[i]];
                } else {
                    array[i] = removeFormattingTokens(array[i]);
                }
            }

            return function (mom) {
                var output = '', i;
                for (i = 0; i < length; i++) {
                    output += isFunction(array[i]) ? array[i].call(mom, format) : array[i];
                }
                return output;
            };
        }

        // format date using native date object
        function formatMoment(m, format) {
            if (!m.isValid()) {
                return m.localeData().invalidDate();
            }

            format = expandFormat(format, m.localeData());
            formatFunctions[format] = formatFunctions[format] || makeFormatFunction(format);

            return formatFunctions[format](m);
        }

        function expandFormat(format, locale) {
            var i = 5;

            function replaceLongDateFormatTokens(input) {
                return locale.longDateFormat(input) || input;
            }

            localFormattingTokens.lastIndex = 0;
            while (i >= 0 && localFormattingTokens.test(format)) {
                format = format.replace(localFormattingTokens, replaceLongDateFormatTokens);
                localFormattingTokens.lastIndex = 0;
                i -= 1;
            }

            return format;
        }

        var match1         = /\d/;            //       0 - 9
        var match2         = /\d\d/;          //      00 - 99
        var match3         = /\d{3}/;         //     000 - 999
        var match4         = /\d{4}/;         //    0000 - 9999
        var match6         = /[+-]?\d{6}/;    // -999999 - 999999
        var match1to2      = /\d\d?/;         //       0 - 99
        var match3to4      = /\d\d\d\d?/;     //     999 - 9999
        var match5to6      = /\d\d\d\d\d\d?/; //   99999 - 999999
        var match1to3      = /\d{1,3}/;       //       0 - 999
        var match1to4      = /\d{1,4}/;       //       0 - 9999
        var match1to6      = /[+-]?\d{1,6}/;  // -999999 - 999999

        var matchUnsigned  = /\d+/;           //       0 - inf
        var matchSigned    = /[+-]?\d+/;      //    -inf - inf

        var matchOffset    = /Z|[+-]\d\d:?\d\d/gi; // +00:00 -00:00 +0000 -0000 or Z
        var matchShortOffset = /Z|[+-]\d\d(?::?\d\d)?/gi; // +00 -00 +00:00 -00:00 +0000 -0000 or Z

        var matchTimestamp = /[+-]?\d+(\.\d{1,3})?/; // 123456789 123456789.123

        // any word (or two) characters or numbers including two/three word month in arabic.
        // includes scottish gaelic two word and hyphenated months
        var matchWord = /[0-9]{0,256}['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFF07\uFF10-\uFFEF]{1,256}|[\u0600-\u06FF\/]{1,256}(\s*?[\u0600-\u06FF]{1,256}){1,2}/i;

        var regexes = {};

        function addRegexToken (token, regex, strictRegex) {
            regexes[token] = isFunction(regex) ? regex : function (isStrict, localeData) {
                return (isStrict && strictRegex) ? strictRegex : regex;
            };
        }

        function getParseRegexForToken (token, config) {
            if (!hasOwnProp(regexes, token)) {
                return new RegExp(unescapeFormat(token));
            }

            return regexes[token](config._strict, config._locale);
        }

        // Code from http://stackoverflow.com/questions/3561493/is-there-a-regexp-escape-function-in-javascript
        function unescapeFormat(s) {
            return regexEscape(s.replace('\\', '').replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function (matched, p1, p2, p3, p4) {
                return p1 || p2 || p3 || p4;
            }));
        }

        function regexEscape(s) {
            return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
        }

        var tokens = {};

        function addParseToken (token, callback) {
            var i, func = callback;
            if (typeof token === 'string') {
                token = [token];
            }
            if (isNumber(callback)) {
                func = function (input, array) {
                    array[callback] = toInt(input);
                };
            }
            for (i = 0; i < token.length; i++) {
                tokens[token[i]] = func;
            }
        }

        function addWeekParseToken (token, callback) {
            addParseToken(token, function (input, array, config, token) {
                config._w = config._w || {};
                callback(input, config._w, config, token);
            });
        }

        function addTimeToArrayFromToken(token, input, config) {
            if (input != null && hasOwnProp(tokens, token)) {
                tokens[token](input, config._a, config, token);
            }
        }

        var YEAR = 0;
        var MONTH = 1;
        var DATE = 2;
        var HOUR = 3;
        var MINUTE = 4;
        var SECOND = 5;
        var MILLISECOND = 6;
        var WEEK = 7;
        var WEEKDAY = 8;

        // FORMATTING

        addFormatToken('Y', 0, 0, function () {
            var y = this.year();
            return y <= 9999 ? '' + y : '+' + y;
        });

        addFormatToken(0, ['YY', 2], 0, function () {
            return this.year() % 100;
        });

        addFormatToken(0, ['YYYY',   4],       0, 'year');
        addFormatToken(0, ['YYYYY',  5],       0, 'year');
        addFormatToken(0, ['YYYYYY', 6, true], 0, 'year');

        // ALIASES

        addUnitAlias('year', 'y');

        // PRIORITIES

        addUnitPriority('year', 1);

        // PARSING

        addRegexToken('Y',      matchSigned);
        addRegexToken('YY',     match1to2, match2);
        addRegexToken('YYYY',   match1to4, match4);
        addRegexToken('YYYYY',  match1to6, match6);
        addRegexToken('YYYYYY', match1to6, match6);

        addParseToken(['YYYYY', 'YYYYYY'], YEAR);
        addParseToken('YYYY', function (input, array) {
            array[YEAR] = input.length === 2 ? hooks.parseTwoDigitYear(input) : toInt(input);
        });
        addParseToken('YY', function (input, array) {
            array[YEAR] = hooks.parseTwoDigitYear(input);
        });
        addParseToken('Y', function (input, array) {
            array[YEAR] = parseInt(input, 10);
        });

        // HELPERS

        function daysInYear(year) {
            return isLeapYear(year) ? 366 : 365;
        }

        function isLeapYear(year) {
            return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
        }

        // HOOKS

        hooks.parseTwoDigitYear = function (input) {
            return toInt(input) + (toInt(input) > 68 ? 1900 : 2000);
        };

        // MOMENTS

        var getSetYear = makeGetSet('FullYear', true);

        function getIsLeapYear () {
            return isLeapYear(this.year());
        }

        function makeGetSet (unit, keepTime) {
            return function (value) {
                if (value != null) {
                    set$1(this, unit, value);
                    hooks.updateOffset(this, keepTime);
                    return this;
                } else {
                    return get(this, unit);
                }
            };
        }

        function get (mom, unit) {
            return mom.isValid() ?
                mom._d['get' + (mom._isUTC ? 'UTC' : '') + unit]() : NaN;
        }

        function set$1 (mom, unit, value) {
            if (mom.isValid() && !isNaN(value)) {
                if (unit === 'FullYear' && isLeapYear(mom.year()) && mom.month() === 1 && mom.date() === 29) {
                    mom._d['set' + (mom._isUTC ? 'UTC' : '') + unit](value, mom.month(), daysInMonth(value, mom.month()));
                }
                else {
                    mom._d['set' + (mom._isUTC ? 'UTC' : '') + unit](value);
                }
            }
        }

        // MOMENTS

        function stringGet (units) {
            units = normalizeUnits(units);
            if (isFunction(this[units])) {
                return this[units]();
            }
            return this;
        }


        function stringSet (units, value) {
            if (typeof units === 'object') {
                units = normalizeObjectUnits(units);
                var prioritized = getPrioritizedUnits(units);
                for (var i = 0; i < prioritized.length; i++) {
                    this[prioritized[i].unit](units[prioritized[i].unit]);
                }
            } else {
                units = normalizeUnits(units);
                if (isFunction(this[units])) {
                    return this[units](value);
                }
            }
            return this;
        }

        function mod(n, x) {
            return ((n % x) + x) % x;
        }

        var indexOf;

        if (Array.prototype.indexOf) {
            indexOf = Array.prototype.indexOf;
        } else {
            indexOf = function (o) {
                // I know
                var i;
                for (i = 0; i < this.length; ++i) {
                    if (this[i] === o) {
                        return i;
                    }
                }
                return -1;
            };
        }

        function daysInMonth(year, month) {
            if (isNaN(year) || isNaN(month)) {
                return NaN;
            }
            var modMonth = mod(month, 12);
            year += (month - modMonth) / 12;
            return modMonth === 1 ? (isLeapYear(year) ? 29 : 28) : (31 - modMonth % 7 % 2);
        }

        // FORMATTING

        addFormatToken('M', ['MM', 2], 'Mo', function () {
            return this.month() + 1;
        });

        addFormatToken('MMM', 0, 0, function (format) {
            return this.localeData().monthsShort(this, format);
        });

        addFormatToken('MMMM', 0, 0, function (format) {
            return this.localeData().months(this, format);
        });

        // ALIASES

        addUnitAlias('month', 'M');

        // PRIORITY

        addUnitPriority('month', 8);

        // PARSING

        addRegexToken('M',    match1to2);
        addRegexToken('MM',   match1to2, match2);
        addRegexToken('MMM',  function (isStrict, locale) {
            return locale.monthsShortRegex(isStrict);
        });
        addRegexToken('MMMM', function (isStrict, locale) {
            return locale.monthsRegex(isStrict);
        });

        addParseToken(['M', 'MM'], function (input, array) {
            array[MONTH] = toInt(input) - 1;
        });

        addParseToken(['MMM', 'MMMM'], function (input, array, config, token) {
            var month = config._locale.monthsParse(input, token, config._strict);
            // if we didn't find a month name, mark the date as invalid.
            if (month != null) {
                array[MONTH] = month;
            } else {
                getParsingFlags(config).invalidMonth = input;
            }
        });

        // LOCALES

        var MONTHS_IN_FORMAT = /D[oD]?(\[[^\[\]]*\]|\s)+MMMM?/;
        var defaultLocaleMonths = 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_');
        function localeMonths (m, format) {
            if (!m) {
                return isArray(this._months) ? this._months :
                    this._months['standalone'];
            }
            return isArray(this._months) ? this._months[m.month()] :
                this._months[(this._months.isFormat || MONTHS_IN_FORMAT).test(format) ? 'format' : 'standalone'][m.month()];
        }

        var defaultLocaleMonthsShort = 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_');
        function localeMonthsShort (m, format) {
            if (!m) {
                return isArray(this._monthsShort) ? this._monthsShort :
                    this._monthsShort['standalone'];
            }
            return isArray(this._monthsShort) ? this._monthsShort[m.month()] :
                this._monthsShort[MONTHS_IN_FORMAT.test(format) ? 'format' : 'standalone'][m.month()];
        }

        function handleStrictParse(monthName, format, strict) {
            var i, ii, mom, llc = monthName.toLocaleLowerCase();
            if (!this._monthsParse) {
                // this is not used
                this._monthsParse = [];
                this._longMonthsParse = [];
                this._shortMonthsParse = [];
                for (i = 0; i < 12; ++i) {
                    mom = createUTC([2000, i]);
                    this._shortMonthsParse[i] = this.monthsShort(mom, '').toLocaleLowerCase();
                    this._longMonthsParse[i] = this.months(mom, '').toLocaleLowerCase();
                }
            }

            if (strict) {
                if (format === 'MMM') {
                    ii = indexOf.call(this._shortMonthsParse, llc);
                    return ii !== -1 ? ii : null;
                } else {
                    ii = indexOf.call(this._longMonthsParse, llc);
                    return ii !== -1 ? ii : null;
                }
            } else {
                if (format === 'MMM') {
                    ii = indexOf.call(this._shortMonthsParse, llc);
                    if (ii !== -1) {
                        return ii;
                    }
                    ii = indexOf.call(this._longMonthsParse, llc);
                    return ii !== -1 ? ii : null;
                } else {
                    ii = indexOf.call(this._longMonthsParse, llc);
                    if (ii !== -1) {
                        return ii;
                    }
                    ii = indexOf.call(this._shortMonthsParse, llc);
                    return ii !== -1 ? ii : null;
                }
            }
        }

        function localeMonthsParse (monthName, format, strict) {
            var i, mom, regex;

            if (this._monthsParseExact) {
                return handleStrictParse.call(this, monthName, format, strict);
            }

            if (!this._monthsParse) {
                this._monthsParse = [];
                this._longMonthsParse = [];
                this._shortMonthsParse = [];
            }

            // TODO: add sorting
            // Sorting makes sure if one month (or abbr) is a prefix of another
            // see sorting in computeMonthsParse
            for (i = 0; i < 12; i++) {
                // make the regex if we don't have it already
                mom = createUTC([2000, i]);
                if (strict && !this._longMonthsParse[i]) {
                    this._longMonthsParse[i] = new RegExp('^' + this.months(mom, '').replace('.', '') + '$', 'i');
                    this._shortMonthsParse[i] = new RegExp('^' + this.monthsShort(mom, '').replace('.', '') + '$', 'i');
                }
                if (!strict && !this._monthsParse[i]) {
                    regex = '^' + this.months(mom, '') + '|^' + this.monthsShort(mom, '');
                    this._monthsParse[i] = new RegExp(regex.replace('.', ''), 'i');
                }
                // test the regex
                if (strict && format === 'MMMM' && this._longMonthsParse[i].test(monthName)) {
                    return i;
                } else if (strict && format === 'MMM' && this._shortMonthsParse[i].test(monthName)) {
                    return i;
                } else if (!strict && this._monthsParse[i].test(monthName)) {
                    return i;
                }
            }
        }

        // MOMENTS

        function setMonth (mom, value) {
            var dayOfMonth;

            if (!mom.isValid()) {
                // No op
                return mom;
            }

            if (typeof value === 'string') {
                if (/^\d+$/.test(value)) {
                    value = toInt(value);
                } else {
                    value = mom.localeData().monthsParse(value);
                    // TODO: Another silent failure?
                    if (!isNumber(value)) {
                        return mom;
                    }
                }
            }

            dayOfMonth = Math.min(mom.date(), daysInMonth(mom.year(), value));
            mom._d['set' + (mom._isUTC ? 'UTC' : '') + 'Month'](value, dayOfMonth);
            return mom;
        }

        function getSetMonth (value) {
            if (value != null) {
                setMonth(this, value);
                hooks.updateOffset(this, true);
                return this;
            } else {
                return get(this, 'Month');
            }
        }

        function getDaysInMonth () {
            return daysInMonth(this.year(), this.month());
        }

        var defaultMonthsShortRegex = matchWord;
        function monthsShortRegex (isStrict) {
            if (this._monthsParseExact) {
                if (!hasOwnProp(this, '_monthsRegex')) {
                    computeMonthsParse.call(this);
                }
                if (isStrict) {
                    return this._monthsShortStrictRegex;
                } else {
                    return this._monthsShortRegex;
                }
            } else {
                if (!hasOwnProp(this, '_monthsShortRegex')) {
                    this._monthsShortRegex = defaultMonthsShortRegex;
                }
                return this._monthsShortStrictRegex && isStrict ?
                    this._monthsShortStrictRegex : this._monthsShortRegex;
            }
        }

        var defaultMonthsRegex = matchWord;
        function monthsRegex (isStrict) {
            if (this._monthsParseExact) {
                if (!hasOwnProp(this, '_monthsRegex')) {
                    computeMonthsParse.call(this);
                }
                if (isStrict) {
                    return this._monthsStrictRegex;
                } else {
                    return this._monthsRegex;
                }
            } else {
                if (!hasOwnProp(this, '_monthsRegex')) {
                    this._monthsRegex = defaultMonthsRegex;
                }
                return this._monthsStrictRegex && isStrict ?
                    this._monthsStrictRegex : this._monthsRegex;
            }
        }

        function computeMonthsParse () {
            function cmpLenRev(a, b) {
                return b.length - a.length;
            }

            var shortPieces = [], longPieces = [], mixedPieces = [],
                i, mom;
            for (i = 0; i < 12; i++) {
                // make the regex if we don't have it already
                mom = createUTC([2000, i]);
                shortPieces.push(this.monthsShort(mom, ''));
                longPieces.push(this.months(mom, ''));
                mixedPieces.push(this.months(mom, ''));
                mixedPieces.push(this.monthsShort(mom, ''));
            }
            // Sorting makes sure if one month (or abbr) is a prefix of another it
            // will match the longer piece.
            shortPieces.sort(cmpLenRev);
            longPieces.sort(cmpLenRev);
            mixedPieces.sort(cmpLenRev);
            for (i = 0; i < 12; i++) {
                shortPieces[i] = regexEscape(shortPieces[i]);
                longPieces[i] = regexEscape(longPieces[i]);
            }
            for (i = 0; i < 24; i++) {
                mixedPieces[i] = regexEscape(mixedPieces[i]);
            }

            this._monthsRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
            this._monthsShortRegex = this._monthsRegex;
            this._monthsStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
            this._monthsShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
        }

        function createDate (y, m, d, h, M, s, ms) {
            // can't just apply() to create a date:
            // https://stackoverflow.com/q/181348
            var date;
            // the date constructor remaps years 0-99 to 1900-1999
            if (y < 100 && y >= 0) {
                // preserve leap years using a full 400 year cycle, then reset
                date = new Date(y + 400, m, d, h, M, s, ms);
                if (isFinite(date.getFullYear())) {
                    date.setFullYear(y);
                }
            } else {
                date = new Date(y, m, d, h, M, s, ms);
            }

            return date;
        }

        function createUTCDate (y) {
            var date;
            // the Date.UTC function remaps years 0-99 to 1900-1999
            if (y < 100 && y >= 0) {
                var args = Array.prototype.slice.call(arguments);
                // preserve leap years using a full 400 year cycle, then reset
                args[0] = y + 400;
                date = new Date(Date.UTC.apply(null, args));
                if (isFinite(date.getUTCFullYear())) {
                    date.setUTCFullYear(y);
                }
            } else {
                date = new Date(Date.UTC.apply(null, arguments));
            }

            return date;
        }

        // start-of-first-week - start-of-year
        function firstWeekOffset(year, dow, doy) {
            var // first-week day -- which january is always in the first week (4 for iso, 1 for other)
                fwd = 7 + dow - doy,
                // first-week day local weekday -- which local weekday is fwd
                fwdlw = (7 + createUTCDate(year, 0, fwd).getUTCDay() - dow) % 7;

            return -fwdlw + fwd - 1;
        }

        // https://en.wikipedia.org/wiki/ISO_week_date#Calculating_a_date_given_the_year.2C_week_number_and_weekday
        function dayOfYearFromWeeks(year, week, weekday, dow, doy) {
            var localWeekday = (7 + weekday - dow) % 7,
                weekOffset = firstWeekOffset(year, dow, doy),
                dayOfYear = 1 + 7 * (week - 1) + localWeekday + weekOffset,
                resYear, resDayOfYear;

            if (dayOfYear <= 0) {
                resYear = year - 1;
                resDayOfYear = daysInYear(resYear) + dayOfYear;
            } else if (dayOfYear > daysInYear(year)) {
                resYear = year + 1;
                resDayOfYear = dayOfYear - daysInYear(year);
            } else {
                resYear = year;
                resDayOfYear = dayOfYear;
            }

            return {
                year: resYear,
                dayOfYear: resDayOfYear
            };
        }

        function weekOfYear(mom, dow, doy) {
            var weekOffset = firstWeekOffset(mom.year(), dow, doy),
                week = Math.floor((mom.dayOfYear() - weekOffset - 1) / 7) + 1,
                resWeek, resYear;

            if (week < 1) {
                resYear = mom.year() - 1;
                resWeek = week + weeksInYear(resYear, dow, doy);
            } else if (week > weeksInYear(mom.year(), dow, doy)) {
                resWeek = week - weeksInYear(mom.year(), dow, doy);
                resYear = mom.year() + 1;
            } else {
                resYear = mom.year();
                resWeek = week;
            }

            return {
                week: resWeek,
                year: resYear
            };
        }

        function weeksInYear(year, dow, doy) {
            var weekOffset = firstWeekOffset(year, dow, doy),
                weekOffsetNext = firstWeekOffset(year + 1, dow, doy);
            return (daysInYear(year) - weekOffset + weekOffsetNext) / 7;
        }

        // FORMATTING

        addFormatToken('w', ['ww', 2], 'wo', 'week');
        addFormatToken('W', ['WW', 2], 'Wo', 'isoWeek');

        // ALIASES

        addUnitAlias('week', 'w');
        addUnitAlias('isoWeek', 'W');

        // PRIORITIES

        addUnitPriority('week', 5);
        addUnitPriority('isoWeek', 5);

        // PARSING

        addRegexToken('w',  match1to2);
        addRegexToken('ww', match1to2, match2);
        addRegexToken('W',  match1to2);
        addRegexToken('WW', match1to2, match2);

        addWeekParseToken(['w', 'ww', 'W', 'WW'], function (input, week, config, token) {
            week[token.substr(0, 1)] = toInt(input);
        });

        // HELPERS

        // LOCALES

        function localeWeek (mom) {
            return weekOfYear(mom, this._week.dow, this._week.doy).week;
        }

        var defaultLocaleWeek = {
            dow : 0, // Sunday is the first day of the week.
            doy : 6  // The week that contains Jan 6th is the first week of the year.
        };

        function localeFirstDayOfWeek () {
            return this._week.dow;
        }

        function localeFirstDayOfYear () {
            return this._week.doy;
        }

        // MOMENTS

        function getSetWeek (input) {
            var week = this.localeData().week(this);
            return input == null ? week : this.add((input - week) * 7, 'd');
        }

        function getSetISOWeek (input) {
            var week = weekOfYear(this, 1, 4).week;
            return input == null ? week : this.add((input - week) * 7, 'd');
        }

        // FORMATTING

        addFormatToken('d', 0, 'do', 'day');

        addFormatToken('dd', 0, 0, function (format) {
            return this.localeData().weekdaysMin(this, format);
        });

        addFormatToken('ddd', 0, 0, function (format) {
            return this.localeData().weekdaysShort(this, format);
        });

        addFormatToken('dddd', 0, 0, function (format) {
            return this.localeData().weekdays(this, format);
        });

        addFormatToken('e', 0, 0, 'weekday');
        addFormatToken('E', 0, 0, 'isoWeekday');

        // ALIASES

        addUnitAlias('day', 'd');
        addUnitAlias('weekday', 'e');
        addUnitAlias('isoWeekday', 'E');

        // PRIORITY
        addUnitPriority('day', 11);
        addUnitPriority('weekday', 11);
        addUnitPriority('isoWeekday', 11);

        // PARSING

        addRegexToken('d',    match1to2);
        addRegexToken('e',    match1to2);
        addRegexToken('E',    match1to2);
        addRegexToken('dd',   function (isStrict, locale) {
            return locale.weekdaysMinRegex(isStrict);
        });
        addRegexToken('ddd',   function (isStrict, locale) {
            return locale.weekdaysShortRegex(isStrict);
        });
        addRegexToken('dddd',   function (isStrict, locale) {
            return locale.weekdaysRegex(isStrict);
        });

        addWeekParseToken(['dd', 'ddd', 'dddd'], function (input, week, config, token) {
            var weekday = config._locale.weekdaysParse(input, token, config._strict);
            // if we didn't get a weekday name, mark the date as invalid
            if (weekday != null) {
                week.d = weekday;
            } else {
                getParsingFlags(config).invalidWeekday = input;
            }
        });

        addWeekParseToken(['d', 'e', 'E'], function (input, week, config, token) {
            week[token] = toInt(input);
        });

        // HELPERS

        function parseWeekday(input, locale) {
            if (typeof input !== 'string') {
                return input;
            }

            if (!isNaN(input)) {
                return parseInt(input, 10);
            }

            input = locale.weekdaysParse(input);
            if (typeof input === 'number') {
                return input;
            }

            return null;
        }

        function parseIsoWeekday(input, locale) {
            if (typeof input === 'string') {
                return locale.weekdaysParse(input) % 7 || 7;
            }
            return isNaN(input) ? null : input;
        }

        // LOCALES
        function shiftWeekdays (ws, n) {
            return ws.slice(n, 7).concat(ws.slice(0, n));
        }

        var defaultLocaleWeekdays = 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_');
        function localeWeekdays (m, format) {
            var weekdays = isArray(this._weekdays) ? this._weekdays :
                this._weekdays[(m && m !== true && this._weekdays.isFormat.test(format)) ? 'format' : 'standalone'];
            return (m === true) ? shiftWeekdays(weekdays, this._week.dow)
                : (m) ? weekdays[m.day()] : weekdays;
        }

        var defaultLocaleWeekdaysShort = 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_');
        function localeWeekdaysShort (m) {
            return (m === true) ? shiftWeekdays(this._weekdaysShort, this._week.dow)
                : (m) ? this._weekdaysShort[m.day()] : this._weekdaysShort;
        }

        var defaultLocaleWeekdaysMin = 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_');
        function localeWeekdaysMin (m) {
            return (m === true) ? shiftWeekdays(this._weekdaysMin, this._week.dow)
                : (m) ? this._weekdaysMin[m.day()] : this._weekdaysMin;
        }

        function handleStrictParse$1(weekdayName, format, strict) {
            var i, ii, mom, llc = weekdayName.toLocaleLowerCase();
            if (!this._weekdaysParse) {
                this._weekdaysParse = [];
                this._shortWeekdaysParse = [];
                this._minWeekdaysParse = [];

                for (i = 0; i < 7; ++i) {
                    mom = createUTC([2000, 1]).day(i);
                    this._minWeekdaysParse[i] = this.weekdaysMin(mom, '').toLocaleLowerCase();
                    this._shortWeekdaysParse[i] = this.weekdaysShort(mom, '').toLocaleLowerCase();
                    this._weekdaysParse[i] = this.weekdays(mom, '').toLocaleLowerCase();
                }
            }

            if (strict) {
                if (format === 'dddd') {
                    ii = indexOf.call(this._weekdaysParse, llc);
                    return ii !== -1 ? ii : null;
                } else if (format === 'ddd') {
                    ii = indexOf.call(this._shortWeekdaysParse, llc);
                    return ii !== -1 ? ii : null;
                } else {
                    ii = indexOf.call(this._minWeekdaysParse, llc);
                    return ii !== -1 ? ii : null;
                }
            } else {
                if (format === 'dddd') {
                    ii = indexOf.call(this._weekdaysParse, llc);
                    if (ii !== -1) {
                        return ii;
                    }
                    ii = indexOf.call(this._shortWeekdaysParse, llc);
                    if (ii !== -1) {
                        return ii;
                    }
                    ii = indexOf.call(this._minWeekdaysParse, llc);
                    return ii !== -1 ? ii : null;
                } else if (format === 'ddd') {
                    ii = indexOf.call(this._shortWeekdaysParse, llc);
                    if (ii !== -1) {
                        return ii;
                    }
                    ii = indexOf.call(this._weekdaysParse, llc);
                    if (ii !== -1) {
                        return ii;
                    }
                    ii = indexOf.call(this._minWeekdaysParse, llc);
                    return ii !== -1 ? ii : null;
                } else {
                    ii = indexOf.call(this._minWeekdaysParse, llc);
                    if (ii !== -1) {
                        return ii;
                    }
                    ii = indexOf.call(this._weekdaysParse, llc);
                    if (ii !== -1) {
                        return ii;
                    }
                    ii = indexOf.call(this._shortWeekdaysParse, llc);
                    return ii !== -1 ? ii : null;
                }
            }
        }

        function localeWeekdaysParse (weekdayName, format, strict) {
            var i, mom, regex;

            if (this._weekdaysParseExact) {
                return handleStrictParse$1.call(this, weekdayName, format, strict);
            }

            if (!this._weekdaysParse) {
                this._weekdaysParse = [];
                this._minWeekdaysParse = [];
                this._shortWeekdaysParse = [];
                this._fullWeekdaysParse = [];
            }

            for (i = 0; i < 7; i++) {
                // make the regex if we don't have it already

                mom = createUTC([2000, 1]).day(i);
                if (strict && !this._fullWeekdaysParse[i]) {
                    this._fullWeekdaysParse[i] = new RegExp('^' + this.weekdays(mom, '').replace('.', '\\.?') + '$', 'i');
                    this._shortWeekdaysParse[i] = new RegExp('^' + this.weekdaysShort(mom, '').replace('.', '\\.?') + '$', 'i');
                    this._minWeekdaysParse[i] = new RegExp('^' + this.weekdaysMin(mom, '').replace('.', '\\.?') + '$', 'i');
                }
                if (!this._weekdaysParse[i]) {
                    regex = '^' + this.weekdays(mom, '') + '|^' + this.weekdaysShort(mom, '') + '|^' + this.weekdaysMin(mom, '');
                    this._weekdaysParse[i] = new RegExp(regex.replace('.', ''), 'i');
                }
                // test the regex
                if (strict && format === 'dddd' && this._fullWeekdaysParse[i].test(weekdayName)) {
                    return i;
                } else if (strict && format === 'ddd' && this._shortWeekdaysParse[i].test(weekdayName)) {
                    return i;
                } else if (strict && format === 'dd' && this._minWeekdaysParse[i].test(weekdayName)) {
                    return i;
                } else if (!strict && this._weekdaysParse[i].test(weekdayName)) {
                    return i;
                }
            }
        }

        // MOMENTS

        function getSetDayOfWeek (input) {
            if (!this.isValid()) {
                return input != null ? this : NaN;
            }
            var day = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
            if (input != null) {
                input = parseWeekday(input, this.localeData());
                return this.add(input - day, 'd');
            } else {
                return day;
            }
        }

        function getSetLocaleDayOfWeek (input) {
            if (!this.isValid()) {
                return input != null ? this : NaN;
            }
            var weekday = (this.day() + 7 - this.localeData()._week.dow) % 7;
            return input == null ? weekday : this.add(input - weekday, 'd');
        }

        function getSetISODayOfWeek (input) {
            if (!this.isValid()) {
                return input != null ? this : NaN;
            }

            // behaves the same as moment#day except
            // as a getter, returns 7 instead of 0 (1-7 range instead of 0-6)
            // as a setter, sunday should belong to the previous week.

            if (input != null) {
                var weekday = parseIsoWeekday(input, this.localeData());
                return this.day(this.day() % 7 ? weekday : weekday - 7);
            } else {
                return this.day() || 7;
            }
        }

        var defaultWeekdaysRegex = matchWord;
        function weekdaysRegex (isStrict) {
            if (this._weekdaysParseExact) {
                if (!hasOwnProp(this, '_weekdaysRegex')) {
                    computeWeekdaysParse.call(this);
                }
                if (isStrict) {
                    return this._weekdaysStrictRegex;
                } else {
                    return this._weekdaysRegex;
                }
            } else {
                if (!hasOwnProp(this, '_weekdaysRegex')) {
                    this._weekdaysRegex = defaultWeekdaysRegex;
                }
                return this._weekdaysStrictRegex && isStrict ?
                    this._weekdaysStrictRegex : this._weekdaysRegex;
            }
        }

        var defaultWeekdaysShortRegex = matchWord;
        function weekdaysShortRegex (isStrict) {
            if (this._weekdaysParseExact) {
                if (!hasOwnProp(this, '_weekdaysRegex')) {
                    computeWeekdaysParse.call(this);
                }
                if (isStrict) {
                    return this._weekdaysShortStrictRegex;
                } else {
                    return this._weekdaysShortRegex;
                }
            } else {
                if (!hasOwnProp(this, '_weekdaysShortRegex')) {
                    this._weekdaysShortRegex = defaultWeekdaysShortRegex;
                }
                return this._weekdaysShortStrictRegex && isStrict ?
                    this._weekdaysShortStrictRegex : this._weekdaysShortRegex;
            }
        }

        var defaultWeekdaysMinRegex = matchWord;
        function weekdaysMinRegex (isStrict) {
            if (this._weekdaysParseExact) {
                if (!hasOwnProp(this, '_weekdaysRegex')) {
                    computeWeekdaysParse.call(this);
                }
                if (isStrict) {
                    return this._weekdaysMinStrictRegex;
                } else {
                    return this._weekdaysMinRegex;
                }
            } else {
                if (!hasOwnProp(this, '_weekdaysMinRegex')) {
                    this._weekdaysMinRegex = defaultWeekdaysMinRegex;
                }
                return this._weekdaysMinStrictRegex && isStrict ?
                    this._weekdaysMinStrictRegex : this._weekdaysMinRegex;
            }
        }


        function computeWeekdaysParse () {
            function cmpLenRev(a, b) {
                return b.length - a.length;
            }

            var minPieces = [], shortPieces = [], longPieces = [], mixedPieces = [],
                i, mom, minp, shortp, longp;
            for (i = 0; i < 7; i++) {
                // make the regex if we don't have it already
                mom = createUTC([2000, 1]).day(i);
                minp = this.weekdaysMin(mom, '');
                shortp = this.weekdaysShort(mom, '');
                longp = this.weekdays(mom, '');
                minPieces.push(minp);
                shortPieces.push(shortp);
                longPieces.push(longp);
                mixedPieces.push(minp);
                mixedPieces.push(shortp);
                mixedPieces.push(longp);
            }
            // Sorting makes sure if one weekday (or abbr) is a prefix of another it
            // will match the longer piece.
            minPieces.sort(cmpLenRev);
            shortPieces.sort(cmpLenRev);
            longPieces.sort(cmpLenRev);
            mixedPieces.sort(cmpLenRev);
            for (i = 0; i < 7; i++) {
                shortPieces[i] = regexEscape(shortPieces[i]);
                longPieces[i] = regexEscape(longPieces[i]);
                mixedPieces[i] = regexEscape(mixedPieces[i]);
            }

            this._weekdaysRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
            this._weekdaysShortRegex = this._weekdaysRegex;
            this._weekdaysMinRegex = this._weekdaysRegex;

            this._weekdaysStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
            this._weekdaysShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
            this._weekdaysMinStrictRegex = new RegExp('^(' + minPieces.join('|') + ')', 'i');
        }

        // FORMATTING

        function hFormat() {
            return this.hours() % 12 || 12;
        }

        function kFormat() {
            return this.hours() || 24;
        }

        addFormatToken('H', ['HH', 2], 0, 'hour');
        addFormatToken('h', ['hh', 2], 0, hFormat);
        addFormatToken('k', ['kk', 2], 0, kFormat);

        addFormatToken('hmm', 0, 0, function () {
            return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2);
        });

        addFormatToken('hmmss', 0, 0, function () {
            return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2) +
                zeroFill(this.seconds(), 2);
        });

        addFormatToken('Hmm', 0, 0, function () {
            return '' + this.hours() + zeroFill(this.minutes(), 2);
        });

        addFormatToken('Hmmss', 0, 0, function () {
            return '' + this.hours() + zeroFill(this.minutes(), 2) +
                zeroFill(this.seconds(), 2);
        });

        function meridiem (token, lowercase) {
            addFormatToken(token, 0, 0, function () {
                return this.localeData().meridiem(this.hours(), this.minutes(), lowercase);
            });
        }

        meridiem('a', true);
        meridiem('A', false);

        // ALIASES

        addUnitAlias('hour', 'h');

        // PRIORITY
        addUnitPriority('hour', 13);

        // PARSING

        function matchMeridiem (isStrict, locale) {
            return locale._meridiemParse;
        }

        addRegexToken('a',  matchMeridiem);
        addRegexToken('A',  matchMeridiem);
        addRegexToken('H',  match1to2);
        addRegexToken('h',  match1to2);
        addRegexToken('k',  match1to2);
        addRegexToken('HH', match1to2, match2);
        addRegexToken('hh', match1to2, match2);
        addRegexToken('kk', match1to2, match2);

        addRegexToken('hmm', match3to4);
        addRegexToken('hmmss', match5to6);
        addRegexToken('Hmm', match3to4);
        addRegexToken('Hmmss', match5to6);

        addParseToken(['H', 'HH'], HOUR);
        addParseToken(['k', 'kk'], function (input, array, config) {
            var kInput = toInt(input);
            array[HOUR] = kInput === 24 ? 0 : kInput;
        });
        addParseToken(['a', 'A'], function (input, array, config) {
            config._isPm = config._locale.isPM(input);
            config._meridiem = input;
        });
        addParseToken(['h', 'hh'], function (input, array, config) {
            array[HOUR] = toInt(input);
            getParsingFlags(config).bigHour = true;
        });
        addParseToken('hmm', function (input, array, config) {
            var pos = input.length - 2;
            array[HOUR] = toInt(input.substr(0, pos));
            array[MINUTE] = toInt(input.substr(pos));
            getParsingFlags(config).bigHour = true;
        });
        addParseToken('hmmss', function (input, array, config) {
            var pos1 = input.length - 4;
            var pos2 = input.length - 2;
            array[HOUR] = toInt(input.substr(0, pos1));
            array[MINUTE] = toInt(input.substr(pos1, 2));
            array[SECOND] = toInt(input.substr(pos2));
            getParsingFlags(config).bigHour = true;
        });
        addParseToken('Hmm', function (input, array, config) {
            var pos = input.length - 2;
            array[HOUR] = toInt(input.substr(0, pos));
            array[MINUTE] = toInt(input.substr(pos));
        });
        addParseToken('Hmmss', function (input, array, config) {
            var pos1 = input.length - 4;
            var pos2 = input.length - 2;
            array[HOUR] = toInt(input.substr(0, pos1));
            array[MINUTE] = toInt(input.substr(pos1, 2));
            array[SECOND] = toInt(input.substr(pos2));
        });

        // LOCALES

        function localeIsPM (input) {
            // IE8 Quirks Mode & IE7 Standards Mode do not allow accessing strings like arrays
            // Using charAt should be more compatible.
            return ((input + '').toLowerCase().charAt(0) === 'p');
        }

        var defaultLocaleMeridiemParse = /[ap]\.?m?\.?/i;
        function localeMeridiem (hours, minutes, isLower) {
            if (hours > 11) {
                return isLower ? 'pm' : 'PM';
            } else {
                return isLower ? 'am' : 'AM';
            }
        }


        // MOMENTS

        // Setting the hour should keep the time, because the user explicitly
        // specified which hour they want. So trying to maintain the same hour (in
        // a new timezone) makes sense. Adding/subtracting hours does not follow
        // this rule.
        var getSetHour = makeGetSet('Hours', true);

        var baseConfig = {
            calendar: defaultCalendar,
            longDateFormat: defaultLongDateFormat,
            invalidDate: defaultInvalidDate,
            ordinal: defaultOrdinal,
            dayOfMonthOrdinalParse: defaultDayOfMonthOrdinalParse,
            relativeTime: defaultRelativeTime,

            months: defaultLocaleMonths,
            monthsShort: defaultLocaleMonthsShort,

            week: defaultLocaleWeek,

            weekdays: defaultLocaleWeekdays,
            weekdaysMin: defaultLocaleWeekdaysMin,
            weekdaysShort: defaultLocaleWeekdaysShort,

            meridiemParse: defaultLocaleMeridiemParse
        };

        // internal storage for locale config files
        var locales = {};
        var localeFamilies = {};
        var globalLocale;

        function normalizeLocale(key) {
            return key ? key.toLowerCase().replace('_', '-') : key;
        }

        // pick the locale from the array
        // try ['en-au', 'en-gb'] as 'en-au', 'en-gb', 'en', as in move through the list trying each
        // substring from most specific to least, but move to the next array item if it's a more specific variant than the current root
        function chooseLocale(names) {
            var i = 0, j, next, locale, split;

            while (i < names.length) {
                split = normalizeLocale(names[i]).split('-');
                j = split.length;
                next = normalizeLocale(names[i + 1]);
                next = next ? next.split('-') : null;
                while (j > 0) {
                    locale = loadLocale(split.slice(0, j).join('-'));
                    if (locale) {
                        return locale;
                    }
                    if (next && next.length >= j && compareArrays(split, next, true) >= j - 1) {
                        //the next array item is better than a shallower substring of this one
                        break;
                    }
                    j--;
                }
                i++;
            }
            return globalLocale;
        }

        function loadLocale(name) {
            var oldLocale = null;
            // TODO: Find a better way to register and load all the locales in Node
            if (!locales[name] && ('object' !== 'undefined') &&
                    module && module.exports) {
                try {
                    oldLocale = globalLocale._abbr;
                    var aliasedRequire = commonjsRequire;
                    aliasedRequire('./locale/' + name);
                    getSetGlobalLocale(oldLocale);
                } catch (e) {}
            }
            return locales[name];
        }

        // This function will load locale and then set the global locale.  If
        // no arguments are passed in, it will simply return the current global
        // locale key.
        function getSetGlobalLocale (key, values) {
            var data;
            if (key) {
                if (isUndefined(values)) {
                    data = getLocale(key);
                }
                else {
                    data = defineLocale(key, values);
                }

                if (data) {
                    // moment.duration._locale = moment._locale = data;
                    globalLocale = data;
                }
                else {
                    if ((typeof console !==  'undefined') && console.warn) {
                        //warn user if arguments are passed but the locale could not be set
                        console.warn('Locale ' + key +  ' not found. Did you forget to load it?');
                    }
                }
            }

            return globalLocale._abbr;
        }

        function defineLocale (name, config) {
            if (config !== null) {
                var locale, parentConfig = baseConfig;
                config.abbr = name;
                if (locales[name] != null) {
                    deprecateSimple('defineLocaleOverride',
                            'use moment.updateLocale(localeName, config) to change ' +
                            'an existing locale. moment.defineLocale(localeName, ' +
                            'config) should only be used for creating a new locale ' +
                            'See http://momentjs.com/guides/#/warnings/define-locale/ for more info.');
                    parentConfig = locales[name]._config;
                } else if (config.parentLocale != null) {
                    if (locales[config.parentLocale] != null) {
                        parentConfig = locales[config.parentLocale]._config;
                    } else {
                        locale = loadLocale(config.parentLocale);
                        if (locale != null) {
                            parentConfig = locale._config;
                        } else {
                            if (!localeFamilies[config.parentLocale]) {
                                localeFamilies[config.parentLocale] = [];
                            }
                            localeFamilies[config.parentLocale].push({
                                name: name,
                                config: config
                            });
                            return null;
                        }
                    }
                }
                locales[name] = new Locale(mergeConfigs(parentConfig, config));

                if (localeFamilies[name]) {
                    localeFamilies[name].forEach(function (x) {
                        defineLocale(x.name, x.config);
                    });
                }

                // backwards compat for now: also set the locale
                // make sure we set the locale AFTER all child locales have been
                // created, so we won't end up with the child locale set.
                getSetGlobalLocale(name);


                return locales[name];
            } else {
                // useful for testing
                delete locales[name];
                return null;
            }
        }

        function updateLocale(name, config) {
            if (config != null) {
                var locale, tmpLocale, parentConfig = baseConfig;
                // MERGE
                tmpLocale = loadLocale(name);
                if (tmpLocale != null) {
                    parentConfig = tmpLocale._config;
                }
                config = mergeConfigs(parentConfig, config);
                locale = new Locale(config);
                locale.parentLocale = locales[name];
                locales[name] = locale;

                // backwards compat for now: also set the locale
                getSetGlobalLocale(name);
            } else {
                // pass null for config to unupdate, useful for tests
                if (locales[name] != null) {
                    if (locales[name].parentLocale != null) {
                        locales[name] = locales[name].parentLocale;
                    } else if (locales[name] != null) {
                        delete locales[name];
                    }
                }
            }
            return locales[name];
        }

        // returns locale data
        function getLocale (key) {
            var locale;

            if (key && key._locale && key._locale._abbr) {
                key = key._locale._abbr;
            }

            if (!key) {
                return globalLocale;
            }

            if (!isArray(key)) {
                //short-circuit everything else
                locale = loadLocale(key);
                if (locale) {
                    return locale;
                }
                key = [key];
            }

            return chooseLocale(key);
        }

        function listLocales() {
            return keys(locales);
        }

        function checkOverflow (m) {
            var overflow;
            var a = m._a;

            if (a && getParsingFlags(m).overflow === -2) {
                overflow =
                    a[MONTH]       < 0 || a[MONTH]       > 11  ? MONTH :
                    a[DATE]        < 1 || a[DATE]        > daysInMonth(a[YEAR], a[MONTH]) ? DATE :
                    a[HOUR]        < 0 || a[HOUR]        > 24 || (a[HOUR] === 24 && (a[MINUTE] !== 0 || a[SECOND] !== 0 || a[MILLISECOND] !== 0)) ? HOUR :
                    a[MINUTE]      < 0 || a[MINUTE]      > 59  ? MINUTE :
                    a[SECOND]      < 0 || a[SECOND]      > 59  ? SECOND :
                    a[MILLISECOND] < 0 || a[MILLISECOND] > 999 ? MILLISECOND :
                    -1;

                if (getParsingFlags(m)._overflowDayOfYear && (overflow < YEAR || overflow > DATE)) {
                    overflow = DATE;
                }
                if (getParsingFlags(m)._overflowWeeks && overflow === -1) {
                    overflow = WEEK;
                }
                if (getParsingFlags(m)._overflowWeekday && overflow === -1) {
                    overflow = WEEKDAY;
                }

                getParsingFlags(m).overflow = overflow;
            }

            return m;
        }

        // Pick the first defined of two or three arguments.
        function defaults(a, b, c) {
            if (a != null) {
                return a;
            }
            if (b != null) {
                return b;
            }
            return c;
        }

        function currentDateArray(config) {
            // hooks is actually the exported moment object
            var nowValue = new Date(hooks.now());
            if (config._useUTC) {
                return [nowValue.getUTCFullYear(), nowValue.getUTCMonth(), nowValue.getUTCDate()];
            }
            return [nowValue.getFullYear(), nowValue.getMonth(), nowValue.getDate()];
        }

        // convert an array to a date.
        // the array should mirror the parameters below
        // note: all values past the year are optional and will default to the lowest possible value.
        // [year, month, day , hour, minute, second, millisecond]
        function configFromArray (config) {
            var i, date, input = [], currentDate, expectedWeekday, yearToUse;

            if (config._d) {
                return;
            }

            currentDate = currentDateArray(config);

            //compute day of the year from weeks and weekdays
            if (config._w && config._a[DATE] == null && config._a[MONTH] == null) {
                dayOfYearFromWeekInfo(config);
            }

            //if the day of the year is set, figure out what it is
            if (config._dayOfYear != null) {
                yearToUse = defaults(config._a[YEAR], currentDate[YEAR]);

                if (config._dayOfYear > daysInYear(yearToUse) || config._dayOfYear === 0) {
                    getParsingFlags(config)._overflowDayOfYear = true;
                }

                date = createUTCDate(yearToUse, 0, config._dayOfYear);
                config._a[MONTH] = date.getUTCMonth();
                config._a[DATE] = date.getUTCDate();
            }

            // Default to current date.
            // * if no year, month, day of month are given, default to today
            // * if day of month is given, default month and year
            // * if month is given, default only year
            // * if year is given, don't default anything
            for (i = 0; i < 3 && config._a[i] == null; ++i) {
                config._a[i] = input[i] = currentDate[i];
            }

            // Zero out whatever was not defaulted, including time
            for (; i < 7; i++) {
                config._a[i] = input[i] = (config._a[i] == null) ? (i === 2 ? 1 : 0) : config._a[i];
            }

            // Check for 24:00:00.000
            if (config._a[HOUR] === 24 &&
                    config._a[MINUTE] === 0 &&
                    config._a[SECOND] === 0 &&
                    config._a[MILLISECOND] === 0) {
                config._nextDay = true;
                config._a[HOUR] = 0;
            }

            config._d = (config._useUTC ? createUTCDate : createDate).apply(null, input);
            expectedWeekday = config._useUTC ? config._d.getUTCDay() : config._d.getDay();

            // Apply timezone offset from input. The actual utcOffset can be changed
            // with parseZone.
            if (config._tzm != null) {
                config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);
            }

            if (config._nextDay) {
                config._a[HOUR] = 24;
            }

            // check for mismatching day of week
            if (config._w && typeof config._w.d !== 'undefined' && config._w.d !== expectedWeekday) {
                getParsingFlags(config).weekdayMismatch = true;
            }
        }

        function dayOfYearFromWeekInfo(config) {
            var w, weekYear, week, weekday, dow, doy, temp, weekdayOverflow;

            w = config._w;
            if (w.GG != null || w.W != null || w.E != null) {
                dow = 1;
                doy = 4;

                // TODO: We need to take the current isoWeekYear, but that depends on
                // how we interpret now (local, utc, fixed offset). So create
                // a now version of current config (take local/utc/offset flags, and
                // create now).
                weekYear = defaults(w.GG, config._a[YEAR], weekOfYear(createLocal(), 1, 4).year);
                week = defaults(w.W, 1);
                weekday = defaults(w.E, 1);
                if (weekday < 1 || weekday > 7) {
                    weekdayOverflow = true;
                }
            } else {
                dow = config._locale._week.dow;
                doy = config._locale._week.doy;

                var curWeek = weekOfYear(createLocal(), dow, doy);

                weekYear = defaults(w.gg, config._a[YEAR], curWeek.year);

                // Default to current week.
                week = defaults(w.w, curWeek.week);

                if (w.d != null) {
                    // weekday -- low day numbers are considered next week
                    weekday = w.d;
                    if (weekday < 0 || weekday > 6) {
                        weekdayOverflow = true;
                    }
                } else if (w.e != null) {
                    // local weekday -- counting starts from beginning of week
                    weekday = w.e + dow;
                    if (w.e < 0 || w.e > 6) {
                        weekdayOverflow = true;
                    }
                } else {
                    // default to beginning of week
                    weekday = dow;
                }
            }
            if (week < 1 || week > weeksInYear(weekYear, dow, doy)) {
                getParsingFlags(config)._overflowWeeks = true;
            } else if (weekdayOverflow != null) {
                getParsingFlags(config)._overflowWeekday = true;
            } else {
                temp = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy);
                config._a[YEAR] = temp.year;
                config._dayOfYear = temp.dayOfYear;
            }
        }

        // iso 8601 regex
        // 0000-00-00 0000-W00 or 0000-W00-0 + T + 00 or 00:00 or 00:00:00 or 00:00:00.000 + +00:00 or +0000 or +00)
        var extendedIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;
        var basicIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;

        var tzRegex = /Z|[+-]\d\d(?::?\d\d)?/;

        var isoDates = [
            ['YYYYYY-MM-DD', /[+-]\d{6}-\d\d-\d\d/],
            ['YYYY-MM-DD', /\d{4}-\d\d-\d\d/],
            ['GGGG-[W]WW-E', /\d{4}-W\d\d-\d/],
            ['GGGG-[W]WW', /\d{4}-W\d\d/, false],
            ['YYYY-DDD', /\d{4}-\d{3}/],
            ['YYYY-MM', /\d{4}-\d\d/, false],
            ['YYYYYYMMDD', /[+-]\d{10}/],
            ['YYYYMMDD', /\d{8}/],
            // YYYYMM is NOT allowed by the standard
            ['GGGG[W]WWE', /\d{4}W\d{3}/],
            ['GGGG[W]WW', /\d{4}W\d{2}/, false],
            ['YYYYDDD', /\d{7}/]
        ];

        // iso time formats and regexes
        var isoTimes = [
            ['HH:mm:ss.SSSS', /\d\d:\d\d:\d\d\.\d+/],
            ['HH:mm:ss,SSSS', /\d\d:\d\d:\d\d,\d+/],
            ['HH:mm:ss', /\d\d:\d\d:\d\d/],
            ['HH:mm', /\d\d:\d\d/],
            ['HHmmss.SSSS', /\d\d\d\d\d\d\.\d+/],
            ['HHmmss,SSSS', /\d\d\d\d\d\d,\d+/],
            ['HHmmss', /\d\d\d\d\d\d/],
            ['HHmm', /\d\d\d\d/],
            ['HH', /\d\d/]
        ];

        var aspNetJsonRegex = /^\/?Date\((\-?\d+)/i;

        // date from iso format
        function configFromISO(config) {
            var i, l,
                string = config._i,
                match = extendedIsoRegex.exec(string) || basicIsoRegex.exec(string),
                allowTime, dateFormat, timeFormat, tzFormat;

            if (match) {
                getParsingFlags(config).iso = true;

                for (i = 0, l = isoDates.length; i < l; i++) {
                    if (isoDates[i][1].exec(match[1])) {
                        dateFormat = isoDates[i][0];
                        allowTime = isoDates[i][2] !== false;
                        break;
                    }
                }
                if (dateFormat == null) {
                    config._isValid = false;
                    return;
                }
                if (match[3]) {
                    for (i = 0, l = isoTimes.length; i < l; i++) {
                        if (isoTimes[i][1].exec(match[3])) {
                            // match[2] should be 'T' or space
                            timeFormat = (match[2] || ' ') + isoTimes[i][0];
                            break;
                        }
                    }
                    if (timeFormat == null) {
                        config._isValid = false;
                        return;
                    }
                }
                if (!allowTime && timeFormat != null) {
                    config._isValid = false;
                    return;
                }
                if (match[4]) {
                    if (tzRegex.exec(match[4])) {
                        tzFormat = 'Z';
                    } else {
                        config._isValid = false;
                        return;
                    }
                }
                config._f = dateFormat + (timeFormat || '') + (tzFormat || '');
                configFromStringAndFormat(config);
            } else {
                config._isValid = false;
            }
        }

        // RFC 2822 regex: For details see https://tools.ietf.org/html/rfc2822#section-3.3
        var rfc2822 = /^(?:(Mon|Tue|Wed|Thu|Fri|Sat|Sun),?\s)?(\d{1,2})\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s(\d{2,4})\s(\d\d):(\d\d)(?::(\d\d))?\s(?:(UT|GMT|[ECMP][SD]T)|([Zz])|([+-]\d{4}))$/;

        function extractFromRFC2822Strings(yearStr, monthStr, dayStr, hourStr, minuteStr, secondStr) {
            var result = [
                untruncateYear(yearStr),
                defaultLocaleMonthsShort.indexOf(monthStr),
                parseInt(dayStr, 10),
                parseInt(hourStr, 10),
                parseInt(minuteStr, 10)
            ];

            if (secondStr) {
                result.push(parseInt(secondStr, 10));
            }

            return result;
        }

        function untruncateYear(yearStr) {
            var year = parseInt(yearStr, 10);
            if (year <= 49) {
                return 2000 + year;
            } else if (year <= 999) {
                return 1900 + year;
            }
            return year;
        }

        function preprocessRFC2822(s) {
            // Remove comments and folding whitespace and replace multiple-spaces with a single space
            return s.replace(/\([^)]*\)|[\n\t]/g, ' ').replace(/(\s\s+)/g, ' ').replace(/^\s\s*/, '').replace(/\s\s*$/, '');
        }

        function checkWeekday(weekdayStr, parsedInput, config) {
            if (weekdayStr) {
                // TODO: Replace the vanilla JS Date object with an indepentent day-of-week check.
                var weekdayProvided = defaultLocaleWeekdaysShort.indexOf(weekdayStr),
                    weekdayActual = new Date(parsedInput[0], parsedInput[1], parsedInput[2]).getDay();
                if (weekdayProvided !== weekdayActual) {
                    getParsingFlags(config).weekdayMismatch = true;
                    config._isValid = false;
                    return false;
                }
            }
            return true;
        }

        var obsOffsets = {
            UT: 0,
            GMT: 0,
            EDT: -4 * 60,
            EST: -5 * 60,
            CDT: -5 * 60,
            CST: -6 * 60,
            MDT: -6 * 60,
            MST: -7 * 60,
            PDT: -7 * 60,
            PST: -8 * 60
        };

        function calculateOffset(obsOffset, militaryOffset, numOffset) {
            if (obsOffset) {
                return obsOffsets[obsOffset];
            } else if (militaryOffset) {
                // the only allowed military tz is Z
                return 0;
            } else {
                var hm = parseInt(numOffset, 10);
                var m = hm % 100, h = (hm - m) / 100;
                return h * 60 + m;
            }
        }

        // date and time from ref 2822 format
        function configFromRFC2822(config) {
            var match = rfc2822.exec(preprocessRFC2822(config._i));
            if (match) {
                var parsedArray = extractFromRFC2822Strings(match[4], match[3], match[2], match[5], match[6], match[7]);
                if (!checkWeekday(match[1], parsedArray, config)) {
                    return;
                }

                config._a = parsedArray;
                config._tzm = calculateOffset(match[8], match[9], match[10]);

                config._d = createUTCDate.apply(null, config._a);
                config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);

                getParsingFlags(config).rfc2822 = true;
            } else {
                config._isValid = false;
            }
        }

        // date from iso format or fallback
        function configFromString(config) {
            var matched = aspNetJsonRegex.exec(config._i);

            if (matched !== null) {
                config._d = new Date(+matched[1]);
                return;
            }

            configFromISO(config);
            if (config._isValid === false) {
                delete config._isValid;
            } else {
                return;
            }

            configFromRFC2822(config);
            if (config._isValid === false) {
                delete config._isValid;
            } else {
                return;
            }

            // Final attempt, use Input Fallback
            hooks.createFromInputFallback(config);
        }

        hooks.createFromInputFallback = deprecate(
            'value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), ' +
            'which is not reliable across all browsers and versions. Non RFC2822/ISO date formats are ' +
            'discouraged and will be removed in an upcoming major release. Please refer to ' +
            'http://momentjs.com/guides/#/warnings/js-date/ for more info.',
            function (config) {
                config._d = new Date(config._i + (config._useUTC ? ' UTC' : ''));
            }
        );

        // constant that refers to the ISO standard
        hooks.ISO_8601 = function () {};

        // constant that refers to the RFC 2822 form
        hooks.RFC_2822 = function () {};

        // date from string and format string
        function configFromStringAndFormat(config) {
            // TODO: Move this to another part of the creation flow to prevent circular deps
            if (config._f === hooks.ISO_8601) {
                configFromISO(config);
                return;
            }
            if (config._f === hooks.RFC_2822) {
                configFromRFC2822(config);
                return;
            }
            config._a = [];
            getParsingFlags(config).empty = true;

            // This array is used to make a Date, either with `new Date` or `Date.UTC`
            var string = '' + config._i,
                i, parsedInput, tokens, token, skipped,
                stringLength = string.length,
                totalParsedInputLength = 0;

            tokens = expandFormat(config._f, config._locale).match(formattingTokens) || [];

            for (i = 0; i < tokens.length; i++) {
                token = tokens[i];
                parsedInput = (string.match(getParseRegexForToken(token, config)) || [])[0];
                // console.log('token', token, 'parsedInput', parsedInput,
                //         'regex', getParseRegexForToken(token, config));
                if (parsedInput) {
                    skipped = string.substr(0, string.indexOf(parsedInput));
                    if (skipped.length > 0) {
                        getParsingFlags(config).unusedInput.push(skipped);
                    }
                    string = string.slice(string.indexOf(parsedInput) + parsedInput.length);
                    totalParsedInputLength += parsedInput.length;
                }
                // don't parse if it's not a known token
                if (formatTokenFunctions[token]) {
                    if (parsedInput) {
                        getParsingFlags(config).empty = false;
                    }
                    else {
                        getParsingFlags(config).unusedTokens.push(token);
                    }
                    addTimeToArrayFromToken(token, parsedInput, config);
                }
                else if (config._strict && !parsedInput) {
                    getParsingFlags(config).unusedTokens.push(token);
                }
            }

            // add remaining unparsed input length to the string
            getParsingFlags(config).charsLeftOver = stringLength - totalParsedInputLength;
            if (string.length > 0) {
                getParsingFlags(config).unusedInput.push(string);
            }

            // clear _12h flag if hour is <= 12
            if (config._a[HOUR] <= 12 &&
                getParsingFlags(config).bigHour === true &&
                config._a[HOUR] > 0) {
                getParsingFlags(config).bigHour = undefined;
            }

            getParsingFlags(config).parsedDateParts = config._a.slice(0);
            getParsingFlags(config).meridiem = config._meridiem;
            // handle meridiem
            config._a[HOUR] = meridiemFixWrap(config._locale, config._a[HOUR], config._meridiem);

            configFromArray(config);
            checkOverflow(config);
        }


        function meridiemFixWrap (locale, hour, meridiem) {
            var isPm;

            if (meridiem == null) {
                // nothing to do
                return hour;
            }
            if (locale.meridiemHour != null) {
                return locale.meridiemHour(hour, meridiem);
            } else if (locale.isPM != null) {
                // Fallback
                isPm = locale.isPM(meridiem);
                if (isPm && hour < 12) {
                    hour += 12;
                }
                if (!isPm && hour === 12) {
                    hour = 0;
                }
                return hour;
            } else {
                // this is not supposed to happen
                return hour;
            }
        }

        // date from string and array of format strings
        function configFromStringAndArray(config) {
            var tempConfig,
                bestMoment,

                scoreToBeat,
                i,
                currentScore;

            if (config._f.length === 0) {
                getParsingFlags(config).invalidFormat = true;
                config._d = new Date(NaN);
                return;
            }

            for (i = 0; i < config._f.length; i++) {
                currentScore = 0;
                tempConfig = copyConfig({}, config);
                if (config._useUTC != null) {
                    tempConfig._useUTC = config._useUTC;
                }
                tempConfig._f = config._f[i];
                configFromStringAndFormat(tempConfig);

                if (!isValid(tempConfig)) {
                    continue;
                }

                // if there is any input that was not parsed add a penalty for that format
                currentScore += getParsingFlags(tempConfig).charsLeftOver;

                //or tokens
                currentScore += getParsingFlags(tempConfig).unusedTokens.length * 10;

                getParsingFlags(tempConfig).score = currentScore;

                if (scoreToBeat == null || currentScore < scoreToBeat) {
                    scoreToBeat = currentScore;
                    bestMoment = tempConfig;
                }
            }

            extend(config, bestMoment || tempConfig);
        }

        function configFromObject(config) {
            if (config._d) {
                return;
            }

            var i = normalizeObjectUnits(config._i);
            config._a = map([i.year, i.month, i.day || i.date, i.hour, i.minute, i.second, i.millisecond], function (obj) {
                return obj && parseInt(obj, 10);
            });

            configFromArray(config);
        }

        function createFromConfig (config) {
            var res = new Moment(checkOverflow(prepareConfig(config)));
            if (res._nextDay) {
                // Adding is smart enough around DST
                res.add(1, 'd');
                res._nextDay = undefined;
            }

            return res;
        }

        function prepareConfig (config) {
            var input = config._i,
                format = config._f;

            config._locale = config._locale || getLocale(config._l);

            if (input === null || (format === undefined && input === '')) {
                return createInvalid({nullInput: true});
            }

            if (typeof input === 'string') {
                config._i = input = config._locale.preparse(input);
            }

            if (isMoment(input)) {
                return new Moment(checkOverflow(input));
            } else if (isDate(input)) {
                config._d = input;
            } else if (isArray(format)) {
                configFromStringAndArray(config);
            } else if (format) {
                configFromStringAndFormat(config);
            }  else {
                configFromInput(config);
            }

            if (!isValid(config)) {
                config._d = null;
            }

            return config;
        }

        function configFromInput(config) {
            var input = config._i;
            if (isUndefined(input)) {
                config._d = new Date(hooks.now());
            } else if (isDate(input)) {
                config._d = new Date(input.valueOf());
            } else if (typeof input === 'string') {
                configFromString(config);
            } else if (isArray(input)) {
                config._a = map(input.slice(0), function (obj) {
                    return parseInt(obj, 10);
                });
                configFromArray(config);
            } else if (isObject(input)) {
                configFromObject(config);
            } else if (isNumber(input)) {
                // from milliseconds
                config._d = new Date(input);
            } else {
                hooks.createFromInputFallback(config);
            }
        }

        function createLocalOrUTC (input, format, locale, strict, isUTC) {
            var c = {};

            if (locale === true || locale === false) {
                strict = locale;
                locale = undefined;
            }

            if ((isObject(input) && isObjectEmpty(input)) ||
                    (isArray(input) && input.length === 0)) {
                input = undefined;
            }
            // object construction must be done this way.
            // https://github.com/moment/moment/issues/1423
            c._isAMomentObject = true;
            c._useUTC = c._isUTC = isUTC;
            c._l = locale;
            c._i = input;
            c._f = format;
            c._strict = strict;

            return createFromConfig(c);
        }

        function createLocal (input, format, locale, strict) {
            return createLocalOrUTC(input, format, locale, strict, false);
        }

        var prototypeMin = deprecate(
            'moment().min is deprecated, use moment.max instead. http://momentjs.com/guides/#/warnings/min-max/',
            function () {
                var other = createLocal.apply(null, arguments);
                if (this.isValid() && other.isValid()) {
                    return other < this ? this : other;
                } else {
                    return createInvalid();
                }
            }
        );

        var prototypeMax = deprecate(
            'moment().max is deprecated, use moment.min instead. http://momentjs.com/guides/#/warnings/min-max/',
            function () {
                var other = createLocal.apply(null, arguments);
                if (this.isValid() && other.isValid()) {
                    return other > this ? this : other;
                } else {
                    return createInvalid();
                }
            }
        );

        // Pick a moment m from moments so that m[fn](other) is true for all
        // other. This relies on the function fn to be transitive.
        //
        // moments should either be an array of moment objects or an array, whose
        // first element is an array of moment objects.
        function pickBy(fn, moments) {
            var res, i;
            if (moments.length === 1 && isArray(moments[0])) {
                moments = moments[0];
            }
            if (!moments.length) {
                return createLocal();
            }
            res = moments[0];
            for (i = 1; i < moments.length; ++i) {
                if (!moments[i].isValid() || moments[i][fn](res)) {
                    res = moments[i];
                }
            }
            return res;
        }

        // TODO: Use [].sort instead?
        function min () {
            var args = [].slice.call(arguments, 0);

            return pickBy('isBefore', args);
        }

        function max () {
            var args = [].slice.call(arguments, 0);

            return pickBy('isAfter', args);
        }

        var now = function () {
            return Date.now ? Date.now() : +(new Date());
        };

        var ordering = ['year', 'quarter', 'month', 'week', 'day', 'hour', 'minute', 'second', 'millisecond'];

        function isDurationValid(m) {
            for (var key in m) {
                if (!(indexOf.call(ordering, key) !== -1 && (m[key] == null || !isNaN(m[key])))) {
                    return false;
                }
            }

            var unitHasDecimal = false;
            for (var i = 0; i < ordering.length; ++i) {
                if (m[ordering[i]]) {
                    if (unitHasDecimal) {
                        return false; // only allow non-integers for smallest unit
                    }
                    if (parseFloat(m[ordering[i]]) !== toInt(m[ordering[i]])) {
                        unitHasDecimal = true;
                    }
                }
            }

            return true;
        }

        function isValid$1() {
            return this._isValid;
        }

        function createInvalid$1() {
            return createDuration(NaN);
        }

        function Duration (duration) {
            var normalizedInput = normalizeObjectUnits(duration),
                years = normalizedInput.year || 0,
                quarters = normalizedInput.quarter || 0,
                months = normalizedInput.month || 0,
                weeks = normalizedInput.week || normalizedInput.isoWeek || 0,
                days = normalizedInput.day || 0,
                hours = normalizedInput.hour || 0,
                minutes = normalizedInput.minute || 0,
                seconds = normalizedInput.second || 0,
                milliseconds = normalizedInput.millisecond || 0;

            this._isValid = isDurationValid(normalizedInput);

            // representation for dateAddRemove
            this._milliseconds = +milliseconds +
                seconds * 1e3 + // 1000
                minutes * 6e4 + // 1000 * 60
                hours * 1000 * 60 * 60; //using 1000 * 60 * 60 instead of 36e5 to avoid floating point rounding errors https://github.com/moment/moment/issues/2978
            // Because of dateAddRemove treats 24 hours as different from a
            // day when working around DST, we need to store them separately
            this._days = +days +
                weeks * 7;
            // It is impossible to translate months into days without knowing
            // which months you are are talking about, so we have to store
            // it separately.
            this._months = +months +
                quarters * 3 +
                years * 12;

            this._data = {};

            this._locale = getLocale();

            this._bubble();
        }

        function isDuration (obj) {
            return obj instanceof Duration;
        }

        function absRound (number) {
            if (number < 0) {
                return Math.round(-1 * number) * -1;
            } else {
                return Math.round(number);
            }
        }

        // FORMATTING

        function offset (token, separator) {
            addFormatToken(token, 0, 0, function () {
                var offset = this.utcOffset();
                var sign = '+';
                if (offset < 0) {
                    offset = -offset;
                    sign = '-';
                }
                return sign + zeroFill(~~(offset / 60), 2) + separator + zeroFill(~~(offset) % 60, 2);
            });
        }

        offset('Z', ':');
        offset('ZZ', '');

        // PARSING

        addRegexToken('Z',  matchShortOffset);
        addRegexToken('ZZ', matchShortOffset);
        addParseToken(['Z', 'ZZ'], function (input, array, config) {
            config._useUTC = true;
            config._tzm = offsetFromString(matchShortOffset, input);
        });

        // HELPERS

        // timezone chunker
        // '+10:00' > ['10',  '00']
        // '-1530'  > ['-15', '30']
        var chunkOffset = /([\+\-]|\d\d)/gi;

        function offsetFromString(matcher, string) {
            var matches = (string || '').match(matcher);

            if (matches === null) {
                return null;
            }

            var chunk   = matches[matches.length - 1] || [];
            var parts   = (chunk + '').match(chunkOffset) || ['-', 0, 0];
            var minutes = +(parts[1] * 60) + toInt(parts[2]);

            return minutes === 0 ?
              0 :
              parts[0] === '+' ? minutes : -minutes;
        }

        // Return a moment from input, that is local/utc/zone equivalent to model.
        function cloneWithOffset(input, model) {
            var res, diff;
            if (model._isUTC) {
                res = model.clone();
                diff = (isMoment(input) || isDate(input) ? input.valueOf() : createLocal(input).valueOf()) - res.valueOf();
                // Use low-level api, because this fn is low-level api.
                res._d.setTime(res._d.valueOf() + diff);
                hooks.updateOffset(res, false);
                return res;
            } else {
                return createLocal(input).local();
            }
        }

        function getDateOffset (m) {
            // On Firefox.24 Date#getTimezoneOffset returns a floating point.
            // https://github.com/moment/moment/pull/1871
            return -Math.round(m._d.getTimezoneOffset() / 15) * 15;
        }

        // HOOKS

        // This function will be called whenever a moment is mutated.
        // It is intended to keep the offset in sync with the timezone.
        hooks.updateOffset = function () {};

        // MOMENTS

        // keepLocalTime = true means only change the timezone, without
        // affecting the local hour. So 5:31:26 +0300 --[utcOffset(2, true)]-->
        // 5:31:26 +0200 It is possible that 5:31:26 doesn't exist with offset
        // +0200, so we adjust the time as needed, to be valid.
        //
        // Keeping the time actually adds/subtracts (one hour)
        // from the actual represented time. That is why we call updateOffset
        // a second time. In case it wants us to change the offset again
        // _changeInProgress == true case, then we have to adjust, because
        // there is no such time in the given timezone.
        function getSetOffset (input, keepLocalTime, keepMinutes) {
            var offset = this._offset || 0,
                localAdjust;
            if (!this.isValid()) {
                return input != null ? this : NaN;
            }
            if (input != null) {
                if (typeof input === 'string') {
                    input = offsetFromString(matchShortOffset, input);
                    if (input === null) {
                        return this;
                    }
                } else if (Math.abs(input) < 16 && !keepMinutes) {
                    input = input * 60;
                }
                if (!this._isUTC && keepLocalTime) {
                    localAdjust = getDateOffset(this);
                }
                this._offset = input;
                this._isUTC = true;
                if (localAdjust != null) {
                    this.add(localAdjust, 'm');
                }
                if (offset !== input) {
                    if (!keepLocalTime || this._changeInProgress) {
                        addSubtract(this, createDuration(input - offset, 'm'), 1, false);
                    } else if (!this._changeInProgress) {
                        this._changeInProgress = true;
                        hooks.updateOffset(this, true);
                        this._changeInProgress = null;
                    }
                }
                return this;
            } else {
                return this._isUTC ? offset : getDateOffset(this);
            }
        }

        function getSetZone (input, keepLocalTime) {
            if (input != null) {
                if (typeof input !== 'string') {
                    input = -input;
                }

                this.utcOffset(input, keepLocalTime);

                return this;
            } else {
                return -this.utcOffset();
            }
        }

        function setOffsetToUTC (keepLocalTime) {
            return this.utcOffset(0, keepLocalTime);
        }

        function setOffsetToLocal (keepLocalTime) {
            if (this._isUTC) {
                this.utcOffset(0, keepLocalTime);
                this._isUTC = false;

                if (keepLocalTime) {
                    this.subtract(getDateOffset(this), 'm');
                }
            }
            return this;
        }

        function setOffsetToParsedOffset () {
            if (this._tzm != null) {
                this.utcOffset(this._tzm, false, true);
            } else if (typeof this._i === 'string') {
                var tZone = offsetFromString(matchOffset, this._i);
                if (tZone != null) {
                    this.utcOffset(tZone);
                }
                else {
                    this.utcOffset(0, true);
                }
            }
            return this;
        }

        function hasAlignedHourOffset (input) {
            if (!this.isValid()) {
                return false;
            }
            input = input ? createLocal(input).utcOffset() : 0;

            return (this.utcOffset() - input) % 60 === 0;
        }

        function isDaylightSavingTime () {
            return (
                this.utcOffset() > this.clone().month(0).utcOffset() ||
                this.utcOffset() > this.clone().month(5).utcOffset()
            );
        }

        function isDaylightSavingTimeShifted () {
            if (!isUndefined(this._isDSTShifted)) {
                return this._isDSTShifted;
            }

            var c = {};

            copyConfig(c, this);
            c = prepareConfig(c);

            if (c._a) {
                var other = c._isUTC ? createUTC(c._a) : createLocal(c._a);
                this._isDSTShifted = this.isValid() &&
                    compareArrays(c._a, other.toArray()) > 0;
            } else {
                this._isDSTShifted = false;
            }

            return this._isDSTShifted;
        }

        function isLocal () {
            return this.isValid() ? !this._isUTC : false;
        }

        function isUtcOffset () {
            return this.isValid() ? this._isUTC : false;
        }

        function isUtc () {
            return this.isValid() ? this._isUTC && this._offset === 0 : false;
        }

        // ASP.NET json date format regex
        var aspNetRegex = /^(\-|\+)?(?:(\d*)[. ])?(\d+)\:(\d+)(?:\:(\d+)(\.\d*)?)?$/;

        // from http://docs.closure-library.googlecode.com/git/closure_goog_date_date.js.source.html
        // somewhat more in line with 4.4.3.2 2004 spec, but allows decimal anywhere
        // and further modified to allow for strings containing both week and day
        var isoRegex = /^(-|\+)?P(?:([-+]?[0-9,.]*)Y)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)W)?(?:([-+]?[0-9,.]*)D)?(?:T(?:([-+]?[0-9,.]*)H)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)S)?)?$/;

        function createDuration (input, key) {
            var duration = input,
                // matching against regexp is expensive, do it on demand
                match = null,
                sign,
                ret,
                diffRes;

            if (isDuration(input)) {
                duration = {
                    ms : input._milliseconds,
                    d  : input._days,
                    M  : input._months
                };
            } else if (isNumber(input)) {
                duration = {};
                if (key) {
                    duration[key] = input;
                } else {
                    duration.milliseconds = input;
                }
            } else if (!!(match = aspNetRegex.exec(input))) {
                sign = (match[1] === '-') ? -1 : 1;
                duration = {
                    y  : 0,
                    d  : toInt(match[DATE])                         * sign,
                    h  : toInt(match[HOUR])                         * sign,
                    m  : toInt(match[MINUTE])                       * sign,
                    s  : toInt(match[SECOND])                       * sign,
                    ms : toInt(absRound(match[MILLISECOND] * 1000)) * sign // the millisecond decimal point is included in the match
                };
            } else if (!!(match = isoRegex.exec(input))) {
                sign = (match[1] === '-') ? -1 : 1;
                duration = {
                    y : parseIso(match[2], sign),
                    M : parseIso(match[3], sign),
                    w : parseIso(match[4], sign),
                    d : parseIso(match[5], sign),
                    h : parseIso(match[6], sign),
                    m : parseIso(match[7], sign),
                    s : parseIso(match[8], sign)
                };
            } else if (duration == null) {// checks for null or undefined
                duration = {};
            } else if (typeof duration === 'object' && ('from' in duration || 'to' in duration)) {
                diffRes = momentsDifference(createLocal(duration.from), createLocal(duration.to));

                duration = {};
                duration.ms = diffRes.milliseconds;
                duration.M = diffRes.months;
            }

            ret = new Duration(duration);

            if (isDuration(input) && hasOwnProp(input, '_locale')) {
                ret._locale = input._locale;
            }

            return ret;
        }

        createDuration.fn = Duration.prototype;
        createDuration.invalid = createInvalid$1;

        function parseIso (inp, sign) {
            // We'd normally use ~~inp for this, but unfortunately it also
            // converts floats to ints.
            // inp may be undefined, so careful calling replace on it.
            var res = inp && parseFloat(inp.replace(',', '.'));
            // apply sign while we're at it
            return (isNaN(res) ? 0 : res) * sign;
        }

        function positiveMomentsDifference(base, other) {
            var res = {};

            res.months = other.month() - base.month() +
                (other.year() - base.year()) * 12;
            if (base.clone().add(res.months, 'M').isAfter(other)) {
                --res.months;
            }

            res.milliseconds = +other - +(base.clone().add(res.months, 'M'));

            return res;
        }

        function momentsDifference(base, other) {
            var res;
            if (!(base.isValid() && other.isValid())) {
                return {milliseconds: 0, months: 0};
            }

            other = cloneWithOffset(other, base);
            if (base.isBefore(other)) {
                res = positiveMomentsDifference(base, other);
            } else {
                res = positiveMomentsDifference(other, base);
                res.milliseconds = -res.milliseconds;
                res.months = -res.months;
            }

            return res;
        }

        // TODO: remove 'name' arg after deprecation is removed
        function createAdder(direction, name) {
            return function (val, period) {
                var dur, tmp;
                //invert the arguments, but complain about it
                if (period !== null && !isNaN(+period)) {
                    deprecateSimple(name, 'moment().' + name  + '(period, number) is deprecated. Please use moment().' + name + '(number, period). ' +
                    'See http://momentjs.com/guides/#/warnings/add-inverted-param/ for more info.');
                    tmp = val; val = period; period = tmp;
                }

                val = typeof val === 'string' ? +val : val;
                dur = createDuration(val, period);
                addSubtract(this, dur, direction);
                return this;
            };
        }

        function addSubtract (mom, duration, isAdding, updateOffset) {
            var milliseconds = duration._milliseconds,
                days = absRound(duration._days),
                months = absRound(duration._months);

            if (!mom.isValid()) {
                // No op
                return;
            }

            updateOffset = updateOffset == null ? true : updateOffset;

            if (months) {
                setMonth(mom, get(mom, 'Month') + months * isAdding);
            }
            if (days) {
                set$1(mom, 'Date', get(mom, 'Date') + days * isAdding);
            }
            if (milliseconds) {
                mom._d.setTime(mom._d.valueOf() + milliseconds * isAdding);
            }
            if (updateOffset) {
                hooks.updateOffset(mom, days || months);
            }
        }

        var add      = createAdder(1, 'add');
        var subtract = createAdder(-1, 'subtract');

        function getCalendarFormat(myMoment, now) {
            var diff = myMoment.diff(now, 'days', true);
            return diff < -6 ? 'sameElse' :
                    diff < -1 ? 'lastWeek' :
                    diff < 0 ? 'lastDay' :
                    diff < 1 ? 'sameDay' :
                    diff < 2 ? 'nextDay' :
                    diff < 7 ? 'nextWeek' : 'sameElse';
        }

        function calendar$1 (time, formats) {
            // We want to compare the start of today, vs this.
            // Getting start-of-today depends on whether we're local/utc/offset or not.
            var now = time || createLocal(),
                sod = cloneWithOffset(now, this).startOf('day'),
                format = hooks.calendarFormat(this, sod) || 'sameElse';

            var output = formats && (isFunction(formats[format]) ? formats[format].call(this, now) : formats[format]);

            return this.format(output || this.localeData().calendar(format, this, createLocal(now)));
        }

        function clone () {
            return new Moment(this);
        }

        function isAfter (input, units) {
            var localInput = isMoment(input) ? input : createLocal(input);
            if (!(this.isValid() && localInput.isValid())) {
                return false;
            }
            units = normalizeUnits(units) || 'millisecond';
            if (units === 'millisecond') {
                return this.valueOf() > localInput.valueOf();
            } else {
                return localInput.valueOf() < this.clone().startOf(units).valueOf();
            }
        }

        function isBefore (input, units) {
            var localInput = isMoment(input) ? input : createLocal(input);
            if (!(this.isValid() && localInput.isValid())) {
                return false;
            }
            units = normalizeUnits(units) || 'millisecond';
            if (units === 'millisecond') {
                return this.valueOf() < localInput.valueOf();
            } else {
                return this.clone().endOf(units).valueOf() < localInput.valueOf();
            }
        }

        function isBetween (from, to, units, inclusivity) {
            var localFrom = isMoment(from) ? from : createLocal(from),
                localTo = isMoment(to) ? to : createLocal(to);
            if (!(this.isValid() && localFrom.isValid() && localTo.isValid())) {
                return false;
            }
            inclusivity = inclusivity || '()';
            return (inclusivity[0] === '(' ? this.isAfter(localFrom, units) : !this.isBefore(localFrom, units)) &&
                (inclusivity[1] === ')' ? this.isBefore(localTo, units) : !this.isAfter(localTo, units));
        }

        function isSame (input, units) {
            var localInput = isMoment(input) ? input : createLocal(input),
                inputMs;
            if (!(this.isValid() && localInput.isValid())) {
                return false;
            }
            units = normalizeUnits(units) || 'millisecond';
            if (units === 'millisecond') {
                return this.valueOf() === localInput.valueOf();
            } else {
                inputMs = localInput.valueOf();
                return this.clone().startOf(units).valueOf() <= inputMs && inputMs <= this.clone().endOf(units).valueOf();
            }
        }

        function isSameOrAfter (input, units) {
            return this.isSame(input, units) || this.isAfter(input, units);
        }

        function isSameOrBefore (input, units) {
            return this.isSame(input, units) || this.isBefore(input, units);
        }

        function diff (input, units, asFloat) {
            var that,
                zoneDelta,
                output;

            if (!this.isValid()) {
                return NaN;
            }

            that = cloneWithOffset(input, this);

            if (!that.isValid()) {
                return NaN;
            }

            zoneDelta = (that.utcOffset() - this.utcOffset()) * 6e4;

            units = normalizeUnits(units);

            switch (units) {
                case 'year': output = monthDiff(this, that) / 12; break;
                case 'month': output = monthDiff(this, that); break;
                case 'quarter': output = monthDiff(this, that) / 3; break;
                case 'second': output = (this - that) / 1e3; break; // 1000
                case 'minute': output = (this - that) / 6e4; break; // 1000 * 60
                case 'hour': output = (this - that) / 36e5; break; // 1000 * 60 * 60
                case 'day': output = (this - that - zoneDelta) / 864e5; break; // 1000 * 60 * 60 * 24, negate dst
                case 'week': output = (this - that - zoneDelta) / 6048e5; break; // 1000 * 60 * 60 * 24 * 7, negate dst
                default: output = this - that;
            }

            return asFloat ? output : absFloor(output);
        }

        function monthDiff (a, b) {
            // difference in months
            var wholeMonthDiff = ((b.year() - a.year()) * 12) + (b.month() - a.month()),
                // b is in (anchor - 1 month, anchor + 1 month)
                anchor = a.clone().add(wholeMonthDiff, 'months'),
                anchor2, adjust;

            if (b - anchor < 0) {
                anchor2 = a.clone().add(wholeMonthDiff - 1, 'months');
                // linear across the month
                adjust = (b - anchor) / (anchor - anchor2);
            } else {
                anchor2 = a.clone().add(wholeMonthDiff + 1, 'months');
                // linear across the month
                adjust = (b - anchor) / (anchor2 - anchor);
            }

            //check for negative zero, return zero if negative zero
            return -(wholeMonthDiff + adjust) || 0;
        }

        hooks.defaultFormat = 'YYYY-MM-DDTHH:mm:ssZ';
        hooks.defaultFormatUtc = 'YYYY-MM-DDTHH:mm:ss[Z]';

        function toString () {
            return this.clone().locale('en').format('ddd MMM DD YYYY HH:mm:ss [GMT]ZZ');
        }

        function toISOString(keepOffset) {
            if (!this.isValid()) {
                return null;
            }
            var utc = keepOffset !== true;
            var m = utc ? this.clone().utc() : this;
            if (m.year() < 0 || m.year() > 9999) {
                return formatMoment(m, utc ? 'YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]' : 'YYYYYY-MM-DD[T]HH:mm:ss.SSSZ');
            }
            if (isFunction(Date.prototype.toISOString)) {
                // native implementation is ~50x faster, use it when we can
                if (utc) {
                    return this.toDate().toISOString();
                } else {
                    return new Date(this.valueOf() + this.utcOffset() * 60 * 1000).toISOString().replace('Z', formatMoment(m, 'Z'));
                }
            }
            return formatMoment(m, utc ? 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]' : 'YYYY-MM-DD[T]HH:mm:ss.SSSZ');
        }

        /**
         * Return a human readable representation of a moment that can
         * also be evaluated to get a new moment which is the same
         *
         * @link https://nodejs.org/dist/latest/docs/api/util.html#util_custom_inspect_function_on_objects
         */
        function inspect () {
            if (!this.isValid()) {
                return 'moment.invalid(/* ' + this._i + ' */)';
            }
            var func = 'moment';
            var zone = '';
            if (!this.isLocal()) {
                func = this.utcOffset() === 0 ? 'moment.utc' : 'moment.parseZone';
                zone = 'Z';
            }
            var prefix = '[' + func + '("]';
            var year = (0 <= this.year() && this.year() <= 9999) ? 'YYYY' : 'YYYYYY';
            var datetime = '-MM-DD[T]HH:mm:ss.SSS';
            var suffix = zone + '[")]';

            return this.format(prefix + year + datetime + suffix);
        }

        function format (inputString) {
            if (!inputString) {
                inputString = this.isUtc() ? hooks.defaultFormatUtc : hooks.defaultFormat;
            }
            var output = formatMoment(this, inputString);
            return this.localeData().postformat(output);
        }

        function from (time, withoutSuffix) {
            if (this.isValid() &&
                    ((isMoment(time) && time.isValid()) ||
                     createLocal(time).isValid())) {
                return createDuration({to: this, from: time}).locale(this.locale()).humanize(!withoutSuffix);
            } else {
                return this.localeData().invalidDate();
            }
        }

        function fromNow (withoutSuffix) {
            return this.from(createLocal(), withoutSuffix);
        }

        function to (time, withoutSuffix) {
            if (this.isValid() &&
                    ((isMoment(time) && time.isValid()) ||
                     createLocal(time).isValid())) {
                return createDuration({from: this, to: time}).locale(this.locale()).humanize(!withoutSuffix);
            } else {
                return this.localeData().invalidDate();
            }
        }

        function toNow (withoutSuffix) {
            return this.to(createLocal(), withoutSuffix);
        }

        // If passed a locale key, it will set the locale for this
        // instance.  Otherwise, it will return the locale configuration
        // variables for this instance.
        function locale (key) {
            var newLocaleData;

            if (key === undefined) {
                return this._locale._abbr;
            } else {
                newLocaleData = getLocale(key);
                if (newLocaleData != null) {
                    this._locale = newLocaleData;
                }
                return this;
            }
        }

        var lang = deprecate(
            'moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.',
            function (key) {
                if (key === undefined) {
                    return this.localeData();
                } else {
                    return this.locale(key);
                }
            }
        );

        function localeData () {
            return this._locale;
        }

        var MS_PER_SECOND = 1000;
        var MS_PER_MINUTE = 60 * MS_PER_SECOND;
        var MS_PER_HOUR = 60 * MS_PER_MINUTE;
        var MS_PER_400_YEARS = (365 * 400 + 97) * 24 * MS_PER_HOUR;

        // actual modulo - handles negative numbers (for dates before 1970):
        function mod$1(dividend, divisor) {
            return (dividend % divisor + divisor) % divisor;
        }

        function localStartOfDate(y, m, d) {
            // the date constructor remaps years 0-99 to 1900-1999
            if (y < 100 && y >= 0) {
                // preserve leap years using a full 400 year cycle, then reset
                return new Date(y + 400, m, d) - MS_PER_400_YEARS;
            } else {
                return new Date(y, m, d).valueOf();
            }
        }

        function utcStartOfDate(y, m, d) {
            // Date.UTC remaps years 0-99 to 1900-1999
            if (y < 100 && y >= 0) {
                // preserve leap years using a full 400 year cycle, then reset
                return Date.UTC(y + 400, m, d) - MS_PER_400_YEARS;
            } else {
                return Date.UTC(y, m, d);
            }
        }

        function startOf (units) {
            var time;
            units = normalizeUnits(units);
            if (units === undefined || units === 'millisecond' || !this.isValid()) {
                return this;
            }

            var startOfDate = this._isUTC ? utcStartOfDate : localStartOfDate;

            switch (units) {
                case 'year':
                    time = startOfDate(this.year(), 0, 1);
                    break;
                case 'quarter':
                    time = startOfDate(this.year(), this.month() - this.month() % 3, 1);
                    break;
                case 'month':
                    time = startOfDate(this.year(), this.month(), 1);
                    break;
                case 'week':
                    time = startOfDate(this.year(), this.month(), this.date() - this.weekday());
                    break;
                case 'isoWeek':
                    time = startOfDate(this.year(), this.month(), this.date() - (this.isoWeekday() - 1));
                    break;
                case 'day':
                case 'date':
                    time = startOfDate(this.year(), this.month(), this.date());
                    break;
                case 'hour':
                    time = this._d.valueOf();
                    time -= mod$1(time + (this._isUTC ? 0 : this.utcOffset() * MS_PER_MINUTE), MS_PER_HOUR);
                    break;
                case 'minute':
                    time = this._d.valueOf();
                    time -= mod$1(time, MS_PER_MINUTE);
                    break;
                case 'second':
                    time = this._d.valueOf();
                    time -= mod$1(time, MS_PER_SECOND);
                    break;
            }

            this._d.setTime(time);
            hooks.updateOffset(this, true);
            return this;
        }

        function endOf (units) {
            var time;
            units = normalizeUnits(units);
            if (units === undefined || units === 'millisecond' || !this.isValid()) {
                return this;
            }

            var startOfDate = this._isUTC ? utcStartOfDate : localStartOfDate;

            switch (units) {
                case 'year':
                    time = startOfDate(this.year() + 1, 0, 1) - 1;
                    break;
                case 'quarter':
                    time = startOfDate(this.year(), this.month() - this.month() % 3 + 3, 1) - 1;
                    break;
                case 'month':
                    time = startOfDate(this.year(), this.month() + 1, 1) - 1;
                    break;
                case 'week':
                    time = startOfDate(this.year(), this.month(), this.date() - this.weekday() + 7) - 1;
                    break;
                case 'isoWeek':
                    time = startOfDate(this.year(), this.month(), this.date() - (this.isoWeekday() - 1) + 7) - 1;
                    break;
                case 'day':
                case 'date':
                    time = startOfDate(this.year(), this.month(), this.date() + 1) - 1;
                    break;
                case 'hour':
                    time = this._d.valueOf();
                    time += MS_PER_HOUR - mod$1(time + (this._isUTC ? 0 : this.utcOffset() * MS_PER_MINUTE), MS_PER_HOUR) - 1;
                    break;
                case 'minute':
                    time = this._d.valueOf();
                    time += MS_PER_MINUTE - mod$1(time, MS_PER_MINUTE) - 1;
                    break;
                case 'second':
                    time = this._d.valueOf();
                    time += MS_PER_SECOND - mod$1(time, MS_PER_SECOND) - 1;
                    break;
            }

            this._d.setTime(time);
            hooks.updateOffset(this, true);
            return this;
        }

        function valueOf () {
            return this._d.valueOf() - ((this._offset || 0) * 60000);
        }

        function unix () {
            return Math.floor(this.valueOf() / 1000);
        }

        function toDate () {
            return new Date(this.valueOf());
        }

        function toArray () {
            var m = this;
            return [m.year(), m.month(), m.date(), m.hour(), m.minute(), m.second(), m.millisecond()];
        }

        function toObject () {
            var m = this;
            return {
                years: m.year(),
                months: m.month(),
                date: m.date(),
                hours: m.hours(),
                minutes: m.minutes(),
                seconds: m.seconds(),
                milliseconds: m.milliseconds()
            };
        }

        function toJSON () {
            // new Date(NaN).toJSON() === null
            return this.isValid() ? this.toISOString() : null;
        }

        function isValid$2 () {
            return isValid(this);
        }

        function parsingFlags () {
            return extend({}, getParsingFlags(this));
        }

        function invalidAt () {
            return getParsingFlags(this).overflow;
        }

        function creationData() {
            return {
                input: this._i,
                format: this._f,
                locale: this._locale,
                isUTC: this._isUTC,
                strict: this._strict
            };
        }

        // FORMATTING

        addFormatToken(0, ['gg', 2], 0, function () {
            return this.weekYear() % 100;
        });

        addFormatToken(0, ['GG', 2], 0, function () {
            return this.isoWeekYear() % 100;
        });

        function addWeekYearFormatToken (token, getter) {
            addFormatToken(0, [token, token.length], 0, getter);
        }

        addWeekYearFormatToken('gggg',     'weekYear');
        addWeekYearFormatToken('ggggg',    'weekYear');
        addWeekYearFormatToken('GGGG',  'isoWeekYear');
        addWeekYearFormatToken('GGGGG', 'isoWeekYear');

        // ALIASES

        addUnitAlias('weekYear', 'gg');
        addUnitAlias('isoWeekYear', 'GG');

        // PRIORITY

        addUnitPriority('weekYear', 1);
        addUnitPriority('isoWeekYear', 1);


        // PARSING

        addRegexToken('G',      matchSigned);
        addRegexToken('g',      matchSigned);
        addRegexToken('GG',     match1to2, match2);
        addRegexToken('gg',     match1to2, match2);
        addRegexToken('GGGG',   match1to4, match4);
        addRegexToken('gggg',   match1to4, match4);
        addRegexToken('GGGGG',  match1to6, match6);
        addRegexToken('ggggg',  match1to6, match6);

        addWeekParseToken(['gggg', 'ggggg', 'GGGG', 'GGGGG'], function (input, week, config, token) {
            week[token.substr(0, 2)] = toInt(input);
        });

        addWeekParseToken(['gg', 'GG'], function (input, week, config, token) {
            week[token] = hooks.parseTwoDigitYear(input);
        });

        // MOMENTS

        function getSetWeekYear (input) {
            return getSetWeekYearHelper.call(this,
                    input,
                    this.week(),
                    this.weekday(),
                    this.localeData()._week.dow,
                    this.localeData()._week.doy);
        }

        function getSetISOWeekYear (input) {
            return getSetWeekYearHelper.call(this,
                    input, this.isoWeek(), this.isoWeekday(), 1, 4);
        }

        function getISOWeeksInYear () {
            return weeksInYear(this.year(), 1, 4);
        }

        function getWeeksInYear () {
            var weekInfo = this.localeData()._week;
            return weeksInYear(this.year(), weekInfo.dow, weekInfo.doy);
        }

        function getSetWeekYearHelper(input, week, weekday, dow, doy) {
            var weeksTarget;
            if (input == null) {
                return weekOfYear(this, dow, doy).year;
            } else {
                weeksTarget = weeksInYear(input, dow, doy);
                if (week > weeksTarget) {
                    week = weeksTarget;
                }
                return setWeekAll.call(this, input, week, weekday, dow, doy);
            }
        }

        function setWeekAll(weekYear, week, weekday, dow, doy) {
            var dayOfYearData = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy),
                date = createUTCDate(dayOfYearData.year, 0, dayOfYearData.dayOfYear);

            this.year(date.getUTCFullYear());
            this.month(date.getUTCMonth());
            this.date(date.getUTCDate());
            return this;
        }

        // FORMATTING

        addFormatToken('Q', 0, 'Qo', 'quarter');

        // ALIASES

        addUnitAlias('quarter', 'Q');

        // PRIORITY

        addUnitPriority('quarter', 7);

        // PARSING

        addRegexToken('Q', match1);
        addParseToken('Q', function (input, array) {
            array[MONTH] = (toInt(input) - 1) * 3;
        });

        // MOMENTS

        function getSetQuarter (input) {
            return input == null ? Math.ceil((this.month() + 1) / 3) : this.month((input - 1) * 3 + this.month() % 3);
        }

        // FORMATTING

        addFormatToken('D', ['DD', 2], 'Do', 'date');

        // ALIASES

        addUnitAlias('date', 'D');

        // PRIORITY
        addUnitPriority('date', 9);

        // PARSING

        addRegexToken('D',  match1to2);
        addRegexToken('DD', match1to2, match2);
        addRegexToken('Do', function (isStrict, locale) {
            // TODO: Remove "ordinalParse" fallback in next major release.
            return isStrict ?
              (locale._dayOfMonthOrdinalParse || locale._ordinalParse) :
              locale._dayOfMonthOrdinalParseLenient;
        });

        addParseToken(['D', 'DD'], DATE);
        addParseToken('Do', function (input, array) {
            array[DATE] = toInt(input.match(match1to2)[0]);
        });

        // MOMENTS

        var getSetDayOfMonth = makeGetSet('Date', true);

        // FORMATTING

        addFormatToken('DDD', ['DDDD', 3], 'DDDo', 'dayOfYear');

        // ALIASES

        addUnitAlias('dayOfYear', 'DDD');

        // PRIORITY
        addUnitPriority('dayOfYear', 4);

        // PARSING

        addRegexToken('DDD',  match1to3);
        addRegexToken('DDDD', match3);
        addParseToken(['DDD', 'DDDD'], function (input, array, config) {
            config._dayOfYear = toInt(input);
        });

        // HELPERS

        // MOMENTS

        function getSetDayOfYear (input) {
            var dayOfYear = Math.round((this.clone().startOf('day') - this.clone().startOf('year')) / 864e5) + 1;
            return input == null ? dayOfYear : this.add((input - dayOfYear), 'd');
        }

        // FORMATTING

        addFormatToken('m', ['mm', 2], 0, 'minute');

        // ALIASES

        addUnitAlias('minute', 'm');

        // PRIORITY

        addUnitPriority('minute', 14);

        // PARSING

        addRegexToken('m',  match1to2);
        addRegexToken('mm', match1to2, match2);
        addParseToken(['m', 'mm'], MINUTE);

        // MOMENTS

        var getSetMinute = makeGetSet('Minutes', false);

        // FORMATTING

        addFormatToken('s', ['ss', 2], 0, 'second');

        // ALIASES

        addUnitAlias('second', 's');

        // PRIORITY

        addUnitPriority('second', 15);

        // PARSING

        addRegexToken('s',  match1to2);
        addRegexToken('ss', match1to2, match2);
        addParseToken(['s', 'ss'], SECOND);

        // MOMENTS

        var getSetSecond = makeGetSet('Seconds', false);

        // FORMATTING

        addFormatToken('S', 0, 0, function () {
            return ~~(this.millisecond() / 100);
        });

        addFormatToken(0, ['SS', 2], 0, function () {
            return ~~(this.millisecond() / 10);
        });

        addFormatToken(0, ['SSS', 3], 0, 'millisecond');
        addFormatToken(0, ['SSSS', 4], 0, function () {
            return this.millisecond() * 10;
        });
        addFormatToken(0, ['SSSSS', 5], 0, function () {
            return this.millisecond() * 100;
        });
        addFormatToken(0, ['SSSSSS', 6], 0, function () {
            return this.millisecond() * 1000;
        });
        addFormatToken(0, ['SSSSSSS', 7], 0, function () {
            return this.millisecond() * 10000;
        });
        addFormatToken(0, ['SSSSSSSS', 8], 0, function () {
            return this.millisecond() * 100000;
        });
        addFormatToken(0, ['SSSSSSSSS', 9], 0, function () {
            return this.millisecond() * 1000000;
        });


        // ALIASES

        addUnitAlias('millisecond', 'ms');

        // PRIORITY

        addUnitPriority('millisecond', 16);

        // PARSING

        addRegexToken('S',    match1to3, match1);
        addRegexToken('SS',   match1to3, match2);
        addRegexToken('SSS',  match1to3, match3);

        var token;
        for (token = 'SSSS'; token.length <= 9; token += 'S') {
            addRegexToken(token, matchUnsigned);
        }

        function parseMs(input, array) {
            array[MILLISECOND] = toInt(('0.' + input) * 1000);
        }

        for (token = 'S'; token.length <= 9; token += 'S') {
            addParseToken(token, parseMs);
        }
        // MOMENTS

        var getSetMillisecond = makeGetSet('Milliseconds', false);

        // FORMATTING

        addFormatToken('z',  0, 0, 'zoneAbbr');
        addFormatToken('zz', 0, 0, 'zoneName');

        // MOMENTS

        function getZoneAbbr () {
            return this._isUTC ? 'UTC' : '';
        }

        function getZoneName () {
            return this._isUTC ? 'Coordinated Universal Time' : '';
        }

        var proto = Moment.prototype;

        proto.add               = add;
        proto.calendar          = calendar$1;
        proto.clone             = clone;
        proto.diff              = diff;
        proto.endOf             = endOf;
        proto.format            = format;
        proto.from              = from;
        proto.fromNow           = fromNow;
        proto.to                = to;
        proto.toNow             = toNow;
        proto.get               = stringGet;
        proto.invalidAt         = invalidAt;
        proto.isAfter           = isAfter;
        proto.isBefore          = isBefore;
        proto.isBetween         = isBetween;
        proto.isSame            = isSame;
        proto.isSameOrAfter     = isSameOrAfter;
        proto.isSameOrBefore    = isSameOrBefore;
        proto.isValid           = isValid$2;
        proto.lang              = lang;
        proto.locale            = locale;
        proto.localeData        = localeData;
        proto.max               = prototypeMax;
        proto.min               = prototypeMin;
        proto.parsingFlags      = parsingFlags;
        proto.set               = stringSet;
        proto.startOf           = startOf;
        proto.subtract          = subtract;
        proto.toArray           = toArray;
        proto.toObject          = toObject;
        proto.toDate            = toDate;
        proto.toISOString       = toISOString;
        proto.inspect           = inspect;
        proto.toJSON            = toJSON;
        proto.toString          = toString;
        proto.unix              = unix;
        proto.valueOf           = valueOf;
        proto.creationData      = creationData;
        proto.year       = getSetYear;
        proto.isLeapYear = getIsLeapYear;
        proto.weekYear    = getSetWeekYear;
        proto.isoWeekYear = getSetISOWeekYear;
        proto.quarter = proto.quarters = getSetQuarter;
        proto.month       = getSetMonth;
        proto.daysInMonth = getDaysInMonth;
        proto.week           = proto.weeks        = getSetWeek;
        proto.isoWeek        = proto.isoWeeks     = getSetISOWeek;
        proto.weeksInYear    = getWeeksInYear;
        proto.isoWeeksInYear = getISOWeeksInYear;
        proto.date       = getSetDayOfMonth;
        proto.day        = proto.days             = getSetDayOfWeek;
        proto.weekday    = getSetLocaleDayOfWeek;
        proto.isoWeekday = getSetISODayOfWeek;
        proto.dayOfYear  = getSetDayOfYear;
        proto.hour = proto.hours = getSetHour;
        proto.minute = proto.minutes = getSetMinute;
        proto.second = proto.seconds = getSetSecond;
        proto.millisecond = proto.milliseconds = getSetMillisecond;
        proto.utcOffset            = getSetOffset;
        proto.utc                  = setOffsetToUTC;
        proto.local                = setOffsetToLocal;
        proto.parseZone            = setOffsetToParsedOffset;
        proto.hasAlignedHourOffset = hasAlignedHourOffset;
        proto.isDST                = isDaylightSavingTime;
        proto.isLocal              = isLocal;
        proto.isUtcOffset          = isUtcOffset;
        proto.isUtc                = isUtc;
        proto.isUTC                = isUtc;
        proto.zoneAbbr = getZoneAbbr;
        proto.zoneName = getZoneName;
        proto.dates  = deprecate('dates accessor is deprecated. Use date instead.', getSetDayOfMonth);
        proto.months = deprecate('months accessor is deprecated. Use month instead', getSetMonth);
        proto.years  = deprecate('years accessor is deprecated. Use year instead', getSetYear);
        proto.zone   = deprecate('moment().zone is deprecated, use moment().utcOffset instead. http://momentjs.com/guides/#/warnings/zone/', getSetZone);
        proto.isDSTShifted = deprecate('isDSTShifted is deprecated. See http://momentjs.com/guides/#/warnings/dst-shifted/ for more information', isDaylightSavingTimeShifted);

        function createUnix (input) {
            return createLocal(input * 1000);
        }

        function createInZone () {
            return createLocal.apply(null, arguments).parseZone();
        }

        function preParsePostFormat (string) {
            return string;
        }

        var proto$1 = Locale.prototype;

        proto$1.calendar        = calendar;
        proto$1.longDateFormat  = longDateFormat;
        proto$1.invalidDate     = invalidDate;
        proto$1.ordinal         = ordinal;
        proto$1.preparse        = preParsePostFormat;
        proto$1.postformat      = preParsePostFormat;
        proto$1.relativeTime    = relativeTime;
        proto$1.pastFuture      = pastFuture;
        proto$1.set             = set;

        proto$1.months            =        localeMonths;
        proto$1.monthsShort       =        localeMonthsShort;
        proto$1.monthsParse       =        localeMonthsParse;
        proto$1.monthsRegex       = monthsRegex;
        proto$1.monthsShortRegex  = monthsShortRegex;
        proto$1.week = localeWeek;
        proto$1.firstDayOfYear = localeFirstDayOfYear;
        proto$1.firstDayOfWeek = localeFirstDayOfWeek;

        proto$1.weekdays       =        localeWeekdays;
        proto$1.weekdaysMin    =        localeWeekdaysMin;
        proto$1.weekdaysShort  =        localeWeekdaysShort;
        proto$1.weekdaysParse  =        localeWeekdaysParse;

        proto$1.weekdaysRegex       =        weekdaysRegex;
        proto$1.weekdaysShortRegex  =        weekdaysShortRegex;
        proto$1.weekdaysMinRegex    =        weekdaysMinRegex;

        proto$1.isPM = localeIsPM;
        proto$1.meridiem = localeMeridiem;

        function get$1 (format, index, field, setter) {
            var locale = getLocale();
            var utc = createUTC().set(setter, index);
            return locale[field](utc, format);
        }

        function listMonthsImpl (format, index, field) {
            if (isNumber(format)) {
                index = format;
                format = undefined;
            }

            format = format || '';

            if (index != null) {
                return get$1(format, index, field, 'month');
            }

            var i;
            var out = [];
            for (i = 0; i < 12; i++) {
                out[i] = get$1(format, i, field, 'month');
            }
            return out;
        }

        // ()
        // (5)
        // (fmt, 5)
        // (fmt)
        // (true)
        // (true, 5)
        // (true, fmt, 5)
        // (true, fmt)
        function listWeekdaysImpl (localeSorted, format, index, field) {
            if (typeof localeSorted === 'boolean') {
                if (isNumber(format)) {
                    index = format;
                    format = undefined;
                }

                format = format || '';
            } else {
                format = localeSorted;
                index = format;
                localeSorted = false;

                if (isNumber(format)) {
                    index = format;
                    format = undefined;
                }

                format = format || '';
            }

            var locale = getLocale(),
                shift = localeSorted ? locale._week.dow : 0;

            if (index != null) {
                return get$1(format, (index + shift) % 7, field, 'day');
            }

            var i;
            var out = [];
            for (i = 0; i < 7; i++) {
                out[i] = get$1(format, (i + shift) % 7, field, 'day');
            }
            return out;
        }

        function listMonths (format, index) {
            return listMonthsImpl(format, index, 'months');
        }

        function listMonthsShort (format, index) {
            return listMonthsImpl(format, index, 'monthsShort');
        }

        function listWeekdays (localeSorted, format, index) {
            return listWeekdaysImpl(localeSorted, format, index, 'weekdays');
        }

        function listWeekdaysShort (localeSorted, format, index) {
            return listWeekdaysImpl(localeSorted, format, index, 'weekdaysShort');
        }

        function listWeekdaysMin (localeSorted, format, index) {
            return listWeekdaysImpl(localeSorted, format, index, 'weekdaysMin');
        }

        getSetGlobalLocale('en', {
            dayOfMonthOrdinalParse: /\d{1,2}(th|st|nd|rd)/,
            ordinal : function (number) {
                var b = number % 10,
                    output = (toInt(number % 100 / 10) === 1) ? 'th' :
                    (b === 1) ? 'st' :
                    (b === 2) ? 'nd' :
                    (b === 3) ? 'rd' : 'th';
                return number + output;
            }
        });

        // Side effect imports

        hooks.lang = deprecate('moment.lang is deprecated. Use moment.locale instead.', getSetGlobalLocale);
        hooks.langData = deprecate('moment.langData is deprecated. Use moment.localeData instead.', getLocale);

        var mathAbs = Math.abs;

        function abs () {
            var data           = this._data;

            this._milliseconds = mathAbs(this._milliseconds);
            this._days         = mathAbs(this._days);
            this._months       = mathAbs(this._months);

            data.milliseconds  = mathAbs(data.milliseconds);
            data.seconds       = mathAbs(data.seconds);
            data.minutes       = mathAbs(data.minutes);
            data.hours         = mathAbs(data.hours);
            data.months        = mathAbs(data.months);
            data.years         = mathAbs(data.years);

            return this;
        }

        function addSubtract$1 (duration, input, value, direction) {
            var other = createDuration(input, value);

            duration._milliseconds += direction * other._milliseconds;
            duration._days         += direction * other._days;
            duration._months       += direction * other._months;

            return duration._bubble();
        }

        // supports only 2.0-style add(1, 's') or add(duration)
        function add$1 (input, value) {
            return addSubtract$1(this, input, value, 1);
        }

        // supports only 2.0-style subtract(1, 's') or subtract(duration)
        function subtract$1 (input, value) {
            return addSubtract$1(this, input, value, -1);
        }

        function absCeil (number) {
            if (number < 0) {
                return Math.floor(number);
            } else {
                return Math.ceil(number);
            }
        }

        function bubble () {
            var milliseconds = this._milliseconds;
            var days         = this._days;
            var months       = this._months;
            var data         = this._data;
            var seconds, minutes, hours, years, monthsFromDays;

            // if we have a mix of positive and negative values, bubble down first
            // check: https://github.com/moment/moment/issues/2166
            if (!((milliseconds >= 0 && days >= 0 && months >= 0) ||
                    (milliseconds <= 0 && days <= 0 && months <= 0))) {
                milliseconds += absCeil(monthsToDays(months) + days) * 864e5;
                days = 0;
                months = 0;
            }

            // The following code bubbles up values, see the tests for
            // examples of what that means.
            data.milliseconds = milliseconds % 1000;

            seconds           = absFloor(milliseconds / 1000);
            data.seconds      = seconds % 60;

            minutes           = absFloor(seconds / 60);
            data.minutes      = minutes % 60;

            hours             = absFloor(minutes / 60);
            data.hours        = hours % 24;

            days += absFloor(hours / 24);

            // convert days to months
            monthsFromDays = absFloor(daysToMonths(days));
            months += monthsFromDays;
            days -= absCeil(monthsToDays(monthsFromDays));

            // 12 months -> 1 year
            years = absFloor(months / 12);
            months %= 12;

            data.days   = days;
            data.months = months;
            data.years  = years;

            return this;
        }

        function daysToMonths (days) {
            // 400 years have 146097 days (taking into account leap year rules)
            // 400 years have 12 months === 4800
            return days * 4800 / 146097;
        }

        function monthsToDays (months) {
            // the reverse of daysToMonths
            return months * 146097 / 4800;
        }

        function as (units) {
            if (!this.isValid()) {
                return NaN;
            }
            var days;
            var months;
            var milliseconds = this._milliseconds;

            units = normalizeUnits(units);

            if (units === 'month' || units === 'quarter' || units === 'year') {
                days = this._days + milliseconds / 864e5;
                months = this._months + daysToMonths(days);
                switch (units) {
                    case 'month':   return months;
                    case 'quarter': return months / 3;
                    case 'year':    return months / 12;
                }
            } else {
                // handle milliseconds separately because of floating point math errors (issue #1867)
                days = this._days + Math.round(monthsToDays(this._months));
                switch (units) {
                    case 'week'   : return days / 7     + milliseconds / 6048e5;
                    case 'day'    : return days         + milliseconds / 864e5;
                    case 'hour'   : return days * 24    + milliseconds / 36e5;
                    case 'minute' : return days * 1440  + milliseconds / 6e4;
                    case 'second' : return days * 86400 + milliseconds / 1000;
                    // Math.floor prevents floating point math errors here
                    case 'millisecond': return Math.floor(days * 864e5) + milliseconds;
                    default: throw new Error('Unknown unit ' + units);
                }
            }
        }

        // TODO: Use this.as('ms')?
        function valueOf$1 () {
            if (!this.isValid()) {
                return NaN;
            }
            return (
                this._milliseconds +
                this._days * 864e5 +
                (this._months % 12) * 2592e6 +
                toInt(this._months / 12) * 31536e6
            );
        }

        function makeAs (alias) {
            return function () {
                return this.as(alias);
            };
        }

        var asMilliseconds = makeAs('ms');
        var asSeconds      = makeAs('s');
        var asMinutes      = makeAs('m');
        var asHours        = makeAs('h');
        var asDays         = makeAs('d');
        var asWeeks        = makeAs('w');
        var asMonths       = makeAs('M');
        var asQuarters     = makeAs('Q');
        var asYears        = makeAs('y');

        function clone$1 () {
            return createDuration(this);
        }

        function get$2 (units) {
            units = normalizeUnits(units);
            return this.isValid() ? this[units + 's']() : NaN;
        }

        function makeGetter(name) {
            return function () {
                return this.isValid() ? this._data[name] : NaN;
            };
        }

        var milliseconds = makeGetter('milliseconds');
        var seconds      = makeGetter('seconds');
        var minutes      = makeGetter('minutes');
        var hours        = makeGetter('hours');
        var days         = makeGetter('days');
        var months       = makeGetter('months');
        var years        = makeGetter('years');

        function weeks () {
            return absFloor(this.days() / 7);
        }

        var round = Math.round;
        var thresholds = {
            ss: 44,         // a few seconds to seconds
            s : 45,         // seconds to minute
            m : 45,         // minutes to hour
            h : 22,         // hours to day
            d : 26,         // days to month
            M : 11          // months to year
        };

        // helper function for moment.fn.from, moment.fn.fromNow, and moment.duration.fn.humanize
        function substituteTimeAgo(string, number, withoutSuffix, isFuture, locale) {
            return locale.relativeTime(number || 1, !!withoutSuffix, string, isFuture);
        }

        function relativeTime$1 (posNegDuration, withoutSuffix, locale) {
            var duration = createDuration(posNegDuration).abs();
            var seconds  = round(duration.as('s'));
            var minutes  = round(duration.as('m'));
            var hours    = round(duration.as('h'));
            var days     = round(duration.as('d'));
            var months   = round(duration.as('M'));
            var years    = round(duration.as('y'));

            var a = seconds <= thresholds.ss && ['s', seconds]  ||
                    seconds < thresholds.s   && ['ss', seconds] ||
                    minutes <= 1             && ['m']           ||
                    minutes < thresholds.m   && ['mm', minutes] ||
                    hours   <= 1             && ['h']           ||
                    hours   < thresholds.h   && ['hh', hours]   ||
                    days    <= 1             && ['d']           ||
                    days    < thresholds.d   && ['dd', days]    ||
                    months  <= 1             && ['M']           ||
                    months  < thresholds.M   && ['MM', months]  ||
                    years   <= 1             && ['y']           || ['yy', years];

            a[2] = withoutSuffix;
            a[3] = +posNegDuration > 0;
            a[4] = locale;
            return substituteTimeAgo.apply(null, a);
        }

        // This function allows you to set the rounding function for relative time strings
        function getSetRelativeTimeRounding (roundingFunction) {
            if (roundingFunction === undefined) {
                return round;
            }
            if (typeof(roundingFunction) === 'function') {
                round = roundingFunction;
                return true;
            }
            return false;
        }

        // This function allows you to set a threshold for relative time strings
        function getSetRelativeTimeThreshold (threshold, limit) {
            if (thresholds[threshold] === undefined) {
                return false;
            }
            if (limit === undefined) {
                return thresholds[threshold];
            }
            thresholds[threshold] = limit;
            if (threshold === 's') {
                thresholds.ss = limit - 1;
            }
            return true;
        }

        function humanize (withSuffix) {
            if (!this.isValid()) {
                return this.localeData().invalidDate();
            }

            var locale = this.localeData();
            var output = relativeTime$1(this, !withSuffix, locale);

            if (withSuffix) {
                output = locale.pastFuture(+this, output);
            }

            return locale.postformat(output);
        }

        var abs$1 = Math.abs;

        function sign(x) {
            return ((x > 0) - (x < 0)) || +x;
        }

        function toISOString$1() {
            // for ISO strings we do not use the normal bubbling rules:
            //  * milliseconds bubble up until they become hours
            //  * days do not bubble at all
            //  * months bubble up until they become years
            // This is because there is no context-free conversion between hours and days
            // (think of clock changes)
            // and also not between days and months (28-31 days per month)
            if (!this.isValid()) {
                return this.localeData().invalidDate();
            }

            var seconds = abs$1(this._milliseconds) / 1000;
            var days         = abs$1(this._days);
            var months       = abs$1(this._months);
            var minutes, hours, years;

            // 3600 seconds -> 60 minutes -> 1 hour
            minutes           = absFloor(seconds / 60);
            hours             = absFloor(minutes / 60);
            seconds %= 60;
            minutes %= 60;

            // 12 months -> 1 year
            years  = absFloor(months / 12);
            months %= 12;


            // inspired by https://github.com/dordille/moment-isoduration/blob/master/moment.isoduration.js
            var Y = years;
            var M = months;
            var D = days;
            var h = hours;
            var m = minutes;
            var s = seconds ? seconds.toFixed(3).replace(/\.?0+$/, '') : '';
            var total = this.asSeconds();

            if (!total) {
                // this is the same as C#'s (Noda) and python (isodate)...
                // but not other JS (goog.date)
                return 'P0D';
            }

            var totalSign = total < 0 ? '-' : '';
            var ymSign = sign(this._months) !== sign(total) ? '-' : '';
            var daysSign = sign(this._days) !== sign(total) ? '-' : '';
            var hmsSign = sign(this._milliseconds) !== sign(total) ? '-' : '';

            return totalSign + 'P' +
                (Y ? ymSign + Y + 'Y' : '') +
                (M ? ymSign + M + 'M' : '') +
                (D ? daysSign + D + 'D' : '') +
                ((h || m || s) ? 'T' : '') +
                (h ? hmsSign + h + 'H' : '') +
                (m ? hmsSign + m + 'M' : '') +
                (s ? hmsSign + s + 'S' : '');
        }

        var proto$2 = Duration.prototype;

        proto$2.isValid        = isValid$1;
        proto$2.abs            = abs;
        proto$2.add            = add$1;
        proto$2.subtract       = subtract$1;
        proto$2.as             = as;
        proto$2.asMilliseconds = asMilliseconds;
        proto$2.asSeconds      = asSeconds;
        proto$2.asMinutes      = asMinutes;
        proto$2.asHours        = asHours;
        proto$2.asDays         = asDays;
        proto$2.asWeeks        = asWeeks;
        proto$2.asMonths       = asMonths;
        proto$2.asQuarters     = asQuarters;
        proto$2.asYears        = asYears;
        proto$2.valueOf        = valueOf$1;
        proto$2._bubble        = bubble;
        proto$2.clone          = clone$1;
        proto$2.get            = get$2;
        proto$2.milliseconds   = milliseconds;
        proto$2.seconds        = seconds;
        proto$2.minutes        = minutes;
        proto$2.hours          = hours;
        proto$2.days           = days;
        proto$2.weeks          = weeks;
        proto$2.months         = months;
        proto$2.years          = years;
        proto$2.humanize       = humanize;
        proto$2.toISOString    = toISOString$1;
        proto$2.toString       = toISOString$1;
        proto$2.toJSON         = toISOString$1;
        proto$2.locale         = locale;
        proto$2.localeData     = localeData;

        proto$2.toIsoString = deprecate('toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)', toISOString$1);
        proto$2.lang = lang;

        // Side effect imports

        // FORMATTING

        addFormatToken('X', 0, 0, 'unix');
        addFormatToken('x', 0, 0, 'valueOf');

        // PARSING

        addRegexToken('x', matchSigned);
        addRegexToken('X', matchTimestamp);
        addParseToken('X', function (input, array, config) {
            config._d = new Date(parseFloat(input, 10) * 1000);
        });
        addParseToken('x', function (input, array, config) {
            config._d = new Date(toInt(input));
        });

        // Side effect imports


        hooks.version = '2.24.0';

        setHookCallback(createLocal);

        hooks.fn                    = proto;
        hooks.min                   = min;
        hooks.max                   = max;
        hooks.now                   = now;
        hooks.utc                   = createUTC;
        hooks.unix                  = createUnix;
        hooks.months                = listMonths;
        hooks.isDate                = isDate;
        hooks.locale                = getSetGlobalLocale;
        hooks.invalid               = createInvalid;
        hooks.duration              = createDuration;
        hooks.isMoment              = isMoment;
        hooks.weekdays              = listWeekdays;
        hooks.parseZone             = createInZone;
        hooks.localeData            = getLocale;
        hooks.isDuration            = isDuration;
        hooks.monthsShort           = listMonthsShort;
        hooks.weekdaysMin           = listWeekdaysMin;
        hooks.defineLocale          = defineLocale;
        hooks.updateLocale          = updateLocale;
        hooks.locales               = listLocales;
        hooks.weekdaysShort         = listWeekdaysShort;
        hooks.normalizeUnits        = normalizeUnits;
        hooks.relativeTimeRounding  = getSetRelativeTimeRounding;
        hooks.relativeTimeThreshold = getSetRelativeTimeThreshold;
        hooks.calendarFormat        = getCalendarFormat;
        hooks.prototype             = proto;

        // currently HTML5 input type only supports 24-hour formats
        hooks.HTML5_FMT = {
            DATETIME_LOCAL: 'YYYY-MM-DDTHH:mm',             // <input type="datetime-local" />
            DATETIME_LOCAL_SECONDS: 'YYYY-MM-DDTHH:mm:ss',  // <input type="datetime-local" step="1" />
            DATETIME_LOCAL_MS: 'YYYY-MM-DDTHH:mm:ss.SSS',   // <input type="datetime-local" step="0.001" />
            DATE: 'YYYY-MM-DD',                             // <input type="date" />
            TIME: 'HH:mm',                                  // <input type="time" />
            TIME_SECONDS: 'HH:mm:ss',                       // <input type="time" step="1" />
            TIME_MS: 'HH:mm:ss.SSS',                        // <input type="time" step="0.001" />
            WEEK: 'GGGG-[W]WW',                             // <input type="week" />
            MONTH: 'YYYY-MM'                                // <input type="month" />
        };

        return hooks;

    })));
    });

    //
    var script$c = {
      name: "DateRangeInput",
      components: {
        Command: Command,
        Icon: Icon
      },
      mixins: [clickoutMixin, dropdownMixin],
      props: {
        value: {
          type: Object,
          default: function _default() {
            return {
              start: '',
              end: ''
            };
          }
        },
        dateFormat: {
          type: String,
          default: 'YYYY-MM-DD'
        },

        /**
         * Whether to show dates via moment.js or as raw strings
         * @prop {Boolean} true use moment.js to humanize dates
         */
        humanize: {
          type: Boolean,
          default: true
        },
        // Input-related properties
        placeholder: {
          type: String,
          default: ''
        },
        active: {
          type: Boolean,
          default: false
        },
        disabled: {
          type: Boolean,
          default: false
        },
        size: {
          type: String,
          default: 'normal',
          validator: function validator(value) {
            return ['large', 'normal', 'small'].indexOf(value) > -1;
          }
        }
      },
      data: function data() {
        return {
          startDate: this.value.start,
          endDate: this.value.end,
          highlightedRange: {}
        };
      },
      computed: {
        id: function id() {
          return "daterange-".concat(v4_1());
        },
        btnSize: function btnSize() {
          if (this.size === 'large') {
            return 'btn-lg';
          } else if (this.size === 'small') {
            return 'btn-sm';
          } else {
            return '';
          }
        },

        /**
         * If both start and end are `''`, return the `placeholder`, otherwise,
         * return the humanized or straight value, depending on the `humanize`
         * property. This also ensures moment isn't run if the value is `''`
         */
        label: function label() {
          var start = this.humanize && this.startDate !== undefined ? moment(this.startDate, 'YYYY-MM-DD').format('MMM Do, YYYY') : this.startDate;
          var end = this.humanize && this.endDate !== undefined ? moment(this.endDate, 'YYYY-MM-DD').format('MMM Do, YYYY') : this.endDate;
          return start !== undefined && end !== undefined ? "".concat(start, " - ").concat(end) : this.placeholder;
        },

        /**
         * Return the right caret for the dropdown when, depending on the `dropup` property
         */
        dropClosedIcon: function dropClosedIcon() {
          return this.dropup ? 'caret-up' : 'caret-down';
        },

        /**
         * Return the right caret for the dropdown when open, depending on the `dropup` property
         */
        dropOpenIcon: function dropOpenIcon() {
          return this.dropup ? 'caret-down' : 'caret-up';
        },

        /**
         * Whether both a start and end value have been selected or not
         */
        bothValuesSelected: function bothValuesSelected() {
          return this.startDate && this.endDate;
        }
      },
      watch: {
        startDate: 'highlight',
        endDate: 'highlight'
      },
      methods: {
        /**
         * Highlight the dates between the start and end in the calendar 
         * when a range is selected.
         */
        highlight: function highlight() {
          if (!this.bothValuesSelected) return;
          this.highlightedRange = {
            start: this.startDate,
            end: this.endDate
          };
        },
        // Event Handlers

        /**
         * Return the selected date range
         */
        emitResult: function emitResult() {
          this.$emit('input', {
            start: this.startDate,
            end: this.endDate
          });
        },

        /**
         * Clear the calendars and reset back to nothing
         */
        clear: function clear() {
          this.startDate = undefined;
          this.endDate = undefined;
          this.highlightedRange = {};
        },

        /**
         * close the dropdown on clicking `cancel`
         */
        close: function close() {
          this.show = false;
        },

        /**
         * Respond to clicking outside of the component as per the clickout mixin
         */
        onClickOut: function onClickOut() {
          this.show = false;
        }
      }
    };

    var css$5 = ".btn-daterange[data-v-60e2682f]{display:inline-flex;width:100%;flex:1 1 auto!important;align-items:center;color:#464a4c;background-color:#fff;border-color:rgba(0,0,0,.15)}.btn-daterange[data-v-60e2682f]:hover{color:#464a4c;background-color:#e6e6e6;border-color:rgba(0,0,0,.15)}.btn-daterange.focus[data-v-60e2682f],.btn-daterange[data-v-60e2682f]:focus{box-shadow:0 0 0 2px rgba(0,0,0,.5)}.btn-daterange.disabled[data-v-60e2682f],.btn-daterange[data-v-60e2682f]:disabled{background-color:#fff;border-color:rgba(0,0,0,.15)}.btn-daterange.active[data-v-60e2682f],.btn-daterange[data-v-60e2682f]:active,.show>.btn-daterange.dropdown-toggle[data-v-60e2682f]{color:#464a4c;background-color:#e6e6e6;background-image:none;border-color:rgba(0,0,0,.15)}.daterange-wrapper[data-v-60e2682f]{flex:1 1 auto;width:100%}.daterange-menu[data-v-60e2682f]{z-index:1000;min-width:10rem;padding:.5rem 0;margin:.125rem 0;color:#292b2c;background-color:#fff;background-clip:padding-box;border:1px solid rgba(0,0,0,.15);border-radius:.25rem}.daterange-menu .daterange-menu__header[data-v-60e2682f]{display:flex;flex-flow:row;justify-content:space-around;align-items:center}.daterange-menu .daterange-menu__content[data-v-60e2682f]{display:flex;flex-flow:row;justify-content:space-between;align-items:flex-start}.daterange-menu .daterange-menu__content .calendar[data-v-60e2682f]{width:320px}.daterange-menu .daterange-menu__footer[data-v-60e2682f]{display:flex;flex-flow:row;justify-content:flex-end;align-items:center;margin:0 .5em}";
    styleInject(css$5);

    /* script */
    const __vue_script__$c = script$c;
    /* template */
    var __vue_render__$9 = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"daterange-wrapper"},[_c('button',{staticClass:"btn btn-daterange",class:[_vm.btnSize, { 'active': _vm.active }],attrs:{"type":"button","aria-pressed":_vm.active,"aria-haspopup":"true","disabled":_vm.disabled},on:{"click":_vm.toggle}},[_c('icon',{staticClass:"mr-2",attrs:{"icon":"calendar"}}),_vm._v(" "),_c('div',{staticClass:"btn-label"},[_vm._v("\n            "+_vm._s(_vm.label)+"\n        ")]),_vm._v(" "),(!_vm.show)?_c('icon',{staticClass:"ml-auto",attrs:{"icon":_vm.dropClosedIcon}}):_c('icon',{staticClass:"ml-auto",attrs:{"icon":_vm.dropOpenIcon}})],1),_vm._v(" "),_c('div',{directives:[{name:"show",rawName:"v-show",value:(!_vm.disabled && _vm.show),expression:"!disabled && show"}],ref:"dropdown",staticClass:"daterange-menu"},[_c('div',{staticClass:"daterange-menu__header"},[_c('div',[_c('b',[_vm._v("Start:")]),_vm._v(" "+_vm._s(_vm.startDate))]),_vm._v(" "),_c('div',[_c('b',[_vm._v("End:")]),_vm._v(" "+_vm._s(_vm.endDate))])]),_vm._v(" "),_c('div',{staticClass:"daterange-menu__content"},[_c('calendar',{attrs:{"highlight":_vm.highlightedRange},on:{"input":_vm.emitResult},model:{value:(_vm.startDate),callback:function ($$v) {_vm.startDate=$$v;},expression:"startDate"}}),_vm._v(" "),_c('calendar',{attrs:{"highlight":_vm.highlightedRange},on:{"input":_vm.emitResult},model:{value:(_vm.endDate),callback:function ($$v) {_vm.endDate=$$v;},expression:"endDate"}})],1),_vm._v(" "),_c('div',{staticClass:"daterange-menu__footer"},[_c('div',{staticClass:"ml-1 mr-auto font-italic"},[_vm._v("\n                Select a start date on the left, then an end date on the right.\n            ")]),_vm._v(" "),_c('command',{attrs:{"type":"warning","size":"small","outline":"","label":"Clear"},on:{"action":_vm.clear}}),_vm._v(" "),_c('command',{staticClass:"ml-1",attrs:{"type":"secondary","size":"small","label":"Close"},on:{"action":_vm.close}})],1)])])};
    var __vue_staticRenderFns__$9 = [];

      /* style */
      const __vue_inject_styles__$c = undefined;
      /* scoped */
      const __vue_scope_id__$c = "data-v-60e2682f";
      /* module identifier */
      const __vue_module_identifier__$c = undefined;
      /* functional template */
      const __vue_is_functional_template__$c = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var DateRangeInput = normalizeComponent_1(
        { render: __vue_render__$9, staticRenderFns: __vue_staticRenderFns__$9 },
        __vue_inject_styles__$c,
        __vue_script__$c,
        __vue_scope_id__$c,
        __vue_is_functional_template__$c,
        __vue_module_identifier__$c,
        undefined,
        undefined
      );

    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    var script$d = {
      name: "FsoFooter",
      props: {
        /**
         * Set the application name
         * @prop {String} appName
         */
        appName: {
          type: String,
          required: true
        },

        /**
         * Set the background css class
         * @prop {String} background
         */
        background: {
          type: String,
          default: 'bg-cool-gray'
        }
      }
    };

    var css$6 = ".bg-cool-gray[data-v-759cd8ad]{background-color:#e2e9eb}.footer[data-v-759cd8ad]{flex:none;display:block;font-size:85%;line-height:85%;width:100%;padding:0 5%}.footer a[data-v-759cd8ad]{font-weight:500;color:#464a4c;text-align:center}.footer a[data-v-759cd8ad]:focus,.footer a[data-v-759cd8ad]:hover{color:#ab0520}.footer p[data-v-759cd8ad]{margin-bottom:0;text-align:center}.footer .hr-dark[data-v-759cd8ad]{margin:4.5px auto}.footer-section[data-v-759cd8ad]{display:flex;flex:auto;flex-direction:row;margin:1rem 0}@media (max-width:840px){.footer-section[data-v-759cd8ad]{flex-direction:column}}.footer-section.footer-section__secondary[data-v-759cd8ad]{align-items:center;justify-content:center}@media (max-width:840px){.footer-section.footer-section__secondary>p[data-v-759cd8ad]{padding:.25rem 0}}.footer-section.footer-section__secondary>.divider[data-v-759cd8ad]{margin:0 1rem}@media (max-width:840px){.footer-section.footer-section__secondary>.divider[data-v-759cd8ad]{display:none}}.footer-section.footer-section__secondary p a[data-v-759cd8ad]{margin:0}.footer-logo[data-v-759cd8ad]{width:300px;margin-right:.5rem;margin-top:auto;margin-bottom:auto;align-self:flex-start;max-height:45px}@media (max-width:840px){.footer-logo[data-v-759cd8ad]{align-self:center;margin-bottom:1rem}}@media (max-width:520px){.footer-logo[data-v-759cd8ad]{width:230px;max-height:35px}}.footer-links[data-v-759cd8ad]{display:flex;flex:auto;align-items:center;justify-content:flex-end}@media (max-width:840px){.footer-links[data-v-759cd8ad]{justify-content:space-around;flex-wrap:wrap}}@media (max-width:520px){.footer-links[data-v-759cd8ad]{flex-direction:column}}.footer-links>a[data-v-759cd8ad]{margin:0 1rem}@media (max-width:840px){.footer-links>a[data-v-759cd8ad]{margin:.25rem 1.25rem}}@media (max-width:520px){.footer-links>a[data-v-759cd8ad]{margin:.25rem 0!important}}.footer-links>a[data-v-759cd8ad]:first-child{margin-left:0}.footer-links>a[data-v-759cd8ad]:last-child{margin-right:0}";
    styleInject(css$6);

    /* script */
    const __vue_script__$d = script$d;
    /* template */
    var __vue_render__$a = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('footer',{staticClass:"footer",class:_vm.background},[_vm._m(0),_vm._v(" "),_c('hr',{staticClass:"hr-dark"}),_vm._v(" "),_c('div',{staticClass:"footer-section footer-section__secondary"},[_c('p',[_vm._v("Copyright 2018 © Arizona Board of Regents")]),_vm._v(" "),_c('p',{staticClass:"divider"},[_vm._v("\n            |\n        ")]),_vm._v(" "),_c('p',[_vm._v("FSO Information Technology")]),_vm._v(" "),_c('p',{staticClass:"divider"},[_vm._v("\n            |\n        ")]),_vm._v(" "),_c('p',[_vm._v("\n            Need help? \n            "),_c('a',{attrs:{"href":'mailto:support@fso.arizona.edu?subject='+ _vm.appName + ' support request'}},[_vm._v("\n                Contact us!\n            ")])])])])};
    var __vue_staticRenderFns__$a = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"footer-section"},[_c('div',{staticClass:"footer-logo"},[_c('a',{attrs:{"href":"http://www.fso.arizona.edu"}},[_c('img',{attrs:{"alt":"Financial Services Office","src":"https://assets.cdn.fso.arizona.edu/UA_FSO_Alternate.svg"}})])]),_vm._v(" "),_c('div',{staticClass:"footer-links"},[_c('a',{attrs:{"href":"http://portal.fso.arizona.edu/about"}},[_vm._v("\n                About\n            ")]),_vm._v(" "),_c('a',{attrs:{"href":"http://portal.fso.arizona.edu"}},[_vm._v("\n                Portal\n            ")]),_vm._v(" "),_c('a',{attrs:{"href":"http://ubet.arizona.edu/business-calendar?tid[0]=25"}},[_vm._v("\n                FSO Calendar\n            ")]),_vm._v(" "),_c('a',{attrs:{"href":"http://www.arizona.edu/information-security-privacy"}},[_vm._v("\n                Security & Privacy\n            ")]),_vm._v(" "),_c('a',{attrs:{"href":"http://www.arizona.edu/copyright"}},[_vm._v("\n                Copyright\n            ")])])])}];

      /* style */
      const __vue_inject_styles__$d = undefined;
      /* scoped */
      const __vue_scope_id__$d = "data-v-759cd8ad";
      /* module identifier */
      const __vue_module_identifier__$d = undefined;
      /* functional template */
      const __vue_is_functional_template__$d = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var FsoFooter = normalizeComponent_1(
        { render: __vue_render__$a, staticRenderFns: __vue_staticRenderFns__$a },
        __vue_inject_styles__$d,
        __vue_script__$d,
        __vue_scope_id__$d,
        __vue_is_functional_template__$d,
        __vue_module_identifier__$d,
        undefined,
        undefined
      );

    var script$e = {
      name: 'FormGroup',
      props: {
        label: {
          type: String,
          default: ''
        },
        labelFor: {
          type: String,
          default: undefined
        },
        labelSrOnly: {
          type: Boolean,
          default: false
        },
        labelSize: {
          type: String,
          default: 'normal',
          validator: function validator(value) {
            return ['small', 'normal', 'large'].indexOf(value) > -1;
          }
        },
        description: {
          type: String,
          default: ''
        },
        inline: {
          type: Boolean,
          default: false
        }
      },
      computed: {
        isLabel: function isLabel() {
          return this.label && this.label !== '' ? true : false;
        },
        isDescription: function isDescription() {
          return this.description && this.description !== '' ? true : false;
        },
        isInline: function isInline() {
          return this.inline;
        },
        isSrOnly: function isSrOnly() {
          return this.labelSrOnly;
        },
        labelClass: function labelClass() {
          var result = 'col-form-label';

          if (this.labelSize === 'small') {
            return "".concat(result, "-sm");
          } else if (this.labelSize === 'large') {
            return "".concat(result, "-lg");
          }

          return result;
        }
      },
      render: function render(h) {
        var label = this.isLabel ? h('label', {
          class: [this.labelClass, {
            'col-sm-2': this.isInline
          }, {
            'sr-only': this.isSrOnly
          }],
          attrs: {
            for: this.labelFor || null,
            'aria-label': this.labelFor || null
          }
        }, this.label) : undefined;
        var description = this.isDescription ? h('small', {
          class: [{
            'form-text': !this.isInline
          }, {
            'ml-2': this.isInline
          }, 'text-muted']
        }, this.description) : undefined;
        return h('div', {
          class: ['form-group', {
            'row': this.isInline
          }]
        }, [label, h('div', {
          class: [{
            'col-sm-10': this.isInline
          }]
        }, [this.$slots.default, description])]);
      }
    };

    /* script */
    const __vue_script__$e = script$e;

    /* template */

      /* style */
      const __vue_inject_styles__$e = undefined;
      /* scoped */
      const __vue_scope_id__$e = undefined;
      /* module identifier */
      const __vue_module_identifier__$e = undefined;
      /* functional template */
      const __vue_is_functional_template__$e = undefined;
      /* style inject */
      
      /* style inject SSR */
      

      
      var FormGroup = normalizeComponent_1(
        {},
        __vue_inject_styles__$e,
        __vue_script__$e,
        __vue_scope_id__$e,
        __vue_is_functional_template__$e,
        __vue_module_identifier__$e,
        undefined,
        undefined
      );

    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    var script$f = {
      name: "Modal",
      inheritAttrs: false,
      model: {
        prop: 'visible',
        event: 'change'
      },
      props: {
        title: {
          type: String,
          default: ''
        },
        size: {
          type: String,
          validator: function validator(value) {
            return ['custom', 'large', 'small', 'normal'].indexOf(value) > -1;
          },
          default: 'normal'
        },
        visible: {
          type: Boolean,
          default: false
        },
        dark: {
          type: Boolean,
          default: false
        },
        noFade: {
          type: Boolean,
          default: false
        },
        disableBackdropClose: {
          type: Boolean,
          default: false
        },
        disableEscClose: {
          type: Boolean,
          default: false
        },
        hideHeader: {
          type: Boolean,
          default: false
        },
        hideFooter: {
          type: Boolean,
          default: false
        },
        hideBackdrop: {
          type: Boolean,
          default: false
        },
        centered: {
          type: Boolean,
          default: false
        },
        maxWidth: {
          type: Number,
          default: 800
        },
        minWidth: {
          type: Number,
          default: 300
        }
      },
      data: function data() {
        return {
          // State of property visibility
          isVisible: this.visible,
          // Indicate whether in a transition
          isTransitioning: false,
          // Manage `show` class
          isShow: false,
          // Manage `modal-visible` class
          isModalVisible: false
        };
      },
      computed: {
        sizeClass: function sizeClass() {
          if (this.size === 'small') {
            return 'modal-sm';
          } else if (this.size === 'large') {
            return 'modal-lg';
          } else if (this.size === 'custom') {
            return 'modal-custom';
          }

          return undefined;
        },
        widthStyles: function widthStyles() {
          if (this.size === 'custom') {
            return "min-width: ".concat(this.minWidth, "px; max-width: ").concat(this.maxWidth, "px");
          } else {
            return "";
          }
        }
      },
      watch: {
        visible: function visible(val) {
          // Trigger the proper event
          val ? this.show() : this.hide();
        }
      },
      mounted: function mounted() {
        if (this.isVisible) {
          this.show();
        }
      },
      beforeDestroy: function beforeDestroy() {
        document.body.classList.remove('modal-open');
      },
      methods: {
        show: function show() {
          var _this = this;

          if (this.isVisible) {
            return;
          }

          this.$nextTick(function () {
            _this.isVisible = true;

            _this.$emit('change', true);
          });
        },
        hide: function hide() {
          if (!this.isVisible) {
            return;
          }

          this.isVisible = false;
          this.$emit('change', false);
        },
        // Transition Handling
        onBeforeEnter: function onBeforeEnter() {
          this.isTransitioning = true;
          document.body.classList.add('modal-open');
        },
        onEnter: function onEnter() {
          this.isModalVisible = true;
          this.$refs.modal.scrollTop = 0;
        },
        onAfterEnter: function onAfterEnter() {
          var _this2 = this;

          this.isShow = true;
          this.isTransitioning = false;
          this.$nextTick(function () {
            _this2.$emit('shown');
          });
        },
        onBeforeLeave: function onBeforeLeave() {
          this.isTransitioning = true;
        },
        onLeave: function onLeave() {
          this.isShow = false;
        },
        onAfterLeave: function onAfterLeave() {
          var _this3 = this;

          this.isModalVisible = false;
          this.isTransitioning = false;
          document.body.classList.remove('modal-open');
          this.$nextTick(function () {
            _this3.$emit('hidden');
          });
        },
        // Events
        // Hide modal when the backdrop is clicked
        onClickOut: function onClickOut() {
          if (this.isVisible && !this.disableBackdropClose) {
            this.hide();
          }
        },
        // Hide modal when Esc is pressed
        onEsc: function onEsc() {
          console.log('hey');

          if (this.isVisible && !this.disableEscClose) {
            this.hide();
          }
        },
        onFocusOut: function onFocusOut(evt) {
          // If focus leaves modal content, bring it back
          var content = this.$refs.content;

          if (this.isVisible && content && !content.contains(evt.relatedTarget)) {
            content.focus();
          }
        }
      }
    };

    var css$7 = ".modal-visible{display:block!important}.modal-title{margin-bottom:0!important;line-height:1.5!important}.modal-body{margin:0}.modal-custom{padding:0 15px;margin:30px auto!important}.modal-dark .modal-content{background-color:#333;color:#fff}.modal-dark .modal-content .modal-header{border-bottom:1px solid #444}.modal-dark .modal-content .modal-header .close{color:#fff;text-shadow:none}.modal-dark .modal-content .modal-footer{border-top:1px solid #444}";
    styleInject(css$7);

    /* script */
    const __vue_script__$f = script$f;
    /* template */
    var __vue_render__$b = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"modal-wrapper"},[_c('transition',{attrs:{"name":"modal"},on:{"before-enter":_vm.onBeforeEnter,"enter":_vm.onEnter,"after-enter":_vm.onAfterEnter,"before-leave":_vm.onBeforeLeave,"leave":_vm.onLeave,"after-leave":_vm.onAfterLeave}},[_c('div',_vm._b({directives:[{name:"show",rawName:"v-show",value:(_vm.isVisible),expression:"isVisible"}],ref:"modal",staticClass:"modal",class:[{'fade': !_vm.noFade},
                          {'show': _vm.isShow},
                          {'modal-visible': _vm.isModalVisible},
                          {'modal-dark': _vm.dark}],attrs:{"role":"dialog","aria-hidden":_vm.visible ? null : 'true',"aria-labelledby":"modal_title","aria-describedby":"modal_content"},on:{"click":_vm.onClickOut,"keyup":function($event){if(!$event.type.indexOf('key')&&_vm._k($event.keyCode,"esc",27,$event.key,["Esc","Escape"])){ return null; }return _vm.onEsc($event)}}},'div',_vm.$attrs,false),[_c('div',{staticClass:"modal-dialog",class:[_vm.sizeClass, {'modal-dialog-centered': _vm.centered}],style:(_vm.widthStyles),attrs:{"role":"document"}},[_c('div',{staticClass:"modal-content",on:{"click":function($event){$event.stopPropagation();},"focusout":_vm.onFocusOut}},[(!_vm.hideHeader)?_c('div',{staticClass:"modal-header"},[_vm._t("header",[_c('h5',{staticClass:"modal-title",attrs:{"id":"modal_title"}},[_vm._v(_vm._s(_vm.title))]),_vm._v(" "),_c('button',{staticClass:"close",attrs:{"type":"button","aria-label":"Close"},on:{"click":_vm.hide}},[_c('span',{attrs:{"aria-hidden":"true"}},[_vm._v("×")])])])],2):_vm._e(),_vm._v(" "),_c('div',{staticClass:"modal-body",attrs:{"id":"modal_content"}},[_vm._t("default")],2),_vm._v(" "),(!_vm.hideFooter)?_c('div',{staticClass:"modal-footer"},[_vm._t("footer",[_c('button',{staticClass:"btn btn-primary",attrs:{"type":"button"},on:{"click":_vm.hide}},[_vm._v("Close")])])],2):_vm._e()])])])]),_vm._v(" "),(!_vm.hideBackdrop)?_c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.isVisible),expression:"isVisible"}],staticClass:"modal-backdrop",class:[{'fade': !_vm.noFade}, {'show': _vm.isShow}]}):_vm._e()],1)};
    var __vue_staticRenderFns__$b = [];

      /* style */
      const __vue_inject_styles__$f = undefined;
      /* scoped */
      const __vue_scope_id__$f = undefined;
      /* module identifier */
      const __vue_module_identifier__$f = undefined;
      /* functional template */
      const __vue_is_functional_template__$f = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var Modal = normalizeComponent_1(
        { render: __vue_render__$b, staticRenderFns: __vue_staticRenderFns__$b },
        __vue_inject_styles__$f,
        __vue_script__$f,
        __vue_scope_id__$f,
        __vue_is_functional_template__$f,
        __vue_module_identifier__$f,
        undefined,
        undefined
      );

    //
    var script$g = {
      name: 'Navbar',
      mixins: [clickoutMixin],
      props: {
        /**
         * Define the theme color
         * @prop {String} theme
         */
        theme: {
          type: String,
          default: 'light',
          validator: function validator(value) {
            return ['light', 'dark', 'custom'].indexOf(value) > -1;
          }
        },

        /**
         * Define the background color
         * @prop {String} background 
         */
        background: {
          type: String,
          default: 'light',
          validator: function validator(value) {
            return ['light', 'dark', 'custom'].indexOf(value) > -1;
          }
        },

        /**
         * Set the breakpoint for collapsing content
         * @prop {String} breakpoint
         */
        breakpoint: {
          type: String,
          default: 'none',
          validator: function validator(value) {
            return ['sm', 'md', 'lg', 'xl', 'none'].indexOf(value) > -1;
          }
        },

        /**
         * Define a custom theme class selector for the component to use
         * @prop {String} customThemeClass
         */
        customThemeClass: {
          type: String,
          default: ''
        },

        /**
         * Define a custom background class selector for the component to use
         * @prop {String} customBackgroundClass
         */
        customBackgroundClass: {
          type: String,
          default: ''
        },

        /**
         * Set the placement of the navbar
         * @prop {String} placement
         */
        placement: {
          type: String,
          default: 'default',
          validator: function validator(value) {
            return ['fixed-top', 'fixed-bottom', 'sticky-top', 'default'].indexOf(value) > -1;
          }
        }
      },
      computed: {
        themeColor: function themeColor() {
          if (this.theme === 'light') {
            return 'navbar-light';
          } else if (this.theme === 'dark') {
            return 'navbar-dark';
          } else if (this.theme === 'custom') {
            return "".concat(this.customThemeClass);
          } else {
            return '';
          }
        },
        bgColor: function bgColor() {
          if (this.background === 'light') {
            return 'bg-light';
          } else if (this.background === 'dark') {
            return 'bg-dark';
          } else if (this.background === 'custom') {
            return "".concat(this.customBackgroundClass);
          } else {
            return '';
          }
        },
        expand: function expand() {
          return this.breakpoint != 'none' ? "navbar-expand-".concat(this.breakpoint) : undefined;
        },
        cssPlacement: function cssPlacement() {
          return this.placement != 'default' ? this.placement : undefined;
        }
      },
      methods: {
        /**
         * Respond to clicking outside of the component as per the clickout mixin
         */
        onClickOut: function onClickOut() {
          if (this.collapsed != false) {
            this.collapsed = false;
          }
        }
      }
    };

    var css$8 = ".bg-light{background-color:#f8f9fa!important}a.bg-light:focus,a.bg-light:hover,button.bg-light:focus,button.bg-light:hover{background-color:#dae0e5!important}.bg-dark{background-color:#343a40!important}a.bg-dark:focus,a.bg-dark:hover,button.bg-dark:focus,button.bg-dark:hover{background-color:#1d2124!important}.navbar-light .navbar-brand{color:rgba(0,0,0,.9)}.navbar-light .navbar-brand:focus,.navbar-light .navbar-brand:hover{color:rgba(0,0,0,.9)}.navbar-light .navbar-nav .nav-link{color:rgba(0,0,0,.5)}.navbar-light .navbar-nav .nav-link:focus,.navbar-light .navbar-nav .nav-link:hover{color:rgba(0,0,0,.7)}.navbar-light .navbar-nav .nav-link.disabled{color:rgba(0,0,0,.3)}.navbar-light .navbar-nav .active>.nav-link,.navbar-light .navbar-nav .nav-link.active,.navbar-light .navbar-nav .nav-link.show,.navbar-light .navbar-nav .show>.nav-link{color:rgba(0,0,0,.9)}.navbar-light .navbar-toggler{color:rgba(0,0,0,.5);border-color:rgba(0,0,0,.1)}.navbar-light .navbar-text{color:rgba(0,0,0,.5)}.navbar-light .navbar-text a{color:rgba(0,0,0,.9)}.navbar-light .navbar-text a:focus,.navbar-light .navbar-text a:hover{color:rgba(0,0,0,.9)}.navbar-dark .navbar-brand{color:#fff}.navbar-dark .navbar-brand:focus,.navbar-dark .navbar-brand:hover{color:#fff}.navbar-dark .navbar-nav .nav-link{color:rgba(255,255,255,.5)}.navbar-dark .navbar-nav .nav-link:focus,.navbar-dark .navbar-nav .nav-link:hover{color:rgba(255,255,255,.75)}.navbar-dark .navbar-nav .nav-link.disabled{color:rgba(255,255,255,.25)}.navbar-dark .navbar-nav .active>.nav-link,.navbar-dark .navbar-nav .nav-link.active,.navbar-dark .navbar-nav .nav-link.show,.navbar-dark .navbar-nav .show>.nav-link{color:#fff}.navbar-dark .navbar-toggler{color:rgba(255,255,255,.5);border-color:rgba(255,255,255,.1)}.navbar-dark .navbar-text{color:rgba(255,255,255,.5)}.navbar-dark .navbar-text a{color:#fff}.navbar-dark .navbar-text a:focus,.navbar-dark .navbar-text a:hover{color:#fff}.navbar{position:relative;display:flex;flex-wrap:wrap;align-items:center;justify-content:flex-start;padding:.5rem 1rem;flex-direction:row}.navbar>.container,.navbar>.container-fluid{display:flex;flex-wrap:wrap;align-items:center;justify-content:space-between}@media (max-width:575.98px){.navbar-expand-sm>.container,.navbar-expand-sm>.container-fluid{padding-right:0;padding-left:0}}@media (min-width:576px){.navbar-expand-sm{flex-flow:row nowrap;justify-content:flex-start}.navbar-expand-sm .navbar-nav{flex-direction:row}.navbar-expand-sm .navbar-nav .dropdown-menu{position:absolute}.navbar-expand-sm .navbar-nav .nav-link{padding-right:.5rem;padding-left:.5rem}.navbar-expand-sm>.container,.navbar-expand-sm>.container-fluid{flex-wrap:nowrap}.navbar-expand-sm .navbar-collapse{display:flex!important;flex-basis:auto}.navbar-expand-sm .navbar-toggler{display:none}}@media (max-width:767.98px){.navbar-expand-md>.container,.navbar-expand-md>.container-fluid{padding-right:0;padding-left:0}}@media (min-width:768px){.navbar-expand-md{flex-flow:row nowrap;justify-content:flex-start}.navbar-expand-md .navbar-nav{flex-direction:row}.navbar-expand-md .navbar-nav .dropdown-menu{position:absolute}.navbar-expand-md .navbar-nav .nav-link{padding-right:.5rem;padding-left:.5rem}.navbar-expand-md>.container,.navbar-expand-md>.container-fluid{flex-wrap:nowrap}.navbar-expand-md .navbar-collapse{display:flex!important;flex-basis:auto}.navbar-expand-md .navbar-toggler{display:none}}@media (max-width:991.98px){.navbar-expand-lg>.container,.navbar-expand-lg>.container-fluid{padding-right:0;padding-left:0}}@media (min-width:992px){.navbar-expand-lg{flex-flow:row nowrap;justify-content:flex-start}.navbar-expand-lg .navbar-nav{flex-direction:row}.navbar-expand-lg .navbar-nav .dropdown-menu{position:absolute}.navbar-expand-lg .navbar-nav .nav-link{padding-right:.5rem;padding-left:.5rem}.navbar-expand-lg>.container,.navbar-expand-lg>.container-fluid{flex-wrap:nowrap}.navbar-expand-lg .navbar-collapse{display:flex!important;flex-basis:auto}.navbar-expand-lg .navbar-toggler{display:none}}@media (max-width:1199.98px){.navbar-expand-xl>.container,.navbar-expand-xl>.container-fluid{padding-right:0;padding-left:0}}@media (min-width:1200px){.navbar-expand-xl{flex-flow:row nowrap;justify-content:flex-start}.navbar-expand-xl .navbar-nav{flex-direction:row}.navbar-expand-xl .navbar-nav .dropdown-menu{position:absolute}.navbar-expand-xl .navbar-nav .nav-link{padding-right:.5rem;padding-left:.5rem}.navbar-expand-xl>.container,.navbar-expand-xl>.container-fluid{flex-wrap:nowrap}.navbar-expand-xl .navbar-collapse{display:flex!important;flex-basis:auto}.navbar-expand-xl .navbar-toggler{display:none}}";
    styleInject(css$8);

    /* script */
    const __vue_script__$g = script$g;
    /* template */
    var __vue_render__$c = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('nav',{staticClass:"navbar navbar-expand",class:[_vm.bgColor, 
                  _vm.themeColor, 
                  _vm.expand, 
                  _vm.cssPlacement],attrs:{"role":"navigation"}},[_vm._t("default")],2)};
    var __vue_staticRenderFns__$c = [];

      /* style */
      const __vue_inject_styles__$g = undefined;
      /* scoped */
      const __vue_scope_id__$g = undefined;
      /* module identifier */
      const __vue_module_identifier__$g = undefined;
      /* functional template */
      const __vue_is_functional_template__$g = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var Navbar = normalizeComponent_1(
        { render: __vue_render__$c, staticRenderFns: __vue_staticRenderFns__$c },
        __vue_inject_styles__$g,
        __vue_script__$g,
        __vue_scope_id__$g,
        __vue_is_functional_template__$g,
        __vue_module_identifier__$g,
        undefined,
        undefined
      );

    var script$h = {
      name: "NavbarBrand",
      props: {
        /**
         * Define the application name. Defaults to `''`, for no title.
         * @prop {String} title
         */
        title: {
          type: String,
          default: ''
        },

        /**
         * Define the link for the title and icon. Defaults to `/`
         * @prop {String} to
         */
        to: {
          type: String,
          default: '/'
        }
      },
      computed: {
        hasTitle: function hasTitle() {
          return this.title !== '' || false;
        }
      },
      render: function render(h) {
        var title = this.hasTitle ? h('span', this.title) : undefined;
        return h('a', {
          class: ['navbar-brand'],
          attrs: {
            href: this.to
          }
        }, [this.$slots.default, title]);
      }
    };

    var css$9 = ".navbar-brand[data-v-5c133ec1]{display:inline-block;padding-top:.29375rem;padding-bottom:.29375rem;margin-right:1rem;font-size:1.375rem;line-height:inherit;white-space:nowrap}.navbar-brand span[data-v-5c133ec1]{margin-left:.25em}.navbar-brand[data-v-5c133ec1]:focus,.navbar-brand[data-v-5c133ec1]:hover{text-decoration:none}";
    styleInject(css$9);

    /* script */
    const __vue_script__$h = script$h;
    /* template */

      /* style */
      const __vue_inject_styles__$h = undefined;
      /* scoped */
      const __vue_scope_id__$h = "data-v-5c133ec1";
      /* module identifier */
      const __vue_module_identifier__$h = undefined;
      /* functional template */
      const __vue_is_functional_template__$h = undefined;
      /* style inject */
      
      /* style inject SSR */
      

      
      var NavbarBrand = normalizeComponent_1(
        {},
        __vue_inject_styles__$h,
        __vue_script__$h,
        __vue_scope_id__$h,
        __vue_is_functional_template__$h,
        __vue_module_identifier__$h,
        undefined,
        undefined
      );

    var script$i = {
      name: "NavbarContent",
      functional: true,
      render: function render(h, context) {
        return h('ul', objectSpread({
          class: ['navbar-nav']
        }, context.data), context.children);
      }
    };

    /* script */
    const __vue_script__$i = script$i;

    /* template */

      /* style */
      const __vue_inject_styles__$i = undefined;
      /* scoped */
      const __vue_scope_id__$i = undefined;
      /* module identifier */
      const __vue_module_identifier__$i = undefined;
      /* functional template */
      const __vue_is_functional_template__$i = undefined;
      /* style inject */
      
      /* style inject SSR */
      

      
      var NavbarContent = normalizeComponent_1(
        {},
        __vue_inject_styles__$i,
        __vue_script__$i,
        __vue_scope_id__$i,
        __vue_is_functional_template__$i,
        __vue_module_identifier__$i,
        undefined,
        undefined
      );

    var navbarEventBus = new Vue();

    var script$j = {
      name: "NavbarCollapse",
      props: {
        /**
         * An identifier for a `NavbarToggle` to target
         * @prop {String} id
         */
        id: {
          type: String,
          required: true
        }
      },
      data: function data() {
        return {
          collapsed: true
        };
      },
      computed: {
        /**
         * Is the collapse collapsed?
         */
        collapseStyle: function collapseStyle() {
          return this.collapsed ? {
            display: 'none'
          } : {
            display: 'flex'
          };
        }
      },
      watch: {
        collapsed: function collapsed(val) {
          if (val) {
            this.$emit('shown', this.id);
          } else if (!val) {
            this.$emit('hidden', this.id);
          }
        }
      },
      mounted: function mounted() {
        // Respond to external triggers
        var vm = this;
        navbarEventBus.$on('navbar::toggle', function (id) {
          if (vm.id === id) {
            vm.toggle();
          }
        });
      },
      methods: {
        /**
         * Toggle the collapse visible or hidden
         */
        toggle: function toggle() {
          this.collapsed = !this.collapsed;
        },
        show: function show() {
          if (this.collapsed !== true) {
            this.collapsed = true;
          }
        },
        hide: function hide() {
          if (this.collapsed !== false) {
            this.collapsed = false;
          }
        }
      },
      render: function render(h) {
        return h('div', {
          class: ['collapse', 'navbar-collapse'],
          style: this.collapseStyle,
          on: {
            click: this.toggle
          }
        }, this.$slots.default);
      }
    };

    var css$a = ".navbar-collapse[data-v-4562026e]{flex-basis:100%;flex-grow:1;align-items:center}";
    styleInject(css$a);

    /* script */
    const __vue_script__$j = script$j;
    /* template */

      /* style */
      const __vue_inject_styles__$j = undefined;
      /* scoped */
      const __vue_scope_id__$j = "data-v-4562026e";
      /* module identifier */
      const __vue_module_identifier__$j = undefined;
      /* functional template */
      const __vue_is_functional_template__$j = undefined;
      /* style inject */
      
      /* style inject SSR */
      

      
      var NavbarCollapse = normalizeComponent_1(
        {},
        __vue_inject_styles__$j,
        __vue_script__$j,
        __vue_scope_id__$j,
        __vue_is_functional_template__$j,
        __vue_module_identifier__$j,
        undefined,
        undefined
      );

    var script$k = {
      name: "NavbarForm",
      functional: true,
      render: function render(h, context) {
        return h('form', objectSpread({
          class: ['form-inline']
        }, context.data), context.children);
      }
    };

    /* script */
    const __vue_script__$k = script$k;

    /* template */

      /* style */
      const __vue_inject_styles__$k = undefined;
      /* scoped */
      const __vue_scope_id__$k = undefined;
      /* module identifier */
      const __vue_module_identifier__$k = undefined;
      /* functional template */
      const __vue_is_functional_template__$k = undefined;
      /* style inject */
      
      /* style inject SSR */
      

      
      var NavbarForm = normalizeComponent_1(
        {},
        __vue_inject_styles__$k,
        __vue_script__$k,
        __vue_scope_id__$k,
        __vue_is_functional_template__$k,
        __vue_module_identifier__$k,
        undefined,
        undefined
      );

    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    var script$l = {
      name: "NavbarItem",
      props: {
        /**
         * The route to navigate to
         * @prop {String} to
         */
        to: {
          type: String,
          required: true
        },

        /**
         * Nuke browser history when navigating, defaults to false
         * @prop {Boolean} replace
         */
        replace: {
          type: Boolean,
          default: false
        },

        /**
         * Indicate that the `to` should be appended to the previous route instead of
         * replacing (the normal behavior)
         * @prop {Boolean} append
         */
        append: {
          type: Boolean,
          default: false
        },

        /**
         * Define the html tag that the router-link should render as. Defaults to `a`.
         * @prop {String} tag
         */
        tag: {
          type: String,
          default: 'a'
        },

        /**
         * Force exact matching to show the link's active class
         * @prop {Boolean} exact
         */
        exact: {
          type: Boolean,
          default: false
        },

        /**
         * Disable clicking this navbar-item
         * @prop {Boolean} disable
         */
        disable: {
          type: Boolean,
          default: false
        }
      },
      computed: {
        isDisabled: function isDisabled() {
          return this.disable;
        }
      }
    };

    /* script */
    const __vue_script__$l = script$l;

    /* template */
    var __vue_render__$d = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"nav-item"},[(!_vm.isDisabled)?_c('router-link',{staticClass:"nav-link",attrs:{"to":_vm.to,"replace":_vm.replace,"append":_vm.append,"tag":_vm.tag,"exact":_vm.exact,"active-class":"active"}},[_vm._t("default")],2):_c('a',{staticClass:"nav-link disabled",attrs:{"disabled":""}},[_vm._t("default")],2)],1)};
    var __vue_staticRenderFns__$d = [];

      /* style */
      const __vue_inject_styles__$l = undefined;
      /* scoped */
      const __vue_scope_id__$l = undefined;
      /* module identifier */
      const __vue_module_identifier__$l = undefined;
      /* functional template */
      const __vue_is_functional_template__$l = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var NavbarItem = normalizeComponent_1(
        { render: __vue_render__$d, staticRenderFns: __vue_staticRenderFns__$d },
        __vue_inject_styles__$l,
        __vue_script__$l,
        __vue_scope_id__$l,
        __vue_is_functional_template__$l,
        __vue_module_identifier__$l,
        undefined,
        undefined
      );

    //
    var script$m = {
      name: "NavbarItemDropdown",
      mixins: [dropdownMixin, clickoutMixin],
      props: {
        /**
         * Label the dropdown link
         * @prop {String} label
         */
        label: {
          type: String,
          default: ''
        },

        /**
         * Define the theme color
         * @prop {String} theme 
         */
        theme: {
          type: String,
          default: 'light',
          validator: function validator(value) {
            return ['light', 'dark', 'custom'].indexOf(value) > -1;
          }
        },

        /**
         * Define a custom theme class selector for the component to use
         * @prop {String} customThemeClass
         */
        customThemeClass: {
          type: String,
          default: ''
        }
      },
      computed: {
        themeClass: function themeClass() {
          if (this.theme === 'light') {
            return 'dropdown-menu__light';
          } else if (this.theme === 'dark') {
            return 'dropdown-menu__dark';
          } else if (this.theme === 'custom') {
            return this.customThemeClass;
          } else {
            return undefined;
          }
        }
      },
      methods: {
        /**
         * Respond to clicking outside of the component as per the clickout mixin
         */
        onClickOut: function onClickOut() {
          this.show = false;
        }
      }
    };

    var css$b = ".dropdown a[data-v-3cdd41bc]{cursor:pointer}.dropdown-menu--show[data-v-3cdd41bc]{display:block!important}.dropdown-menu__light[data-v-3cdd41bc]{background-color:#f8f9fa!important}.dropdown-menu__light .dropdown-item[data-v-3cdd41bc]:hover{background-color:#dae0e5}.dropdown-menu__dark[data-v-3cdd41bc]{background-color:#343a40!important;border-color:#636c72}.dropdown-menu__dark .dropdown-item[data-v-3cdd41bc]{color:#fff}.dropdown-menu__dark .dropdown-item[data-v-3cdd41bc]:hover{background-color:#4b545c}.dropdown-menu__dark .dropdown-divider[data-v-3cdd41bc]{background-color:#636c72}.dropdown-menu__dark .dropdown-header[data-v-3cdd41bc]{background-color:#6f7980}";
    styleInject(css$b);

    /* script */
    const __vue_script__$m = script$m;
    /* template */
    var __vue_render__$e = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"nav-item dropdown"},[_c('a',{staticClass:"nav-link dropdown-toggle",attrs:{"aria-haspopup":"true","aria-expanded":_vm.show},on:{"click":_vm.toggle}},[_vm._v("\n        "+_vm._s(_vm.label)+"\n    ")]),_vm._v(" "),_c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.show),expression:"show"}],ref:"dropdown",staticClass:"dropdown-menu",class:[
                 {'dropdown-menu--show': _vm.show},
                 _vm.themeClass]},[_vm._t("default")],2)])};
    var __vue_staticRenderFns__$e = [];

      /* style */
      const __vue_inject_styles__$m = undefined;
      /* scoped */
      const __vue_scope_id__$m = "data-v-3cdd41bc";
      /* module identifier */
      const __vue_module_identifier__$m = undefined;
      /* functional template */
      const __vue_is_functional_template__$m = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var NavbarItemDropdown = normalizeComponent_1(
        { render: __vue_render__$e, staticRenderFns: __vue_staticRenderFns__$e },
        __vue_inject_styles__$m,
        __vue_script__$m,
        __vue_scope_id__$m,
        __vue_is_functional_template__$m,
        __vue_module_identifier__$m,
        undefined,
        undefined
      );

    var script$n = {
      name: "NavbarText",
      functional: true,
      render: function render(h, context) {
        return h('span', objectSpread({
          class: ['navbar-text']
        }, context.data), context.children);
      }
    };

    var css$c = ".navbar-text[data-v-ba7b3718]{display:inline-block;padding-top:.5rem;padding-bottom:.5rem}";
    styleInject(css$c);

    /* script */
    const __vue_script__$n = script$n;
    /* template */

      /* style */
      const __vue_inject_styles__$n = undefined;
      /* scoped */
      const __vue_scope_id__$n = "data-v-ba7b3718";
      /* module identifier */
      const __vue_module_identifier__$n = undefined;
      /* functional template */
      const __vue_is_functional_template__$n = undefined;
      /* style inject */
      
      /* style inject SSR */
      

      
      var NavbarText = normalizeComponent_1(
        {},
        __vue_inject_styles__$n,
        __vue_script__$n,
        __vue_scope_id__$n,
        __vue_is_functional_template__$n,
        __vue_module_identifier__$n,
        undefined,
        undefined
      );

    var script$o = {
      name: "NavbarToggler",
      props: {
        /**
         * The id of a `NavbarCollapse` to toggle
         */
        target: {
          type: String,
          required: true
        }
      },
      methods: {
        /**
         * Send a toggle emit to be caught by the target
         */
        toggle: function toggle() {
          navbarEventBus.$emit('navbar::toggle', this.target);
        }
      },
      render: function render(h) {
        return h('button', {
          class: ['navbar-toggler'],
          attrs: {
            type: 'button',
            ariaControls: this.target
          },
          on: {
            click: this.toggle
          }
        }, [h('i', {
          class: ['fa', 'fa-bars', 'navbar-toggler__icon'],
          attrs: {
            ariaHidden: true
          }
        })]);
      }
    };

    var css$d = ".navbar-toggler[data-v-bd235cd0]{align-self:center;border:none!important;padding:0}.navbar-toggler .navbar-toggler__icon[data-v-bd235cd0]{vertical-align:middle}";
    styleInject(css$d);

    /* script */
    const __vue_script__$o = script$o;
    /* template */

      /* style */
      const __vue_inject_styles__$o = undefined;
      /* scoped */
      const __vue_scope_id__$o = "data-v-bd235cd0";
      /* module identifier */
      const __vue_module_identifier__$o = undefined;
      /* functional template */
      const __vue_is_functional_template__$o = undefined;
      /* style inject */
      
      /* style inject SSR */
      

      
      var NavbarToggler = normalizeComponent_1(
        {},
        __vue_inject_styles__$o,
        __vue_script__$o,
        __vue_scope_id__$o,
        __vue_is_functional_template__$o,
        __vue_module_identifier__$o,
        undefined,
        undefined
      );

    function _arrayWithHoles(arr) {
      if (Array.isArray(arr)) return arr;
    }

    var arrayWithHoles = _arrayWithHoles;

    var arrayWithHoles$1 = /*#__PURE__*/Object.freeze({
        default: arrayWithHoles,
        __moduleExports: arrayWithHoles
    });

    function _iterableToArrayLimit(arr, i) {
      var _arr = [];
      var _n = true;
      var _d = false;
      var _e = undefined;

      try {
        for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
          _arr.push(_s.value);

          if (i && _arr.length === i) break;
        }
      } catch (err) {
        _d = true;
        _e = err;
      } finally {
        try {
          if (!_n && _i["return"] != null) _i["return"]();
        } finally {
          if (_d) throw _e;
        }
      }

      return _arr;
    }

    var iterableToArrayLimit = _iterableToArrayLimit;

    var iterableToArrayLimit$1 = /*#__PURE__*/Object.freeze({
        default: iterableToArrayLimit,
        __moduleExports: iterableToArrayLimit
    });

    function _nonIterableRest() {
      throw new TypeError("Invalid attempt to destructure non-iterable instance");
    }

    var nonIterableRest = _nonIterableRest;

    var nonIterableRest$1 = /*#__PURE__*/Object.freeze({
        default: nonIterableRest,
        __moduleExports: nonIterableRest
    });

    var arrayWithHoles$2 = ( arrayWithHoles$1 && arrayWithHoles ) || arrayWithHoles$1;

    var iterableToArrayLimit$2 = ( iterableToArrayLimit$1 && iterableToArrayLimit ) || iterableToArrayLimit$1;

    var nonIterableRest$2 = ( nonIterableRest$1 && nonIterableRest ) || nonIterableRest$1;

    function _slicedToArray(arr, i) {
      return arrayWithHoles$2(arr) || iterableToArrayLimit$2(arr, i) || nonIterableRest$2();
    }

    var slicedToArray = _slicedToArray;

    // Various utility functions for internal use
    // Determine if an element is an HTML Element
    var isElement = function isElement(el) {
      return Boolean(el && el.nodeType === Node.ELEMENT_NODE);
    }; // Get an element given an ID

    var getById = function getById(id) {
      return document.getElementById(/^#/.test(id) ? id.slice(1) : id) || null;
    };
    var toType = function toType(val) {
      return _typeof_1(val);
    };
    var isObject = function isObject(obj) {
      return obj !== null && _typeof_1(obj) === 'object';
    };
    var isString = function isString(val) {
      return toType(val) === 'string';
    };
    /**
     * Sleep an async function for a specified number of milliseconds in a non-blocking manner
     * @param {Number} ms milliseconds
     */

    var sleep = function sleep(ms) {
      return new Promise(function (res) {
        return setTimeout(res, ms);
      });
    };

    // - implement clickout mixin when in click mode?

    var script$p = {
      name: "Popover",
      inheritAttrs: false,
      model: {
        prop: 'visible',
        event: 'visibilityupdated'
      },
      props: {
        /** 
         * Turn off the animation
         * @prop {Boolean} animation
         */
        animation: {
          type: Boolean,
          default: true
        },

        /**
         * Explicitely set the container, otherwise it will default to body
         * @prop {String} container
         */
        container: {
          type: String,
          default: 'body'
        },

        /**
         * Set the content
         * @prop {String} content
         */
        content: {
          type: String,
          default: ''
        },

        /**
         * Define a delay, either for both open and close (a Number),
         * or seperately for each (an object: `{ start: 0, end: 0 }`)
         * delay is defined in milliseconds
         * @prop {(Number|Object)} delay
         */
        delay: {
          type: [Number, Object],
          default: 0
        },

        /**
         * Disable the popover
         * @prop {Boolean} disabled
         */
        disabled: {
          type: Boolean,
          default: false
        },

        /**
         * Enable or disable flipping on its axis if space disappears
         * @prop {Boolean} disableFlip
         */
        disableFlip: {
          type: Boolean,
          default: false
        },

        /**
         * Define an offset for rendering the dropdown
         * @prop {(Number|String)} offset
         */
        offset: {
          type: [Number, String],
          default: undefined
        },

        /**
         * Set the initial render placement. The default, `auto` will
         * render in a predeterimined order based on the content
         * around the target element
         * @prop {String} placement
         */
        placement: {
          type: String,
          default: 'auto',
          validator: function validator(value) {
            return ['auto', 'left', 'left-start', 'left-end', 'top', 'top-start', 'top-end', 'right', 'right-start', 'right-end', 'bottom', 'bottom-start', 'bottom-end'].indexOf(value) > -1;
          }
        },

        /**
         * Set the target element, which the popover will render from
         * based on the trigger type
         * @prop {(String|Object|window.Element} target
         */
        target: {
          type: [String, Object, window.Element],
          required: true
        },

        /**
         * Set the title. Leave blank to hide the title area.
         * @prop {String} title
         */
        title: {
          type: String,
          default: ''
        },

        /**
         * Set trigger that reveals the popover
         * multiple options can be passed in seperated by a space
         * 'none` is mutually exclusive, only allows popover to be shown
         * via the v-model (visible) prop
         * @prop {String} trigger
         */
        trigger: {
          type: String,
          default: 'none',
          // TODO: here's probably a way better way to do this...
          validator: function validator(value) {
            return ['click', 'click hover', 'click focus', 'click focus hover', 'click hover focus', 'hover', 'hover click', 'hover focus', 'hover click focus', 'hover focus click', 'focus', 'focus click', 'focus hover', 'focus click hover', 'focus hover click', 'none'].indexOf(value) > -1;
          }
        },

        /**
         * Set the popover visible
         * @prop {Boolean} visible
         */
        visible: {
          type: Boolean,
          default: false
        }
      },
      data: function data() {
        return {
          isVisible: this.visible,
          currentTargetElement: null,
          currentPlacement: this.placement,
          currentContainer: ''
        };
      },
      computed: {
        /**
         * Compute the class for displaying the proper placement
         * @returns {String}
         */
        placementClass: function placementClass() {
          // Strip the variation from the placement
          var _this$currentPlacemen = this.currentPlacement.split(/-/),
              _this$currentPlacemen2 = slicedToArray(_this$currentPlacemen, 1),
              placement = _this$currentPlacemen2[0];

          return "bs-popover-".concat(placement);
        },

        /**
         * Compute whether the title element should be rendered
         * @returns {Boolean}
         */
        showTitle: function showTitle() {
          return this.title !== '';
        },

        /**
         * Build a more usable representation of the delay prop
         * @returns {Object}
         */
        delayConfig: function delayConfig() {
          return isObject(this.delay) ? this.delay : {
            start: this.delay,
            end: this.delay
          };
        },

        /**
         * Build a list of triggerable options to register as listeners
         * @returns {Object}
         */
        listeners: function listeners() {
          var _this = this;

          var listeners = {};
          this.trigger.split(/\s+/).forEach(function (t) {
            switch (t) {
              case 'click':
                listeners.click = _this.toggle;
                break;

              case 'hover':
                listeners.mouseenter = _this.show;
                listeners.touchstart = _this.show;
                listeners.mouseleave = _this.hide;
                listeners.touchend = _this.hide;
                break;

              case 'focus':
                listeners.focus = _this.show;
                listeners.blur = _this.hide;
                break;
              // case `none`

              default:
            }
          });
          return listeners;
        }
      },
      watch: {
        // Toggle hiding/showing as needed
        visible: function visible(val) {
          // Trigger the proper event
          val ? this.show() : this.hide();
        },
        // React to changes in the container prop
        container: function container() {
          this.resetContainer();
          this.setContainer();
        },
        // React to changes in the target prop
        target: function target() {
          this.resetTarget();
          this.setTarget();
        }
      },
      created: function created() {
        // Set initial placement
        this.currentPlacement = this.placement; // Non-reactive property to hold popover config callback

        this._popover = null;
      },
      mounted: function mounted() {
        this.setContainer();
        this.setTarget(); // Build the popover properly if `visible` is true on render

        if (this.isVisible) {
          this.show();
        }
      },
      updated: function updated() {
        this.update();
      },
      beforeDestroy: function beforeDestroy() {
        this.destroyPopover();
        this.resetContainer();
        this.resetTarget();
      },
      methods: {
        /**
         * Toggle the popovers visibility
         */
        toggle: function toggle() {
          this.isVisible ? this.hide() : this.show();
        },

        /**
         * Show the popover
         */
        show: function () {
          var _show = asyncToGenerator(
          /*#__PURE__*/
          regenerator.mark(function _callee() {
            return regenerator.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    if (!(this.disabled || this.isVisible)) {
                      _context.next = 2;
                      break;
                    }

                    return _context.abrupt("return");

                  case 2:
                    // Create if we have a target
                    if (this.currentTargetElement) {
                      this.createPopover();
                    } // Handle any delay set up


                    if (!(this.delayConfig.start > 0)) {
                      _context.next = 6;
                      break;
                    }

                    _context.next = 6;
                    return sleep(this.delayConfig.start);

                  case 6:
                    this.isVisible = true;

                  case 7:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));

          function show() {
            return _show.apply(this, arguments);
          }

          return show;
        }(),

        /**
         * Hide the popover and handle garbage collection
         */
        hide: function () {
          var _hide = asyncToGenerator(
          /*#__PURE__*/
          regenerator.mark(function _callee2() {
            return regenerator.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    if (!(this.disabled || !this.isVisible)) {
                      _context2.next = 2;
                      break;
                    }

                    return _context2.abrupt("return");

                  case 2:
                    // Make sure the _popper instance is destroyed after it's hidden
                    this.$once('hidden', this.destroyPopover); // Handle any delay set up

                    if (!(this.delayConfig.end > 0)) {
                      _context2.next = 6;
                      break;
                    }

                    _context2.next = 6;
                    return sleep(this.delayConfig.end);

                  case 6:
                    this.isVisible = false;

                  case 7:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));

          function hide() {
            return _hide.apply(this, arguments);
          }

          return hide;
        }(),

        /**
         * Create the popover
         */
        createPopover: function createPopover() {
          var _this2 = this;

          if (this.currentTargetElement) {
            this._popover = new Popper(this.currentTargetElement, // Target element
            this.$el, // The popover
            {
              placement: this.currentPlacement,
              modifiers: {
                offset: {
                  offset: this.offset || 0
                },
                flip: {
                  enabled: !this.disableFlip
                },
                arrow: {
                  element: this.$refs.arrow
                }
              },
              onCreate: function onCreate(data) {
                _this2.currentPlacement = data.placement;
              },
              onUpdate: function onUpdate(data) {
                _this2.currentPlacement = data.placement;
              }
            });
          }
        },

        /**
         * Destroy the popover
         */
        destroyPopover: function destroyPopover() {
          if (this._popover) {
            this._popover.destroy();

            this._popover = null;
          }
        },

        /**
         * Render the popover html in the specified location of the page, by default it
         * will render in the body unless the `container` prop is set otherwise.
         */
        setContainer: function setContainer() {
          var $el = this.$el;
          var currentContainer = this.container;

          if (isString(this.container)) {
            currentContainer = $el.ownerDocument.querySelector(this.container);
          }

          if (currentContainer) {
            this._initialParentElement = $el.parentElement;
            this._initialNextElementSibling = $el.nextElementSibling;
            this.currentContainer = currentContainer;
            currentContainer.appendChild($el);
          }
        },

        /**
         * Handle selecting the correct target for the popover to attach to, and if
         * any listeners exist, set those
         */
        setTarget: function setTarget() {
          var target = this.target; // Figure out what the target is

          if (isString(target)) {
            // assume the string is an id
            target = getById(target);
          } else if (isObject(target) && isElement(target.$el)) {
            // Component reference
            target = target.$el;
          }

          if (target) {
            for (var event in this.listeners) {
              target.addEventListener(event, this.listeners[event]);
            }

            this.currentTargetElement = target;
          }
        },

        /**
         * Reset the render location upon destruction
         */
        resetContainer: function resetContainer() {
          if (this._initialParentElement) {
            this._initialParentElement.insertBefore(this.$el, this._initialNextElementSibling);
          }
        },

        /**
         * Reset the target element upon destruction and remove any listeners
         */
        resetTarget: function resetTarget() {
          if (this.currentTargetElement) {
            this.currentTargetElement = null;
          }
        },

        /**
         * Used by the directive to trigger updates
         */
        update: function update() {
          if (this._popover) {
            this._popover.scheduleUpdate();
          }
        }
      }
    };

    var css$e = ".bs-popover-transition--fade-enter-active,.bs-popover-transition--fade-leave-active{transition:opacity .15s}.bs-popover-transition--fade-enter,.bs-popover-transition--fade-leave-to{opacity:0}.bs-popover{position:absolute;top:0;left:0;z-index:1060;display:block;max-width:276px;font-family:MiloWeb,Verdana,Geneva,sans-serif;font-style:normal;font-weight:400;line-height:1.5;text-align:left;text-align:start;text-decoration:none;text-shadow:none;text-transform:none;letter-spacing:normal;word-break:normal;word-spacing:normal;white-space:normal;line-break:auto;font-size:.875rem;word-wrap:break-word;background-color:#fff;background-clip:padding-box;border:1px solid rgba(0,0,0,.2);border-radius:.3rem}.bs-popover .arrow{position:absolute;display:block;width:1rem;height:.5rem;margin:0 .3rem}.bs-popover .arrow::after,.bs-popover .arrow::before{position:absolute;display:block;content:\"\";border-color:transparent;border-style:solid}.bs-popover-auto[x-placement^=top],.bs-popover-top{margin-bottom:.5rem}.bs-popover-auto[x-placement^=top] .arrow,.bs-popover-top .arrow{bottom:calc((.5rem + 1px) * -1)}.bs-popover-auto[x-placement^=top] .arrow::after,.bs-popover-auto[x-placement^=top] .arrow::before,.bs-popover-top .arrow::after,.bs-popover-top .arrow::before{border-width:.5rem .5rem 0}.bs-popover-auto[x-placement^=top] .arrow::before,.bs-popover-top .arrow::before{bottom:0;border-top-color:rgba(0,0,0,.25)}.bs-popover-auto[x-placement^=top] .arrow::after,.bs-popover-top .arrow::after{bottom:1px;border-top-color:#fff}.bs-popover-auto[x-placement^=right],.bs-popover-right{margin-left:.5rem}.bs-popover-auto[x-placement^=right] .arrow,.bs-popover-right .arrow{left:calc((.5rem + 1px) * -1);width:.5rem;height:1rem;margin:.3rem 0}.bs-popover-auto[x-placement^=right] .arrow::after,.bs-popover-auto[x-placement^=right] .arrow::before,.bs-popover-right .arrow::after,.bs-popover-right .arrow::before{border-width:.5rem .5rem .5rem 0}.bs-popover-auto[x-placement^=right] .arrow::before,.bs-popover-right .arrow::before{left:0;border-right-color:rgba(0,0,0,.25)}.bs-popover-auto[x-placement^=right] .arrow::after,.bs-popover-right .arrow::after{left:1px;border-right-color:#fff}.bs-popover-auto[x-placement^=bottom],.bs-popover-bottom{margin-top:.5rem}.bs-popover-auto[x-placement^=bottom] .arrow,.bs-popover-bottom .arrow{top:calc((.5rem + 1px) * -1)}.bs-popover-auto[x-placement^=bottom] .arrow::after,.bs-popover-auto[x-placement^=bottom] .arrow::before,.bs-popover-bottom .arrow::after,.bs-popover-bottom .arrow::before{border-width:0 .5rem .5rem .5rem}.bs-popover-auto[x-placement^=bottom] .arrow::before,.bs-popover-bottom .arrow::before{top:0;border-bottom-color:rgba(0,0,0,.25)}.bs-popover-auto[x-placement^=bottom] .arrow::after,.bs-popover-bottom .arrow::after{top:1px;border-bottom-color:#fff}.bs-popover-auto[x-placement^=bottom] .bs-popover-header::before,.bs-popover-bottom .bs-popover-header::before{position:absolute;top:0;left:50%;display:block;width:1rem;margin-left:-.5rem;content:\"\";border-bottom:1px solid #f7f7f7}.bs-popover-auto[x-placement^=left],.bs-popover-left{margin-right:.5rem}.bs-popover-auto[x-placement^=left] .arrow,.bs-popover-left .arrow{right:calc((.5rem + 1px) * -1);width:.5rem;height:1rem;margin:.3rem 0}.bs-popover-auto[x-placement^=left] .arrow::after,.bs-popover-auto[x-placement^=left] .arrow::before,.bs-popover-left .arrow::after,.bs-popover-left .arrow::before{border-width:.5rem 0 .5rem .5rem}.bs-popover-auto[x-placement^=left] .arrow::before,.bs-popover-left .arrow::before{right:0;border-left-color:rgba(0,0,0,.25)}.bs-popover-auto[x-placement^=left] .arrow::after,.bs-popover-left .arrow::after{right:1px;border-left-color:#fff}.bs-popover-header{padding:.5rem .75rem;margin-bottom:0;font-size:1rem;color:inherit;background-color:#f7f7f7;border-bottom:1px solid #ebebeb;border-top-left-radius:calc(.3rem - 1px);border-top-right-radius:calc(.3rem - 1px)}.bs-popover-header:empty{display:none}.bs-popover-body{padding:.5rem .75rem;color:#212529}";
    styleInject(css$e);

    /* script */
    const __vue_script__$p = script$p;
    /* template */
    var __vue_render__$f = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('transition',{attrs:{"name":_vm.animation ? 'bs-popover-transition--fade' : ''},on:{"before-enter":function($event){_vm.$emit('show') && _vm.$emit('visibilityupdated', true);},"after-enter":function($event){return _vm.$emit('shown')},"before-leave":function($event){_vm.$emit('hide') && _vm.$emit('visibilityupdated', false);},"after-leave":function($event){return _vm.$emit('hidden')}}},[_c('div',_vm._g(_vm._b({directives:[{name:"show",rawName:"v-show",value:(_vm.isVisible),expression:"isVisible"}],staticClass:"bs-popover",class:_vm.placementClass,attrs:{"aria-hidden":!_vm.isVisible,"role":"tooltip"}},'div',_vm.$attrs,false),_vm.$listeners),[_c('div',{ref:"arrow",staticClass:"arrow"}),_vm._v(" "),_vm._t("title",[(_vm.showTitle)?_c('h3',{staticClass:"bs-popover-header"},[_vm._v("\n                "+_vm._s(_vm.title)+"\n            ")]):_vm._e()]),_vm._v(" "),_vm._t("default",[_c('div',{staticClass:"bs-popover-body"},[_vm._v("\n                "+_vm._s(_vm.content)+"\n            ")])])],2)])};
    var __vue_staticRenderFns__$f = [];

      /* style */
      const __vue_inject_styles__$p = undefined;
      /* scoped */
      const __vue_scope_id__$p = undefined;
      /* module identifier */
      const __vue_module_identifier__$p = undefined;
      /* functional template */
      const __vue_is_functional_template__$p = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var Popover = normalizeComponent_1(
        { render: __vue_render__$f, staticRenderFns: __vue_staticRenderFns__$f },
        __vue_inject_styles__$p,
        __vue_script__$p,
        __vue_scope_id__$p,
        __vue_is_functional_template__$p,
        __vue_module_identifier__$p,
        undefined,
        undefined
      );

    //
    var script$q = {
      name: "Radio",
      mixins: [inputMixin],
      inheritAttrs: false,
      model: {
        prop: "value",
        event: "change"
      },
      props: {
        label: {
          type: String,
          default: undefined
        },
        name: {
          type: String,
          required: true
        },
        value: {
          type: [String, Number, Boolean],
          required: true
        },
        checked: {
          type: Boolean,
          default: false
        }
      },
      data: function data() {
        return {
          isChecked: this.checked
        };
      },
      methods: {
        /**
         * Emits a change event if the radio is selected
         */
        updated: function updated() {
          if (this.isChecked === false) {
            this.isChecked = true;
          }

          this.$emit('change', this.value);
        },

        /**
         * Clear the select of it's checked state, emits an empty string to the v-model binding.
         */
        clear: function clear() {
          this.isChecked = false;
          this.$emit('change', '');
        }
      }
    };

    var css$f = ".form-check.form-check-inline[data-v-0309dbd8]{margin-bottom:0!important}";
    styleInject(css$f);

    /* script */
    const __vue_script__$q = script$q;
    /* template */
    var __vue_render__$g = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"form-check",class:[{'disabled': _vm.disabled},
                                     {'form-check-inline': _vm.inline}]},[_c('label',{staticClass:"custom-control custom-radio"},[_c('input',_vm._b({ref:"radio",staticClass:"custom-control-input",class:[{'form-control-success': _vm.success}, 
                                                         {'form-control-warning': _vm.warning}, 
                                                         {'form-control-danger': _vm.danger}],attrs:{"type":"radio","name":_vm.name,"disabled":_vm.disabled},domProps:{"value":_vm.value,"checked":_vm.isChecked},on:{"change":_vm.updated}},'input',_vm.$attrs,false)),_vm._v("\n        "+_vm._s(_vm.label)+"\n        "),_c('span',{staticClass:"custom-control-indicator"})])])};
    var __vue_staticRenderFns__$g = [];

      /* style */
      const __vue_inject_styles__$q = undefined;
      /* scoped */
      const __vue_scope_id__$q = "data-v-0309dbd8";
      /* module identifier */
      const __vue_module_identifier__$q = undefined;
      /* functional template */
      const __vue_is_functional_template__$q = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var Radio = normalizeComponent_1(
        { render: __vue_render__$g, staticRenderFns: __vue_staticRenderFns__$g },
        __vue_inject_styles__$q,
        __vue_script__$q,
        __vue_scope_id__$q,
        __vue_is_functional_template__$q,
        __vue_module_identifier__$q,
        undefined,
        undefined
      );

    //
    var script$r = {
      name: "RadioGroup",
      components: {
        Radio: Radio
      },
      mixins: [inputMixin],
      props: {
        /**
         * A label for all radios contained in the group
         * @type {String}
         * @default ''
         */
        groupLabel: {
          type: String,
          default: ''
        },

        /**
         * Name the radio group
         * @type {String}
         */
        name: {
          type: String,
          required: true
        },

        /**
         * A list of radio controls to include
         * @property {Object} options
         * @property {String} options.label - the radio input label
         * @property {String} options.value - the value to return when selected
         * @property {Boolean} options.checked - indicate the radio should be selected initially
         * @property {Boolean} options.disabled - indicate the radio should be disabled
         */
        options: {
          type: Array,
          required: true
        },

        /**
         * Data binding. If an initial value is sent, it will be the default if no one selects anything. Be sure
         * to set the default option's checked property to `true` to show the user there is a default value.
         * @type {String}
         * @default ''
         */
        value: {
          type: String,
          default: ''
        }
      },
      data: function data() {
        return {
          selectedValue: this.value
        };
      },
      methods: {
        onChange: function onChange(val) {
          this.selectedValue = val;
          this.$emit('input', val);
        },

        /**
         * Disabled each radio by it's own option or via disabling the entire control set
         * @param {Boolean} component - disable the entire radio group
         * @param {Boolean} individual - disable via options for a specific radio input
         */
        isDisabled: function isDisabled(component, individual) {
          return component === true || individual === true ? true : false;
        },

        /**
         * A function to reset the radios to no selection, emits an empty string to the v-model binding.
         */
        clear: function clear() {
          this.$children.forEach(function (radio) {
            return radio.clear();
          });
          this.$emit('input', '');
        }
      }
    };

    /* script */
    const __vue_script__$r = script$r;

    /* template */
    var __vue_render__$h = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"form-group",class:[{'disabled': _vm.disabled},
                                     {'has-success': _vm.success}, 
                                     {'has-warning': _vm.warning}, 
                                     {'has-danger': _vm.danger}]},[(_vm.groupLabel != '')?_c('label',[_vm._v("\n        "+_vm._s(_vm.groupLabel)+"\n    ")]):_vm._e(),_vm._v(" "),_c('div',{attrs:{"role":"radiogroup","tabindex":"-1"}},_vm._l((_vm.options),function(option,key){return _c('radio',{key:key,attrs:{"id":_vm.name + '-radio-' + key,"inline":_vm.inline,"disabled":_vm.isDisabled(_vm.disabled, option.disabled),"name":_vm.name,"label":option.label,"checked":option.checked || false,"value":option.value,"success":_vm.success,"warning":_vm.warning,"danger":_vm.danger},on:{"change":_vm.onChange}})}),1),_vm._v(" "),(_vm.stateEnabled && _vm.success)?_c('div',{staticClass:"form-control-feedback"},[_vm._v(_vm._s(_vm.successMessage))]):_vm._e(),_vm._v(" "),(_vm.stateEnabled && _vm.warning)?_c('div',{staticClass:"form-control-feedback"},[_vm._v(_vm._s(_vm.warningMessage))]):_vm._e(),_vm._v(" "),(_vm.stateEnabled && _vm.danger)?_c('div',{staticClass:"form-control-feedback"},[_vm._v(_vm._s(_vm.dangerMessage))]):_vm._e(),_vm._v(" "),(_vm.isDescription)?_c('small',{staticClass:"text-muted",class:[{'form-text': !_vm.inlineDescription}],attrs:{"id":_vm.id + 'Help'}},[_vm._v("\n        "+_vm._s(_vm.description)+"\n    ")]):_vm._e()])};
    var __vue_staticRenderFns__$h = [];

      /* style */
      const __vue_inject_styles__$r = undefined;
      /* scoped */
      const __vue_scope_id__$r = undefined;
      /* module identifier */
      const __vue_module_identifier__$r = undefined;
      /* functional template */
      const __vue_is_functional_template__$r = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var RadioGroup = normalizeComponent_1(
        { render: __vue_render__$h, staticRenderFns: __vue_staticRenderFns__$h },
        __vue_inject_styles__$r,
        __vue_script__$r,
        __vue_scope_id__$r,
        __vue_is_functional_template__$r,
        __vue_module_identifier__$r,
        undefined,
        undefined
      );

    //
    //
    //
    //
    //
    //
    //
    //
    var script$s = {
      name: "Redbar",
      props: {
        /**
         * Set the background css class
         * @prop {String} background
         */
        background: {
          type: String,
          default: 'bg-accent'
        }
      }
    };

    var css$g = ".bg-accent[data-v-6abd1400]{background-color:#ab0520!important}.redbar[data-v-6abd1400]{flex:none;padding:0 10px}.redbar img[data-v-6abd1400]{margin:.5rem auto;min-width:250px;max-width:300px;max-height:22px}";
    styleInject(css$g);

    /* script */
    const __vue_script__$s = script$s;
    /* template */
    var __vue_render__$i = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('header',{staticClass:"redbar",class:_vm.background},[_vm._m(0)])};
    var __vue_staticRenderFns__$i = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('a',{attrs:{"href":"http://www.arizona.edu","title":"The Univerity of Arizona"}},[_c('img',{attrs:{"alt":"The University of Arizona","src":"https://assets.cdn.fso.arizona.edu/ua_wordmark_line_logo_white_rgb.svg"}})])}];

      /* style */
      const __vue_inject_styles__$s = undefined;
      /* scoped */
      const __vue_scope_id__$s = "data-v-6abd1400";
      /* module identifier */
      const __vue_module_identifier__$s = undefined;
      /* functional template */
      const __vue_is_functional_template__$s = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var Redbar = normalizeComponent_1(
        { render: __vue_render__$i, staticRenderFns: __vue_staticRenderFns__$i },
        __vue_inject_styles__$s,
        __vue_script__$s,
        __vue_scope_id__$s,
        __vue_is_functional_template__$s,
        __vue_module_identifier__$s,
        undefined,
        undefined
      );

    //
    //
    //
    //
    //
    //
    //
    //
    //
    var script$t = {
      name: "SegmentedToggleOption",
      model: {
        prop: "value",
        event: "change"
      },
      props: {
        /**
         * Set the label
         * @prop {String}
         */
        label: {
          type: String,
          default: undefined
        },

        /**
         * Set the button type
         * @prop {String}
         */
        type: {
          type: String,
          default: 'secondary',
          validator: function validator(value) {
            return ['secondary', 'success', 'warning', 'danger', 'info', 'primary', 'uared'].indexOf(value) > -1;
          }
        },

        /**
         * Toggle outline mode
         * * @prop {Boolean}
         */
        outline: {
          type: Boolean,
          default: false
        },

        /**
         * Set the size
         * @prop {String}
         */
        size: {
          type: String,
          default: 'normal',
          validator: function validator(value) {
            return ['large', 'normal', 'small'].indexOf(value) > -1;
          }
        },

        /**
         * Group radio buttons together as one
         * @prop {String}
         */
        name: {
          type: String,
          required: true
        },

        /**
         * Disable the control
         * * @prop {Boolean}
         */
        disabled: {
          type: Boolean,
          default: false
        },

        /**
         * Set the initial state
         * * @prop {Boolean}
         */
        checked: {
          type: Boolean,
          default: false
        },

        /**
         * The value to return when it is selected
         * @prop {String}
         */
        value: {
          type: [String, Number, Boolean],
          required: true
        }
      },
      data: function data() {
        return {
          isChecked: this.checked
        };
      },
      computed: {
        isOutline: function isOutline() {
          if (this.outline === true) {
            return 'btn-outline-';
          } else {
            return 'btn-';
          }
        },
        btnSize: function btnSize() {
          if (this.size === 'large') {
            return 'btn-lg';
          } else if (this.size === 'small') {
            return 'btn-sm';
          } else {
            return '';
          }
        }
      },
      methods: {
        updated: function updated() {
          if (this.isChecked === false) {
            this.isChecked = true;
          }

          this.$emit('change', this.value);
        },

        /**
         * Allow to be set unchecked via code.
         */
        setUnChecked: function setUnChecked() {
          this.isChecked = false;
        },

        /**
         * Allow to be checked via code.
         */
        setChecked: function setChecked() {
          this.isChecked = true;
          this.updated();
        }
      }
    };

    /* script */
    const __vue_script__$t = script$t;

    /* template */
    var __vue_render__$j = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('label',{staticClass:"btn",class:[_vm.isOutline + _vm.type, _vm.btnSize, {'active': _vm.isChecked}]},[_c('input',{attrs:{"type":"radio","name":_vm.name,"disabled":_vm.disabled},domProps:{"value":_vm.value,"checked":_vm.isChecked},on:{"change":_vm.updated}}),_vm._v(" "),_c('span',{staticClass:"mx-auto"},[_vm._v(_vm._s(_vm.label))])])};
    var __vue_staticRenderFns__$j = [];

      /* style */
      const __vue_inject_styles__$t = undefined;
      /* scoped */
      const __vue_scope_id__$t = undefined;
      /* module identifier */
      const __vue_module_identifier__$t = undefined;
      /* functional template */
      const __vue_is_functional_template__$t = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var SegmentedToggleOption = normalizeComponent_1(
        { render: __vue_render__$j, staticRenderFns: __vue_staticRenderFns__$j },
        __vue_inject_styles__$t,
        __vue_script__$t,
        __vue_scope_id__$t,
        __vue_is_functional_template__$t,
        __vue_module_identifier__$t,
        undefined,
        undefined
      );

    //
    //       - when we have bootstrap v4.x, redo to work like: https://github.com/twbs/bootstrap/issues/23728 instead of class defined locally
    //       - allow writing each `segmented-toggle-option` manually in the html?

    var script$u = {
      name: "SegmentedToggle",
      components: {
        SegmentedToggleOption: SegmentedToggleOption
      },
      extends: CommandGroup,
      props: {
        /**
         * A label for all radios contained in the group
         */
        groupLabel: {
          type: String,
          default: ''
        },

        /**
         * Name the radio group
         */
        name: {
          type: String,
          required: true
        },

        /**
         * A list of radio controls to include
         */
        options: {
          type: Array,
          required: true
        },

        /**
         * Data binding. If an initial value is sent, it will be the default if no one selects anything. Be sure
         * to set the default option's checked property to `true` to show the user there is a default value.
         */
        value: {
          type: String,
          default: ''
        },
        type: {
          type: String,
          default: 'secondary',
          validator: function validator(value) {
            return ['secondary', 'success', 'warning', 'danger', 'info', 'primary', 'uared'].indexOf(value) > -1;
          }
        },
        outline: {
          type: Boolean,
          default: false
        },
        disabled: {
          type: Boolean,
          default: false
        },

        /**
         * Make the control take up it's full width rather than fit to its content
         */
        fullWidth: {
          type: Boolean,
          default: false
        }
      },
      data: function data() {
        return {
          selectedValue: this.value
        };
      },
      methods: {
        onSelected: function onSelected(val) {
          this.selectedValue = val;
          this.$emit('input', this.selectedValue); // Uncheck all other inputs

          this.$children.forEach(function (toggle) {
            if (toggle._props.value !== val && toggle.isChecked === true) {
              toggle.setUnChecked();
            }
          });
        },

        /**
         * Disabled each radio by it's own option or via disabling the entire control set
         * @param {Boolean} component - disable the entire radio group
         * @param {Boolean} individual - disable via options for a specific radio input
         */
        isDisabled: function isDisabled(component, individual) {
          return component === true || individual === true ? true : false;
        },

        /**
         * Allow setting to it's original value (what was sent as the `value` property) externally via code.
         */
        setToDefault: function setToDefault() {
          this.setTo(this.value);
        },

        /**
         * Allow setting to a value externally via code.
         */
        setTo: function setTo(val) {
          // TODO: this could be a promise. It checks the options first, then returns 'that doesn't exist' 
          // if its bad, allowing for error handling?
          this.$children.forEach(function (toggle) {
            if (toggle._props.value === val && toggle.isChecked !== true) {
              toggle.setChecked();
            }
          });
        }
      }
    };

    var css$h = ".btn-toggle-group>.btn,.btn-toggle-group>.btn-group>.btn{margin-bottom:0}.btn-toggle-group>.btn input[type=checkbox],.btn-toggle-group>.btn input[type=radio],.btn-toggle-group>.btn-group>.btn input[type=checkbox],.btn-toggle-group>.btn-group>.btn input[type=radio]{position:absolute;clip:rect(0,0,0,0);pointer-events:none}.full-width{display:flex}.full-width .btn{display:flex;flex:auto}";
    styleInject(css$h);

    /* script */
    const __vue_script__$u = script$u;
    /* template */
    var __vue_render__$k = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"btn-group btn-toggle-group",class:[_vm.btnSize, { 'btn-group-vertical': _vm.vertical }, { 'full-width': _vm.fullWidth }],attrs:{"role":"radiogroup","tabindex":"-1","aria-label":_vm.assistiveLabel}},[(_vm.groupLabel != '')?_c('label',[_vm._v("\n        "+_vm._s(_vm.groupLabel)+"\n    ")]):_vm._e(),_vm._v(" "),_vm._l((_vm.options),function(option,key){return _c('segmented-toggle-option',{key:key,attrs:{"type":_vm.type,"outline":_vm.outline,"disabled":_vm.isDisabled(_vm.disabled, option.disabled),"name":_vm.name,"label":option.label,"value":option.value,"checked":option.checked || false},on:{"change":_vm.onSelected}})})],2)};
    var __vue_staticRenderFns__$k = [];

      /* style */
      const __vue_inject_styles__$u = undefined;
      /* scoped */
      const __vue_scope_id__$u = undefined;
      /* module identifier */
      const __vue_module_identifier__$u = undefined;
      /* functional template */
      const __vue_is_functional_template__$u = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var SegmentedToggle = normalizeComponent_1(
        { render: __vue_render__$k, staticRenderFns: __vue_staticRenderFns__$k },
        __vue_inject_styles__$u,
        __vue_script__$u,
        __vue_scope_id__$u,
        __vue_is_functional_template__$u,
        __vue_module_identifier__$u,
        undefined,
        undefined
      );

    /*
     * Mixin to provide form-input-addons to inputs that can use them.
     */
    var inputAddonMixin = {
      data: function data() {
        return {
          addonVisibility: {
            start: false,
            end: false
          }
        };
      },
      props: {
        addon: {
          type: String,
          default: 'none',
          validator: function validator(value) {
            return ['none', 'both', 'start', 'end'].indexOf(value) > -1;
          }
        }
      },
      methods: {
        addonConfig: function addonConfig() {
          if (this.addon === 'none') {
            this.addonVisibility.start = false;
            this.addonVisibility.end = false;
          } else if (this.addon === 'both') {
            this.addonVisibility.start = true;
            this.addonVisibility.end = true;
          } else if (this.addon === 'start') {
            this.addonVisibility.start = true;
            this.addonVisibility.end = false;
          } else if (this.addon === 'end') {
            this.addonVisibility.start = false;
            this.addonVisibility.end = true;
          }
        }
      },
      mounted: function mounted() {
        this.addonConfig();
      }
    };

    //
    var script$v = {
      name: 'SelectSingle',
      components: {
        VueMultiselect: VueMultiselect
      },
      mixins: [inputMixin],
      inheritAttrs: false,
      props: {
        /**
         * An identifier for template handling
         * @prop {String}
         */
        id: {
          type: String,
          required: true
        },

        /**
         * Presets the selected value
         * @prop {String}
         */
        value: {
          type: String,
          default: ''
        },

        /**
         * Placeholder text for no input
         * @prop {String}
         */
        placeholder: {
          type: String,
          default: 'Select an option...'
        },

        /**
         * An array of objects
         * @prop {Array}
         */
        options: {
          type: Array,
          required: true
        },

        /**
         * The value to return from the options object
         * @prop {String}
         */
        returnValue: {
          type: String,
          default: "value"
        },

        /**
         * The value to show in the options selection and when an item is selected
         * @prop {String}
         */
        selectionLabel: {
          type: String,
          default: "text"
        }
      },
      computed: {
        selectedValue: function selectedValue() {
          var _this = this;

          return this.options.find(function (o) {
            return o[_this.returnValue] === _this.value;
          });
        }
      },
      methods: {
        updateValue: function updateValue(result) {
          if (result) {
            this.$emit('input', result[this.returnValue]);
          } else {
            // The selection has been de-selected (i.e., no selection)
            this.$emit('input', '');
          }
        }
      }
    };

    var css$i = "@charset \"UTF-8\";.multiselect__single{white-space:nowrap;overflow:hidden;text-overflow:ellipsis}fieldset[disabled] .multiselect{pointer-events:none}.multiselect,.multiselect__input,.multiselect__single{font-family:inherit;font-size:inherit;touch-action:manipulation}.multiselect{box-sizing:content-box;display:block;position:relative;width:100%;min-height:calc(2.25rem + 2px);text-align:left;line-height:1.5;color:#464a4c}.multiselect *{box-sizing:border-box}.multiselect:focus{outline:0}.multiselect--disabled{pointer-events:none;opacity:.65}.multiselect--active{z-index:50}.multiselect--active:not(.multiselect--above) .multiselect__current,.multiselect--active:not(.multiselect--above) .multiselect__input,.multiselect--active:not(.multiselect--above) .multiselect__tags{border-bottom-left-radius:0;border-bottom-right-radius:0}.multiselect--active .multiselect__select{transform:rotateZ(180deg)}.multiselect--active .multiselect__content-wrapper,.multiselect--active .multiselect__tags{border-color:#395180}.multiselect--active .multiselect__tags{border-bottom-color:rgba(0,0,0,.15)}.multiselect--above.multiselect--active .multiselect__current,.multiselect--above.multiselect--active .multiselect__input,.multiselect--above.multiselect--active .multiselect__tags{border-top-left-radius:0;border-top-right-radius:0}.multiselect__input,.multiselect__single{position:relative;display:inline-block;min-height:calc(2.25rem + 2px)/2;line-height:calc(2.25rem + 2px)/2;border:none;border-radius:.25rem;background:#fff;padding:0 0 0 .75rem;width:calc(100%);transition:border .1s ease;box-sizing:border-box;margin-bottom:.375rem;vertical-align:top}.multiselect__input::-webkit-input-placeholder{color:#636c72}.multiselect__input::-moz-placeholder{color:#636c72}.multiselect__input::-ms-input-placeholder{color:#636c72}.multiselect__input::placeholder{color:#636c72}.multiselect__tag~.multiselect__input,.multiselect__tag~.multiselect__single{width:auto}.multiselect__input:hover,.multiselect__single:hover{border-color:#cfcfcf}.multiselect__input:focus,.multiselect__single:focus{border-color:#a8a8a8;outline:0}.multiselect__single{padding-left:.3rem!important;margin-bottom:.375rem;color:inherit}.multiselect__input{padding-left:.3rem!important}.multiselect__tags-wrap{display:inline}.multiselect__tags{min-height:calc(2.25rem + 2px);display:block;padding:.375rem calc(2.25rem + 2px) 0 .375rem;border-radius:.25rem;border:1px solid rgba(0,0,0,.15);background:#fff;font-family:inherit;font-size:inherit}.multiselect__tag{position:relative;display:inline-block;padding:.25rem 2rem .25rem .4rem;border-radius:.25rem;margin-right:.4rem;color:#fff;background:#395180;white-space:nowrap;overflow:hidden;max-width:100%;text-overflow:ellipsis;font-size:80%;font-weight:400}.multiselect__tag-icon{cursor:pointer;margin-left:.25rem;position:absolute;right:0;top:0;bottom:0;font-style:initial;width:1.6rem;text-align:center;line-height:1.6rem;transition:all .2s ease;font-size:80%;font-weight:400}.multiselect__tag-icon:after{content:\"×\";color:#fff;font-size:220%}.multiselect__tag-icon:focus,.multiselect__tag-icon:hover{background:#4967a3}.multiselect__tag-icon:focus:after,.multiselect__tag-icon:hover:after{color:#fff}.multiselect__current{line-height:calc(2.25rem + 2px)/2;min-height:calc(2.25rem + 2px);box-sizing:border-box;display:block;overflow:hidden;padding:8px 30px 0 12px;white-space:nowrap;margin:0;text-decoration:none;border-radius:.25rem;border:1px solid rgba(0,0,0,.15);cursor:pointer}.multiselect__select{line-height:calc(2.25rem + 2px)/2;display:block;position:absolute;box-sizing:border-box;width:calc(2.25rem + 2px);height:calc(2.25rem + 2px);right:0;top:0;padding:8px 8px;margin:0;text-decoration:none;text-align:center;cursor:pointer;transition:transform .2s ease}.multiselect__select:before{position:relative;right:0;top:50%;color:#464a4c;border-style:solid;border-width:5px 5px 0 5px;border-color:#464a4c transparent transparent transparent;content:\"\"}.multiselect__placeholder{color:#636c72;display:inline-block;padding-top:2px;padding-left:.3em}.multiselect--active .multiselect__placeholder{display:none}.multiselect__content-wrapper{position:absolute;display:block;background:#fff;width:100%;max-height:240px;overflow:auto;border:1px solid rgba(0,0,0,.15);border-top:none;border-bottom-left-radius:.25rem;border-bottom-right-radius:.25rem;z-index:50;-webkit-overflow-scrolling:touch}.multiselect__content{list-style:none;display:inline-block;padding:0;margin:0;min-width:100%;vertical-align:top}.multiselect--above .multiselect__content-wrapper{bottom:100%;border-radius:.25rem .25rem 0 0;border-bottom:none;border-top:1px solid rgba(0,0,0,.15)}.multiselect__content::webkit-scrollbar{display:none}.multiselect__element{display:block}.multiselect__option{display:block;padding:.375rem .75rem;min-height:calc(2.25rem + 2px);line-height:calc(2.25rem + 2px)/2;text-decoration:none;text-transform:none;vertical-align:middle;position:relative;cursor:pointer;white-space:nowrap}.multiselect__option:after{top:0;right:0;position:absolute;line-height:calc(2.25rem + 2px);padding-right:12px;padding-left:20px;font-family:inherit;font-size:inherit}.multiselect__option--highlight{background:#395180;outline:0;color:#fff}.multiselect__option--highlight:after{content:attr(data-select);background:#395180;color:#fff}.multiselect__option--selected{background:#f7f7f9;color:#292b2c;font-weight:700}.multiselect__option--selected:after{content:attr(data-selected);color:#636c72}.multiselect__option--selected.multiselect__option--highlight{background:#636c72;color:#fff}.multiselect__option--selected.multiselect__option--highlight:after{background:#636c72;content:attr(data-deselect);color:#fff}.multiselect--disabled{background:#eceeef;pointer-events:none}.multiselect--disabled .multiselect__current,.multiselect--disabled .multiselect__select{background:#eceeef;color:#636c72}.multiselect__option--disabled{background:#eceeef;color:#636c72;cursor:text;pointer-events:none}.multiselect__option--group{background:#eceeef;color:#636c72}.multiselect__option--group.multiselect__option--highlight{background:#636c72;color:#eceeef}.multiselect__option--group.multiselect__option--highlight:after{background:#636c72}.multiselect__option--disabled.multiselect__option--highlight{background:#eceeef}.multiselect__option--group-selected.multiselect__option--highlight{background:#636c72;color:#fff}.multiselect__option--group-selected.multiselect__option--highlight:after{background:#636c72;content:attr(data-deselect);color:#fff}.multiselect-enter-active,.multiselect-leave-active{transition:all .15s ease}.multiselect-enter,.multiselect-leave-active{opacity:0}.multiselect__strong{margin-bottom:.375rem;line-height:calc(2.25rem + 2px)/2;display:inline-block;vertical-align:top}.multiselect__spinner{position:absolute;right:0;top:0;width:calc(2.25rem + 2px);height:calc(2.25rem + 2px);background:#fff;display:block}.multiselect__spinner:after,.multiselect__spinner:before{position:absolute;content:\"\";top:50%;left:50%;margin:-8px 0 0 -8px;width:16px;height:16px;border-radius:100%;border:2px solid transparent;border-top-color:#292b2c;box-shadow:0 0 0 1px transparent}.multiselect__spinner:before{-webkit-animation:spinning 2.4s cubic-bezier(.41,.26,.2,.62);animation:spinning 2.4s cubic-bezier(.41,.26,.2,.62);-webkit-animation-iteration-count:infinite;animation-iteration-count:infinite}.multiselect__spinner:after{-webkit-animation:spinning 2.4s cubic-bezier(.51,.09,.21,.8);animation:spinning 2.4s cubic-bezier(.51,.09,.21,.8);-webkit-animation-iteration-count:infinite;animation-iteration-count:infinite}@-webkit-keyframes spinning{from{transform:rotate(0)}to{transform:rotate(2turn)}}@keyframes spinning{from{transform:rotate(0)}to{transform:rotate(2turn)}}.multiselect__loading-enter-active,.multiselect__loading-leave-active{transition:opacity .4s ease-in-out;opacity:1}.multiselect__loading-enter,.multiselect__loading-leave-active{opacity:0}[dir=rtl] .multiselect{text-align:right}[dir=rtl] .multiselect__select{right:auto;left:1px}[dir=rtl] .multiselect__tags{padding:.375rem .375rem 0 calc(2.25rem + 2px)}[dir=rtl] .multiselect__content{text-align:right}[dir=rtl] .multiselect__option:after{right:auto;left:0}[dir=rtl] .multiselect__clear{right:auto;left:12px}[dir=rtl] .multiselect__spinner{right:auto;left:1px}.multiselect--success .multiselect__tags{border-color:#5c8727}.multiselect--warning .multiselect__tags{border-color:#f19e1f}.multiselect--danger .multiselect__tags{border-color:#cc665e}";
    styleInject(css$i);

    /* script */
    const __vue_script__$v = script$v;
    /* template */
    var __vue_render__$l = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"form-group",class:[{'has-success': _vm.success}, 
                                     {'has-warning': _vm.warning}, 
                                     {'has-danger': _vm.danger},
                                     {'row': _vm.inline}]},[(_vm.isLabel)?_c('label',{class:[{'col-sm-2 col-form-label': _vm.inline}],attrs:{"for":_vm.id,"aria-label":_vm.id}},[_vm._v("\n        "+_vm._s(_vm.label)+"\n    ")]):_vm._e(),_vm._v(" "),_c('div',{class:[{'col-sm-10': _vm.inline}]},[_c('vue-multiselect',_vm._b({class:[{'multiselect--success': _vm.danger},
                                               {'multiselect--warning': _vm.warning}, 
                                               {'multiselect--danger': _vm.danger}],attrs:{"id":_vm.id,"value":_vm.selectedValue,"options":_vm.options,"placeholder":_vm.placeholder,"show-labels":false,"disabled":_vm.disabled,"label":_vm.selectionLabel,"track-by":_vm.returnValue},on:{"input":_vm.updateValue}},'vue-multiselect',_vm.$attrs,false)),_vm._v(" "),(_vm.stateEnabled && _vm.success && !_vm.disabled)?_c('div',{staticClass:"form-control-feedback"},[_vm._v("\n            "+_vm._s(_vm.successMessage)+"\n        ")]):_vm._e(),_vm._v(" "),(_vm.stateEnabled && _vm.warning && !_vm.disabled)?_c('div',{staticClass:"form-control-feedback"},[_vm._v("\n            "+_vm._s(_vm.warningMessage)+"\n        ")]):_vm._e(),_vm._v(" "),(_vm.stateEnabled && _vm.danger && !_vm.disabled)?_c('div',{staticClass:"form-control-feedback"},[_vm._v("\n            "+_vm._s(_vm.dangerMessage)+"\n        ")]):_vm._e(),_vm._v(" "),(_vm.isDescription)?_c('small',{staticClass:"text-muted",class:[{'form-text': !_vm.inlineDescription}],attrs:{"id":_vm.id + 'Help'}},[_vm._v("\n            "+_vm._s(_vm.description)+"\n        ")]):_vm._e()],1)])};
    var __vue_staticRenderFns__$l = [];

      /* style */
      const __vue_inject_styles__$v = undefined;
      /* scoped */
      const __vue_scope_id__$v = undefined;
      /* module identifier */
      const __vue_module_identifier__$v = undefined;
      /* functional template */
      const __vue_is_functional_template__$v = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var SelectSingle = normalizeComponent_1(
        { render: __vue_render__$l, staticRenderFns: __vue_staticRenderFns__$l },
        __vue_inject_styles__$v,
        __vue_script__$v,
        __vue_scope_id__$v,
        __vue_is_functional_template__$v,
        __vue_module_identifier__$v,
        undefined,
        undefined
      );

    //
    var script$w = {
      name: 'SelectMultiple',
      components: {
        VueMultiselect: VueMultiselect
      },
      mixins: [inputMixin],
      inheritAttrs: false,
      props: {
        /**
         * An identifier for template handling
         * @prop {String}
         */
        id: {
          type: String,
          required: true
        },

        /**
         * Presets the selected value
         * @prop {Array}
         */
        value: {
          type: Array,
          default: function _default() {
            return [];
          }
        },

        /**
         * Placeholder text for no input
         * @prop {String}
         */
        placeholder: {
          type: String,
          default: 'Select an option...'
        },

        /**
         * An array of objects
         * @prop {Array}
         */
        options: {
          type: Array,
          required: true
        },

        /**
         * The value to return from the options object (sets "track-by" on vue-multiselect)
         * @prop {String}
         */
        returnValue: {
          type: String,
          default: "value"
        },

        /**
         * The value to show in the options selection and when an item is selected (sets "label" on vue-multiselect)
         * @prop {String}
         */
        selectionLabel: {
          type: String,
          default: "text"
        }
      },
      computed: {
        selectedValues: function selectedValues() {
          var _this = this;

          var values = [];
          this.options.forEach(function (o) {
            if (_this.value.includes(o[_this.returnValue])) {
              values.push(o);
            }
          });
          return values;
        }
      },
      methods: {
        updateValue: function updateValue(value) {
          var _this2 = this;

          var arrayOfStrings = value.map(function (v) {
            return v[_this2.returnValue];
          });
          this.$emit('input', arrayOfStrings);
        }
      }
    };

    /* script */
    const __vue_script__$w = script$w;

    /* template */
    var __vue_render__$m = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"form-group",class:[{'has-success': _vm.success}, 
                                     {'has-warning': _vm.warning}, 
                                     {'has-danger': _vm.danger},
                                     {'row': _vm.inline}]},[(_vm.isLabel)?_c('label',{class:[{'col-sm-2 col-form-label': _vm.inline}],attrs:{"for":_vm.id,"aria-label":_vm.id}},[_vm._v("\n        "+_vm._s(_vm.label)+"\n    ")]):_vm._e(),_vm._v(" "),_c('div',{class:[{'col-sm-10': _vm.inline}]},[_c('vue-multiselect',_vm._b({class:[{'multiselect--success': _vm.danger},
                                               {'multiselect--warning': _vm.warning}, 
                                               {'multiselect--danger': _vm.danger}],attrs:{"id":_vm.id,"multiple":"","value":_vm.selectedValues,"options":_vm.options,"placeholder":_vm.placeholder,"show-labels":false,"disabled":_vm.disabled,"label":_vm.selectionLabel,"track-by":_vm.returnValue},on:{"input":_vm.updateValue}},'vue-multiselect',_vm.$attrs,false)),_vm._v(" "),(_vm.stateEnabled && _vm.success && !_vm.disabled)?_c('div',{staticClass:"form-control-feedback"},[_vm._v("\n            "+_vm._s(_vm.successMessage)+"\n        ")]):_vm._e(),_vm._v(" "),(_vm.stateEnabled && _vm.warning && !_vm.disabled)?_c('div',{staticClass:"form-control-feedback"},[_vm._v("\n            "+_vm._s(_vm.warningMessage)+"\n        ")]):_vm._e(),_vm._v(" "),(_vm.stateEnabled && _vm.danger && !_vm.disabled)?_c('div',{staticClass:"form-control-feedback"},[_vm._v("\n            "+_vm._s(_vm.dangerMessage)+"\n        ")]):_vm._e(),_vm._v(" "),(_vm.isDescription)?_c('small',{staticClass:"text-muted",class:[{'form-text': !_vm.inlineDescription}],attrs:{"id":_vm.id + 'Help'}},[_vm._v("\n            "+_vm._s(_vm.description)+"\n        ")]):_vm._e()],1)])};
    var __vue_staticRenderFns__$m = [];

      /* style */
      const __vue_inject_styles__$w = undefined;
      /* scoped */
      const __vue_scope_id__$w = undefined;
      /* module identifier */
      const __vue_module_identifier__$w = undefined;
      /* functional template */
      const __vue_is_functional_template__$w = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var SelectMultiple = normalizeComponent_1(
        { render: __vue_render__$m, staticRenderFns: __vue_staticRenderFns__$m },
        __vue_inject_styles__$w,
        __vue_script__$w,
        __vue_scope_id__$w,
        __vue_is_functional_template__$w,
        __vue_module_identifier__$w,
        undefined,
        undefined
      );

    //
    //
    //
    //
    //
    //
    //
    //

    /** TODO:
     * - option to collapse title/content when not current
     * - vertical alignment
     * - send content from a stepper item to a output to another display in this main component
     * - allow usage from an object
     */
    var script$x = {
      name: "Stepper",
      model: {
        prop: 'current',
        event: 'change'
      },
      props: {
        /**
         * The current step
         * @type {Number}
         * @default 1
         */
        current: {
          type: Number,
          default: 1
        },

        /**
         * A list of steps, which will generate stepper-items
         * @type {Object}
         * @default undefined
         */
        steps: {
          type: Object,
          default: undefined
        }
      },
      computed: {
        max: function max() {
          return this.$children.length;
        }
      },
      watch: {
        current: function current() {
          this.onChange();
        }
      },
      mounted: function mounted() {
        this.onChange();
      },
      methods: {
        /**
         * Flip to the previous step
         */
        previous: function previous() {
          var step = this.current - 1;

          if (step >= 1) {
            this.$emit('change', step);
          }
        },

        /**
         * Flip to the next step
         */
        next: function next() {
          var step = this.current + 1;

          if (step <= this.max) {
            this.$emit('change', step);
          }
        },

        /**
         * React to change of `current`
         */
        onChange: function onChange() {
          // Loop through stepper-items and set their status' and visibility
          var current = this.current;
          this.$children.forEach(function (item, index) {
            // Get the 1-based step number
            var step = index + 1; // Less than is `finished`, same is `current`, more is `pending`

            if (step < current) {
              item.setStatus('finished');
            } else if (step > current) {
              item.setStatus('pending');
            } else {
              item.setStatus('current');
            }
          });
        }
      }
    };

    var css$j = ".stepper .stepper-tabs[data-v-4eb7eb81]{display:flex;flex-direction:row;justify-content:space-between}.stepper .stepper-content[data-v-4eb7eb81]{min-height:100px}";
    styleInject(css$j);

    /* script */
    const __vue_script__$x = script$x;
    /* template */
    var __vue_render__$n = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"stepper",attrs:{"role":"navigation"}},[_c('div',{staticClass:"stepper-tabs"},[_vm._t("default")],2)])};
    var __vue_staticRenderFns__$n = [];

      /* style */
      const __vue_inject_styles__$x = undefined;
      /* scoped */
      const __vue_scope_id__$x = "data-v-4eb7eb81";
      /* module identifier */
      const __vue_module_identifier__$x = undefined;
      /* functional template */
      const __vue_is_functional_template__$x = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var Stepper = normalizeComponent_1(
        { render: __vue_render__$n, staticRenderFns: __vue_staticRenderFns__$n },
        __vue_inject_styles__$x,
        __vue_script__$x,
        __vue_scope_id__$x,
        __vue_is_functional_template__$x,
        __vue_module_identifier__$x,
        undefined,
        undefined
      );

    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //

    /** TODO:
     * - custom statuses
     * - custom colors?
     * - default label?
     */
    var statusOptions = ['finished', 'pending', 'current'];
    var script$y = {
      name: "StepperItem",
      props: {
        /**
         * The label to display
         */
        label: {
          type: [Number, String],
          default: ''
        },

        /**
         * Specify a default font-awesome icon to display instead of a label
         */
        icon: {
          type: String,
          default: undefined
        },

        /**
         * The title
         */
        title: {
          type: String,
          default: ''
        },

        /**
         * The decription
         */
        description: {
          type: String,
          default: ''
        }
      },
      data: function data() {
        return {
          /**
           * The current status of this step
           */
          status: ''
        };
      },
      computed: {
        // Just some convenience properties
        current: function current() {
          return this.status === 'current';
        },
        pending: function pending() {
          return this.status === 'pending';
        },
        finished: function finished() {
          return this.status === 'finished';
        },
        finishedIcon: function finishedIcon() {
          if (this.status === 'finished') {
            return 'check';
          } else {
            return undefined;
          }
        }
      },
      methods: {
        setStatus: function setStatus(toSet) {
          if (statusOptions.indexOf(toSet) > -1) {
            this.status = toSet;
          } else {
            console.error("Error: Cannot set status: ".concat(toSet, ". Must be one of ").concat(statusOptions, "!"));
          }
        }
      }
    };

    var css$k = ".stepper-item[data-v-60e2d914]{flex:auto;margin-right:.5rem}.stepper-item[data-v-60e2d914]:last-child{margin-right:0;flex:none}.stepper-item:last-child .stepper-item__line[data-v-60e2d914]{display:none!important}.stepper-item.stepper-item--current .stepper-item__icon[data-v-60e2d914]{border-color:#395180!important;background-color:#395180!important;color:#fff!important}.stepper-item.stepper-item--current .stepper-item__title[data-v-60e2d914]{color:#000!important}.stepper-item.stepper-item--finished .stepper-item__icon[data-v-60e2d914]{border-color:#395180!important;color:#395180!important}.stepper-item.stepper-item--finished .stepper-item__title[data-v-60e2d914]{color:#395180!important}.stepper-item.stepper-item--finished .stepper-item__line[data-v-60e2d914]{background-color:#395180!important}.stepper-item .stepper-item__header[data-v-60e2d914]{display:flex}.stepper-item .stepper-item__header .stepper-item__icon[data-v-60e2d914]{border:1px solid;border-color:gray;border-radius:.75rem;color:gray;font-size:.875rem;line-height:1.375rem;padding-left:.25rem;padding-right:.25rem;text-align:center;display:inline-block;height:1.5rem;min-width:1.5rem;position:relative}.stepper-item .stepper-item__header .stepper-item__title[data-v-60e2d914]{margin-left:.5rem;color:gray;margin-bottom:0;align-self:center}.stepper-item .stepper-item__header .stepper-item__line[data-v-60e2d914]{display:block;flex:auto;background-color:gray;align-self:center;height:1px;margin-left:.5rem}.stepper-item .stepper-item__description[data-v-60e2d914]{display:flex;align-self:flex-start;margin-left:2rem;color:#464a4c}";
    styleInject(css$k);

    /* script */
    const __vue_script__$y = script$y;
    /* template */
    var __vue_render__$o = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"stepper-item",class:[{ 'stepper-item--current': _vm.current },
                  { 'stepper-item--finished': _vm.finished },
                  { 'stepper-item--pending': _vm.pending }]},[_c('div',{staticClass:"stepper-item__header"},[_c('div',{staticClass:"stepper-item__icon"},[(_vm.finishedIcon)?_c('icon',{attrs:{"icon":_vm.finishedIcon}}):(_vm.icon)?_c('icon',{attrs:{"icon":_vm.icon}}):_c('span',[_vm._v(_vm._s(_vm.label))])],1),_vm._v(" "),_c('h6',{staticClass:"stepper-item__title"},[_vm._v(_vm._s(_vm.title))]),_vm._v(" "),_c('span',{staticClass:"stepper-item__line"})]),_vm._v(" "),_c('small',{staticClass:"stepper-item__description"},[_vm._v(_vm._s(_vm.description))])])};
    var __vue_staticRenderFns__$o = [];

      /* style */
      const __vue_inject_styles__$y = undefined;
      /* scoped */
      const __vue_scope_id__$y = "data-v-60e2d914";
      /* module identifier */
      const __vue_module_identifier__$y = undefined;
      /* functional template */
      const __vue_is_functional_template__$y = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var StepperItem = normalizeComponent_1(
        { render: __vue_render__$o, staticRenderFns: __vue_staticRenderFns__$o },
        __vue_inject_styles__$y,
        __vue_script__$y,
        __vue_scope_id__$y,
        __vue_is_functional_template__$y,
        __vue_module_identifier__$y,
        undefined,
        undefined
      );

    //
    var script$z = {
      name: "TextBox",
      mixins: [inputMixin, inputAddonMixin],
      inheritAttrs: false,
      props: {
        id: {
          type: String,
          required: true
        },
        value: {
          type: [String, Number],
          default: ''
        },
        type: {
          type: String,
          default: 'text',
          validator: function validator(value) {
            return ['text', 'password', 'datetime-local', 'date', 'month', 'time', 'week', 'number', 'email', 'url', 'search', 'tel', 'color'].indexOf(value) > -1;
          }
        }
      },
      methods: {
        onInput: function onInput(event) {
          this.$emit('input', event.target.value);
        }
      }
    };

    /* script */
    const __vue_script__$z = script$z;

    /* template */
    var __vue_render__$p = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"form-group",class:[{'has-success': _vm.success}, 
                                     {'has-warning': _vm.warning}, 
                                     {'has-danger': _vm.danger},
                                     {'row': _vm.inline}]},[(_vm.isLabel)?_c('label',{class:[{'col-sm-2 col-form-label': _vm.inline}],attrs:{"for":_vm.id,"aria-label":_vm.id}},[_vm._v("\n        "+_vm._s(_vm.label)+"\n    ")]):_vm._e(),_vm._v(" "),_c('div',{class:[{'col-sm-10': _vm.inline}]},[_c('div',{staticClass:"input-group"},[(_vm.addonVisibility.start)?_c('span',{staticClass:"input-group-addon"},[_vm._t("addon-start")],2):_vm._e(),_vm._v(" "),_c('input',_vm._b({ref:"input",staticClass:"form-control",class:[{'form-control-sm': _vm.size === 'small'},
                                {'form-control-lg': _vm.size === 'large'},
                                {'form-control-success': _vm.success}, 
                                {'form-control-warning': _vm.warning}, 
                                {'form-control-danger': _vm.danger}],attrs:{"type":_vm.type,"disabled":_vm.disabled,"id":_vm.id,"aria-describedby":_vm.id + 'Help',"placeholder":_vm.placeholder,"autocomplete":_vm.isAutocomplete},domProps:{"value":_vm.value},on:{"input":_vm.onInput}},'input',_vm.$attrs,false)),_vm._v(" "),(_vm.addonVisibility.end)?_c('span',{staticClass:"input-group-addon"},[_vm._t("addon-end")],2):_vm._e()]),_vm._v(" "),(_vm.stateEnabled && _vm.success && !_vm.disabled)?_c('div',{staticClass:"form-control-feedback"},[_vm._v("\n            "+_vm._s(_vm.successMessage)+"\n        ")]):_vm._e(),_vm._v(" "),(_vm.stateEnabled && _vm.warning && !_vm.disabled)?_c('div',{staticClass:"form-control-feedback"},[_vm._v("\n            "+_vm._s(_vm.warningMessage)+"\n        ")]):_vm._e(),_vm._v(" "),(_vm.stateEnabled && _vm.danger && !_vm.disabled)?_c('div',{staticClass:"form-control-feedback"},[_vm._v("\n            "+_vm._s(_vm.dangerMessage)+"\n        ")]):_vm._e(),_vm._v(" "),(_vm.isDescription)?_c('small',{staticClass:"text-muted",class:[{'form-text': !_vm.inlineDescription}],attrs:{"id":_vm.id + 'Help'}},[_vm._v("\n            "+_vm._s(_vm.description)+"\n        ")]):_vm._e()])])};
    var __vue_staticRenderFns__$p = [];

      /* style */
      const __vue_inject_styles__$z = undefined;
      /* scoped */
      const __vue_scope_id__$z = undefined;
      /* module identifier */
      const __vue_module_identifier__$z = undefined;
      /* functional template */
      const __vue_is_functional_template__$z = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var TextBox = normalizeComponent_1(
        { render: __vue_render__$p, staticRenderFns: __vue_staticRenderFns__$p },
        __vue_inject_styles__$z,
        __vue_script__$z,
        __vue_scope_id__$z,
        __vue_is_functional_template__$z,
        __vue_module_identifier__$z,
        undefined,
        undefined
      );

    //
    var script$A = {
      name: "TextView",
      mixins: [inputMixin],
      inheritAttrs: false,
      props: {
        id: {
          type: String,
          required: true
        },
        value: {
          type: String,
          default: ''
        }
      },
      methods: {
        updateValue: function updateValue(val) {
          this.$refs.input.value = val;
          this.$emit('input', val);
        }
      }
    };

    /* script */
    const __vue_script__$A = script$A;

    /* template */
    var __vue_render__$q = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"form-group",class:[{'has-success': _vm.success}, 
                                     {'has-warning': _vm.warning}, 
                                     {'has-danger': _vm.danger},
                                     {'row': _vm.inline}]},[(_vm.isLabel)?_c('label',{class:[{'col-sm-2 col-form-label': _vm.inline}],attrs:{"for":_vm.id,"aria-label":_vm.id}},[_vm._v("\n        "+_vm._s(_vm.label)+"\n    ")]):_vm._e(),_vm._v(" "),_c('div',{class:[{'col-sm-10': _vm.inline}]},[_c('textarea',_vm._b({ref:"input",staticClass:"form-control",class:[{'form-control-success': _vm.success}, 
                               {'form-control-warning': _vm.warning}, 
                               {'form-control-danger': _vm.danger}],attrs:{"disabled":_vm.disabled,"id":_vm.id,"aria-describedby":_vm.id + 'Help',"placeholder":_vm.placeholder,"autocomplete":_vm.autocomplete},domProps:{"value":_vm.value},on:{"input":function($event){return _vm.updateValue($event.target.value)}}},'textarea',_vm.$attrs,false)),_vm._v(" "),(_vm.stateEnabled && _vm.success && !_vm.disabled)?_c('div',{staticClass:"form-control-feedback"},[_vm._v("\n            "+_vm._s(_vm.successMessage)+"\n        ")]):_vm._e(),_vm._v(" "),(_vm.stateEnabled && _vm.warning && !_vm.disabled)?_c('div',{staticClass:"form-control-feedback"},[_vm._v("\n            "+_vm._s(_vm.warningMessage)+"\n        ")]):_vm._e(),_vm._v(" "),(_vm.stateEnabled && _vm.danger && !_vm.disabled)?_c('div',{staticClass:"form-control-feedback"},[_vm._v("\n            "+_vm._s(_vm.dangerMessage)+"\n        ")]):_vm._e(),_vm._v(" "),(_vm.isDescription)?_c('small',{staticClass:"text-muted",class:[{'form-text': !_vm.inlineDescription}],attrs:{"id":_vm.id + 'Help'}},[_vm._v("\n            "+_vm._s(_vm.description)+"\n        ")]):_vm._e()])])};
    var __vue_staticRenderFns__$q = [];

      /* style */
      const __vue_inject_styles__$A = undefined;
      /* scoped */
      const __vue_scope_id__$A = undefined;
      /* module identifier */
      const __vue_module_identifier__$A = undefined;
      /* functional template */
      const __vue_is_functional_template__$A = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var TextView = normalizeComponent_1(
        { render: __vue_render__$q, staticRenderFns: __vue_staticRenderFns__$q },
        __vue_inject_styles__$A,
        __vue_script__$A,
        __vue_scope_id__$A,
        __vue_is_functional_template__$A,
        __vue_module_identifier__$A,
        undefined,
        undefined
      );

    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    var script$B = {
      name: 'Toolbar',
      props: {
        disableCollapse: {
          type: Boolean,
          default: false
        },
        primaryBreakpoint: {
          type: Number,
          default: 600
        },
        secondaryBreakpoint: {
          type: Number,
          default: 800
        }
      },
      data: function data() {
        return {
          // Reference data properties
          style: null,
          styleRef: null
        };
      },
      computed: {
        css: function css() {
          if (!this.disableCollapse) {
            return "\n                    @media screen and (max-width: ".concat(this.primaryBreakpoint, "px) { ").concat(this.collapseStyle('primary'), " }\n                    @media screen and (max-width: ").concat(this.secondaryBreakpoint, "px) { ").concat(this.collapseStyle('secondary'), " }\n                ");
          }

          return '';
        }
      },
      watch: {
        css: function css() {
          // On css value change update style content
          this.style.textContent = this.css;
        }
      },
      mounted: function mounted() {
        // Create style node
        var style = document.createElement('style');
        style.type = "text/css";
        style.appendChild(document.createTextNode('')); // Assign references on vm

        this.styleRef = style;
        this.style = style.childNodes[0]; // Assign css the the style node

        this.style.textContent = this.css; // Append to the head

        document.head.appendChild(style);
      },
      beforeDestroy: function beforeDestroy() {
        // Remove the style node from the head
        this.style.parentElement.removeChild(this.style);
      },
      methods: {
        collapseStyle: function collapseStyle(selector) {
          return "\n                .".concat(selector, " .btn i { margin-right: 0px !important; }\n                .").concat(selector, " .btn-group .btn i { margin-right: 0px !important; }\n                .").concat(selector, " .btn .btn-label { display: none; }\n                .").concat(selector, " .btn-group .btn .btn-label { display: none; }\n            ");
        }
      }
    };

    var css$l = ".toolbar[data-v-b758e08c]{width:auto;display:flex;flex-direction:row;flex-wrap:wrap;justify-content:space-between;list-style:none}.toolbar .primary[data-v-b758e08c],.toolbar .secondary[data-v-b758e08c]{display:flex;margin-bottom:.25em}.toolbar .form-group[data-v-b758e08c],.toolbar .input-group[data-v-b758e08c]{display:inline-flex;margin-bottom:0;width:auto}.toolbar .btn[data-v-b758e08c],.toolbar .btn-group[data-v-b758e08c],.toolbar .form-group[data-v-b758e08c],.toolbar .input-group[data-v-b758e08c]{margin-left:.125em;margin-right:.125em}.toolbar .btn .btn[data-v-b758e08c],.toolbar .btn-group .btn[data-v-b758e08c],.toolbar .form-group .btn[data-v-b758e08c],.toolbar .input-group .btn[data-v-b758e08c]{margin:0}.toolbar .btn-group[data-v-b758e08c]:first-child,.toolbar .btn[data-v-b758e08c]:first-child,.toolbar .form-group[data-v-b758e08c]:first-child,.toolbar .input-group[data-v-b758e08c]:first-child{margin-left:0;margin-right:.125em}.toolbar .btn-group[data-v-b758e08c]:last-child,.toolbar .btn[data-v-b758e08c]:last-child,.toolbar .form-group[data-v-b758e08c]:last-child,.toolbar .input-group[data-v-b758e08c]:last-child{margin-left:.125em;margin-right:0}";
    styleInject(css$l);

    /* script */
    const __vue_script__$B = script$B;
    /* template */
    var __vue_render__$r = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"toolbar",attrs:{"role":"toolbar"}},[_c('div',{staticClass:"primary"},[_vm._t("primary")],2),_vm._v(" "),_c('div',{staticClass:"secondary"},[_vm._t("secondary")],2)])};
    var __vue_staticRenderFns__$r = [];

      /* style */
      const __vue_inject_styles__$B = undefined;
      /* scoped */
      const __vue_scope_id__$B = "data-v-b758e08c";
      /* module identifier */
      const __vue_module_identifier__$B = undefined;
      /* functional template */
      const __vue_is_functional_template__$B = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var Toolbar = normalizeComponent_1(
        { render: __vue_render__$r, staticRenderFns: __vue_staticRenderFns__$r },
        __vue_inject_styles__$B,
        __vue_script__$B,
        __vue_scope_id__$B,
        __vue_is_functional_template__$B,
        __vue_module_identifier__$B,
        undefined,
        undefined
      );

    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    var script$C = {
      name: "Toast",
      props: {
        id: {
          type: String,
          required: true
        },
        type: {
          type: String,
          default: 'secondary',
          validator: function validator(value) {
            return ['success', 'warning', 'danger', 'info', 'secondary', 'primary', 'uared'].indexOf(value) > -1;
          }
        },
        content: {
          type: String,
          default: undefined
        },
        title: {
          type: String,
          default: ''
        },
        message: {
          type: String,
          default: ''
        }
      },
      methods: {
        handleClick: function handleClick() {
          this.$emit('toastClicked');
        }
      }
    };

    var css$m = ".alert{padding:.75rem 1.25rem;margin-bottom:1rem;border:1px solid transparent;border-radius:.25rem}.alert-heading{color:inherit}.alert-link{font-weight:700}.alert-dismissible .close{position:relative;top:-.75rem;right:-1.25rem;padding:.75rem 1.25rem;color:inherit}.alert-success{background-color:#c1e199;border-color:#b5dc85;color:#5c8727}.alert-success hr{border-top-color:#a9d671}.alert-success .alert-link{color:#415f1c}.alert-info{background-color:#d7f0f5;border-color:#c2e9f1;color:#31b4cf}.alert-info hr{border-top-color:#ade1ec}.alert-info .alert-link{color:#2790a6}.alert-warning{background-color:#fce1b6;border-color:#fbd79e;color:#d0830d}.alert-warning hr{border-top-color:#facd85}.alert-warning .alert-link{color:#a0650a}.alert-danger{background-color:#e5b0ab;border-color:#de9d98;color:#ab0520}.alert-danger hr{border-top-color:#d88b85}.alert-danger .alert-link{color:#790417}.toast{width:100%;padding:.75rem 1.25rem;margin-bottom:1rem;border:1px solid transparent;border-radius:.25rem}.toast-secondary{background-color:#fff;border-color:#ccc;color:#292b2c}.toast-secondary hr{border-top-color:#bfbfbf}.toast-secondary .alert-link{color:#101112}.toast-primary{background-color:#395180;border-color:#395180;color:#fff}.toast-primary hr{border-top-color:#31466e}.toast-primary .alert-link{color:#e6e6e6}.toast-uared{background-color:#ab0520;border-color:#ab0520;color:#fff}.toast-uared hr{border-top-color:#92041b}.toast-uared .alert-link{color:#e6e6e6}.toast-success{background-color:#5c8727;border-color:#5c8727;color:#fff}.toast-success hr{border-top-color:#4f7321}.toast-success .alert-link{color:#e6e6e6}.toast-info{background-color:#84d2e2;border-color:#84d2e2;color:#fff}.toast-info hr{border-top-color:#6fcadd}.toast-info .alert-link{color:#e6e6e6}.toast-warning{background-color:#f19e1f;border-color:#f19e1f;color:#fff}.toast-warning hr{border-top-color:#e8920f}.toast-warning .alert-link{color:#e6e6e6}.toast-danger{background-color:#cc665e;border-color:#cc665e;color:#fff}.toast-danger hr{border-top-color:#c6544b}.toast-danger .alert-link{color:#e6e6e6}";
    styleInject(css$m);

    /* script */
    const __vue_script__$C = script$C;
    /* template */
    var __vue_render__$s = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"toast",class:[ 'toast-' + _vm.type],attrs:{"id":'#' + _vm.id,"role":"alert"},on:{"click":_vm.handleClick}},[_c('div',{attrs:{"slot":"content"},slot:"content"},[(_vm.content !== undefined)?_c('div',{domProps:{"innerHTML":_vm._s(_vm.content)}}):_c('div',{staticClass:"toast-content"},[_c('h5',{staticClass:"mb-0"},[_vm._v(_vm._s(_vm.title))]),_vm._v(" "),_c('p',{staticClass:"mb-0"},[_vm._v(_vm._s(_vm.message))])])])])};
    var __vue_staticRenderFns__$s = [];

      /* style */
      const __vue_inject_styles__$C = undefined;
      /* scoped */
      const __vue_scope_id__$C = undefined;
      /* module identifier */
      const __vue_module_identifier__$C = undefined;
      /* functional template */
      const __vue_is_functional_template__$C = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var Toast = normalizeComponent_1(
        { render: __vue_render__$s, staticRenderFns: __vue_staticRenderFns__$s },
        __vue_inject_styles__$C,
        __vue_script__$C,
        __vue_scope_id__$C,
        __vue_is_functional_template__$C,
        __vue_module_identifier__$C,
        undefined,
        undefined
      );

    var toastEventBus = new Vue();

    // TODO: Temporarily fix this for SSR, we need to do this better in the long run though.

    var Velocity = require('velocity-animate');

    var STATE = {
      IDLE: 'idle',
      DESTROYED: 'destroyed'
    }; // Default animation for Velocity

    var ANIMATION = {
      enter: function enter(el) {
        return {
          height: [el.clientHeight, 0],
          opacity: [1, 0]
        };
      },
      leave: {
        height: 0,
        opacity: [0, 1]
      }
    };
    var script$D = {
      name: "ToastGroup",
      components: {
        Toast: Toast
      },
      props: {
        group: {
          type: String,
          default: ''
        },
        width: {
          type: String,
          default: "300px"
        },
        // Options: x: left, center, right; y: top, bottom;
        position: {
          type: String,
          default: "top right"
        },
        // Override the normal animation implementation
        animation: {
          type: Object,
          default: undefined
        },
        // Length of transition
        speed: {
          type: Number,
          default: 300
        },
        // Time notification will stay visible (0 means forever)
        duration: {
          type: Number,
          default: 0
        }
      },
      data: function data() {
        return {
          list: []
        };
      },
      computed: {
        toastGroupStyles: function toastGroupStyles() {
          var _this$getPosition = this.getPosition(this.position),
              x = _this$getPosition.x,
              y = _this$getPosition.y;

          var styles = defineProperty$1({
            width: this.width
          }, y, '0px');

          if (x === 'center') {
            styles['left'] = 0;
            styles['right'] = 0;
            styles['margin'] = 'auto';
          } else {
            styles[x] = '0px';
          }

          return styles;
        }
      },
      mounted: function mounted() {
        toastEventBus.$on('add', this.addItem);
      },
      methods: {
        // Add an item to the list
        addItem: function addItem(item) {
          var _this = this;

          // Use the group, or make the group '' if none is supplied
          item.group = item.group || ''; // Don't notify in this group if it's the wrong group

          if (this.group !== item.group) {
            return;
          } // Clean all items if `clean: true` is passed


          if (item.clean) {
            this.destroyAllItems();
            return;
          } // Catch an individual toast's duration override


          var duration = typeof item.duration === 'number' ? item.duration : this.duration; // Catch an individual toast's speed override

          var speed = typeof item.speed === 'number' ? item.speed : this.speed;
          var toast = {
            id: item.id || "toast-" + v4_1().toString(),
            type: item.type,
            state: STATE.IDLE,
            speed: speed,
            length: duration + 2 * speed
          }; // Set the toast content

          if (item.content) {
            toast.content = item.content;
          } else {
            toast.title = item.title;
            toast.message = item.message;
          } // Set a timer if duration is set


          if (duration > 0) {
            toast.timer = setTimeout(function () {
              _this.destroyItem(toast);
            }, toast.length);
          }

          this.list.push(toast);
        },
        // change the item state and clean it from the array
        destroyItem: function destroyItem(item) {
          clearTimeout(item.timer);
          Vue.set(item, 'state', STATE.DESTROYED);
          this.cleanItems();
        },
        // destroy all items in the list
        destroyAllItems: function destroyAllItems() {
          this.list.forEach(function (item) {
            clearTimeout(item.timer);
            Vue.set(item, 'state', STATE.DESTROYED);
          });
          this.cleanItems();
        },
        // remove 'destroyed' items
        cleanItems: function cleanItems() {
          this.list = this.list.filter(function (v) {
            return v.state !== STATE.DESTROYED;
          });
        },
        // Generate styles for the specified ToastGroup position
        getPosition: function getPosition(position) {
          // Make sure the position coordinates given actually work
          var options = {
            x: ['left', 'center', 'right'],
            y: ['top', 'bottom']
          };
          var x,
              y = null; // Turn the string into an array

          if (typeof position === 'string') {
            position = position.split(/\s+/gi).filter(function (v) {
              return v;
            });
          } // Verify the options


          position.forEach(function (p) {
            if (options.x.indexOf(p) !== -1) {
              x = p;
            }

            if (options.y.indexOf(p) !== -1) {
              y = p;
            }
          });
          return {
            x: x,
            y: y
          };
        },
        // Generate the animations
        getAnimation: function getAnimation(el) {
          var enter, leave; // Allow a full override

          if (this.animation !== undefined) {
            enter = this.animation.enter(el);
            leave = this.animation.leave;
          } else {
            enter = ANIMATION.enter(el);
            leave = ANIMATION.leave;
          }

          return {
            enter: enter,
            leave: leave
          };
        },
        // Set enter animation
        enter: function enter(el, done) {
          var _this$getAnimation = this.getAnimation(el),
              enter = _this$getAnimation.enter;

          Velocity(el, enter, {
            duration: this.speed,
            complete: done
          });
        },
        // set leave animation
        leave: function leave(el, done) {
          var _this$getAnimation2 = this.getAnimation(el),
              leave = _this$getAnimation2.leave;

          Velocity(el, leave, {
            duration: this.speed,
            complete: done
          });
        }
      }
    };

    var css$n = ".toast-group-wrapper[data-v-b26332d6]{display:block;position:fixed;z-index:2000;margin-top:5px}.toast-group-wrapper .toast-item[data-v-b26332d6]{margin:0 5px 5px}";
    styleInject(css$n);

    /* script */
    const __vue_script__$D = script$D;
    /* template */
    var __vue_render__$t = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"toast-group-wrapper",style:(_vm.toastGroupStyles)},[_c('transition-group',{attrs:{"css":false},on:{"enter":_vm.enter,"leave":_vm.leave,"afterLeave":_vm.cleanItems}},_vm._l((_vm.list),function(item){return _c('div',{key:item.id,staticClass:"toast-item"},[_c('toast',{attrs:{"id":item.id,"type":item.type,"content":item.content,"title":item.title,"message":item.message},on:{"toastClicked":function($event){return _vm.destroyItem(item)}}})],1)}),0)],1)};
    var __vue_staticRenderFns__$t = [];

      /* style */
      const __vue_inject_styles__$D = undefined;
      /* scoped */
      const __vue_scope_id__$D = "data-v-b26332d6";
      /* module identifier */
      const __vue_module_identifier__$D = undefined;
      /* functional template */
      const __vue_is_functional_template__$D = false;
      /* style inject */
      
      /* style inject SSR */
      

      
      var ToastGroup = normalizeComponent_1(
        { render: __vue_render__$t, staticRenderFns: __vue_staticRenderFns__$t },
        __vue_inject_styles__$D,
        __vue_script__$D,
        __vue_scope_id__$D,
        __vue_is_functional_template__$D,
        __vue_module_identifier__$D,
        undefined,
        undefined
      );



    var components = /*#__PURE__*/Object.freeze({
        Alert: Alert,
        Calendar: Calendar,
        CheckBox: CheckBox,
        CollapsePanel: CollapsePanel,
        Command: Command,
        CommandDropdown: CommandDropdown,
        CommandGroup: CommandGroup,
        DateInput: DateInput,
        DateRangeInput: DateRangeInput,
        DropdownDivider: DropdownDivider,
        DropdownHeader: DropdownHeader,
        DropdownItem: DropdownItem,
        Icon: Icon,
        FsoFooter: FsoFooter,
        FormGroup: FormGroup,
        Modal: Modal,
        Navbar: Navbar,
        NavbarBrand: NavbarBrand,
        NavbarContent: NavbarContent,
        NavbarCollapse: NavbarCollapse,
        NavbarForm: NavbarForm,
        NavbarItem: NavbarItem,
        NavbarItemDropdown: NavbarItemDropdown,
        NavbarText: NavbarText,
        NavbarToggler: NavbarToggler,
        Popover: Popover,
        Radio: Radio,
        RadioGroup: RadioGroup,
        Redbar: Redbar,
        SegmentedToggle: SegmentedToggle,
        SegmentedToggleOption: SegmentedToggleOption,
        SelectSingle: SelectSingle,
        SelectMultiple: SelectMultiple,
        Stepper: Stepper,
        StepperItem: StepperItem,
        TextBox: TextBox,
        TextView: TextView,
        Toolbar: Toolbar,
        Toast: Toast,
        ToastGroup: ToastGroup
    });

    // TODO: Temporarily fix this for SSR, need to do better in the long run though
    // import InputMask from 'inputmask';
    var InputMask = require('inputmask');

    var findInput = function findInput(el) {
      var result;

      try {
        if (el.tagName === 'INPUT' || el.tagName === 'TEXTAREA') {
          result = el;
        } else if (el.getElementsByTagName('input').length > 0) {
          result = el.getElementsByTagName('input')[0];
        } else if (el.getElementsByTagName('textarea').length > 0) {
          result = el.getElementsByTagName('textarea')[0];
        }

        if (result === undefined) throw "element is undefined: this is not a valid element";
      } catch (e) {
        console.error('v-f-mask can\'t identify a valid element to mask.');
        console.error(e);
      }

      return result;
    };

    var valueIsString = function valueIsString(value) {
      return typeof value === 'string' || value instanceof String;
    };

    var mask = function mask(el, mods, value) {
      if (value.length < 1) {
        return;
      } // Determine if el is an `input` or if we need to go deeper for the `input` (i.e. using a Vueoom component)


      var element = findInput(el); // Stop executing if there is no element to work with

      if (!element) return; // Handle modifiers

      var isJit = mods.jit ? true : false;
      var isGreedy = mods.greedy ? true : false; // If the value is just a string, lets make it an object

      var mask = valueIsString(value) ? {
        mask: value
      } : value; // Build config object

      var maskConfig = objectSpread({}, mask, {
        jitMasking: isJit,
        greedy: isGreedy
      });

      InputMask(maskConfig).mask(element);
    };

    var unmask = function unmask(el) {
      var element = findInput(el);
      if (!element) return;
      InputMask.remove(element);
    };

    var mask$1 = {
      bind: function bind(el, _ref) {
        var modifiers = _ref.modifiers,
            value = _ref.value;
        mask(el, modifiers, value);
      },
      unbind: function unbind(el) {
        unmask(el);
      }
    };

    var TRIGGERS = {
      click: 'click',
      hover: 'hover',
      focus: 'focus'
    };

    var parseModifiers = function parseModifiers(modifiers) {
      // Default config
      var config = {
        animation: true,
        delay: 0,
        disableFlip: false,
        offset: undefined,
        placement: 'auto',
        trigger: 'click'
      }; // Handle trigger config modifiers

      var triggers = [];
      Object.keys(TRIGGERS).forEach(function (trigger) {
        if (modifiers[trigger]) {
          triggers.push(trigger);
        }
      });
      console.log(triggers);

      if (triggers.length > 0) {
        config.trigger = triggers.join(' ');
      } // Handle general config modifiers


      Object.keys(modifiers).forEach(function (mod) {
        if (/^noAnimation$/.test(mod)) {
          config.animation = false;
        } else if (/^delay-\d+$/.test(mod)) ; else if (/^disableFlip$/.test(mod)) {
          config.disableFlip = mod;
        } else if (/^offset-\d+$/.test(mod)) ; else if (/^(auto|top(-start|-end)?|bottom(-start|-end)?|left(-start|-end)?|right(-start|-end)?)$/.test(mod)) {
          config.placement = mod;
        }
      });
      console.log(config);
      return config;
    };

    var popover = {
      bind: function bind(el, _ref) {
        var modifiers = _ref.modifiers,
            value = _ref.value;
        var FPopover = Vue.extend(Popover);
        var title = el.getAttribute('title') || '';
        var config = parseModifiers(modifiers);
        el.$fPopover = new FPopover({
          propsData: {
            animation: config.animation,
            content: value,
            delay: config.delay,
            disableFlip: config.disableFlip,
            offset: config.offset,
            placement: config.placement,
            target: el,
            title: title,
            trigger: config.trigger
          }
        });
      },
      inserted: function inserted(el) {
        el.$fPopover.$mount();
      },
      update: function update(el) {
        el.$fPopover.update();
      },
      unbind: function unbind(el) {
        el.$fPopover.$destroy();
      }
    };



    var directives = /*#__PURE__*/Object.freeze({
        fMask: mask$1,
        fPopover: popover
    });

    var addToast = function addToast(notification) {
      toastEventBus.$emit('add', notification);
    };

    function _iterableToArray(iter) {
      if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
    }

    var iterableToArray = _iterableToArray;

    var iterableToArray$1 = /*#__PURE__*/Object.freeze({
        default: iterableToArray,
        __moduleExports: iterableToArray
    });

    var iterableToArray$2 = ( iterableToArray$1 && iterableToArray ) || iterableToArray$1;

    function _toArray(arr) {
      return arrayWithHoles$2(arr) || iterableToArray$2(arr) || nonIterableRest$2();
    }

    var toArray = _toArray;

    /**
     * Compare the arrays with es6 mind-blowing awesomeness
     * see: http://stackoverflow.com/a/34256998
     */
    // Private Functions
    // Test
    var isArray = Array.isArray; // Strict Comparison

    var equal = function equal(x) {
      return function (y) {
        return x === y;
      };
    }; // Loose Comparison

    /*eslint-disable*/


    var looseEqual = function looseEqual(x) {
      return function (y) {
        return x == y;
      };
    };
    /*eslint-enable*/
    // Simple comparison
    // arrayCompare :: (a -> b -> Bool) -> [a] -> [b] -> Bool


    var compare = function compare(f) {
      return function (_ref) {
        var _ref2 = toArray(_ref),
            x = _ref2[0],
            xs = _ref2.slice(1);

        return function (_ref3) {
          var _ref4 = toArray(_ref3),
              y = _ref4[0],
              ys = _ref4.slice(1);

          if (x === undefined && y === undefined) {
            return true;
          } else if (!f(x)(y)) {
            return false;
          } else {
            return compare(f)(xs)(ys);
          }
        };
      };
    }; // Deep comparison
    // arrayDeepCompare :: (a -> b -> Bool) -> [a] -> [b] -> Bool


    var deepCompare = function deepCompare(f) {
      return compare(function (a) {
        return function (b) {
          if (isArray(a) && isArray(b)) {
            return deepCompare(f)(a)(b);
          } else {
            return f(a)(b);
          }
        };
      });
    }; // Public Functions


    var arrayCompare = function arrayCompare(a1, a2, type) {
      if (type === 'loose') {
        return compare(looseEqual)(a1)(a2);
      } else {
        return compare(equal)(a1)(a2);
      }
    };

    var arrayDeepCompare = function arrayDeepCompare(a1, a2, type) {
      if (type === 'loose') {
        return deepCompare(looseEqual)(a1)(a2);
      } else {
        return deepCompare(equal)(a1)(a2);
      }
    };

    var ArrayHelper = {
      arrayCompare: arrayCompare,
      arrayDeepCompare: arrayDeepCompare
    };

    /**
     * This set of helper functions assists with enacting animations on elements through code.
     * It is designed to work with animate.css (https://github.com/daneden/animate.css), though
     * could be extended or modified to work with other libraries as well.
     * See each function for usage.
     */
    var end = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend'; // Run one animation on an element. This is intended for attention getters
    // Usage: AnimationHelper.animateOnce('#idOfElement', 'shake');

    var animateOnce = function animateOnce(element, animation) {
      $(element).addClass('animated ' + animation).one(end, function () {
        $(element).removeClass('animated ' + animation);
      });
    }; // Run two animations back to back, the last when the first finishes
    // Usage: AnimationHelper.animateTwice('#idOfElement', 'first', 'second')
    // Usage example: AnimationHelper.animateTwice('#modal', 'fadeInDown', 'fadeOutLeft')


    var animateTwice = function animateTwice(element, animation1, animation2) {
      $(element).addClass('animated ' + animation1).one(end, function () {
        $(element).removeClass(animation1).addClass(animation2).one(end, function () {
          $(element).removeClass('animated ' + animation2);
        });
      });
    };

    var animateInfinite = function animateInfinite(element, animation) {
      $(element).addClass('animated infinite ' + animation);
    }; // TODO: Promise based function? similar to or based on https://github.com/lukejacksonn/Actuate
    // This will allow for chaining with delays and triggers


    var Animation = {
      animateOnce: animateOnce,
      animateTwice: animateTwice,
      animateInfinite: animateInfinite
    };

    // Components

    var install = function install(Vue, config) {
      if (config === null) {
        config = {};
      } // Register components


      for (var _i = 0, _Object$values = Object.values(components); _i < _Object$values.length; _i++) {
        var c = _Object$values[_i];
        Vue.component(c.name, c);
      } // Register directives


      for (var d in directives) {
        Vue.directive(d, directives[d]);
      } // Register Global methods


      Vue.prototype.$toast = addToast;
    };

    var Vueoom = {
      install: install
    }; // Automatic installation if Vue has been added to the global scope.

    if (typeof window !== 'undefined' && window.Vue) {
      window.Vue.use(Vueoom);
    }

    exports.Alert = Alert;
    exports.Animation = Animation;
    exports.ArrayHelper = ArrayHelper;
    exports.Calendar = Calendar;
    exports.CheckBox = CheckBox;
    exports.ClickoutMixin = clickoutMixin;
    exports.CollapsePanel = CollapsePanel;
    exports.Command = Command;
    exports.CommandDropdown = CommandDropdown;
    exports.CommandGroup = CommandGroup;
    exports.CommandMixin = commandMixin;
    exports.DateInput = DateInput;
    exports.DateRangeInput = DateRangeInput;
    exports.DropdownDivider = DropdownDivider;
    exports.DropdownHeader = DropdownHeader;
    exports.DropdownItem = DropdownItem;
    exports.FormGroup = FormGroup;
    exports.FsoFooter = FsoFooter;
    exports.Icon = Icon;
    exports.InputAddonMixin = inputAddonMixin;
    exports.InputMixin = inputMixin;
    exports.Modal = Modal;
    exports.Navbar = Navbar;
    exports.NavbarBrand = NavbarBrand;
    exports.NavbarCollapse = NavbarCollapse;
    exports.NavbarContent = NavbarContent;
    exports.NavbarForm = NavbarForm;
    exports.NavbarItem = NavbarItem;
    exports.NavbarItemDropdown = NavbarItemDropdown;
    exports.NavbarText = NavbarText;
    exports.NavbarToggler = NavbarToggler;
    exports.Popover = Popover;
    exports.Radio = Radio;
    exports.RadioGroup = RadioGroup;
    exports.Redbar = Redbar;
    exports.SegmentedToggle = SegmentedToggle;
    exports.SegmentedToggleOption = SegmentedToggleOption;
    exports.SelectMultiple = SelectMultiple;
    exports.SelectSingle = SelectSingle;
    exports.Stepper = Stepper;
    exports.StepperItem = StepperItem;
    exports.TextBox = TextBox;
    exports.TextView = TextView;
    exports.Toast = Toast;
    exports.ToastGroup = ToastGroup;
    exports.Toolbar = Toolbar;
    exports.default = Vueoom;
    exports.fMask = mask$1;
    exports.fPopover = popover;

    Object.defineProperty(exports, '__esModule', { value: true });

}));
//# sourceMappingURL=vueoom.js.map
